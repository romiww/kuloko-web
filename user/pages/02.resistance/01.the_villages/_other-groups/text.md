---
title: 'Weitere Gruppen'
body_classes: modular
---

Auch Gruppen wie **Ende Gelände, Fridays for Future** und **Extinction Rebellion** sind in der Region aktiv und leisten vielfältigen Widerstand mit Soli-Aktionen, Besetzungen und Unterstützung der Infrastrukturen der Klimagerechtigkeitsbewegungen.
</br> </br>
Seit Juli 2020 gibt es in Lützerath, dem Dorf, das als nächstes der Grube weichen soll, eine Dauermahnwache, die Kulturveranstaltungen organisiert hat und einen Anlaufpunkt für den Widerstand gegen die Braunkohle in der Region darstellt. [Zur Mahnwache Lützerath!](http://www.mahnwache-luetzerath.org/)
</br> </br>
Auch in Keyenberg gibt es seit September 2020 einen solchen Ort: „Unser Aller Wald“, ein Baumhausdorf, das Menschen inspirieren und dazu befähigen soll, sich selbst und die Bedingungen, unter denen wir leben, zu verändern. [Zu unser aller Wald!](https://unserallerwald.noblogs.org )
</br> </br>
<span class="h4"> Und wir alle treffen bei der „Kultur ohne Kohle“ zusammen, vielfältig, bunt und widerständig! </span>