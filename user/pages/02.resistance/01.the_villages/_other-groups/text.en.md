---
title: 'Other Groups'
body_classes: modular
---

Groups such as **Ende Gelände, Fridays for Future** and **Extinction Rebellion** are also active in the region and provide diverse resistance with solo actions, occupations and support for the infrastructures of the climate justice movements.
</br> </br>
Since July 2020, there is a permanent vigil in Lützerath, the village that is supposed to be next to make way for the mine, which has organized cultural events and is a focal point for resistance to lignite in the region. [To the vigil Lützerath!](http://www.mahnwache-luetzerath.org/)
</br> </br>
Also in Keyenberg, there is such a place since September 2020: "Unser Aller Wald", a tree house village that aims to inspire and empower people to change themselves and the conditions in which we live. [To "Unser Aller Wald"](https://unserallerwald.noblogs.org )
</br> </br>
<span class="h4"> And we all meet at the KuloKo together, diverse, colorful and resistant! </span>