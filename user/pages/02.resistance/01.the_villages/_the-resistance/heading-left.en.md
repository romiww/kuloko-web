---
title: 'the resistance'
body_classes: modular
---

The mine Garzweiler II will continue to destroy the landscape over the next 18 years, extracting coal and tons of stored CO2 from the ground, destroying the homes of another 1,500 regional people and a lot more mainly in the Global South that have to leave their homes because of climate crisis. The region around the mine is suffering from RWE's attrition. Until 2038 coal is to be extracted in Germany, knowing very well that like this the 1.5 degree limit set in the Paris Climate Agreement will be exceeded by Germany. And it has been clear for a long time that energy from lignite is not necessary to meet our electricity needs. Forced resettlements, the destruction of villages, forests and fertile farmland can no longer be justified by RWE's corporate interests. 
More Information at **[alle-doerfer-bleiben.de](https://www.alle-doerfer-bleiben.de/)** 
</br>
A long-sighted, systemic critique against fossil capitalism has been growing in the region for years. Thus not only the Hambacher Forst next to the mine Hambach became a home where solidarity could be experienced while resisting injust politics. In the villages around the mine Garzweiler II  the resistance against the profit-driven climate crime which destroys livelihoods all over the world is growing:
</br> </br>
