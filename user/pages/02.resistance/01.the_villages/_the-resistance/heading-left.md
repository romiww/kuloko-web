---
title: 'Der Widerstand'
body_classes: modular
---

Der Tagebau Garzweiler II soll sich in den nächsten 18 Jahren weiter durch die Landschaft fressen, Kohle und damit Tonnen von gespeichertem CO2 aus dem Boden holen und den Lebensort von weiteren 1.500 Menschen und tausenden weiteren weltweit vor allem im Globalen Süden zerstören, die ihr Zuhause aufgrund der Klimakrise verlieren. Die Region um den Tagebau herum leidet unter der Zermürbung durch RWE. Bis 2038 soll Kohle in Deutschland gefördert werden, wohlwissend dass damit die 1,5-Grad-Grenze aus dem Pariser Klimaabkommen von Deutschland überschritten werden wird. Und längst steht fest, dass die Energie aus Braunkohle nicht nötig ist, um unseren Bedarf an Strom zu decken. Zwangsumsiedlungen, die Zerstörung von Dörfern, Wäldern und fruchtbaren Ackerland sind daher nicht weiter mit den Konzerninteressen von RWE zu rechtfertigen.
</br>
Mehr Infos auf **[alle-doerfer-bleiben.de](https://www.alle-doerfer-bleiben.de/?target=blank)**
</br></br>
Eine weitsichtige, systemgebundene Kritik gegen den fossilen Kapitalismus wächst seit Jahren in der Region. So wurde nicht nur der Hambacher Forst am Tagebau Hambach für viele Menschen zu einem Zuhause, in dem Solidarität gelebt werden kann und gleichzeitig politischer Widerstand geleistet wird – auch in den Dörfern am Tagebau Garzweiler II gibt es schon lange Widerstand gegen das profitgetriebene Klimaverbrechen, das Lebensgrundlagen auf der ganzen Welt zerstört:
<i class="fa fa-map-marker"></i>
<i class="fa fa-calendar"></i>
<i class="fa fa-mouse-pointer"></i>
<i class="fa fa-headphones"></i>