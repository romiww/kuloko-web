---
title: 'ADB und Co Kacheln'
class: medium
features:
    -
        header: '"Alle Dörfer Bleiben"'
        text: 'ist eine Gruppe widerständiger Dofbewohner*innen, die Demonstrationen & Diskussionen rund um den Erhalt der bedrohten Dörfer organisieren.'
        url: 'https://www.alle-doerfer-bleiben.de/'
        button: 'Zur Website!'
        target: blank
        colors: yellow-bg
    -
        header: '"Kirchen(n) im Dorf lassen"'
        text: 'lädt regelmäßig zu Gottesdiensten an der Kante ein, kämpft für den Erhalt der bedrohten Kirchen & für globale Klimagerechtigkeit.'
        url: 'https://www.kirchen-im-dorf-lassen.de/'
        button: 'Zur Website!'
        target: blank
        colors: yellow-bg
    -
        header: '"Menschenrecht vor Bergrecht"'
        text: 'ist eine Gruppe von Anwohner*innen aus dem Umfeld des Tagebaus, die den juristischen Weg gegen die von RWE angestrebten Enteignungen gehen.'
        url: 'https://menschenrecht-vor-bergrecht.de/'
        button: 'Zur Website!'
        target: blank
        colors: yellow-bg
---

