---
title: 'ADB und Co Kacheln'
class: medium
features:
    -
        header: '"Alle Dörfer Bleiben" ("All Villages Remain")'
        text: 'is a group of resistant residents who organize demonstrations & discussions around the preservation of the threatened villages.'
        url: 'https://www.alle-doerfer-bleiben.de/'
        button: 'See the website!'
        target: blank
        colors: yellow-bg
    -
        header: '"Kirche(n) im Dorf lassen" ("Leave The Church(es) In The Village")'
        text: 'regularly invites people to worship on the edge, fights for the preservation of threatened churches & for global climate justice.'
        url: 'https://www.kirchen-im-dorf-lassen.de/'
        button: 'See the website!'
        target: blank
        colors: yellow-bg
    -
        header: '"Menschenrecht vor Bergrecht" ("Human Right Before Mining Law")'
        text: 'is a group of residents from the area around the open pit mine who are taking legal action against the expropriations sought by RWE.'
        url: 'https://menschenrecht-vor-bergrecht.de/'
        button: 'See the website!'
        target: blank
        colors: yellow-bg
---

