---
title: ''
background:
    image: /user/pages/images/guitars-light.png
    repeat: true
author: 'Alle Dörfer Bleiben-Aufruf'
heading_styles: null
paragraph_styles: light
mark_bg: null
---

„Wir stehen solidarisch dafür ein, dass alle Siedlungen und Orte bestehen bleiben und ein lebendiges Zusammenleben sowie eine gerechte und selbstbestimmte Transformation möglich werden.“