---
title: ''
background:
    image: /user/pages/images/guitars-light.png
    repeat: true
author: 'Alle Dörfer Bleiben-Aufruf'
heading_styles: null
paragraph_styles: light
mark_bg: null
---

„We stand in solidarity to ensure that all settlements and places remain and that a lively coexistence and a just and self-determined transformation become possible.“