---
title: 'The Villages'
hero_title_color: light
hero_background: /user/pages/images/thevillages.jpg
hero_foreground: /user/pages/images/village.svg
menu: 'The Villages'
published: true
---

## The villages at the Garzweiler II open pit mine

In western Germany, between Aachen, Cologne and Mönchengladbach, lies the Rhineland. Several million tons of lignite are mined there every year in the three opencast mines Hambach, Garzweiler II and Inden, making it the largest lignite mining area in Europe and also one of the largest sources of CO2. As a result, RWE's Neurath, Niederaussem and Weisweiler power plants located in the Rhenish mining area are among the five most damaging power plants in Europe - together with the Frimmersdorf power plant, they record annual CO2 emissions of 57.63 million tons (as of 2019) and are thus responsible for approximately 13% of Germany's greenhouse gas emissions. Despite these immensely high emissions, the "Coal Commission" (consisting of representatives from politics, coal companies and society) set up in June 2018 has dated the coal exit to 2038, which means that hundreds of millions of tons of lignite are also to be mined in the Rhenish mining area in the coming years.
<p>At the same time, it has long been clear that the opencast mines are a massive encroachment on nature, the landscape and groundwater, and that the Rhenish lignite mining area must be shut down as quickly as possible if Germany is to come anywhere close to meeting the targets set out in the Paris Climate Agreement for compliance with the 1.5°C limit. But it is not only nature and the global climate that suffer from coal - the social structures in the surrounding villages and the homes of many people are also destroyed in the process. In Germany alone, for example, more than 120,000 people have had to leave their homes for the extraction of lignite. And in the Rhineland, too, despite the decision to phase out coal, preparations continue for forced relocations and the destruction of villages, forests and fertile farmland. </p>

![village](/user/pages/images/Keyenberg.jpeg)

While the expansion of the Hambach open pit mine and the accompanying destruction of the remnants of the Hambach Forest could be prevented by a broad alliance of climate activists, NGOs and committed citizens in October 2018, there are so far no concrete protective measures for the villages of Berverath, Keyenberg, Kuckum, Lützerath, Oberwestrich and Unterwestrich, which are located on the edge of the Garzweiler II open pit mine. And this, although a few months ago it even became known that a study commissioned by the Ministry of Economics already concluded in December 2019 that the villages can also be saved within the limits set by the Coal Commission.
<p>For 2021, a variety of actions, events and protests are planned by residents and groups of the climate justice movement. It is important to help the struggle for the threatened villages to more attention, to develop a social awareness of the problem, to celebrate the resistance, to build new structures of solidarity and not least to stop the open pit mining, in order to come one step closer to a climate-just and livable world.</p>

![protest](/user/pages/images/hammock.jpg)