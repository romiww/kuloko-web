---
title: 'Die Dörfer'
hero_title_color: light
hero_background: /user/pages/images/thevillages.jpg
hero_foreground: /user/pages/images/village.svg
menu: 'Die Dörfer'
published: true
---

## Die Dörfer am Tagebau Garzweiler II

Im Westen Deutschlands, zwischen Aachen, Köln und Mönchengladbach, liegt das Rheinland. Dort werden in den drei Tagebauen Hambach, Garzweiler II und Inden jährlich mehrere Millionen Tonnen Braunkohle gefördert, womit es das größte Braunkohle-Abbaugebiet Europas und gleichzeitig eine der größten CO2-Quellen ist. Die im Rheinischen Revier stehenden RWE-Kraftwerke Neurath, Niederaußem und Weisweiler gehören dadurch zu den fünf schädlichsten Kraftwerken Europas – gemeinsam mit dem Kraftwerk Frimmersdorf verzeichnen sie einen jährlichen CO2-Ausstoß von 57,63 Millionen Tonnen (Stand 2019) und sind damit für ca. 13 % der Treibhausgasemissionen Deutschlands verantwortlich. Trotz dieser immens hohen Emmissionen hat die im Juni 2018 eingesetzte „Kohlekommission“ (bestehend aus Verteter:innen aus Politik, Kohleunternehmen und Gesellschaft) den Kohleausstieg auf 2038 datiert, wodurch auch im Rheinischen Revier in den nächsten Jahren noch hunderte Millionen Tonnen Braunkohle abgebaggert werden sollen.
<p> Dabei ist längst klar, dass durch die Tagebaue ein massiver Eingriff in Natur, Landschaft und Grundwasser vorgenommen wird und das rheinische Braunkohlerevier schnellstmöglich stillgelegt werden muss, wenn Deutschland die im Pariser Klimavertrag festgelegten Ziele zur Einhaltung der 1,5°C-Grenze auch nur annähernd einhalten möchte. Doch nicht nur die Natur und das weltweite Klima leiden unter der Kohle – auch die sozialen Strukturen in den umliegenden Dörfern und das Zuhause vieler Menschen werden dabei zerstört. So mussten allein in Deutschland bislang über 120.000 Menschen für die Förderung von Braunkohle ihr Zuhause verlassen. Und auch im Rheinland werden trotz beschlossenem Kohleausstieg weiter Zwangsumsiedlungen und die Zerstörung von Dörfern, Wäldern und fruchtbarem Ackerland vorbereitet. </p>

![village](/user/pages/images/Keyenberg.jpeg)

Während die Erweiterung des Tagebaus Hambach und die damit einhergehende Zerstörung der Reste des Hambacher Waldes durch ein breites Bündnis aus Klimaaktivist:innen, NGOs und engagierten Bürger:innen im Oktober 2018 verhindert werden konnte, gibt es für die am Rand des Tagebaus Garzweiler II liegenden Dörfer Berverath, Keyenberg, Kuckum, Lützerath, Oberwestrich und Unterwestrich bisher keine konkreten Schutzmaßnahmen. Und das, obwohl vor einigen Monaten sogar bekannt wurde, dass eine vom Wirtschaftsministerium in Auftrag gegebene Studie schon im Dezember 2019 zu dem Schluss kam, dass die Dörfer auch innerhalb der von der Kohlekommission gesetzten Grenzen gerettet werden können.

<p> Für 2021 sind von Anwohner:innen und Gruppen der Klimagerechtigkeitsbewegung vielfältige Aktionen, Veranstaltungen und Proteste geplant. Es gilt, dem Kampf um die bedrohten Dörfer zu mehr Aufmerksamkeit zu verhelfen, ein gesellschaftliches Bewusstsein für die Problematik zu entwickeln, den Widerstand zu feiern, neue solidarische Strukuren aufzubauen und nicht zuletzt den Tagebau zu stoppen, um einer klimagerechten und lebenswerten Welt einen Schritt näher zu kommen. </p>

![protest](/user/pages/images/hammock.jpg)