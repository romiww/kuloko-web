---
title: 'Global Justice'
menu: 'Global Justice'
hero_title_color: light
hero_background: /user/pages/images/climatejusticenow2.jpeg
hero_foreground: /user/pages/images/bar.svg
content:
    items: '@self.modular'
    order:
        custom: null
media_order: destruction.jpeg
---

# Global climate justice or why we resist.

The destruction of villages at the Garzweiler II open pit mine is just one of many impacts caused by RWE's reckless lignite mining. By burning lignite, RWE is responsible for an immensely high amount of CO2 emissions and thus contributes significantly to fueling the climate catastrophe.
And even though the consequences of the climate catastrophe will have an impact on all our lives in the long term, we have by no means all contributed equally to global warming, nor are we all equally affected by it.
While some people fuel the climate crisis with short-haul flights and have enough money to relocate to places that won't be flooded by rising sea levels, others have to fear for their homes, which are endangered by the pursuit of profit by companies like RWE. But it is not only the financial situation that determines how well people can protect themselves from the effects of the climate catastrophe.  <span class="popover">FLINTA*s (?)<span class="popover-container">all people who define themselves as women, lesbian, inter, non-binary, trans or agender</span></span> are on average more affected by the climate disaster, because they often have worse access to information and resources due to patriarchal structures and are more involved in agricultural work, which is strongly affected by natural disasters and extreme climate change.

![destruction](/user/pages/images/destruction.jpeg) 

While large climate-damaging corporations continue to be fed money, workers are persuaded they can save the climate by simply buying organic produce, and at the same time are the people least likely to receive support when resources become scarce. This creates the impression that the responsibility for ever-increasing CO2 emissions lies exclusively with individuals and their individual consumption behavior, instead of focusing on the underlying social structures and the destructive actions of large corporations like RWE. 
This is also shown in the <span class="popover">imperial way of life (?)<span class="popover-container">designating our current consumption and production behavior, which is based on an immense exploitation of people and nature, thereby outsourcing costs to other countries/social areas and supporting existing power relations</span></span> of the countries of the <span class="popover">Global North (? )<span class="popover-container">The term "Global North" describes a privileged position endowed with advantages, while the term "Global South" refers to a social, political and economic position disadvantaged in the global system. The division refers to the different experience of colonialism and exploitation - once as profiting and once as exploited.</span></span>, which is based on (post-)colonial exploitation structures and requires a disproportionate access to resources and labor in other parts of the world. And even apparent "solutions" to combat climate catastrophe, such as planting tree monocultures, often lead to <span class="popover">land grabbing (?)<span class="popover-container">the large-scale and sometimes illegal and violent appropriation of land by companies or state actors</span></span> in countries of the <span class="popover">Global South (? )<span class="popover-container">The term "Global North" is used to describe a privileged position endowed with advantages, while the term "Global South" refers to a social, political, and economic position disadvantaged in the global system. The division refers to the different experience with colonialism and exploitation - once as profiting and once as exploited.</span></span> and thereby consolidate existing power and violence relations.



## Colonial continuities

While in the countries of the Global North the capitalist growth logic is pushed to the extreme and a large part of the global CO2 emissions are produced, the consequences of climate catastrophe are already a cruel reality, especially in countries of the Global South. Natural disasters such as storms, fires and floods are becoming more frequent, and people are losing their material livelihoods, homes or lives. Because of this urgency, a climate justice movement has been forming in countries of the Global South for decades, and we must learn from and build on their struggles. → For more detailed information on worldwide climate struggles, especially in the Global South, see **[here.](https://ejatlas.org/country)** 


Moreover, by destroying the livelihoods of many people, climate catastrophe has become one of the most common global causes of flight. Because our current capitalist economy is based on a compulsion for growth and the constant pursuit of profit, we are forced to consume more and more resources, driving global warming. If we fail to change this economic mode, more and more parts of our world will become uninhabitable, forcing more and more people to leave their homes. But the logic of "us and them", created and maintained by states and nations, prevents the freedom of movement of certain people. It creates borders and segregation and is shown in its cruelest extent in camps like Moria and the behavior of the European border control agency Frontex, through which the Mediterranean Sea has become the currently deadliest border in the world.



## Think global, block local! 

So we are not all in the same boat, as often portrayed, but have very different responsibilities and conditions to respond to the consequences of the climate catastrophe, which is further reinforced by the unfair global distribution of medical care, evacuation options, information and (aid) resources. 

So let's fight for a world where we don't continue to destroy the livelihood of many people just to keep the Western European, white affluent society going. We need to understand how closely the climate catastrophe is linked to racist, colonial and <span class="popover">patriarchal (?)<span class="popover-container">patriarchy = social system in which social relations, values, norms and behaviors are dominated and represented by male domination</span></span> structures, think struggles together and stand up together for a climate-just and livable world for all!