---
title: 'Globale Gerechtigkeit'
menu: 'Globale Gerechtigkeit'
hero_title_color: light
hero_background: /user/pages/images/climatejusticenow2.jpeg
hero_foreground: /user/pages/images/bar.svg
content:
    items: '@self.modular'
    order:
        custom: null
media_order: destruction.jpeg
---

# Globale Klimagerechtigkeit oder warum wir Widerstand leisten

Die Zerstörung der Dörfer am Tagebau Garzweiler II ist nur eine von vielen Auswirkungen, die durch den rücksichtslosen Braunkohleabbau von RWE entstehen. Durch die Verbrennung von Braunkohle ist RWE für eine immens hohe Menge an CO2-Emissionen verantwortlich und trägt somit maßgeblich zur Anfeuerung der Klimakatastrophe bei.
Und auch wenn die Folgen der Klimakatastrophe langfristig gesehen Auswirkungen auf unser aller Leben haben werden, haben wir keinesfalls alle gleichermaßen zur Erderhitzung beigetragen und sind auch nicht alle gleichermaßen von ihr betroffen.
Während die einen mit Kurzstreckenflügen die Klimakrise anfachen und genug Geld besitzen an Orte umzusiedeln, die nicht durch den steigenden Meeresspiegel überschwemmt werden, müssen andere Menschen um ihr Zuhause bangen, welches durch das Profitstreben von Unternehmen wie RWE gefährdet ist. Doch nicht nur die finanzielle Situation entscheidet, wie gut sich Menschen vor den Auswirkungen der Klimakatastrophe schützen können.  <span class="popover">FLINTA*s (?)<span class="popover-container">alle Personen, die sich als Frauen, Lesben, inter, nicht-binär, trans oder agender definieren</span></span> sind durchschnittlich stärker von der Klimakatastrophe betroffen, da sie aufgrund von patriarchalen Strukturen oft einen schlechteren Zugang zu Informationen und Ressourcen besitzen und vermehrt in der landwirtschaftlichen Arbeit tätig sind, die von Naturkatastrophen und extremen Klimaveränderungen stark betroffen ist.

![destruction](/user/pages/images/destruction.jpeg) 

Während große klimaschädliche Unternehmen weiter mit Geld gefüttert werden, wird Arbeitnehmer:innen eingeredet sie könnten mit dem bloßen Kaufen von Bio-Produkten das Klima retten und sind gleichzeitig die Menschen, die am wenigsten Unterstützung erhalten, wenn Ressourcen knapp werden. Dadurch entsteht der Eindruck, die Verantwortung für die immer weiter steigenden CO2-Emissionen läge ausschließlich bei den einzelnen Menschen und ihrem individuellen Konsumverhalten, statt die dem zugrundeliegenden gesellschaftlichen Strukturen und das zerstörerische Handeln großer Konzerne wie RWE in den Blick zu nehmen. 
Dies zeigt sich ebenfalls in der <span class="popover">imperialen Lebensweise (?)<span class="popover-container">bezeichnet unser derzeitiges Konsum- und Produktionsverhalten, das auf einer immensen Ausbeutung von Mensch und Natur basiert, dabei Kosten in anderen Länder/Gesellschaftsbereiche auslagert und bestehende Herrschaftsverhältnisse stützt</span></span> der Länder des <span class="popover">Globalen Nordens (?)<span class="popover-container">Mit dem Begriff “Globaler Norden” wird eine mit Vorteilen bedachte, privilegierte Position beschrieben, während sich der Begriff “Globaler Süden” auf eine im globalen System benachteiligte gesellschaftliche, politische und ökonomische Position bezieht. Die Einteilung verweist auf die unterschiedliche Erfahrung mit Kolonialismus und Ausbeutung - einmal als Profitierende und einmal als Ausgebeutete.</span></span>, die auf (post-)kolonialen Ausbeutungsstrukturen beruht und einen unverhältnismäßigen Zugriff auf Ressourcen und Arbeitskraft in anderen Teilen der Welt erfodert. Und auch scheinbare „Lösungsvorschläge“ zur Bekämpfung der Klimakatastrophe wie beispielsweise das Pflanzen von Baum-Monokulturen führen oft zu <span class="popover">Landraub (?)<span class="popover-container">die großflächige und teils illegale und gewaltvolle Aneignung von Land durch Firmen oder staatliche Akteure</span></span> in Ländern des <span class="popover">Globalen Südens (?)<span class="popover-container">Mit dem Begriff “Globaler Norden” wird eine mit Vorteilen bedachte, privilegierte Position beschrieben, während sich der Begriff “Globaler Süden” auf eine im globalen System benachteiligte gesellschaftliche, politische und ökonomische Position bezieht. Die Einteilung verweist auf die unterschiedliche Erfahrung mit Kolonialismus und Ausbeutung - einmal als Profitierende und einmal als Ausgebeutete.</span></span> und festigen dadurch bestehende Macht- und Gewaltverhältnisse.



## Koloniale Kontinuitäten

Während in den Ländern des Globalen Nordens die kapitalistische Wachstumslogik auf die Spitze getrieben wird und einen Großteil der weltweiten CO2-Emissionen erzeugt wird, sind die Folgen der Klimakatastrophe insbesondere in Ländern des Globalen Südens schon jetzt grausame Realität. Naturkatastrophen wie Stürme, Brände und Überflutungen häufen sich, Menschen verlieren ihre materielle Lebensgrundlage, ihr Zuhause oder ihr Leben. Aufgrund dieser Dringlichkeit formiert sich in Ländern des Globalen Südens schon seit Jahrzehnten eine Klimagerechtigkeitsbewegung, von der wir lernen und an deren Kämpfe wir anknüpfen müssen. → Genauere Informationen zu weltweiten Klimakämpfen, insbesondere im Globalen Süden, findest du **[hier.](https://ejatlas.org/country)** 


Zudem ist die Klimakatastrophe durch die Zerstörung der Lebensgrundlagen vieler Menschen zu einer der häufigsten weltweiten Fluchtursachen geworden. Da unsere derzeitige kapitalistische Wirtschaftsweise auf einem Zwang nach Wachstum und dem ständigen Streben nach Profit basiert, sind wir gezwungen, immer mehr Ressourcen zu verbrauchen und so die Erderhitzung voranzutreiben. Schaffen wir es nicht, diese Wirtschaftsweise zu ändern, werden immer mehr Teile unserer Welt unbewohnbar, sodass immer mehr Menschen gezwungen werden, ihre Heimat zu verlassen. Doch die Logik von “Wir und die Anderen”, die durch Staaten und Nationen geschaffen und aufrechterhalten wird, verhindert die Bewegungsfreiheit bestimmter Menschen. Sie schafft Grenzen und Abgrenzung und zeigt sich in ihrem grausamsten Ausmaß in Lagern wie Moria und dem Verhalten der europäischen Grenzschutzbehörde Frontex, durch die das Mittelmeer zur momentan tödlichsten Grenze der Welt geworden ist.



## Think global, block local! 

Wir sitzen also nicht, wie oft dargestellt, alle in einem Boot, sondern verfügen über ganz unterschiedliche Verantwortlichkeiten und Voraussetzungen, um auf die Folgen der Klimakatastrophe reagieren zu können, was durch die ungerechte weltweite Verteilung von medizinischer Versorgung, Evakuationsmöglichkeiten, Informationen und (Hilfs-)Ressourcen nochmal verstärkt wird. 

Lasst uns also für eine Welt kämpfen, in der wir nicht weiter die Lebensgrundlage vieler Menschen zerstören, nur um die westeuropäische, weiße Wohlstandsgesellschaft am Laufen zu halten. Wir müssen verstehen, wie eng die Klimakatastrophe mit rassistischen, kolonialen und <span class="popover">patriarchalen (?)<span class="popover-container">Patriarchat = gesellschaftliches System, in dem soziale Beziehungen, Werte, Normen und Verhaltensweisen von männlicher Herrschaft dominiert und repräsentiert werden</span></span> Strukturen verknüpft ist, Kämpfe zusammendenken und gemeinsam für eine klimagerechte und lebenswerte Welt für alle einstehen!