---
title: 'Zeitraum verlängert!'
taxonomy:
    category:
        - news
published: true
date: '02-07-2021 00:00'
---

Die Kuloko wurde verlängert, Yippie!  Weil es vor und nach dem eigentlichen Programm-Teil vieles auf- und abzubauen gibt und es dafür ganz schön viele Menschen braucht, werden wir ab sofort den Aufbau (01.-05.08.) und den Abbau (16.-22.08.) als Teil der Kuloko sehen und das auch so kommunizieren.