---
title: 'Lützi lebt und Lützi bleibt!'
taxonomy:
    category:
        - news
---

Was geht eigentlich in Lützerath? - Einblick in den aktuellen Stand um Lützerath und die drohende Enteignung



Seit 2020 ist das
Dorf Lützerath am Tagebau Garzweiler II  noch akuter als schon zuvor
von der Zerstörung bedroht. Im Jahr 2021 hat RWE begonnen Bäume
roden und Häuser abreißen zu lassen. Schon im Sommer 2020 formierte
sich vor Ort sichtbarer Widerstand, welcher weiterhin dauerhaft vor
Ort aktiv ist. Die nach den
Protesten gegen den Abriss der Landstraße L277, die den noch
einzigen offiziellen Zugang zum Dorf dargestellt hatte, entstandene
Mahnwache an der Grubenkante, ist weiterhin jeden Tag zugänglich und
dient als Anlaufpunkt für Menschen, die sich über den Ort
informieren wollen. Die Mahnwache ist dabei auch Anlaufpunkt geworden
für Veranstaltungen und Kultur, um auf die Bedrohung durch den
Tagebau aufmerksam zu machen. Hinter der Mahnwache stehen Menschen,
die mit den lokalen widerständigen Gruppen, wie „Alle Dörfer
bleiben“, „Kirche(n) im Dorf lassen“, „Unser aller Wald“
oder „Menschenrecht vor Bergrecht“, gut vernetzt sind. Der
Ort steckt also voller Leben, welches durch RWE bedroht wird. 




Auch die Häuser und
das Land von Eckhard Heukamp werden von RWE versucht in Anspruch zu
nehmen. Im April 2021 kündigte dieser jedoch an, seinen Wohnsitz in
Lützerath nicht aufzugeben und diese Inanspruchnahme juristisch
anzufechten. Vor dem Verwaltungsgericht Aachen hat Eckhard nun
Widerspruch eingelegt. 

RWE will diese Klage
allerdings nicht abwarten und hat eine „vorzeitige
Besitzeinweisung“ zum 01. November 2021 bei der zuständigen
Bezirksregierung Arnsberg beantragt. Wie sich Arnsberg dazu
positioniert steht noch aus.
Uns zeigt es jedoch
ganz deutlich, dass RWE vor nichts zurückschreckt, um eigene
Profitinteressen durchzuboxen. 




Vor einigen Tagen
wurde bekannt, dass es eventuell in Lützerath ein fast vergessenes
Kapellengrundstück gibt, welches weder im Besitz von Eckard Heukamp,
noch von RWE ist und nun von der christlichen Initiative „Kirche(n)
im Dorf lassen“ (kurz KiDl) versucht wird aufzukaufen. Die
vermuteten Rechtsnachfolgerin Pfarrerei Christkönig Erkelenz hat
Ende Juni ein Kaufangebot von KiDl erhalten. Der potentielle Verkauf
dieses Grundstücks könnte ein neuer Wendepunkt im Kontext Lützerath
und Enteignung darstellen. Mehr Infos gibt es **[hier.](https://www.kirchen-im-dorf-lassen.de/positionen/pressemitteilungen/27-06-21-pm-kappellengrundst%C3%BCck/)**



Werdet Teil der
Kuloko, um deutlich zu zeigen: Lützi lebt und wir lassen uns nicht
einschüchtern! Und haltet euch bereit, um im Herbst mit lauten
Protesten entschlossen gegen die Enteignung durch RWE vorzugehen,
denn: Lützi lebt nicht nur, Lützi bleibt!