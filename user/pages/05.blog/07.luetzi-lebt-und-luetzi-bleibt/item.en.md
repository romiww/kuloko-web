---
title: 'Lützi is alive and remains!'
taxonomy:
    category:
        - news
---

What is actually going on in Lützerath? - Insight into the current state of affairs around Lützerath and the threat of expropriation



Since 2020, the village of Lützerath at the Garzweiler II open pit mine has been threatened with destruction even more acutely than before. In 2021, RWE began clearing trees and demolishing houses. Already in the summer of 2020, visible resistance formed on site, which continues to be permanently active on site. After the protests against the demolition of the L277 road, which was the only official access to the village, the vigil at the edge of the pit is still accessible every day and serves as a contact point for people who want to learn more about the place. In the process, the vigil has also become a focal point for events and culture to draw attention to the threat posed by open pit mining. Behind the vigil are people who are well networked with local resistant groups, such as "Alle Dörfer bleiben", "Kirche(n) im Dorf lassen", "Unser aller Wald" or "Menschenrecht vor Bergrecht". So the village is full of life, which is threatened by RWE. 



RWE is also trying to claim the houses and land of Eckhard Heukamp. In April 2021, however, he announced that he would not give up his residence in Lützerath and would legally challenge this claim. Eckhard has now lodged an appeal with the Aachen Administrative Court. 

RWE, however, does not want to wait for this appeal and has applied to the responsible Arnsberg district government for an "early transfer of possession" as of November 1, 2021. Arnsberg's position on this has yet to be determined. It shows us however completely clearly that RWE shrinks from nothing, in order to push through own profit interests. 




Some time ago it became known that there is possibly in Lützerath an almost forgotten chapel property, which is neither in the possession of Eckard Heukamp, nor of RWE and now by the Christian initiative "Kirche(n) im Dorf lassen" (briefly KiDl) is tried to buy up. The presumed legal successor, Pfarrerei Christkönig Erkelenz, received a purchase offer from KiDl at the end of June. The potential sale of this property could represent a new turning point in the context of Lützerath and expropriation. More info is available **[here.](https://www.kirchen-im-dorf-lassen.de/positionen/pressemitteilungen/27-06-21-pm-kappellengrundst%C3%BCck/)**



Become part of the KuloKo to clearly show: Lützi is alive and we will not be
intimidated! And be ready to take decisive action against the expropriation by RWE with loud protests in autumn, because: Lützi not only lives, Lützi remains!
