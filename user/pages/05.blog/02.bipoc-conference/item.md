---
title: 'BIPoC-Klimakonferenz auf der Kuloko'
taxonomy:
    category:
        - news
date: '17-07-2021 08:51'
---

Bock, mal nicht nur mit weißen beim Plenum zu sitzen?
Bock, andere BIPoCs kennenzulernen, die aktiv werden wollen?
Bock, zusammen Klimakrise, White Supremacy und Patriarchat zu zerschlagen?

Eine unabhängige Gruppe an BIPOCs macht sich auf den Weg, einen Konferenztag auf dem klimapolitischen Festival "Kultur ohne Kohle - eine rheinische Landpartie" zu planen. Denn im Dorf geht's um die ganze Welt. Wir machen Kultur ohne Kohlonialismus und laden alle Menschen, die Rassismus erfahren ein: Wir möchten uns vernetzen, bilden, feiern, widerständig und für einander da sein. Zusammen sind wir stärker, zusammen schaffen wir eine migrantische, dekoloniale Klimabewegung. Welche Workshops und Podien wir machen, was es zum Mittagessen gibt, wen wir einladen, welche Musik dabei läuft - all das gilt es noch zu gestalten, und einen verbindenden Orga Prozess zu erleben.

### Wenn du dabei sein willst, melde dich gerne per Mail an [info@kultur-ohne-kohle.de](mailto:info@kultur-ohne-kohle.de)