---
title: 'Unser Aller Wald und Kultur ohne Kohle'
published: true
taxonomy:
    category:
        - news
---

Am Rand von Keyenberg, zwischen der Kirche und dem Sportplatz, steht ein kleiner Laubwald. In weniger als zehn Minuten lässt er sich durchqueren. Doch die Wege gibt es erst seit kurzem. Seit September letzten Jahres, seit es Unser Aller Wald ist.
Zu Anfang gab es nur ein Baumhaus in einer hohen Buche und ein größeres, mehrstöckiges, den Tower. Mittlerweile ist Unser Aller Wald ein ganzes Baumhausdorf geworden. Ein Baumhausdorf gegen den fossilen Kapitalismus. Aber warum?

Unser Aller Wald will nicht nur die Kohle stoppen, sondern gegen den fossilen Kapitalismus eintreten, der globale soziale Ungerechtigkeit und die Klimakrise immer weiter anheizt. Aber Kohle und Kapitalismus stoppen stellt die Frage nach dem Danach: Wie gestalten wir unser Zusammenleben in einer gerechten Welt?

„Alles für alle“ steht rot auf blau auf einem der Banner, das zwischen den Bäumen hängt. Unser aktuelles Wirtschaftssystem, beruht darauf, dass Menschen für Geld arbeiten müssen, um essen und wohnen zu können – obwohl doch genug für alle da ist. Unser Aller Wald und Kultur, deshalb soll beides ohne Kohle gehen. Freiwilliges Beitragen ist eine der zentralen Säulen des Zusammenlebens hier. Weil die Klimakrise im Kapitalismus nicht gelöst werden kann, braucht es solidarische Freiräume, in denen ein Vorgeschmack auf die Utopie ausprobiert werden kann. 

Auch während der KuloKo ist Unser Aller Wald am Start. Gemeinsam werden wir einen solidarischen, diskriminierungsarmen Raum schaffen, in dem wir einander kennen lernen können. Ob es ums Klettern lernen oder den Austausch über Utopie geht – an- und miteinander lernt und wächst es sich doch am besten!

Mehr Infos: https://unserallerwald.noblogs.org/