---
title: 'Packing List'
redirect: /infos/packing-list
taxonomy:
    category:
        - news
---

You've been asking yourself all along what you should actually take to the Kuloko? Spoons!!! And a bicycle. You can find everything else here on the packing list.