---
title: 'Neuer Termin für die Kuloko'
taxonomy:
    category:
        - news
published: true
date: '16-05-2021 13:47'
---

Aufgrund der aktuellen Pandemie-Entwicklungen wird die „Kultur ohne Kohle“ auf den **06.08. - 15.08.2021** verschoben.

Wir wollen weiterhin daran festhalten gemeinsam mit euch die Dörfer mit Leben zu füllen und den Widerstand zu feiern, allerdings lieber ein paar Monate später und damit umso sicherer. Wir verfolgen die aktuelle Corona-Lage und entwickeln Konzepte für einen verantwortungsvollen Umgang damit. Alle weiteren Infos folgen in Zukunft auf unseren Social-Media-Kanälen und hier auf der Website.

Bleibt gespannt!