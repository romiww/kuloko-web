---
title: Kompoletten-Bau-Wochenende
taxonomy:
    category:
        - news
published: true
date: '18-07-2021 00:00'
---

Vom 30. Juli bis 01. August, also kurz vor dem Startschuss für den
Aufbau der Kuloko, wollen wir mit euch ein Skillshare für das Bauen
von transportfähigen Kompoletten (Komposttoiletten) in Lützerath
machen. Wir haben uns gegen das Mieten der Kompoletten entschieden,
um für weniger Geld eine langfristige Erweiterung der Infrastruktur
für die politische Bewegungen möglich zu machen. Und dafür
brauchen wir eure Hilfe. 






Freitag
wollen wir mit einem Vorbereitungstag starten, dessen Beginn am
Nachmittag ist und mit der Vorbereitung, Einweisung, Anschauen der
Pläne etc. für das Bauen am Samstag und Sonntag gefüllt sein wird.
Samstag und Sonntag sind dann Bautage. Samstag von Morgens ab 10 Uhr
bis Abends und Sonntag ab 10 Uhr bis Mittags/Nachmittags.





Ihr
braucht kein Vorwissen mitzubringen, denn es wird ein angeleitetes
Skillshare stattfinden, bei dem wir lernen wie wir einfach
Kompoletten bauen können. Wenn ihr habt, bringt aber gerne noch
eigenes Werkzeug, wie Akkuschrauber, Handkreissägen, Zollstock,
Stifte etc. mit. Die Kompoletten können wenn sie fertig sind einfach
wieder in 4 Teile zerlegt werden, sodass sie leicht transportiert und
gelagert werden können. Wir werden sowohl barrierearme als auch
nicht-barrierearme Kompoletten bauen.





Also
kommt vorbei, wenn ihr Lust habt zu werkeln, zu bauen und neue
handwerkliche Fähigkeiten zu erlernen für eine Infrastruktur, die
direkt auf der Kuloko zum Einsatz kommt. Außerdem könnt ihr das
Gelernte auch super für eure nächste Besetzung, Veranstaltung,
Gartenlaube oder WG nutzen.

Wir
freuen uns auf euch!

Du hast Fragen, Anmerkungen oder magst bescheid sagen, dass du dabei bist? Schreib gerne eine Mail an [infrabau@riseup.net](mailto:infrabau@riseup.net)