---
title: Hygienekonzept
published: true
---

## Hygienekonzept - Umgang mit Corona
Wir wollen ein sicheres, achtsames und verantwortungsvolles Festival, auf dem sich alle Menschen wohlfühlen können. Dazu gehört auch ein angemessener Umgang mit der Coronalage. Unser Hygienekonzept, das wir deshalb für die Kuloko erarbeitet haben, findet ihr [hier](/infos/hgienekonzept).