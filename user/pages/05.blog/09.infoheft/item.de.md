---
title: 'Info- und Programmheft'
taxonomy:
    category:
        - news
---

(Fast) Alles, was du für die Kuloko wissen brauchst, findest du im Kuloko Info- und Programmheft. <br> **WICHTIG:** das Programmheft wird nicht mehr geupdated. Das bedeutet, dass mögliche Programmänderungen dort nicht übernommen werden. Das Programm unter [Timetable](/program/timetable) ist immer möglichst aktuell, aber die absolut heißesten Neuigkeiten und Programmänderungen gibt es nur im [Telegram Infochannel](https://t.me/kulokofestival)
<div>
<div class="button-container" style="height: 2.5rem;"> 
	
	<a href="/program/timetable" target="_blank">    <div class="feature-button rounded-edges yellow-bg black">
	Zum Timetable!
    </div>
    </a>
</div>
</div>					

<div>
<div class="button-container" style="height: 2.5rem;"> 
	
	<a href="https://t.me/kulokofestival" target="_blank">    <div class="feature-button rounded-edges yellow-bg black">
	Telegram Channel!
    </div>
    </a>
</div>
</div>					

<div>
<div class="button-container" style="height: 2.5rem;"> 
	
	<a href="/blog/infoheft/Kuloko_Infoheft.pdf" target="_blank">    <div class="feature-button rounded-edges yellow-bg black">
	Zum Infoheft!
    </div>
    </a>
</div>
</div>							  
