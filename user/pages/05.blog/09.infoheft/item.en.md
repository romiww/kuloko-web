---
title: 'Info and program booklet'
taxonomy:
    category:
        - news
---

Unfortunately there was not enough time to translate the booklet into English. We hope you will find all relevant information on the website.
**IMPORTANT:** The program booklet is no longer updated. This means that possible program changes will not be reflected there. The program under [Timetable](/program/timetable) is always as up to date as possible, but the absolute hottest news and program changes can only be found in the [Telegram Channel](t.me/kulokofestival).
<div>
<div class="button-container" style="height: 2.5rem;"> 
	
	<a href="/blog/infoheft/Kuloko_Infoheft.pdf" target="_blank">    <div class="feature-button rounded-edges yellow-bg black">
	See the booklet!
    </div>
    </a>
</div>
</div>							  
