---
title: 'Kuloko Telegram Channel'
date: '28-07-2021 00:00'
category:
    - news
taxonomy:
    category:
        - news
---

Join the Kuloko Telegram Channel! There you will find daily updates on the Kuloko program and participation structure, a carpool exchange and all kinds of news concerning the Kuloko.
<div>
<div class="button-container" style="height: 2.5rem;"> 
	
	<a href="https://t.me/kulokofestival" target="_blank">    <div class="feature-button rounded-edges yellow-bg black">
	Info Channel
    </div>
    </a>
</div>
</div>		

By the way, there is also a Kuloko networking group on Telegram. This is a place to network with other festival participants. Whether you're looking for a ride, soccer teammates, or your chest bag, you can forward your requests and info directly to everyone here.
<div>
<div class="button-container" style="height: 2.5rem;"> 
	
	<a href="https://t.me/kuloko" target="_blank">    <div class="feature-button rounded-edges yellow-bg black">
	Networking Group
    </div>
    </a>
</div>
</div>		