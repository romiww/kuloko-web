---
title: 'Kuloko Telegram Känale'
date: '28-07-2021 00:00'
category:
    - news
taxonomy:
    category:
        - news
---

Komm in den Kuloko Telegram Channel! Dort gibt es während der Kuloko Tagesaktuelle Infos zu Programm und Mitmach-Struktur, Eine Mitfahrbörse und alle Möglichen News, die die Kuloko betreffen.
<div>
<div class="button-container" style="height: 2.5rem;"> 
	
	<a href="https://t.me/kulokofestival" target="_blank">    <div class="feature-button rounded-edges yellow-bg black">
	Infohannel
    </div>
    </a>
</div>
</div>		


Übrigens, es gibt auch eine Kuloko Vernetzungsgruppe auf Telegram. Hier ist Platz, um sich mit anderen Festivalteilnehmer\*innen zu vernetzen. Ob ihr eine Mitfahrgelegenheit, Fußball Mitspieler\*innen oder euren Brustbeutel sucht, hier könnt ihr eure Anfragen und Infos direkt an alle weiterleiten.
<div>
<div class="button-container" style="height: 2.5rem;"> 
	
	<a href="https://t.me/kuloko" target="_blank">    <div class="feature-button rounded-edges yellow-bg black">
	Vernetzungsgruppe
    </div>
    </a>
</div>
</div>		