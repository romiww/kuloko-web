---
title: 'Klimacamp Rheinland goes Kuloko'
taxonomy:
    category:
        - news
---

Na, freut ihr euch schon, endlich wieder den Sommer auf dem Klimacamp im Rheinland (KCR) zu verbringen und habt gleichzeitig Lust, bei der Kultur ohne Kohle vorbeizuschauen und Musik und Kultur in den Dörfern zu erleben?


Das trifft sich gut, denn die Kuloko und das KCR finden dieses Jahr gemeinsam statt! Vom 06.-15. August werden wir uns bilden und vernetzen, alternative Formen des Zusammenlebens erproben, die Dörfer mit viel Kunst, Kultur und Aktivismus beleben und gemeinsam den Widerstand feiern. Das genaue Workshop-Programm des Klimacamps wird gerade noch ausgetüftelt – Infos dazu findet ihr bald hier oder auf http://www.klimacamp-im-rheinland.de.