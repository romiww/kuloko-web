---
title: 'Corona und Kuloko?'
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
taxonomy:
    category:
        - news
---

Wir nehmen die Pandemie ernst und arbeiten an Hygienekonzepten und corona-konformen Veranstaltungsszenarien, um eine möglichst schöne, aber auch sichere rheinische Landpartie für euch auf die Beine zu stellen. Wir halten euch hier auf dem Laufenden und informieren euch frühzeitig über Veränderungen in der Veranstaltungsumsetzung.