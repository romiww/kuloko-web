---
title: 'Stations of resistance'
---

### Play stations on the way to utopia 
"One hand washes the other" - says such an old saying from ancient Greece, which comes from a satire; just sounds like a lot of work.  :)
But the wish and the vision is another, a bigger one! We would like to make a utopia tangible and experienceable at the Kuloko. The utopia of a world free of exchange logic. It is about growing together, learning together, developing together and it is also about opening the space for each individual. So that everyone can participate.
Be part of the lived utopia and visit our festival stations!

### 1. NoNation-TogetherStation (Info Points)
In order for all of us to feel comfortable at KuloKo, it is important that we work together. Therefore, check the "NoNationTogetherStation" info points from time to time to see if anyone, anywhere, still needs your help. Info points are at MaWa Lützerath and MaWa Keyenberg. Tasks will be: kitchen shifts, e.g. washing plates or cutting apples for muesli, bar keeping, being a steward, participating in toilet cleaning actions, and of course the set-up (31.07-05.08.) and the dismantling (16.-22.08.) want to be managed. 

### 2. Denk-Mal-Station (Think-About-It-Station; Theaterwiesen)
The remaining villages hide countless monuments that would be lost forever with the destruction of RWE. Help to draw attention to the monuments! For this purpose we want to build yellow frames together. 
The frames will then contain information that briefly describes the monuments. 
Of course, you can also place your Denk-Mal frame somewhere else - on something that you find particularly beautiful, for example. You will find the Denk-Mal station on the Theaterwiese (Kuckum). 

### 3. Demo-Station (at MaWaLü)
At this station we want to look back on our common resistant and colorful history. It is meant to be an exhibition of your personal artifacts of resistance. Feel free to bring your favorite demo signs and transpis from home and display them in the collective exhibition of climate political resistance.

### 4. Feel-Station (at MaWaKey) 
Here you will find small pieces of wood on which you can write your thoughts and feelings about the climate and humanity crisis. Afterwards you can hang your thoughts on a cord at the edge of the L277. Everyone should see that thoughts are hanging here that will not only shape the KuloKo festival but also our common future. Questions you could ask yourself for this would be: What disappointments arise in you? What could a common future in the villages look like? What should happen to the coal mine when the excavators finally leave?
