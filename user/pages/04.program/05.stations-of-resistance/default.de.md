---
title: 'Stationen der Widerstands'
---

### Spielstationen auf dem Weg zur Utopie 
„Eine Hand wäscht die Andere“ - sagt so ein alter Spruch aus dem alten Griechenland, der aus einer Satire stammt; hört sich nur nach Arbeit an.  :)
Doch der Wunsch und die Vision ist eine andere, eine Größere! Wir möchten auf der Kuloko eine Utopie greifbar und erfahrbar machen. Die Utopie einer tauschlogikfreien Welt. Es geht um ein gemeinsames Wachsen, zusammen Lernen, gemeinsames Entwickeln und es geht auch darum, den Raum für jede*n Einzelne*n zu öffnen. Damit alle mitgestalten können.
Sei auch du Teil der gelebten Utopie und besuche unsere Festivalstationen !

### 1. NoNation-TogetherStation (Infopunkte)
Damit wir uns alle auf der KuloKo wohlfühlen können, ist es wichtig anfallende Arbeiten zusammen zu meistern. Schau deshalb ab und an an den „NoNationTogetherStation“-Infopunkten vorbei, ob irgendwo, irgendwer noch Deine Hilfe braucht. Infopunkte sind an der MaWa Lützerath und MaWa Keyenberg. Aufgaben werden sein: Küchenschichten, z.B. Teller waschen oder Äpfel für Müsli schnibbeln, Barkeeping, Ordner*in sein, bei Kloputzaktionen mitmachen, und natürlich wollen auch der Aufbau (31.07-05.08.) und der Abbau (16-22.08) gestemmt werden. 

### 2. Denk-Mal-Station (Theaterwiesen)
In den noch verbliebenen Dörfern verbergen sich unzählige Denkmäler, die mit der Zerstörung von RWE für ewig verloren gehen würden. Hilf mit auf die Denkmäler aufmerksam zu machen! Hierfür wollen wir gemeinsam gelbe Rahmen bauen. 
In die Rahmen kommen dann Informationen, auf denen die Denkmäler kurz beschreiben werden. 
Natürlich könnt Ihr euren Denk-Mal-Rahmen auch wo anders anbringen - an etwas, das ihr ganz besonders schön findet zum Beispiel. Die Denk-Mal-Station findet ihr auf der Theaterwiese. 

### 3. Demo-Station (bei MaWaLü)
An dieser Station wollen wir auf unsere gemeinsame widerständige und bunte Geschichte zurückblicken. Es soll eine Ausstellung eurer persönlichen Artefakte des Widerstands sein. Bringt gerne eure Liebingsdemoschilder und Transpis von Zuhause mit und stellt sie in der kollektiven Ausstellung des klimapolitischen Widerstands aus.
### 4. Feel-Station (bei MaWaKey) 
Hier findet ihr kleine Holzstücke, auf die ihr eure Gedanken und Gefühle hinsichtlich der Klima- und Menschheitskrise schreiben könnt. Anschließend könnt ihr Eure Gedanken an einer Kordel an die Schnur am Rande der L277 hängen. Alle sollen sehen, dass hier Gedanken hängen, die nicht nur das KuloKo-Festival gestalten sondern auch unsere gemeinsame Zukunft gestalten werden. Fragen, die Du Dir hierfür stellen könntest, wären: Welche Enttäuschungen kommen in Euch auf? Wie könnte eine gemeinsame Zukunft in den Dörfern aussehen? Was sollte mit der Kohlengrube passieren, wenn sich die Bagger endlich verziehen?
