---
title: content-beginning
body_classes: modular
image_align: left
---

## Workshops
Übrigens: die **Workshops** auf der Kuloko bzw. **Beschreibungen** zu diesen findest du unter [Lineup](/program/lineup). Auf dieser Seite gibt es eine allgemeinere Beschreibung des inhaltlichen Programms. Wann welcher Programmpunkt stattfindet siehst du im [Timetable](/program/timetable)

## Freies Programm
Ab Sonntag dem 08.08, gibt es ein vielfältiges inhaltliches Programm mit Workshops, Skill-Shares und mehr in Lützerath (Klimacamp), Unterwestrich (Schlosswiese) und Keyenberg (MaStaMO Camp). Alle Infos dazu finden sich im Zeitplan. 

## Sprache
Die Podiumsdiskussionen werden ins Englische/Deutsche gedolmetscht und bei Bedarf und nach Möglichkeit in weitere Sprachen. In den Workshops werden wir uns um Flüsterübersetzungen bemühen. Bei konkretem Bedarf an Übersetzung für einzelne Veranstaltungen sprecht uns gern im Vorhinein über den Infopunkt auf dem Klimacamp an und wir versuchen, eine Übersetzung zu finden.

## Aufbauendes Programm
System Change not Climate Change! 
Mit jeder Horrornachricht über globale Klimafolgen und jedem erneuten politischen Versagen der Regierenden wird dieser Ruf lauter. Aber was bedeutet er eigentlich? Was wollen wir stattdessen und wie kommen wir dahin? Und vor allem: Was bedeutet das für unsere Praxis? Diesen Fragen wollen wir uns zusammen mit spannenden Referent*innen widmen. Dabei gibt es ab Montag, den 09.08.2021, tagsüber bis zu vier Workshops und abends eine gemeinsame Diskussion mit den Referent*innen. Das Programm baut thematisch aufeinander auf, doch es ist ebenso möglich nur einzelne Programmpunkte zu besuchen!


