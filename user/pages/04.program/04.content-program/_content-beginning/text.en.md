---
title: content-beginning
body_classes: modular
---

## Open Program
Starting on Sunday, August 8, there will be a diverse program with workshops, skill-shares and more in Lützerath (Climate Camp), Unterwestrich (Schlosswiese) and Keyenberg (MaStaMO Camp).  All info can be found in the schedule. 

## Language
The panel discussions will be interpreted into English/German and, if needed and possible, into other languages. In the workshops we will try to provide whispered translations. If you have a specific need for translation for individual events, please contact us in advance via the info point at the Climate Camp and we will try to find a translation.

## Successive Program
System Change not Climate Change! 
With every horror story about global climate impacts and every renewed political failure of those in power, this call grows louder. But what does it actually mean? What do we want instead and how do we get there? And most importantly, what does it mean for our practice? We want to address these questions together with exciting speakers. Starting on Monday, August 9, 2021, there will be up to four workshops during the day and a joint discussion with the referees in the evening. The program follows on from each other thematically, but it is also possible to attend only individual program points!


