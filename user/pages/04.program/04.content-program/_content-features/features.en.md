---
title: content-features
body_classes: modular
class: large
features:
    -
        text: '<p style="text-align:justify!important"> It is clear to all of us that not everything is running properly, but what exactly is the problem? We look at society, the state and capitalism and try to find out what exactly is going wrong. <br> <strong> Among others with: </strong> <br> * Sergen Canoglu on green capitalism <br> * Eva von Redecker on critical theory and "Sachherrschaft" <br> * Camila Schmid on colonialism and racism <br> * Riv on multiple discrimination <br> * Alexander Neupert-Doppler on Fossil Capitalism and Utopias. </p>'
        target: blank
        colors: turkoise-bg
        header: 'Criticism (09.-10.08.)'
    -
        text: '<p style="text-align:justify!important"> Some thought that it was impossible to theorize about a liberated society because our thoughts are stuck in the unfree society we come from. Others painted colorful pictures of a society to strive for. We want to dare to ask: System Change, where to? How can we organize society differently? <br> <strong> Among others with: </strong> <br> * Mihir Sharam on "decolonial questions about climate activism"<br> * Lexy with a queer_feminist <br> * Utopia Workshop<br> * Simon Sutterlütti on Caring Commonism</p>'
        target: blank
        colors: turkoise-bg
        header: 'Utopia (11.08.)'
    -
        text: '<p style="text-align:justify!important"> Reform or revolution? With, against or past the state? Now that we have figured out what is wrong and guessed what could be instead, we turn to the big question: How do we get there?  <br> <strong> Among others with: </strong> <br> * Jojo Klick on Commons und social movements <br> * Initiative Demokratischer Konföderalismus on "Revolution in the BRD?!"</p>'
        target: blank
        colors: turkoise-bg
        header: 'Transformation (12.08.)'
    -
        text: '<p style="text-align:justify!important"> Enough talk, now let us do it! The weekend will be about our practice.  We want to discuss ways to be sensitive to discrimination and create spaces for empowerment. It will also be about the interplay of different dimensions of domination that have grown historically. We want to look at how feminist and anti-racist struggles belong together with the struggle for climate justice. Because together we are strong! <br> <strong> Among others with: </strong> Zade Abdullah and Noura Hammouda on <br> * Calais – the disaster in the heart of europe <br> * Mamadou Bah on the intercommunication between BIPoC and the white society. <br> * BUWA collective on classism in the climate movement <br> * Autonomous feminist summer school on body mapping <br> * BIPoC only panel on a decolonial climate justice practice. </p>'
        target: blank
        colors: turkoise-bg
        header: ' Practice - Connecting fights (13.-14.08.)'
    -
        text: '<p style="text-align:justify!important"> On Sunday we want to network, exchange and build alliances. For this purpose, three fishbowl discussions are planned, in which different political groups will tell about their practice, how they see the political situation right now, which important next steps are seen and what they wish from other left emancipatory groups...</p>'
        target: blank
        colors: turkoise-bg
        header: 'Practice - Networking (15.08.)'
---

