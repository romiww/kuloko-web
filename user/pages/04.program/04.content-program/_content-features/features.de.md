---
title: content-features
body_classes: modular
class: large
features:
    -
        text: '<p style="text-align:justify!important"> Dass nicht alles rund läuft ist uns wohl allen klar, aber was ist denn jetzt genau das Problem? Wir schauen uns die Gesellschaft, den Staat und den Kapitalismus an und versuchen herauszufinden, was eigentlich genau falsch läuft. <br> <strong> Unter anderem mit: </strong> <br> * Sergen Canoglu zu Grünem Kapitalismus <br> * Eva von Redecker zu kritischer Theorie und Sachherrschaft <br> * Camila Schmid zu Kolonialismus und Rassismus <br> * Riv zu Mehrfachdiskriminierung <br> * Alexander Neupert-Doppler zu fossilem Kapitalismus und Utopien. </p>'
        target: blank
        colors: turkoise-bg
        header: 'Kritik (09.-10.08.)'
    -
        text: '<p style="text-align:justify!important"> Manche waren der Meinung über eine befreite Gesellschaft könne man nicht theoretisieren, weil unsere Gedanken in der unfreien Gesellschaft aus der wir kommen verhaftet sind. Andere malten bunte Bilder von einer anzustrebenen Gesellschaft. Wir wollen es wagen zu fragen: System Change, wohin? Wie können wir Gesellschaft anders organisieren? <br> <strong> Unter anderem mit: </strong> <br> * Mihir Sharam zu "decolonial questions about climate activism"<br> * Lexy mit einer queer_feministischen <br> * Utopienwerkstatt<br> * Simon Sutterlütti zu Caring Commonism</p>'
        target: blank
        colors: turkoise-bg
        header: 'Utopie (11.08.)'
    -
        text: '<p style="text-align:justify!important"> Reform oder Revolution? Mit, gegen oder am Staat vorbei? Jetzt, wo wir ergründet haben, was falsch läuft, und erahnt haben, was statt dessen sein könnte, widmen wir uns der großen Frage: Wie kommen wir dorthin?  <br> <strong> Unter anderem mit: </strong> <br> * Jojo Klick zu Commons und soziale Bewegungen <br> * Initiative Demokratischer Konföderalismus zu "Revolution inder BRD?!"</p>'
        target: blank
        colors: turkoise-bg
        header: 'Transformation (12.08.)'
    -
        text: '<p style="text-align:justify!important"> Genug geredet, jetzt wird gemacht! Das Wochenende soll rund um unsere Praxis gehen.  Wir wollen Möglichkeiten besprechen, uns diskriminierungssensibel zu verhalten und Räume für Empowerment schaffen. Es wird auch um das Zusammenspiel unterschiedlicher Herrschaftsdimensionen gehen, die historisch gewachsen sind. Wir wollen uns anschauen, wie feministische und antirassistische Kämpfe mit dem um Klimagerechtigkeit zusammengehören. Denn zusammen sind wir stark! <br> <strong> Unter anderem mit: </strong> Zade Abdullah und Noura Hammouda zu <br> * Calais – die Katastrophe im Herzen Europas <br> * Mamadou Bah zur Interkommunikations zwischen BIPoC und der weißen Gesellschaft <br> * BUWA Kollektiv zu Klasssismus in der Klimabewegung <br> * Autonome feministische Sommerschule zu Body Mapping <br> * BIPoC only Panel zu einer dekolonialen Klimagerechtigkeitspraxis </p>'
        target: blank
        colors: turkoise-bg
        header: 'Praxis - Kämpfe Verbinden (13.-14.08.)'
    -
        text: '<p style="text-align:justify!important"> Am Sonntag geht es uns darum, uns zu vernetzen, auszutauschen und Bündnisse zu bilden. Dafür sind drei Fishbowl Diskussionen geplant, in denen unterschiedliche politische Gruppen von ihrer Praxis erzählen, wie sie gerade die politische Lage sehen, welche wichtigen nächsten Schritte gesehen werden und was sie sich von anderen linken emanzipatorischen Gruppen wünschen...</p>'
        target: blank
        colors: turkoise-bg
        header: 'Praxis - Vernetzung (15.08.)'
---

