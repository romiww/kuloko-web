---
title: 'BIPoC Conference'
menu: 'BIPoC Conference'
redirect: /bipoc-conference
hero_title_color: light
hero_background: /user/pages/images/bipoc-conference1.jpg
hero_foreground: /user/pages/images/bar.svg
content:
    items: '@self.modular'
    order:
        custom:
            - _
---

