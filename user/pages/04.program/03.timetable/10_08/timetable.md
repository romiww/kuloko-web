---
timetable:
  afternoon:
  - - category: others
      lineup_dir: alle_dörfer_bleiben_handwerksmarkt
      sorting: '14:00'
      spot: ''
      subtitle: ''
      time_info: 14:00 - 20:00
      title: Alle Dörfer bleiben - Handwerksmarkt
  - - category: others
      lineup_dir: ende_gelände_bochum_ein_reuliges_gesetz_noversgnrw_künstlerische_performance
      sorting: '16:00'
      spot: ''
      subtitle: verfilmte Theaterperformance
      time_info: 16:00 - 16:45
      title: 'Ende Gelände Bochum - Ein Reuliges Gesetz, #NoVersGNRW - künstlerische
        Performance'
    - category: education
      lineup_dir: brand_i_vom_eigentum_an_land_und_wäldern
      sorting: '17:00'
      spot: ''
      subtitle: Anschließend Gespräch zu neokolonialem Klimaschutz mit Filmemacherin
        Susanne Fasbender und Peter Emorinken Donatus
      time_info: 17:00 - 20:00
      title: BRAND I - Vom Eigentum an Land und Wäldern
  - - category: education
      lineup_dir: anarchie_!was_ist_das_
      sorting: '15:00'
      spot: ''
      subtitle: Workshop
      time_info: 15:00 - 16:30
      title: Anarchie?!Was ist das?
    - category: education
      lineup_dir: arts_n'_activism_find_your_voice_(1_3)
      sorting: '15:00'
      spot: ''
      subtitle: Workshopreihe
      time_info: 15:00 - 18:15
      title: Arts N' Activism - Find your Voice (1/3)
    - category: education
      lineup_dir: digitale_selbstverteidigung
      sorting: '16:45'
      spot: ''
      subtitle: IT-Sicherheit für Aktivist:innen
      time_info: 16:45 - 18:15
      title: Digitale Selbstverteidigung
  - - category: others
      lineup_dir: kaffee_kuchen
      sorting: '14:30'
      spot: ''
      subtitle: Literatur und gemütliches Beisammensein
      time_info: 14:30 - 18:00
      title: Kaffee&Kuchen
    - category: education
      lineup_dir: wein_klima
      sorting: '16:00'
      spot: WSZ 04
      subtitle: Eine interaktive Einführung
      time_info: 16:00 - 17:30
      title: Wein & Klima
  - []
  - - category: education
      lineup_dir: kohle_kairós_konkrete_utopie
      sorting: '14:30'
      spot: WSZ 02
      subtitle: Workshop
      time_info: 14:30 - 16:30
      title: Kohle, Kairós, Konkrete Utopie
    - category: education
      lineup_dir: intersektionalität_in_sozialen_kämpfen_umsetzen_
      sorting: '14:30'
      spot: WSZ 01
      subtitle: Ein diskriminierungskritischer Blick auf Klima- und Tierrechtsbewegung
      time_info: 14:30 - 16:30
      title: 'Intersektionalität in sozialen Kämpfen umsetzen:'
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: gesellschaftskritik
      sorting: '17:30'
      spot: Zirkuszelt
      subtitle: Eine Podiumsdiskussion mit einigen Referent*innen des Tages.
      time_info: 17:30 - 19:00
      title: Gesellschaftskritik
  - []
  - []
  evening:
  - - category: others
      lineup_dir: autoren_im_team
      sorting: '19:00'
      spot: ''
      subtitle: Mary Winkens & G. A. Scharff
      time_info: 19:00 - 19:30
      title: Autoren im Team
  - []
  - []
  - - category: education
      lineup_dir: „wind_aus_nord_süd“
      sorting: '20:00'
      spot: WSZ 04
      subtitle: Die Klimakrise im Roman
      time_info: 20:00 - 21:30
      title: „Wind aus Nord-Süd“
  - - category: others
      lineup_dir: florencia_geschichten_überliefern
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Florencia - Geschichten überliefern
  - []
  - - category: others
      lineup_dir: der_andersort
      sorting: '19:00'
      spot: ''
      subtitle: Interaktives Hörstück
      time_info: 19:00 - 20:00
      title: Der Andersort
  - - category: others
      lineup_dir: die_migrationserb_innen_hungry_for_justice
      sorting: '20:00'
      spot: ''
      subtitle: Postmigrantische & Postkoloniale Performance
      time_info: 20:00 - 21:00
      title: Die Migrationserb:innen - Hungry for justice
    - category: music
      lineup_dir: virus_mensch
      sorting: '21:00'
      spot: ''
      subtitle: Punk
      time_info: 21:00 - 21:45
      title: Virus:Mensch
    - category: music
      lineup_dir: katterbach_rohundungeschliffen
      sorting: '22:00'
      spot: ''
      subtitle: ''
      time_info: 22:00 - 23:00
      title: Katterbach rohundungeschliffen
  header:
  - Markt- und Theaterwiese
  - Scheunenkino
  - MaStaMo
  - Schlosswiese
  - Unser aller Wald
  - Klimacamp Rheinland
  - MaWaLü
  - Wiesenbühne
  morning:
  - - category: others
      lineup_dir: feline_przyborowski_kirschgärten
      sorting: 00:00
      spot: ''
      subtitle: Installation frei nach Anton Tschechow
      time_info: ganztägig
      title: Feline Przyborowski - Kirschgärten
  - - category: others
      lineup_dir: daniel_chatard_widerstand_gegen_die_braunkohle_im_rheinland
      sorting: 00:00
      spot: ''
      subtitle: Dokumentarfotografie
      time_info: ganztägig
      title: Daniel Chatard - Widerstand gegen die Braunkohle im Rheinland
  - - category: others
      lineup_dir: yoga
      sorting: 08:00
      spot: Zirkuszelt
      subtitle: ''
      time_info: 08:00 - 08:45
      title: Yoga
    - category: others
      lineup_dir: morgenplenum
      sorting: 09:30
      spot: Zirkuszelt
      subtitle: ''
      time_info: 09:30 - 10:30
      title: Morgenplenum
    - category: education
      lineup_dir: anarchafeminismus
      sorting: '11:45'
      spot: ''
      subtitle: Workshop
      time_info: 11:45 - 13:15
      title: Anarchafeminismus
  - - category: education
      lineup_dir: how_to_do_a_kleingruppenaktion
      sorting: '10:30'
      spot: WSZ 04
      subtitle: Basics Teil 1
      time_info: 10:30 - 12:30
      title: How-To-Do-a-Kleingruppenaktion
    - category: education
      lineup_dir: fahrradtour_zur_kante
      sorting: '11:00'
      spot: ''
      subtitle: 'Mit Infos und Geschichten - Start: Schlosswiese'
      time_info: 11:00 - 12:30
      title: Fahrradtour zur Kante
  - - category: others
      lineup_dir: feldarbeit_in_der_praxis
      sorting: '11:15'
      spot: ''
      subtitle: Gemeinsame Anfahrt zum Feld in MG
      time_info: 11:15 - ?
      title: Feldarbeit in der Praxis
  - - category: education
      lineup_dir: energiecharta_vertrag
      sorting: '10:30'
      spot: WSZ 03
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: Energiecharta-Vertrag
    - category: education
      lineup_dir: kolonialismus_rassismus_
      sorting: '10:30'
      spot: WSZ 01
      subtitle: Und was hat das mit uns zu tun?
      time_info: 10:30 - 12:30
      title: Kolonialismus, Rassismus...
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
    - category: others
      lineup_dir: feldarbeit_in_der_praxis
      sorting: '11:00'
      spot: ''
      subtitle: Gemeinsame Anfahrt zum Feld in MG
      time_info: 11:00 - 16:00
      title: Feldarbeit in der Praxis
  - []
  - []
title: Di 10.08.

---
