---
timetable:
  afternoon:
  - []
  - []
  - - category: others
      lineup_dir: alle_dörfer_bleiben_adb_café
      sorting: '14:00'
      spot: ''
      subtitle: auf dem Winzenhof
      time_info: 14:00 - 18:00
      title: Alle Dörfer bleiben - ADB Café
    - category: music
      lineup_dir: lisa_schlipper_ralf_hintzen
      sorting: '15:00'
      spot: ''
      subtitle: Balladen, Pop, Aktuelles
      time_info: 15:00 - 15:45
      title: Lisa Schlipper & Ralf Hintzen
    - category: music
      lineup_dir: speechless
      sorting: '16:15'
      spot: ''
      subtitle: akustische Cover-Musik
      time_info: 16:15 - 17:00
      title: speechless
    - category: music
      lineup_dir: clara_clasen
      sorting: '17:30'
      spot: ''
      subtitle: Acoustic Rock
      time_info: 17:30 - 18:15
      title: Clara Clasen
  - []
  - - category: education
      lineup_dir: pressetraining
      sorting: '14:30'
      spot: WSZ 01
      subtitle: ABC, Storytelling, Interviews und mehr
      time_info: 14:30 - 16:30
      title: Pressetraining
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: klimagerechtigkeit_und_feminismus
      sorting: '16:00'
      spot: Zirkuszelt
      subtitle: Podiumsdiskussion
      time_info: 16:00 - 17:30
      title: Klimagerechtigkeit und Feminismus
  - - category: others
      lineup_dir: der_andersort
      sorting: '16:00'
      spot: ''
      subtitle: Interaktives Hörstück
      time_info: 16:00 - 17:00
      title: Der Andersort
  - - category: others
      lineup_dir: armin_wühle
      sorting: '15:00'
      spot: ''
      subtitle: ''
      time_info: 15:00 - 16:00
      title: Armin Wühle
    - category: music
      lineup_dir: chom_sound_of_resistance
      sorting: '17:00'
      spot: ''
      subtitle: Akkustik Gitarre & Gesang
      time_info: 17:00 - 18:00
      title: Chom - Sound of Resistance
  evening:
  - []
  - []
  - - category: music
      lineup_dir: beets'n'berries
      sorting: '18:45'
      spot: ''
      subtitle: Akustik-Pop
      time_info: 18:45 - 20:00
      title: beets'n'berries
  - - category: others
      lineup_dir: florencia_geschichten_überliefern
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Florencia - Geschichten überliefern
    - category: education
      lineup_dir: geschichten_über_klimagerechtigkeit
      sorting: '19:30'
      spot: ''
      subtitle: Buchlesung und anschließende Diskussion
      time_info: 19:30 - 21:00
      title: Geschichten über Klimagerechtigkeit
  - []
  - []
  - - category: others
      lineup_dir: horsta_fly_next_to_you
      sorting: '19:30'
      spot: ''
      subtitle: Drag Performance
      time_info: 19:30 - 20:00
      title: Horsta - Fly next to you
    - category: music
      lineup_dir: diva_daneben
      sorting: '20:00'
      spot: ''
      subtitle: Softpunk, Alternative folk | Sophia Mix
      time_info: 20:00 - 21:00
      title: Diva Daneben
    - category: music
      lineup_dir: kollektiv_eigenklang_kuloko_set
      sorting: '22:00'
      spot: ''
      subtitle: ''
      time_info: 22:00 - 01:00
      title: kollektiv eigenklang - kuloko-set
  header:
  - MaStaMo
  - Schlosswiese
  - Hofbühne
  - Unser aller Wald
  - Klimacamp Rheinland
  - MaWaLü
  - Wiesenbühne
  morning:
  - - category: others
      lineup_dir: mastamo_kennenlernplenum
      sorting: 09:30
      spot: Zirkuszelt
      subtitle: ''
      time_info: 09:30 - 11:30
      title: MaStaMo-Kennenlernplenum
  - - category: education
      lineup_dir: mein_dorf_der_zukunft
      sorting: '10:00'
      spot: WSZ 04
      subtitle: Eine Wandelwerkstatt
      time_info: 10:00 - 13:00
      title: Mein Dorf der Zukunft
  - []
  - - category: others
      lineup_dir: kollektiv_eigenklang
      sorting: '10:00'
      spot: ''
      subtitle: INTERACTIVE AUDIOVISUAL EXPERIENCE
      time_info: 10:00 - 17:30
      title: kollektiv eigenklang
  - - category: others
      lineup_dir: julia_pesch_klima_gerechtigkeit_bildung_!
      sorting: '10:30'
      spot: WSZ 01
      subtitle: Wie kann Klimagerechtigkeit in Bildungsräumen aufgegriffen werden?
      time_info: 10:30 - 12:30
      title: Julia Pesch - Klima.Gerechtigkeit.Bildung?!
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: no_border_aktivismus_zwischen_italien_und_frankreich
      sorting: '11:00'
      spot: WSZ 02
      subtitle: Erfahrungsbericht und Gesprächsrunde
      time_info: 11:00 - 12:30
      title: No-Border-Aktivismus zwischen Italien und Frankreich
  - []
  - []
title: So 08.08.

---
