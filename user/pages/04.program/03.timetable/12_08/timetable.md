---
timetable:
  afternoon:
  - - category: music
      lineup_dir: lebenslaute
      sorting: '13:30'
      spot: ''
      subtitle: Klassische Musik, klassische und internationale Lieder
      time_info: 13:30 - 14:30
      title: Lebenslaute Kleinensemble
    - category: others
      lineup_dir: alle_dörfer_bleiben_handwerksmarkt
      sorting: '14:00'
      spot: ''
      subtitle: ''
      time_info: 14:00 - 20:00
      title: Alle Dörfer bleiben - Handwerksmarkt
    - category: music
      lineup_dir: yala
      sorting: '15:00'
      spot: ''
      subtitle: Singer/Songwriter*in
      time_info: 15:00 - 15:45
      title: Yala
    - category: others
      lineup_dir: autoren_im_team
      sorting: '16:00'
      spot: ''
      subtitle: Mary Winkens & G. A. Scharff
      time_info: 16:00 - 16:30
      title: Autoren im Team
  - - category: education
      lineup_dir: still_burning
      sorting: '17:30'
      spot: ''
      subtitle: A web series about the global hardcoal supply chain and resistance
        to it
      time_info: 17:30 - 19:00
      title: Still Burning
  - - category: education
      lineup_dir: arts_n'_activism_find_your_voice_(3_3)
      sorting: '15:00'
      spot: ''
      subtitle: Workshopreihe
      time_info: 15:00 - 18:15
      title: Arts N' Activism - Find your Voice (3/3)
    - category: education
      lineup_dir: erfahrungen_aus_der_danni_besetzung_part_!
      sorting: '15:00'
      spot: ''
      subtitle: Austausch und Diskussion
      time_info: 15:00 - 16:30
      title: Erfahrungen aus der Danni-Besetzung - Part !
    - category: education
      lineup_dir: erfahrungen_aus_der_danni_besetzung_part_ii
      sorting: '16:45'
      spot: ''
      subtitle: Austausch und Diskussion - Part II
      time_info: 16:45 - 18:15
      title: Erfahrungen aus der Danni-Besetzung - Part II
  - - category: education
      lineup_dir: intellektuelle_neonazis_
      sorting: '14:30'
      spot: WSZ 04
      subtitle: Narrative der Neuen Rechten
      time_info: 14:30 - 16:30
      title: Intellektuelle Neonazis?
    - category: education
      lineup_dir: selbstbestimmte_bildung_und_gesellschaftlicher_wandel
      sorting: '16:30'
      spot: WSZ 04
      subtitle: Workshop
      time_info: 16:30 - 18:30
      title: Selbstbestimmte Bildung und gesellschaftlicher Wandel
  - - category: music
      lineup_dir: lebenslaute
      sorting: '13:30'
      spot: ''
      subtitle: Klassische Musik, klassische und internationale Lieder
      time_info: 13:30 - 14:30
      title: Lebenslaute
  - - category: education
      lineup_dir: kämpfende_keimformen
      sorting: '14:30'
      spot: WSZ 01
      subtitle: 'Durch Commons und soziale Bewegungen den Kapitalismus

        aufheben'
      time_info: 14:30 - 16:30
      title: 'Kämpfende Keimformen

        '
    - category: education
      lineup_dir: rassimus_überwinden_nur_ein_traum_
      sorting: '14:30'
      spot: WSZ 02
      subtitle: Workshop
      time_info: 14:30 - 16:30
      title: Rassimus überwinden -  Nur ein Traum?
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: transformation
      sorting: '17:30'
      spot: Zirkuszelt
      subtitle: Eine Podiumsdiskussion mit einigen Referent*innen des Tages.
      time_info: 17:30 - 19:00
      title: Transformation
  - []
  - - category: others
      lineup_dir: klimakollektiv_haltestelle_irrweg
      sorting: '15:00'
      spot: ''
      subtitle: Tanztheater
      time_info: 15:00 - 15:45
      title: Klimakollektiv - Haltestelle Irrweg
  evening:
  - []
  - - category: education
      lineup_dir: die_rote_linie_widerstand_im_hambacher_forst
      sorting: '20:00'
      spot: ''
      subtitle: Anschließend Gespräch mit der Filmemacherin Karin de Miguel
      time_info: 20:00 - 22:00
      title: Die rote Linie - Widerstand im Hambacher Forst
  - []
  - []
  - []
  - - category: education
      lineup_dir: (wen)_wählt_die_klimagerechtigkeit_
      sorting: '20:00'
      spot: Zirkuszelt
      subtitle: Podiumsdiskussion
      time_info: 20:00 - 21:30
      title: (Wen) wählt die Klimagerechtigkeit?
  - - category: music
      lineup_dir: no_chronicles
      sorting: '20:00'
      spot: ''
      subtitle: Riot Jazz punk
      time_info: 20:00 - 21:00
      title: No chronicles
    - category: music
      lineup_dir: fartuuna
      sorting: '21:30'
      spot: ''
      subtitle: Fällt aus!
      time_info: 21:30 - 22:30
      title: Fartuuna
    - category: music
      lineup_dir: yala
      sorting: '21:30'
      spot: ''
      subtitle: Singer/Songwriter*in
      time_info: 21:30 - 22:30
      title: Yala
    - category: music
      lineup_dir: karmakind
      sorting: '23:00'
      spot: ''
      subtitle: Electronic Live Act
      time_info: 23:00 - 00:00
      title: Karmakind
  - []
  header:
  - Markt- und Theaterwiese
  - Scheunenkino
  - MaStaMo
  - Schlosswiese
  - Unser aller Wald
  - Klimacamp Rheinland
  - Wiesenbühne
  - Lützerather Wald
  morning:
  - - category: others
      lineup_dir: apgw_slrhng
      sorting: 00:00
      spot: ''
      subtitle: Solar-Lichtinstallation
      time_info: ganztägig
      title: apgw - slrhng
    - category: others
      lineup_dir: feline_przyborowski_kirschgärten
      sorting: 00:00
      spot: ''
      subtitle: Installation frei nach Anton Tschechow
      time_info: ganztägig
      title: Feline Przyborowski - Kirschgärten
  - - category: others
      lineup_dir: daniel_chatard_widerstand_gegen_die_braunkohle_im_rheinland
      sorting: 00:00
      spot: ''
      subtitle: Dokumentarfotografie
      time_info: ganztägig
      title: Daniel Chatard - Widerstand gegen die Braunkohle im Rheinland
  - - category: others
      lineup_dir: yoga
      sorting: 08:00
      spot: Zirkuszelt
      subtitle: ''
      time_info: 08:00 - 08:45
      title: Yoga
    - category: others
      lineup_dir: morgenplenum
      sorting: 09:30
      spot: Zirkuszelt
      subtitle: ''
      time_info: 09:30 - 10:30
      title: Morgenplenum
  - - category: education
      lineup_dir: kommunikationsguerilla
      sorting: '10:30'
      spot: WSZ 04
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: Kommunikationsguerilla
  - - category: others
      lineup_dir: feldarbeit_in_der_praxis
      sorting: '11:15'
      spot: ''
      subtitle: Gemeinsame Anfahrt zum Feld in MG
      time_info: 11:15 - ?
      title: Feldarbeit in der Praxis
  - - category: education
      lineup_dir: revolution_in_der_brd_!
      sorting: '10:30'
      spot: WSZ 01
      subtitle: Demokratischer Konföderalismus als Lösungansatz
      time_info: 10:30 - 12:30
      title: Revolution in der BRD?!
    - category: education
      lineup_dir: naturromantik_und_zivilisationskritik_in_umweltbewegungen
      sorting: '10:30'
      spot: WSZ 03
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: Naturromantik und Zivilisationskritik in Umweltbewegungen
    - category: education
      lineup_dir: moderationstraining
      sorting: '10:30'
      spot: WSZ 02
      subtitle: 'Moderation ist wichtig für die Revolution! '
      time_info: 10:30 - 12:30
      title: Moderationstraining
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
    - category: others
      lineup_dir: feldarbeit_in_der_praxis
      sorting: '11:00'
      spot: ''
      subtitle: Gemeinsame Anfahrt zum Feld in MG
      time_info: 11:00 - 16:00
      title: Feldarbeit in der Praxis
  - []
  - []
title: Do 12.08.

---
