---
timetable:
  afternoon:
  - - category: others
      lineup_dir: alle_dörfer_bleiben_handwerksmarkt
      sorting: '14:00'
      spot: ''
      subtitle: ''
      time_info: 14:00 - 20:00
      title: Alle Dörfer bleiben - Handwerksmarkt
    - category: music
      lineup_dir: lebenslaute
      sorting: '15:30'
      spot: ''
      subtitle: Klassische Musik, klassische und internationale Lieder
      time_info: 15:30 - 16:30
      title: Lebenslaute Kleinensemble
    - category: others
      lineup_dir: kurt_lehmkuhl
      sorting: '17:00'
      spot: ''
      subtitle: Literatur
      time_info: 17:00 - 18:00
      title: Kurt Lehmkuhl
  - - category: others
      lineup_dir: danni_aktivisti_generation_autokorrektur
      sorting: '17:00'
      spot: ''
      subtitle: Filmvorführung mit Filmgespräch in deutscher Sprache mit englischen
        Untertiteln
      time_info: 17:00 - 19:00
      title: 'Danni Aktivisti - Generation Autokorrektur '
  - - category: education
      lineup_dir: arts_n'_activism_find_your_voice_(2_3)
      sorting: '15:00'
      spot: ''
      subtitle: Workshopreihe
      time_info: 15:00 - 18:15
      title: Arts N' Activism - Find your Voice (2/3)
  - - category: others
      lineup_dir: kaffee_kuchen
      sorting: '14:30'
      spot: ''
      subtitle: Literatur und gemütliches Beisammensein
      time_info: 14:30 - 18:00
      title: Kaffee&Kuchen
    - category: education
      lineup_dir: wie_umgehen_mit_dem_zustand_der_welt_
      sorting: '15:30'
      spot: WSZ 04
      subtitle: Über Hoffnung, Zweifel und wie wir weiter gehen
      time_info: 15:30 - 17:00
      title: Wie umgehen mit dem Zustand der Welt?
    - category: education
      lineup_dir: perspektiven_in_der_politarbeit
      sorting: '17:15'
      spot: ''
      subtitle: Gesprächsrunde
      time_info: 17:15 - 19:00
      title: Perspektiven in der Politarbeit
  - - category: music
      lineup_dir: lebenslaute
      sorting: '13:30'
      spot: ''
      subtitle: Klassische Musik, klassische und internationale Lieder
      time_info: 13:30 - 14:30
      title: Lebenslaute
  - - category: education
      lineup_dir: caring_commonism
      sorting: '14:30'
      spot: WSZ 01
      subtitle: Jenseits des grünen Kapitalismus
      time_info: 14:30 - 16:30
      title: Caring Commonism
    - category: education
      lineup_dir: making_kin_with_strangers_decolonial_questions_about_climate_activism_2
      sorting: '14:30'
      spot: WSZ 01
      subtitle: 'Mit fremden verwandt werden: dekoloniale Fragen zu Klimaaktivismus'
      time_info: 14:30 - 16:30
      title: 'Making kin with strangers: Decolonial questions about climate activism
        2'
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: utopie
      sorting: '17:30'
      spot: Zirkuszelt
      subtitle: Eine Podiumsdiskussion mit einigen Referent*innen des Tages.
      time_info: 17:30 - 19:00
      title: Utopie
  - []
  evening:
  - - category: music
      lineup_dir: klaus_der_geiger_mit_andreas_ottmer
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Klaus der Geiger - mit Andreas Ottmer
  - - category: education
      lineup_dir: homo_communis_–_wir_für_alle
      sorting: '19:30'
      spot: ''
      subtitle: Dokufilm mit anschließendem Gespräch
      time_info: 19:30 - 22:00
      title: HOMO COMMUNIS – WIR FÜR ALLE
  - - category: others
      lineup_dir: danni_gäst_innenhaus_offene_lese_musikbühne
      sorting: '20:00'
      spot: Zirkuszelt
      subtitle: Open Stage
      time_info: 20:00 - 23:00
      title: Danni/Gäst_innenhaus - Offene Lese- & Musikbühne
  - - category: music
      lineup_dir: die_schon_wieder
      sorting: '19:00'
      spot: ''
      subtitle: ''
      time_info: 19:00 - 20:00
      title: Die schon wieder
  - - category: others
      lineup_dir: florencia_geschichten_überliefern
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Florencia - Geschichten überliefern
    - category: others
      lineup_dir: ronan_winter_lesung_und_gespräch_"ökoterroristin"
      sorting: '19:30'
      spot: ''
      subtitle: Ein Jugendroman über Klimaaktivismus von Ronan Winter
      time_info: 19:30 - 20:30
      title: 'Ronan Winter - Lesung und Gespräch: "Ökoterroristin"'
  - []
  - - category: music
      lineup_dir: asundo_suminga
      sorting: '20:00'
      spot: ''
      subtitle: Rap/ Hip-Hop/ African Dancehall
      time_info: 20:00 - 20:40
      title: Asundo Suminga
    - category: music
      lineup_dir: arbeitstitel_tortenschlacht
      sorting: '21:00'
      spot: ''
      subtitle: folk-hop, trafo-kitsch, singer-songwriter*in
      time_info: 21:00 - 22:00
      title: Arbeitstitel Tortenschlacht
    - category: music
      lineup_dir: afra
      sorting: '22:00'
      spot: ''
      subtitle: Soul/Pop
      time_info: 22:00 - 23:00
      title: Afra
  header:
  - Markt- und Theaterwiese
  - Scheunenkino
  - MaStaMo
  - Schlosswiese
  - Unser aller Wald
  - Klimacamp Rheinland
  - Wiesenbühne
  morning:
  - - category: others
      lineup_dir: apgw_slrhng
      sorting: 00:00
      spot: ''
      subtitle: Solar-Lichtinstallation
      time_info: ganztägig
      title: apgw - slrhng
    - category: others
      lineup_dir: feline_przyborowski_kirschgärten
      sorting: 00:00
      spot: ''
      subtitle: Installation frei nach Anton Tschechow
      time_info: ganztägig
      title: Feline Przyborowski - Kirschgärten
  - - category: others
      lineup_dir: daniel_chatard_widerstand_gegen_die_braunkohle_im_rheinland
      sorting: 00:00
      spot: ''
      subtitle: Dokumentarfotografie
      time_info: ganztägig
      title: Daniel Chatard - Widerstand gegen die Braunkohle im Rheinland
  - - category: others
      lineup_dir: yoga
      sorting: 08:00
      spot: Zirkuszelt
      subtitle: ''
      time_info: 08:00 - 08:45
      title: Yoga
    - category: others
      lineup_dir: morgenplenum
      sorting: 09:30
      spot: Zirkuszelt
      subtitle: ''
      time_info: 09:30 - 10:30
      title: Morgenplenum
    - category: education
      lineup_dir: theater_der_unterdrückten
      sorting: '11:00'
      spot: Dachsbau
      subtitle: Theaterworkshop
      time_info: 11:00 - 14:00
      title: Theater der Unterdrückten
    - category: education
      lineup_dir: transformative_and_restorative_justice
      sorting: '11:45'
      spot: ''
      subtitle: 'Alternative Umgänge mit Gewalt, Polizei und Sicherheit #freeElla
        #freeBjörn #FreeThemAll'
      time_info: 11:45 - 13:15
      title: Transformative and Restorative Justice
  - - category: education
      lineup_dir: atomkraft_als_falsche_lösung
      sorting: '10:30'
      spot: WSZ 04
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: 'Atomkraft als falsche Lösung '
  - - category: others
      lineup_dir: unser_aller_wald_klettertraining
      sorting: '10:30'
      spot: ''
      subtitle: Skill-Share
      time_info: 10:30 - 15:00
      title: Unser Aller Wald - Klettertraining
    - category: others
      lineup_dir: feldarbeit_in_der_praxis
      sorting: '11:15'
      spot: ''
      subtitle: Gemeinsame Anfahrt zum Feld in MG
      time_info: 11:15 - ?
      title: Feldarbeit in der Praxis
  - - category: education
      lineup_dir: making_kin_with_strangers_decolonial_questions_about_climate_activism_1
      sorting: '10:30'
      spot: WSZ 01
      subtitle: 'Mit fremden verwandt werden: dekoloniale Fragen zu Klimaaktivismus'
      time_info: 10:30 - 12:30
      title: 'Making kin with strangers: Decolonial questions about climate activism
        1'
    - category: education
      lineup_dir: aktionssmartphones_sicher_benutzen
      sorting: '10:30'
      spot: WSZ 03
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: Aktionssmartphones sicher benutzen
    - category: others
      lineup_dir: sinja_und_helene_vorlesestunde_für_kinder
      sorting: '10:30'
      spot: ''
      subtitle: Geschichten im Kinderzelt - circa ab 8 Jahre
      time_info: 10:30 - 12:30
      title: Sinja und Helene - Vorlesestunde für Kinder
    - category: education
      lineup_dir: queer_feministische_utopiewerkstatt
      sorting: '10:30'
      spot: WSZ 02
      subtitle: Workshop, Austausch
      time_info: 10:30 - 12:30
      title: Queer_feministische Utopiewerkstatt
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
    - category: others
      lineup_dir: feldarbeit_in_der_praxis
      sorting: '11:00'
      spot: ''
      subtitle: Gemeinsame Anfahrt zum Feld in MG
      time_info: 11:00 - 16:00
      title: Feldarbeit in der Praxis
  - []
title: Mi 11.08.

---
