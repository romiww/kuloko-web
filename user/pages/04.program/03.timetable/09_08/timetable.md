---
timetable:
  afternoon:
  - - category: others
      lineup_dir: alle_dörfer_bleiben_handwerksmarkt
      sorting: '14:00'
      spot: ''
      subtitle: ''
      time_info: 14:00 - 20:00
      title: Alle Dörfer bleiben - Handwerksmarkt
  - []
  - - category: education
      lineup_dir: open_street_map
      sorting: '16:45'
      spot: ''
      subtitle: How to?
      time_info: 16:45 - 18:15
      title: Open Street Map
  - - category: others
      lineup_dir: kaffee_kuchen
      sorting: '14:30'
      spot: ''
      subtitle: Literatur und gemütliches Beisammensein
      time_info: 14:30 - 18:00
      title: Kaffee&Kuchen
    - category: education
      lineup_dir: theorien_des_wandels
      sorting: '16:00'
      spot: WSZ 03
      subtitle: Wie Veränderung möglich wird
      time_info: 16:00 - 17:30
      title: Theorien des Wandels
  - []
  - - category: education
      lineup_dir: sachherrschaftskritik
      sorting: '14:30'
      spot: WSZ 01
      subtitle: 'Intersektionaler Materialismus oder die Weigerung, weiterzuzerstören.
        Online Format.

        '
      time_info: 14:30 - 16:30
      title: 'Sachherrschaftskritik

        '
    - category: education
      lineup_dir: kapitalismus_staat_geschlecht_scheiße_!
      sorting: '14:30'
      spot: WSZ 02
      subtitle: Workshop
      time_info: 14:30 - 16:30
      title: Kapitalismus/Staat + Geschlecht = Scheiße?!
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: gesellschaftskritik
      sorting: '17:30'
      spot: Zirkuszelt
      subtitle: Eine Podiumsdiskussion mit einigen Referent*innen des Tages.
      time_info: 17:30 - 19:00
      title: Gesellschaftskritik
  - []
  evening:
  - []
  - - category: others
      lineup_dir: die_kraft_der_schmetterlinge
      sorting: '20:00'
      spot: ''
      subtitle: Dokumentation
      time_info: 20:00 - 21:30
      title: Die Kraft der Schmetterlinge
  - []
  - []
  - - category: others
      lineup_dir: florencia_geschichten_überliefern
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Florencia - Geschichten überliefern
  - []
  - - category: others
      lineup_dir: alle_dörfer_bleiben_aktive_erzählen_geschichten_aus_ihrem_erfahrungsschatz_im_lokalen_widerstand_wie_sie_dazu_gekommen_sind_was_sie_bewegt_wann_sie_sich_erfolgreich_und_wann_frustriert_gefühlt_haben_
      sorting: '20:00'
      spot: ''
      subtitle: ''
      time_info: 20:00 - 22:00
      title: Alle Dörfer bleiben - Aktive erzählen Geschichten aus ihrem Erfahrungsschatz
        im lokalen Widerstand - wie sie dazu gekommen sind, was sie bewegt, wann sie
        sich erfolgreich und wann frustriert gefühlt haben.
  header:
  - Markt- und Theaterwiese
  - Scheunenkino
  - MaStaMo
  - Schlosswiese
  - Unser aller Wald
  - Klimacamp Rheinland
  - MaWaLü
  morning:
  - - category: others
      lineup_dir: feline_przyborowski_kirschgärten
      sorting: 00:00
      spot: ''
      subtitle: Installation frei nach Anton Tschechow
      time_info: ganztägig
      title: Feline Przyborowski - Kirschgärten
  - - category: others
      lineup_dir: daniel_chatard_widerstand_gegen_die_braunkohle_im_rheinland
      sorting: 00:00
      spot: ''
      subtitle: Dokumentarfotografie
      time_info: ganztägig
      title: Daniel Chatard - Widerstand gegen die Braunkohle im Rheinland
  - - category: others
      lineup_dir: yoga
      sorting: 08:00
      spot: Zirkuszelt
      subtitle: ''
      time_info: 08:00 - 08:45
      title: Yoga
    - category: others
      lineup_dir: morgenplenum
      sorting: 09:30
      spot: Zirkuszelt
      subtitle: ''
      time_info: 09:30 - 10:30
      title: Morgenplenum
  - - category: education
      lineup_dir: wie_wir_uns_organisieren_können
      sorting: '10:30'
      spot: WSZ 04
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: Wie wir uns organisieren können
  - []
  - - category: education
      lineup_dir: grüner_kapitalismus
      sorting: '10:30'
      spot: WSZ 01
      subtitle: Antwort auf die Klimakrise?
      time_info: 10:30 - 12:30
      title: Grüner Kapitalismus
    - category: education
      lineup_dir: wie_möchte_man(n)_verhüten_
      sorting: '10:30'
      spot: WSZ 03
      subtitle: Zwischen patriarchaler Realität und feministischen Utopien
      time_info: 10:30 - 12:30
      title: Wie möchte Man(n) verhüten?
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: anarchie_!was_ist_das_
      sorting: '10:30'
      spot: WSZ 02
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: Anarchie?!Was ist das?
  - []
title: Mo 09.08.

---
