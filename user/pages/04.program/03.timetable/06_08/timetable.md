---
timetable:
  afternoon:
  - []
  - - category: others
      lineup_dir: kollektiv_eigenklang
      sorting: '16:00'
      spot: ''
      subtitle: INTERACTIVE AUDIOVISUAL EXPERIENCE
      time_info: 16:00 - ?
      title: kollektiv eigenklang
  - - category: music
      lineup_dir: provinzcrew
      sorting: '17:00'
      spot: ''
      subtitle: (Theater-) Rap
      time_info: 17:00 - 18:30
      title: Provinzcrew
  evening:
  - - category: music
      lineup_dir: katterbach_rohundungeschliffen
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 18:45
      title: Katterbach rohundungeschliffen
    - category: music
      lineup_dir: mascha
      sorting: '19:15'
      spot: ''
      subtitle: Singersongwriter/ Pop Musik
      time_info: 19:15 - 20:00
      title: Mascha
    - category: music
      lineup_dir: rob_sure
      sorting: '20:30'
      spot: ''
      subtitle: Pop
      time_info: 20:30 - 21:30
      title: ROB SURE
    - category: music
      lineup_dir: byggesett_orchestra
      sorting: '22:00'
      spot: ''
      subtitle: Electronic Groove Trance Drone
      time_info: 22:00 - 23:00
      title: Byggesett Orchestra
  - - category: others
      lineup_dir: florencia_geschichten_überliefern
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Florencia - Geschichten überliefern
  - - category: music
      lineup_dir: morpho
      sorting: '19:00'
      spot: ''
      subtitle: Electronic Art Pop
      time_info: 19:00 - 20:30
      title: Morpho
    - category: music
      lineup_dir: finna
      sorting: '21:30'
      spot: ''
      subtitle: queerfeministischer Rap
      time_info: 21:30 - 22:30
      title: Finna
    - category: music
      lineup_dir: mariybu
      sorting: '22:30'
      spot: ''
      subtitle: HipHop
      time_info: 22:30 - 23:30
      title: Mariybu
    - category: music
      lineup_dir: intaktogene
      sorting: '23:30'
      spot: ''
      subtitle: Melodic Techno/Progressive House
      time_info: 23:30 - 01:30
      title: Intaktogene
    - category: music
      lineup_dir: intaktogene
      sorting: '23:30'
      spot: ''
      subtitle: Melodic Techno/Progressive House
      time_info: 23:30 - 01:30
      title: Intaktogene
  header:
  - Hofbühne
  - Unser aller Wald
  - Wiesenbühne
  morning:
  - []
  - []
  - []
title: Fr 06.08.

---
