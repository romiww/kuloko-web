---
timetable:
  afternoon:
  - - category: music
      lineup_dir: jooles_nick
      sorting: '16:00'
      spot: ''
      subtitle: Singer-Songwriter
      time_info: 16:00 - 16:45
      title: Jooles & Nick
    - category: music
      lineup_dir: stefan_pieck
      sorting: '17:15'
      spot: ''
      subtitle: Singer Songwriter Punk
      time_info: 17:15 - 18:00
      title: Stefan Pieck
  - []
  - - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
  - - category: others
      lineup_dir: theaterwerkstatt_bethel_die_welt_hochwerfen
      sorting: '16:30'
      spot: ''
      subtitle: ''
      time_info: 16:30 - 17:00
      title: Theaterwerkstatt Bethel - DIE WELT HOCHWERFEN
  - - category: music
      lineup_dir: findus
      sorting: '15:30'
      spot: ''
      subtitle: ''
      time_info: 15:30 - 16:30
      title: Findus
    - category: music
      lineup_dir: the_punkelelics
      sorting: '17:00'
      spot: ''
      subtitle: Ukulelismus total!
      time_info: 17:00 - 18:00
      title: The Punkelelics
  evening:
  - - category: music
      lineup_dir: julie_and_me
      sorting: '18:30'
      spot: ''
      subtitle: Akustische Zwei-Klang-Band
      time_info: 18:30 - 19:15
      title: Julie and me
    - category: music
      lineup_dir: jësse_und_diwi
      sorting: '22:00'
      spot: ''
      subtitle: 80er und 90er music
      time_info: 22:00 - 23:00
      title: Jësse und Diwi
  - - category: others
      lineup_dir: florencia_geschichten_überliefern
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Florencia - Geschichten überliefern
  - []
  - - category: others
      lineup_dir: wild_roots_performance_collective_klimaaktivistische_theaterperformance
      sorting: '18:30'
      spot: ''
      subtitle: ''
      time_info: 18:30 - 19:30
      title: Wild Roots Performance Collective - Klimaaktivistische Theaterperformance
  - - category: music
      lineup_dir: ezé_wendtoin
      sorting: '21:00'
      spot: ''
      subtitle: Chanson, Afro-Folk, Latino & westafrikanische Rhythmen
      time_info: 21:00 - 22:00
      title: Ezé Wendtoin
    - category: music
      lineup_dir: boringpain
      sorting: '23:00'
      spot: ''
      subtitle: Trash 'n Bass
      time_info: 23:00 - 01:00
      title: BoringPain
    - category: music
      lineup_dir: sonori_b2b_luna_x_tatsachen_jane
      sorting: '25:00'
      spot: ''
      subtitle: Technoider Latinx Queer Disco Pop & HipHop
      time_info: 01:00 - 03:00
      title: Sonori b2b Luna X // Tatsachen & Jane
  header:
  - Hofbühne
  - Unser aller Wald
  - Klimacamp Rheinland
  - MaWaLü
  - Wiesenbühne
  morning:
  - []
  - - category: others
      lineup_dir: kollektiv_eigenklang
      sorting: 00:00
      spot: ''
      subtitle: INTERACTIVE AUDIOVISUAL EXPERIENCE
      time_info: ganztägig
      title: kollektiv eigenklang
  - - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
  - []
  - []
title: Sa 07.08.

---
