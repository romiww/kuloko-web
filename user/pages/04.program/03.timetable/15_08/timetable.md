---
timetable:
  afternoon:
  - - category: music
      lineup_dir: föld
      sorting: '17:30'
      spot: ''
      subtitle: Experimental Ambient Psychedelic
      time_info: 17:30 - 18:00
      title: Föld
  - []
  - []
  - - category: others
      lineup_dir: None
      sorting: '13:30'
      spot: FLINTA* Zelt
      subtitle: ''
      time_info: 13:30 - ?
      title: Flinta Vernetzung
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: der_ea_rheinland_stellt_sich_vor
      sorting: '16:30'
      spot: WSZ 01
      subtitle: ''
      time_info: 16:30 - 17:00
      title: Der EA Rheinland stellt sich vor
  - []
  - - category: music
      lineup_dir: bonbonlegerin
      sorting: '15:00'
      spot: ''
      subtitle: Folk Punk
      time_info: 15:00 - 16:00
      title: Bonbonlegerin
    - category: music
      lineup_dir: johanna_zeul
      sorting: '17:00'
      spot: ''
      subtitle: wild crazy singer songwriter
      time_info: 17:00 - 18:00
      title: Johanna Zeul
  evening:
  - - category: others
      lineup_dir: theaterkollektiv_der_hs_niederrhein
      sorting: '18:30'
      spot: ''
      subtitle: How to kill your Planet slowly
      time_info: 18:30 - 19:30
      title: Theaterkollektiv der HS Niederrhein
  - []
  - []
  - - category: others
      lineup_dir: lady_kitty´s_hell´s_belles
      sorting: '21:00'
      spot: Zirkuszelt
      subtitle: die Schönheiten des Ödlands
      time_info: 21:00 - 22:00
      title: Lady Kitty´s Hell´s Belles
  - []
  - - category: music
      lineup_dir: rap!fugees
      sorting: '19:00'
      spot: ''
      subtitle: HipHop
      time_info: 19:00 - 20:00
      title: Rap!fugees
    - category: music
      lineup_dir: seasoul
      sorting: '23:00'
      spot: ''
      subtitle: Dream Pop
      time_info: 23:00 - 00:00
      title: Seasoul
  header:
  - Markt- und Theaterwiese
  - Scheunenkino
  - MaStaMo
  - Klimacamp Rheinland
  - MaWaLü
  - Wiesenbühne
  morning:
  - - category: others
      lineup_dir: apgw_slrhng
      sorting: 00:00
      spot: ''
      subtitle: Solar-Lichtinstallation
      time_info: ganztägig
      title: apgw - slrhng
    - category: others
      lineup_dir: feline_przyborowski_kirschgärten
      sorting: 00:00
      spot: ''
      subtitle: Installation frei nach Anton Tschechow
      time_info: ganztägig
      title: Feline Przyborowski - Kirschgärten
  - - category: others
      lineup_dir: daniel_chatard_widerstand_gegen_die_braunkohle_im_rheinland
      sorting: 00:00
      spot: ''
      subtitle: Dokumentarfotografie
      time_info: ganztägig
      title: Daniel Chatard - Widerstand gegen die Braunkohle im Rheinland
  - - category: others
      lineup_dir: yoga
      sorting: 08:00
      spot: Zirkuszelt
      subtitle: ''
      time_info: 08:00 - 08:45
      title: Yoga
    - category: others
      lineup_dir: mastamo_abschlussplenum
      sorting: 09:30
      spot: Zirkuszelt
      subtitle: ''
      time_info: 09:30 - 10:30
      title: MaStaMo-Abschlussplenum
  - - category: others
      lineup_dir: bildungskollektiv_bonn_utopiemaschine
      sorting: 00:00
      spot: ''
      subtitle: Eine künstlerisch-kreative Intervention für Klein und Groß
      time_info: ganztägig
      title: Bildungskollektiv Bonn - Utopiemaschine
    - category: education
      lineup_dir: zad_rheinland
      sorting: '10:30'
      spot: Treffen am Infopunkt
      subtitle: Hüttenbauworkshop
      time_info: 10:30 - 16:30
      title: ZAD Rheinland
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
  - - category: others
      lineup_dir: dorfspaziergang
      sorting: '11:30'
      spot: ''
      subtitle: Demo
      time_info: 11:30 - 15:00
      title: Dorfspaziergang
  - []
title: So 15.08.

---
