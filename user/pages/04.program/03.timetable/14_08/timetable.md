---
timetable:
  afternoon:
  - - category: others
      lineup_dir: militanz
      sorting: '16:00'
      spot: ''
      subtitle: antikapitalistische Mitmachperformance
      time_info: 16:00 - 16:30
      title: Militanz
    - category: music
      lineup_dir: heart_core
      sorting: '16:30'
      spot: ''
      subtitle: A cappella Chormusik
      time_info: 16:30 - 17:30
      title: Heart Core
  - []
  - - category: others
      lineup_dir: mastamo_vernetzung_ausblick_andockmöglichkeiten
      sorting: '15:00'
      spot: ''
      subtitle: Vernetzung
      time_info: 15:00 - 16:30
      title: MaStaMo - Vernetzung, Ausblick, Andockmöglichkeiten
    - category: education
      lineup_dir: links_leben_mit_kindern_zwischen_utopie_und_realität_
      sorting: '16:45'
      spot: ''
      subtitle: Austauschraum für Hauptbezugspersonen von Kindern
      time_info: 16:45 - 18:15
      title: Links leben mit Kindern. Zwischen Utopie und Realität.
  - - category: education
      lineup_dir: einstieg_in_gender_und_feminismus
      sorting: '14:30'
      spot: WSZ 04
      subtitle: Workshop
      time_info: 14:30 - 16:30
      title: Einstieg in Gender und Feminismus
    - category: education
      lineup_dir: klimagerechtigkeit
      sorting: '16:30'
      spot: WSZ 04
      subtitle: Eine Einführung
      time_info: 16:30 - 18:30
      title: Klimagerechtigkeit
  - - category: others
      lineup_dir: ponyreiten
      sorting: '15:00'
      spot: ''
      subtitle: In Keyenberg
      time_info: 15:00 - 18:00
      title: Ponyreiten
  - - category: others
      lineup_dir: None
      sorting: '13:30'
      spot: FLINTA* Zelt
      subtitle: ''
      time_info: 13:30 - ?
      title: Flinta Vernetzung
    - category: education
      lineup_dir: intersectionality_through_movement
      sorting: '14:30'
      spot: WSZ 01
      subtitle: ''
      time_info: 14:30 - 16:30
      title: Intersectionality through movement
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: kämpfe_verbinden
      sorting: '17:30'
      spot: Zirkuszelt
      subtitle: BIPoC only panel
      time_info: 17:30 - 19:00
      title: Kämpfe verbinden
  - - category: others
      lineup_dir: bazon_brock_stop_the_beast!
      sorting: '16:00'
      spot: ''
      subtitle: Gemäldeenthüllung
      time_info: 16:00 - 17:00
      title: Bazon Brock - Stop the Beast!
  - - category: music
      lineup_dir: daizen
      sorting: '13:00'
      spot: ''
      subtitle: Psychedelische Weltmusik
      time_info: 13:00 - 14:00
      title: Daizen
    - category: others
      lineup_dir: theaterkollektiv_der_hs_niederrhein
      sorting: '15:00'
      spot: ''
      subtitle: How to kill your Planet slowly
      time_info: 15:00 - 16:00
      title: Theaterkollektiv der HS Niederrhein
  evening:
  - - category: music
      lineup_dir: gerd_schinkel
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Gerd Schinkel
    - category: others
      lineup_dir: riadh_ben_ammar_die_falle
      sorting: '21:00'
      spot: ''
      subtitle: ''
      time_info: 21:00 - 22:00
      title: Riadh Ben Ammar - Die Falle
  - - category: education
      lineup_dir: die_erreichte_autonomie_das_leben_der_parteianhänger*innen_–_el_pilar_de_la_autonomia_y_la_vida_
      sorting: '18:00'
      spot: ''
      subtitle: Film/Doku
      time_info: 18:00 - 18:30
      title: Die erreichte Autonomie & das Leben der Parteianhänger*innen – El Pilar
        de la Autonomia y La Vida...
    - category: education
      lineup_dir: zapatistas_das_recht_glücklich_zu_sein
      sorting: '18:30'
      spot: ''
      subtitle: Die Kämpfe der zapatistischen Frauen in Chiapas/Mexiko
      time_info: 18:30 - 20:00
      title: 'Zapatistas - Das Recht glücklich zu sein '
    - category: education
      lineup_dir: corazón_del_tiempo
      sorting: '20:00'
      spot: ''
      subtitle: Spielfilm von Alberto Cortés
      time_info: 20:00 - 21:45
      title: Corazón del tiempo
    - category: others
      lineup_dir: corazon_del tiempo
      sorting: '20:00'
      spot: ''
      subtitle: Spielfilm
      time_info: 20:00 - 22:00
      title: corazon del tiempo
  - []
  - []
  - []
  - []
  - - category: music
      lineup_dir: t_r_anzgeschichten_inge_hat_eine_leiter
      sorting: '18:00'
      spot: ''
      subtitle: Queerfeministische installative Soundperformance & Tanzveranstaltung
      time_info: 18:00 - 20:30
      title: T_r_anzgeschichten - Inge hat eine Leiter
  - - category: music
      lineup_dir: kapa_tult
      sorting: '18:00'
      spot: ''
      subtitle: Indie-Pop
      time_info: 18:00 - 19:00
      title: Kapa Tult
    - category: music
      lineup_dir: microphone_mafia
      sorting: '19:30'
      spot: ''
      subtitle: Hip-Hop
      time_info: 19:30 - 21:00
      title: Microphone Mafia
    - category: music
      lineup_dir: faulenza
      sorting: '22:00'
      spot: ''
      subtitle: Trans*female Rap
      time_info: 22:00 - 23:00
      title: FaulenzA
    - category: music
      lineup_dir: pexa
      sorting: '23:30'
      spot: ''
      subtitle: QueerRap, Hip Hop, Trap
      time_info: 23:30 - 01:30
      title: Pexa
    - category: music
      lineup_dir: pexa
      sorting: '23:30'
      spot: ''
      subtitle: QueerRap, Hip Hop, Trap
      time_info: 23:30 - 01:30
      title: Pexa
    - category: music
      lineup_dir: be_là
      sorting: '25:00'
      spot: ''
      subtitle: Techno
      time_info: 01:30 - 03:30
      title: be là
  header:
  - Markt- und Theaterwiese
  - Scheunenkino
  - MaStaMo
  - Schlosswiese
  - MaWaKey
  - Klimacamp Rheinland
  - MaWaLü
  - Wiesenbühne
  morning:
  - - category: others
      lineup_dir: apgw_slrhng
      sorting: 00:00
      spot: ''
      subtitle: Solar-Lichtinstallation
      time_info: ganztägig
      title: apgw - slrhng
    - category: others
      lineup_dir: feline_przyborowski_kirschgärten
      sorting: 00:00
      spot: ''
      subtitle: Installation frei nach Anton Tschechow
      time_info: ganztägig
      title: Feline Przyborowski - Kirschgärten
    - category: others
      lineup_dir: militanz_tanzworkshop
      sorting: '11:00'
      spot: ''
      subtitle: Workshop
      time_info: 11:00 - 14:00
      title: MiliTanz - Tanzworkshop
  - - category: others
      lineup_dir: daniel_chatard_widerstand_gegen_die_braunkohle_im_rheinland
      sorting: 00:00
      spot: ''
      subtitle: Dokumentarfotografie
      time_info: ganztägig
      title: Daniel Chatard - Widerstand gegen die Braunkohle im Rheinland
  - - category: others
      lineup_dir: yoga
      sorting: 08:00
      spot: Zirkuszelt
      subtitle: ''
      time_info: 08:00 - 08:45
      title: Yoga
    - category: others
      lineup_dir: morgenplenum
      sorting: 09:30
      spot: Zirkuszelt
      subtitle: ''
      time_info: 09:30 - 10:30
      title: Morgenplenum
    - category: others
      lineup_dir: phylipien_einführung_in_die_nähmaschine
      sorting: '10:30'
      spot: Meisennest
      subtitle: Skillshare
      time_info: 10:30 - 11:30
      title: Phylipien - Einführung in die Nähmaschine
    - category: others
      lineup_dir: julia_lesung_aus_einem_utopischen_roman
      sorting: '11:45'
      spot: ''
      subtitle: Lesung
      time_info: 11:45 - 13:15
      title: Julia - Lesung aus einem utopischen Roman
  - - category: education
      lineup_dir: skillshare_öffentlichkeitsarbeit
      sorting: '10:30'
      spot: WSZ 04
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: Skillshare Öffentlichkeitsarbeit
  - []
  - - category: education
      lineup_dir: interkommunikation_zwischen_bipoc_und_der_weißen_gesellschaft
      sorting: 00:00
      spot: WSZ 02
      subtitle: 'Communication between Bipoc and the white (dominated) society '
      time_info: '? - 16:30'
      title: Interkommunikation zwischen BIPoC und der weißen Gesellschaft
    - category: others
      lineup_dir: bildungskollektiv_bonn_utopiemaschine
      sorting: 00:00
      spot: ''
      subtitle: Eine künstlerisch-kreative Intervention für Klein und Groß
      time_info: ganztägig
      title: Bildungskollektiv Bonn - Utopiemaschine
    - category: education
      lineup_dir: nachhaltiger_konsum_geht_das_überhaubt_!
      sorting: '10:30'
      spot: WSZ 03
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: Nachhaltiger Konsum - Geht das überhaubt?!
    - category: education
      lineup_dir: calais_die_katastrophe_im_herzen_europas
      sorting: '10:30'
      spot: WSZ 01
      subtitle: Partizipativer Vortrag und Workshop
      time_info: 10:30 - 12:30
      title: Calais - Die Katastrophe im Herzen Europas
    - category: education
      lineup_dir: transformative_konfliktbearbeitung_straflogiken_überwinden
      sorting: '10:30'
      spot: WSZ 02
      subtitle: ''
      time_info: 10:30 - 12:30
      title: Transformative Konfliktbearbeitung-Straflogiken überwinden
    - category: education
      lineup_dir: zad_rheinland
      sorting: '10:30'
      spot: Treffen am Infopunkt
      subtitle: Hüttenbauworkshop
      time_info: 10:30 - 16:30
      title: ZAD Rheinland
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
  - - category: others
      lineup_dir: gemeinsame_anreise_"defend_kurdistan_demo"
      sorting: '10:00'
      spot: ''
      subtitle: ''
      time_info: 10:00 - 18:00
      title: Gemeinsame Anreise "Defend Kurdistan Demo"
  - []
title: Sa 14.08.

---
