---
timetable:
  afternoon:
  - - category: music
      lineup_dir: pappnasen_rotschwarz
      sorting: '17:00'
      spot: ''
      subtitle: ''
      time_info: 17:00 - 17:30
      title: Pappnasen Rotschwarz
  - - category: education
      lineup_dir: represente
      sorting: '17:00'
      spot: ''
      subtitle: Anschließend ein Filmgespräch mit Dario Farcy
      time_info: 17:00 - 19:00
      title: RePresente
  - - category: education
      lineup_dir: flugreisen
      sorting: '15:00'
      spot: ''
      subtitle: Austausch
      time_info: 15:00 - 16:30
      title: Flugreisen
    - category: education
      lineup_dir: child_free_living
      sorting: '16:45'
      spot: ''
      subtitle: Workshop
      time_info: 16:45 - 18:15
      title: Child- free living
  - - category: education
      lineup_dir: ländliche_utopien_2
      sorting: '14:30'
      spot: WSZ 04
      subtitle: Wie wird eine Vision vom guten Landleben für alle real?
      time_info: 14:30 - 16:30
      title: Ländliche Utopien 2
  - []
  - - category: others
      lineup_dir: None
      sorting: '13:30'
      spot: FLINTA* Zelt
      subtitle: ''
      time_info: 13:30 - ?
      title: Flinta Vernetzung
    - category: education
      lineup_dir: klimabewegung_und_die_koloniale_kontinuität_(digital)
      sorting: '14:00'
      spot: WSZ 02
      subtitle: ''
      time_info: 14:00 - 17:00
      title: Klimabewegung und die koloniale Kontinuität (digital)
    - category: education
      lineup_dir: klassismus_in_der_klimabewegung
      sorting: '14:30'
      spot: WSZ 03
      subtitle: Workshop
      time_info: 14:30 - 16:30
      title: Klassismus in der Klimabewegung
    - category: education
      lineup_dir: kritische_männlichkeit*en
      sorting: '14:30'
      spot: WSZ 01
      subtitle: Die Lust am Zweifeln
      time_info: 14:30 - 16:30
      title: Kritische Männlichkeit*en
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '14:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 14:30 - 16:30
      title: Workshops im Open Space
    - category: education
      lineup_dir: how_to_make_our_actvist_places_more_inclusive_
      sorting: '17:30'
      spot: Zirkuszelt
      subtitle: A decolonial, feminist perspective  - with Paulina Trejo Mendez
      time_info: 17:30 - 19:00
      title: How to make our actvist places more inclusive?
  - - category: others
      lineup_dir: theaterkollektiv_der_hs_niederrhein
      sorting: '13:00'
      spot: ''
      subtitle: How to kill your Planet slowly
      time_info: 13:00 - 14:00
      title: Theaterkollektiv der HS Niederrhein
    - category: music
      lineup_dir: describing_unity
      sorting: '14:00'
      spot: ''
      subtitle: synth folk punk
      time_info: 14:00 - 15:00
      title: describing unity
    - category: music
      lineup_dir: pogendroblem
      sorting: '16:00'
      spot: ''
      subtitle: punk
      time_info: 16:00 - 17:00
      title: pogendroblem
  - - category: others
      lineup_dir: klimakollektiv_haltestelle_irrweg
      sorting: '15:00'
      spot: ''
      subtitle: Tanztheater
      time_info: 15:00 - 15:45
      title: Klimakollektiv - Haltestelle Irrweg
  evening:
  - - category: music
      lineup_dir: shuffle
      sorting: '18:00'
      spot: ''
      subtitle: ''
      time_info: 18:00 - 19:00
      title: Shuffle
    - category: music
      lineup_dir: piece_of_peace
      sorting: '20:30'
      spot: ''
      subtitle: Vocal Ensemble
      time_info: 20:30 - 21:30
      title: Piece of Peace
  - - category: education
      lineup_dir: at_the_margin
      sorting: '20:00'
      spot: ''
      subtitle: Anschließend Gespräch mit den Filmemacher*innen Sara Hüther & Sita
        Scherer
      time_info: 20:00 - 22:00
      title: At the margin
  - []
  - []
  - []
  - - category: music
      lineup_dir: lebenslaute
      sorting: '19:00'
      spot: ''
      subtitle: Klassische Musik | Essen & Plaudern
      time_info: 19:00 - 22:00
      title: Lebenslaute - auf Eckarts Hof neben dem KCR
  - - category: music
      lineup_dir: herringedeck
      sorting: '18:00'
      spot: ''
      subtitle: Deutschpunk
      time_info: 18:00 - 19:00
      title: HerrinGedeck
    - category: music
      lineup_dir: kollektiv_abschaum
      sorting: '21:00'
      spot: ''
      subtitle: Pop-Punk
      time_info: 21:00 - 22:30
      title: Kollektiv Abschaum
    - category: music
      lineup_dir: missratene_töchter
      sorting: '22:30'
      spot: ''
      subtitle: Anarchaschlager
      time_info: 22:30 - 23:30
      title: missratene Töchter
    - category: music
      lineup_dir: ami
      sorting: '23:30'
      spot: ''
      subtitle: Dark-Techno
      time_info: 23:30 - 01:00
      title: Ami
  - - category: others
      lineup_dir: klimakollektiv_haltestelle_irrweg
      sorting: '19:00'
      spot: ''
      subtitle: Tanztheater
      time_info: 19:00 - 19:30
      title: Klimakollektiv - Haltestelle Irrweg
  header:
  - Markt- und Theaterwiese
  - Scheunenkino
  - MaStaMo
  - Schlosswiese
  - Unser aller Wald
  - Klimacamp Rheinland
  - Wiesenbühne
  - Lützerather Wald
  morning:
  - - category: others
      lineup_dir: apgw_slrhng
      sorting: 00:00
      spot: ''
      subtitle: Solar-Lichtinstallation
      time_info: ganztägig
      title: apgw - slrhng
    - category: others
      lineup_dir: feline_przyborowski_kirschgärten
      sorting: 00:00
      spot: ''
      subtitle: Installation frei nach Anton Tschechow
      time_info: ganztägig
      title: Feline Przyborowski - Kirschgärten
  - - category: others
      lineup_dir: daniel_chatard_widerstand_gegen_die_braunkohle_im_rheinland
      sorting: 00:00
      spot: ''
      subtitle: Dokumentarfotografie
      time_info: ganztägig
      title: Daniel Chatard - Widerstand gegen die Braunkohle im Rheinland
  - - category: others
      lineup_dir: None
      sorting: 05:30
      spot: Treffpunkt Infopoint
      subtitle: Fahrradtour
      time_info: 05:30 - ?
      title: Simon - Radtour zum Morgengrauen
    - category: others
      lineup_dir: yoga
      sorting: 08:00
      spot: Zirkuszelt
      subtitle: ''
      time_info: 08:00 - 08:45
      title: Yoga
    - category: others
      lineup_dir: morgenplenum
      sorting: 09:30
      spot: Zirkuszelt
      subtitle: ''
      time_info: 09:30 - 10:30
      title: Morgenplenum
    - category: education
      lineup_dir: akt_stehen_eine_feministische_perspektive
      sorting: '11:00'
      spot: FLINTA* Zelt
      subtitle: Workshop, Kunst
      time_info: 11:00 - 12:30
      title: Akt stehen - eine feministische Perspektive
  - - category: education
      lineup_dir: ländliche_utopien_1
      sorting: '10:00'
      spot: WSZ 04
      subtitle: (De-)konstruktion von Dörflichkeit
      time_info: 10:00 - 12:30
      title: Ländliche Utopien 1
  - - category: others
      lineup_dir: feldarbeit_in_der_praxis
      sorting: '11:15'
      spot: ''
      subtitle: Gemeinsame Anfahrt zum Feld in MG
      time_info: 11:15 - ?
      title: Feldarbeit in der Praxis
  - - category: education
      lineup_dir: body_mapping
      sorting: 09:30
      spot: WSZ 01
      subtitle: FLINTA*only
      time_info: 09:30 - 13:00
      title: body mapping
    - category: education
      lineup_dir: howto_lockon's_benutzen_sich_fest_ketten
      sorting: '10:30'
      spot: WSZ 03
      subtitle: Workshop
      time_info: 10:30 - 12:30
      title: HowTo LockOn's benutzen / sich fest ketten
    - category: education
      lineup_dir: workshops_im_open_space
      sorting: '10:30'
      spot: ''
      subtitle: Mehr Infos vor Ort
      time_info: 10:30 - 12:30
      title: Workshops im Open Space
    - category: others
      lineup_dir: None
      sorting: '10:30'
      spot: vor dem Awareness-Zelt
      subtitle: ''
      time_info: 10:30 - 13:00
      title: weiterer Austausch zu kultureller Aneignung
    - category: others
      lineup_dir: feldarbeit_in_der_praxis
      sorting: '11:00'
      spot: ''
      subtitle: Gemeinsame Anfahrt zum Feld in MG
      time_info: 11:00 - 16:00
      title: Feldarbeit in der Praxis
  - []
  - []
title: Fr 13.08.

---
