---
category: education
ext_url: ''
id: Alexander_Neupert_Doppler
name: Alexander Neupert-Doppler
place: Klimacamp Rheinland | WSZ 02
sample: ''
subtitle: Workshop
summary_text: ''
taxonomy:
  tag:
  - utopie
  - klimagerechtigkeit
  - workshop
time_info: Di 10.08. | 14:30 - 16:30
title: Kohle, Kairós, Konkrete Utopie

---

Schon 1957 warnten die Ozeanographen Roger Revelle und Hans E. Suess
vor der Erderhitzung durch CO2. Warum ist in den vergangenen 64 Jahren
so wenig dagegen unternommen worden? Im Workshop werden zwei Thesen
dazu zur Diskussion gestellt: Erstens hängen viele Menschen nach wie vor
der Vorstellung eines chronlogischen Fortschritts an. Auch die Grünen setzen
z.B. in ihrem aktuellen Wahlkampfprogramm darauf, dass technische Fortschritte
in der E-Automobilität dazu führen werden, dass "das Auto der Zukunft weiter
in Deutschland entwickelt und produziert wird" (S. 19). Was fehlt ist eine Vorstellung
von Veränderung, die nicht auf bloßen Fortschritt, sondern Gelegenheiten (nach dem
griechischen Gott der Gelegenheiten: Kairós) zur Umkehr setzt.
Zweitens fehlen konkrete Utopien, was denn an die Stelle des fossilen Kapitalismus treten
soll. Darum beschränkt sich Klimapolitik heutzutage auf die Besteuerung des Konsums,
statt die Produktionsweise der 100 Unternehmen in Frage zu stellen, die weltweit für
70% des CO2-Ausstoßes verantwortlich sind. Wie könnte eine kairologische und utopische
Strategie der Bewegung für Klimagerechtigkeit aussehen? Darüber wird im Workshop beraten.

Dr. Alexander Neupert-Doppler ist Philosoph und Politikwissenschaftler.
Er veröffentliche Bücher zu den Widrigkeiten des 'Staatsfetischismus' (2013),
den Möglichkeiten der 'Utopie' (2015), Gelegenheiten im 'Kairós' (2019) und
der Notwendigkeit von 'Organisation' (2021). Im Wintersemester 2021/2022
vertritt er die Professur für Sozialphilosophie an der Hochschule Düsseldorf.
