---
category: education
ext_url: ''
id: Nisha
name: Nisha
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: ABC, Storytelling, Interviews und mehr
summary_text: ''
taxonomy:
  tag:
  - aktivismus
  - kommunikation
time_info: So 08.08. | 14:30 - 16:30
title: Pressetraining

---

Du organisierst dich in der Klimabewegung und willst wissen, wie du (mehr) Öffentlichkeit für Aktionen schaffen kannst? Du wolltest schon immer mal wissen, wie so ein Interview geht und was die Technik abc eigentlich bedeutet? Du interessierst dich dafür, unsere Geschichten von Klimagerechtigkeit und mehr zu erzählen?
Dann komm zum Pressetraining! Zusammen werden wir uns anschauen, was gute Stories ausmachen, wie wir sie erzählen können und in Interviews überzeugen. Das machen wir in einer Mischung aus Input, Brainstorm, praktischer Übung und Austausch. Danach hast du ein paar Werkzeuge an der Hand, um in eine empowernde Pressearbeit in (d)einem Projekt zu starten.
