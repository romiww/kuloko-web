---
category: education
ext_url: ''
id: Autonome_feministische_Sommerschule
name: Autonome feministische Sommerschule
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: FLINTA*only
summary_text: ''
taxonomy:
  tag:
  - feminismus
time_info: Fr 13.08. | 09:30 - 13:00
title: body mapping

---

We propose a workshop on body mapping. This is to explore the body- territory as we move and live in it. It is to map our collective body-territory of resistance against climate change and multiple systems of oppressions. We are talking about a territory that is not bound by national borders or imaginary frontiers imposed by systems of patriarchal, racial and colonial domination, but by our shared spaces, wounds, imaginations, hopes, desires, strengths and struggles as bodily experience. This methodology was developed by the Latin American collective Miradas Críticas del Territorio desde el Feminismo and became the center of an autonomous feminist summer school in 2019. From that experience we worked on a fanzine that brings our collective reflections on the mapping exercise, ideas, and methodological implications. After a long process we have finalized the bilingual fanzine (Spanish – German) and would like to share the methodology, reflections, and printed work with those who attend the workshop. In the workshop we will introduce the work we have collectively done, and the methodology used. Afterwards, we will do an exercise to explore our bodily sensations and memory and then create collective body-territory-maps. We will invite participants to share their map(s). This will be followed by a circle of reflection. This is where people can share their own insights regarding the methodology. Finally, we will share the fanzine with everyone. Max. 12 participants, open for women, lesbians, inter, trans, non-binary, agender, gender dissident,… people (all gender except cis men)
