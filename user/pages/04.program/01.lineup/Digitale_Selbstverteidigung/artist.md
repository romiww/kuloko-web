---
category: education
ext_url: ''
id: KLARA_KOLLEKTIV
name: KLARA KOLLEKTIV
place: MaStaMo
sample: ''
subtitle: IT-Sicherheit für Aktivist:innen
summary_text: ''
taxonomy:
  tag:
  - aktivismus
time_info: Di 10.08. | 16:45 - 18:15
title: Digitale Selbstverteidigung

---

Kennenlernen von Maßnahmen, die für Aktivist:innen wichtig und sinnvoll sind, um sich und Mitstreiter:innen zu schützen.
In Zeiten, in denen Überwachung quasi allgegenwärtig ist,
und das Verhalten von Menschen über technologische Mittel
gesteuert wird, ist es immer wichtiger, dass wir lernen
uns dagegen zu verteidigen. Besonders politische
Aktivist:innen können schnell und unbemerkt in den Fokus
von Ermittlungsbehörden kommen.Wir schauen uns daher an, welche Maßnahmen im Bereich des
politischen Aktivismus sinnvoll und wichtig sind, um sich
selbst und Mitstreiter:innen zu schützen.Unser Vortrag zur digitalen Selbstverteidigung enthält
sowohl theoretische als auch praktische Elemente.
Themen sind unter anderem sichere Kommunikation,
sicheres Surfen, Passwörter, Verschlüsselung, Meta-Daten,
und Live-Betriebssysteme.
