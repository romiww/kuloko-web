---
category: others
ext_url: https://www.nrw-versammlungsgesetz-stoppen.de/
id: Ende_Gelande_Bochum
place: Scheunenkino
sample: ''
subtitle: verfilmte Theaterperformance
summary_text: ''
taxonomy:
  tag:
  - ''
time_info: Di 10.08. | 16:00 - 16:45
title: 'Ende Gelände Bochum - Ein Reuliges Gesetz, #NoVersGNRW - künstlerische Performance'

---

Das neue Versammlungsverhinderungsgesetz betrifft alle, die in NRW für eine (klima-)gerechtere Welt einstehen. Diese Fortsetzung der Kriminalisierung von Protest (siehe NoPolGNRW) ist juristisch bereits häufig ausführlich zerlegt worden. Hier wird in theatralisch aufbereiteten Redebeiträgen die emotionale Perspektive aus diversen sozialen Kämpfen zum Ausdruck gebracht. In einem aufwühlenden Appell wird Verzweiflung, Wut, aber auch Mut Platz eingeräumt und betont, dass die vielfältigen Inhalte & Aktionsformen solidarisch aufeinander bezogen werden müssen. Mehr Informationen unter nrw-versammlungsgesetz-stoppen.de
