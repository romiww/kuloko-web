---
category: education
ext_url: ''
id: ausgeco2hlt
name: ausgeco2hlt
place: Schlosswiese | WSZ 03
sample: ''
subtitle: Wie Veränderung möglich wird
summary_text: ''
taxonomy:
  tag:
  - workshop
  - transformation
  - organisierung
time_info: Mo 09.08. | 16:00 - 17:30
title: Theorien des Wandels

---

Bagger blockieren, Zerstörung aufhalten, öffentliche Aufmerksamkeit schaffen, viele werden - und dann? Druck aufbauen - aber gegenüber wem eigentlich?

Wir wollen in dem Workshop der Frage nachgehen wie das was wir in unserem Aktivismus machen zu größeren gesellschaftlichen Veränderungen führen kann: welche verschiedene Ansatzpunkte gibt es? Wie können diese so zusammen wirken, dass sie uns stärker machen? Und welche Rolle spielt eigentlich der Staat dabei?

Wir beleuchten auch unsere eigene Rolle: welche Vorstellungen habe ich eigentlich davon wie sich Welt verändern lässt? Wo fühle ich mich wirksam und sinnvoll? Sich die eigenen oft impliziten Theorien des Wandels bewusst zu machen kann uns dabei helfen zielgerichtet Strategien zu entwickeln und besser mit anderen zusammen zu arbeiten.
