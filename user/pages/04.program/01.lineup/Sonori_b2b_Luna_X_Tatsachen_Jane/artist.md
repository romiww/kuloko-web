---
category: music
ext_url: ''
id: Sonori_b2b_Luna_X_Tatsachen_Jane
place: Wiesenbühne
sample: ''
subtitle: Technoider Latinx Queer Disco Pop & HipHop
summary_text: Wo der Beton aus jahrzehntelanger männlicher DJ-Dominanz brüchig wird,
  sprießen neue Soundlandschaften. Sonori und Luna X vom Bonner FLINTA*-DJ-Kollektiv
  Tatsachen und Jane haben eine Fusion aus feministischem HipHop, Disco, House und
  Latinx-Beats im Gepäck.
taxonomy:
  tag:
  - techno
  - pop
  - hiphop
  - dj
time_info: 08.08. | 01:00 - 03:00
title: Sonori b2b Luna X // Tatsachen & Jane

---


