---
category: music
ext_url: https://www.sarah-fartuun-heinze.de/musik/
id: Fartuuna
place: Wiesenbühne
sample: ''
subtitle: Fällt aus!
summary_text: In den unterschiedlichsten Farben bereist Fartuuna mit ihrer Ukulele
  singenderweise Origamilabyrinthe, Blumenbeete voller aschfahler Neurosen, die Welten
  ihrer Lieblingsvideospiele, gönnt sich dann und wann mal eine Fee und lacht dabei,
  schon aus Prinzip, lauter.
taxonomy:
  tag:
  - singer-songwriter*in
  - ukulele
time_info: Do 12.08. | 21:30 - 22:30
title: Fartuuna

---

In den unterschiedlichsten Farben bereist Fartuuna mit ihrer Ukulele singenderweise Origamilabyrinthe, Blumenbeete voller aschfahler Neurosen, die Welten ihrer Lieblingsvideospiele, gönnt sich dann und wann mal eine Fee. Dabei lacht sie schon aus Prinzip lauter. Nicht nur, aber auch dem Alltagsrassismus ins Gesicht. Und, wie sie in einem ihrer Lieder singt, »…das ist auch gut so!«

Wenn Fartuuna nicht auf kleinen, großen und mittelgroßen Bühnen ukulelisierenderweise unterwegs ist heißt sie Sarah Fartuun Heinze und arbeitet als multidisziplinäre freiberufliche Künstlerin und Kulturelle Bildnerin an der Schnittstelle zu Theater, Games, Musik und Empowerment.

Sie versteht sich als Ästhetische Forscherin, ist Teil der Initiative Creative Gaming, freie Autorin und Teil der Neuen Deutschen Medienmacher*Innen.

Und: Sarah spielt gerne. Eines ihrer Lieblingsspiele ist "Zelda: Ocarina of Time", vermutlich weil da der Schlüssel zu den meisten Rätseln die Musik ist. Wie so oft, auch fernab von Bildschirmen und Theaterbühnen.
