---
category: music
ext_url: https://www.ox-fanzine.de/interview/2lhud-3037
id: Stefan_Pieck
place: Hofbühne
sample: https://youtu.be/JYOC5tMEdlk
subtitle: Singer Songwriter Punk
summary_text: Stefan Pieck dieses Mal solo. Seine eigenen Songs behandeln häufig soziale
  und gesellschaftliche Themen. Er war schon als Schüler Mitbegründer der Band 1.
  Mai 87, Deutschpunk aus Erkelenz, dann ab 2002 der Kölner Punkband 2LHUD.
taxonomy:
  tag:
  - singer-songwriter*in
  - punk
  - lokales
time_info: Sa 07.08. | 17:15 - 18:00
title: Stefan Pieck

---

1. Mai 87 hieß die Band, die Stefan mit drei anderen jungen Musikern, damals alle noch Schüler, bereits 1987 gründete. Deutschpunk mit eigenen Texten und eigener Musik. Nach der Auflösung im Jahr 2000 gründete Stefan mit vier anderen Musikern 2LHUD als Kölner Punkband mit Einflüssen aus Dub und Reggae.
