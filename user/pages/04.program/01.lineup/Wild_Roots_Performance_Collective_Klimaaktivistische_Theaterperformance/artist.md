---
category: others
ext_url: ''
id: Wild_Roots_Performance_Collective
place: MaWaLü
sample: ''
subtitle: ''
summary_text: Durch verschiedene Methoden des Theaters und Tanzes transformieren wir
  vielfältige Perspektiven zu kulturellen und gesellschaftspolitischen Aktionen um
  Diskurse bezüglich eines Guten Lebens für alle zu ermöglichen.
taxonomy:
  tag:
  - performance
  - theater
time_info: Sa 07.08. | 18:30 - 19:30
title: Wild Roots Performance Collective - Klimaaktivistische Theaterperformance

---


