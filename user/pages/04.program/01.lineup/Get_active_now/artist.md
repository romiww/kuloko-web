---
category: education
ext_url: ''
id: ''
name: ''
place: ''
sample: ''
subtitle: Kreative Aktionsformen für eine solidarische Gesellschaft
summary_text: ''
taxonomy:
  tag:
  - workshop
  - aktivismus
time_info: ''
title: Get active now

---

Ein Workshop zu kreativen Aktivismus und politische Aktionsformen. Wie kann gesellschaftliches Engagement wirksam, nachhaltig und bei Bedarf auch zugleich witzig sein?
