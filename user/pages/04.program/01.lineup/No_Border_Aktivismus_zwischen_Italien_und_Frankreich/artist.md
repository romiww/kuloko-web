---
category: education
ext_url: www.keshaniya.org
id: Kesha_Niya
name: Kesha Niya
place: Klimacamp Rheinland | WSZ 02
sample: ''
subtitle: Erfahrungsbericht und Gesprächsrunde
summary_text: Kesha Niya ist ein Graswurzelkollektiv, das an der ital.-franz. Grenze
  arbeitet und sich mit people on the move solidarisiert. Wir wollen einen offenen
  Gesprächsraum schaffen, um über Zusammenhänge mit der sog. Klimakrise zu sprechen
  & von den Erfahrungen der Aktivistinnen dort zu berichten. www.keshaniya.org
taxonomy:
  tag:
  - workshop
  - storytelling
  - rassismus
time_info: So 08.08. | 11:00 - 12:30
title: No-Border-Aktivismus zwischen Italien und Frankreich

---

Kesha Niya (sorani-kurdisch für "kein Problem!") ist ein Graswurzelkollektiv, das an der italienisch-französischen Grenze arbeitet und dort für und mit Menschen kocht, die nach oder innerhalb von Europa migrieren wollen - und dabei illegalerweise von der französischen Polizei an der Weiterreise gehindert werden. Der Workshop soll ein offener Gesprächsraum dafür sein, was das mit der sog. Klimakrise zu tun hat, welche Erfahrungen Aktivistinnen dort gemacht haben und darüber, was Solidarität bedeutet. Mehr Informationen zu dem Kollektiv: https://keshaniya.org/about/
