---
category: education
ext_url: https://youtu.be/0Uj3E-24puw
id: Zade_Abdullah_Noura_Hammouda
name: Zade Abdullah & Noura Hammouda
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: Partizipativer Vortrag und Workshop
summary_text: ''
taxonomy:
  tag:
  - vortrag
  - workshop
  - rassismus
  - klimagerechtigkeit
time_info: Sa 14.08. | 10:30 - 12:30
title: Calais - Die Katastrophe im Herzen Europas

---

Seitdem das große Lager in der Nordfranzösischen Hafenstadt Calais im Jahr 2016 geräumt wurde, verschlimmern sich die menschenunwürdigen Lebensbedingungen für Geflüchtete stetig.
Die Geflüchteten kommen nach Calais, weil sie über die Grenze nach Großbritannien wollen. Viele haben Familienangehörige oder andere Beziehungen dort. Verzweifelt versuchen einige mit Schlauchbooten über den Ärmelkanal zu gelangen. Dabei ertrinken regelmäßig Menschen.
Derzeit schlafen etwa 1500 Geflüchtete in einfachen Zelten auf der Straße entlang der Küste. 
Wie konnte diese Situation entstehen? Und warum werden die Zelte in denen Leute leben täglich unter unter physischer Polizeigewalt geräumt? Was haben kolonial-rassistische Weltbilder damit zu tun? Und wie passt die Klimakrise in dieses Bild?
Zade Abdullah und Noura Hammouda waren vor Ort und werden uns in dem Vortrag Antworten auf unsere Fragen geben.
