---
category: education
ext_url: ''
id: Still_Burning_Network
name: Still Burning Network
place: Scheunenkino
sample: ''
subtitle: A web series about the global hardcoal supply chain and resistance to it
summary_text: ''
taxonomy:
  tag:
  - film
  - klimagerechtigkeit
time_info: Do 12.08. | 17:30 - 19:00
title: Still Burning

---

Three films about three distant but connected places together form a mosaic of global hard coal supply chains and resistance to it. They are the result of old and newly woven connections between people in Colombia, Russia and Germany: Colonialism and Resistance, Capitalism and Criticism, and Climate Justice and Solidarity. https://stillburning.net/films/
