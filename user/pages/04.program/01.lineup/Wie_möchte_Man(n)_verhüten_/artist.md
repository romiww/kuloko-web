---
category: education
ext_url: ''
id: Charlotte_Peltre_Philipp_Hofmann
name: Charlotte Peltre & Philipp Hofmann
place: Klimacamp Rheinland | WSZ 03
sample: ''
subtitle: Zwischen patriarchaler Realität und feministischen Utopien
summary_text: ''
taxonomy:
  tag:
  - feminismus
  - workshop
time_info: Mo 09.08. | 10:30 - 12:30
title: Wie möchte Man(n) verhüten?

---

Ein Workshop zu Zeugungsverhütung - Es geht dabei sowohl um eigene Biographie als auch um Geschichte und strukturelle Bedingungen von Verhütungen und um Alternativen und Utopien.
