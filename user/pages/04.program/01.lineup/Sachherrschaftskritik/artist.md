---
category: education
ext_url: ''
id: Eva_von_Redecker
name: Eva von Redecker
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: 'Intersektionaler Materialismus oder die Weigerung, weiterzuzerstören. Online
  Format.

  '
summary_text: ''
taxonomy:
  tag:
  - kapitalismus
  - feminismus
  - workshop
time_info: Mo 09.08. | 14:30 - 16:30
title: 'Sachherrschaftskritik

  '

---

Der Workshop stellt einen Ansatz vor, mit dem sich 
verschiedene Herrschaftsachsen in ihrem kapitalistischen Zusammenspiel 
analysieren lassen: die Sachherrschaftskritik. Ausgehend von der 
Kategorie der Eigentumsform (also was es heißt, etwas zu besitzen), 
lässt sich die marxistische Analyse der Warengesellschaft vertiefen und 
auffächern. Fast alles, worum soziale Bewegungen heute kämpfen ist mal 
eigentümlich verdinglicht worden: schwarzes Leben, weibliche 
Reproduktionsfähigkeit, natürliche Ressourcen. Diese Herrschaft ist 
überlebt, und doch wird sie immer verbissener verteidigt. Polizeigewalt 
und Grenzregime, Verweigerung geschlechtlicher Selbstbestimmung und 
Abwertung von Sorgearbeit, Fossilindustrie und konventionelle 
Landwirtschaft halten die Sachherrschaft über's Leben aufrecht. Aber 
ihnen entgegen steht nicht weniger als eine Revolution: das Wissen, dass 
wir auch rettend, regenerierend, teilend und pflegend leben könnten. Eva von Redecker ist Philosophin - zur Zeit mit einem 
Forschungsprojekt zu Autoritarismus an der Uni Verona - und schreibt 
manchmal lieber über Leben und Tod als über akademische Debatten. Ihr 
Buch /Revolution für das Leben/(S.Fischer 2020) handelt von 
kapitalistischer Zerstörung und vom Protest (u.a. von Ende Geläde) dagegen.
