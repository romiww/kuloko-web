---
category: music
ext_url: http://sophiamix.net/music/
id: Diva_Daneben
place: Wiesenbühne
sample: www.sophiamix.net/schoenheiten
subtitle: Softpunk, Alternative folk | Sophia Mix
summary_text: Diva Daneben singt eingängige Melodien mit komplexen Texten. Lieder
  über das Leben, rote Fäden weben und wieder auftrennen, weil wir wuchsen. Sie textet
  deutsch und englisch, spielt Gitarre und Akkordeon und Meta Beyer begleitet am Bass.
taxonomy:
  tag:
  - punk
  - folk
time_info: So 08.08. | 20:00 - 21:00
title: Diva Daneben

---

Sophia Mix performt Softpunk, eingängige Melodien mit komplexen Texten. Aka Diva Daneben singt neue Lieder von der Insel, von Strassen, Felsen und von Herzen. Über Lust-, Frust- und Verlustängste; über das Leben, rote Fäden weben und wieder auftrennen, weil wir wuchsen. Sie textet deutsch und englisch, spielt Gitarre und Akkordeon, mit Einflüssen aus Blues, Alternative Folk und Punk. Meta Beyer begleitet am Bass.
