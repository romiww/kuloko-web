---
category: music
ext_url: https://punkelelics.blogspot.com/
id: The_Punkelelics
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=C1tS5yycgcY
subtitle: Ukulelismus total!
summary_text: Die PUNKELELICS beweisen mit ihren Interpretationen sowohl prähistorischer
  als auch neuzeitlicher Punkrock-Klassiker, dass Punksongs im Kern gefühlvolle Balladen
  sind. So kommen die zornigen Lieder über korrupte Staaten, verkommene Gesellschaften
  und unerfüllte Liebe in völlig neuem Gewand daher.
taxonomy:
  tag:
  - punk
  - akustik
  - cover
  - balladen
time_info: Sa 07.08. | 17:00 - 18:00
title: The Punkelelics

---

Nach dem Besuch eines sehr vergnüglichen Slime-Konzerts in Köln 2011 entsteht die Idee zu den Punkelelics. Warum nicht Punk auch mal langsam, met Jeföhl und mit Ukulele? Ihre Erfahrungen aus gemeinsamen Kneipenchorzeiten ergänzen sie mit ihrer Leidenschaft zum Instrument Ukulele. Tadaa, die Punkelelics sind da!

Damals noch als Trio treten die Punkelelics mit ihren Interpretationen sowohl prähistorischer als auch neuzeitlicher Punkrock-Klassiker den Beweis an, dass Punksongs im Kern gefühlvolle Balladen sind – wenn man dabei nicht so rumbrüllt! Virtuos schälen sie die verborgenen melancholischen Harmonien heraus und hauchen den Songs mit ihren Ukulelen und ihrem mehrstimmigen Gesang frisches Leben ein. So kommen die zornigen Lieder über korrupte Staaten, verkommene Gesellschaften und unerfüllte Liebe in einem völlig neuen Gewand daher. Nicht von ungefähr wurden sie auch mal die „Peter, Paul & Mary des Punk“ genannt.

2012 zeigen sie sich auf dem Ukulele Hotspot in Winterswijk mit ihrer Version von Sex Pistols’ „No Feelings“ erstmals einem größeren Ukulele-Fachpublikum. Im Lauf der Jahre folgen viele weitere Auftritte in Kneipen, Klubs, Galerien, Wohnzimmern und einem Treppenhaus, und neben neuen Arrangements entwickeln sich die Punkelelics auch musikalisch weiter: Seit dem Jahr 2016 ist aus dem Trio ein Quartett geworden. Mittlerweile ergänzt und bereichert eine Bassistin mit ihrem E-Uke-Bass den musikalischen Teppich der Band.

Ab jetzt geben die Punkelelics nicht mehr 75%, sondern 100% auf der Bühne.
Ukulelismus total!

The Punkelelics sind: Claudia, Norbert, Murat (Gesang, Ukulele), Alix (Bass-Ukulele)
