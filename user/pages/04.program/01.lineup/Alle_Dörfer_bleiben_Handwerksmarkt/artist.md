---
category: others
ext_url: ''
id: ''
place: Markt- und Theaterwiese
sample: ''
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - lokales
  - café
  - markt
  - kinder
time_info: Mo 09.08. - Do 12.08. | immer 14:00 - 20:00
title: Alle Dörfer bleiben - Handwerksmarkt

---

Der Markt gibt Menschen, vor allem aus der Region, die Möglichkeit, ihre selbsthergestellten Dinge auszustellen. Ob aus Holz oder Glas, Genähtes oder Gestricktes, sowie sebst hergestellte Nahrungsmittel alle findet Platz bei uns ... und auch für die Kleinsten was dabei: Es gibt ein buntes Programm von Lesung eines Kinderbuchs "Aufstand im Korallenriff" über Kinderschminken...vielleicht besucht uns auch das Spielmobil!
