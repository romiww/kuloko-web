---
category: others
ext_url: ''
id: Marcel_Kopper_und_Hubert_Perschke
place: ''
sample: ''
subtitle: Langzeit-Fotodokumentation Hambach und Co.
summary_text: ''
taxonomy:
  tag:
  - fotografie
time_info: ''
title: Marcel Köpper und Hubert Perschke

---

Marcel Köpper und Hubert Perschke dokumentieren seit vielen Jahren die Veränderungen, Ereignisse, Proteste und Aktionen im Rheinischen Braunkohlerevier. Eine kleine bildliche Auswahl ist in ihrer Ausstellung zu sehen.
