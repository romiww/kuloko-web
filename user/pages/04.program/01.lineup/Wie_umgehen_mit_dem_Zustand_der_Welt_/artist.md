---
category: education
ext_url: ''
id: ''
name: ausgeco2hlt
place: Schlosswiese | WSZ 04
sample: ''
subtitle: Über Hoffnung, Zweifel und wie wir weiter gehen
summary_text: ''
taxonomy:
  tag:
  - austausch
  - aktivismus
  - organisierung
  - workshop
time_info: Mi 11.08. | 15:30 - 17:00
title: Wie umgehen mit dem Zustand der Welt?

---

Der Blick in die Nachrichten macht häufig schlechte Laune, und Schlimmeres. Kommt in 30 Jahren noch Wasser aus dem Hahn, und wenn ja, für wen und wieviele? Werden immer mehr Menschen an immer hochgerüsteten Grenzen sterben oder werden wir das verhindern können? Haben wir Hoffnung – und wenn ja, auf was? Was machen wir mit den düsteren Gefühlen, die uns zuweilen anfallen? Trauen wir uns, sie mit anderen zu teilen? Woher nehmen wir die Kraft, trotzdem weiter oder politisch aktiv zu sein?
Wenn euch diese Fragen bewegen, laden wir euch zu einem offenen Austausch darüber, wie es uns mit dem Zustand der Welt geht, auf  „unsere“ gemütliche Schlosswiese mit Kuchen und Kaffee.
Mit Wilm, Iestyn und Dorothee
