---
category: music
ext_url: https://seasoulmusic.com
id: Seasoul
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=lQUfOLq-xfc
subtitle: Dream Pop
summary_text: SEASOUL begleitet ihren Gesang allein am Piano oder an der Gitarre.
  Mal emotional bewegt, mal anklagend und manchmal auch aus sicherer Distanz erzählend,
  webt sich SEASOUL seither ein Gewand aus Melodien, Texten und Arrangements, welches
  sie würdevoll trägt.
taxonomy:
  tag:
  - pop
time_info: So 15.08. | 23:00 - 00:00
title: Seasoul

---

Die Musikerin, Songwriterin, Produzentin und Wahlberlinerin Vanessa Sonnenfroh alias SEASOUL balanciert seit eh und je mit den Gegensätzen in ihren Liedern. Und das mit wachsendem Erfolg. Denn wenn wir uns Beispiele wie „Yin & Yang“, „Mond & Sonne“, „Hell & Dunkel“ sowie „Natur & Stadt“ vor Augen führen, wird schnell klar, das Gegensätze meistens im Abhängigkeitsverhältnis zueinander stehen. Willkommen in den Sphären von SEASOUL!
Vanessa Sonnenfroh stammt aus Baden-Württemberg, wo sie bereits von klein auf mit vor allem klassischer Musik konfrontiert wird. Die Liebe zum Musizieren formt sich in jener Zeit. Auf Reisen in Europa und Neuseeland entdeckt sie schließlich die Leidenschaft für einen rauen Folksound á la Bob Dylan und spielt, inspiriert von dessen Gigs im Greenwich Village der frühen 60er Jahre, in Kneipen, Bars und auf der Straße.
 
In jenen Anfangsjahren begleitet SEASOUL ihren Gesang allein am Piano oder an der Gitarre. Mitunter dringen Text und Musik dabei in die tiefsten Tiefen der Seele vor. Mal emotional bewegt, mal anklagend und manchmal auch aus sicherer Distanz erzählend, webt sich SEASOUL seither ein Gewand aus Melodien, Texten und Arrangements, welches sie würdevoll trägt. Das Bild des geheimnisvollen Meerestiefs ist dabei als eine Art Alter Ego zu begreifen.
Nach einem kleinen Zwischenstop in Hamburg, zog die umtriebige junge Frau für einen Studienplatz im Fach Musikproduktion nach Berlin, wo die Karriere ihren Lauf nimmt. Zahlreiche Live-Konzerte, Erfahrungen und Kollaborationen später, hat sich ihr musikalisches Spektrum erweitert. Nur selten scheut sich SEASOUL vor einem ihr fremden Musikgenre und entscheidet dabei stets intuitiv. Jetzt hat sie genau den richtigen Produzenten für ihre Ideen gefunden. Fabian Bork alias DREAMMAKER hat die Fusion- Lust seiner Protagonistin erkannt – und so kombiniert das Duo Singer-Songwriter-Klänge mit Jazzharmonien, Dream Pop mit Hip Hop und Soul und variiert Folk Lyrics auf synthetischen Keyboard-Flächen.
Im Herbst 2020 erscheint eine gemeinsame EP von SEASOUL und DREAMMAKER. Wer nicht so lange warten möchte, kann sich auf SoundCloud, Spotify und Co. sämtliche Veröffentlichungen der letzten Jahre anhören und sich davon überzeugen lassen, dass Gegensätze selten eine so harmonische Einheit bildeten.
Text: Johannes Martin
