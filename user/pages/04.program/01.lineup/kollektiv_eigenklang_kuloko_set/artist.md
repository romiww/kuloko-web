---
category: music
ext_url: https://soundcloud.com/eigenklang
id: ''
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=ML980zFGmvs
subtitle: ''
summary_text: sounds recorded @kuloko
taxonomy:
  tag:
  - dj
  - klangwelten
  - tanz
time_info: So 08.08. 22:00 - Mo 09.08. 01:00
title: kollektiv eigenklang - kuloko-set

---

Kollektiv Eigenklang is an audio-visual performance collective exploring the unlimited posibilities of improvised and interactive live producing.
