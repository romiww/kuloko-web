---
category: others
ext_url: https://www.bildungskollektiv-bonn.de/
id: Bildungskollektiv_Bonn
place: Klimacamp Rheinland
sample: ''
subtitle: Eine künstlerisch-kreative Intervention für Klein und Groß
summary_text: ''
taxonomy:
  tag:
  - utopie
time_info: Sa 14.08. - So 15.08.
title: Bildungskollektiv Bonn - Utopiemaschine

---

Die Utopiemaschine ist im Rahmen des Projekts „BonnUtopia“ entstanden. Sie ist wunderbar wandelbar und steckt voller Geheimnisse. Sie ist mit dabei, wenn Menschen Ideen spinnen wollen. Sie knüpft unsichtbare Fäden, steckt voller Kreativität und Krempel, damit Menschen ihre eigenen Utopien mit allen Sinnen künstlerisch-kreativ entstehen lassen können. Utopische Landkarten können gemalt oder mit Ton gestaltet werden. Utopiesongs komponiert oder neue Pässe gedruckt werden. Utopien können am Telefon ausgetauscht werden und die Maschine kann um utopischen Rat gefragt oder die Zukunft vorhergesagt werden…
Während der Kuloko sind Ideen für Utopien im Rheinland nach dem sofortigem Braunkohleausstieg gefragt. Die Maschine ist mobil und ist ein flexibler Ort, an dem sich Menschen begegnen und ins Gespräch und eine kreative Interaktion kommen. Lasst Euch überraschen, wann und wo sie auftaucht.
Die Utopiemaschine wurde in Kooperation mit Friedrich Boell - https://www.lleob.de/ - und Friederike Dreier - https://kunstkommode.org/ - entwickelt.
