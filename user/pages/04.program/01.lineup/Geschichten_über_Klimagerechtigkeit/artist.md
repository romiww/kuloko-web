---
category: education
ext_url: ''
id: Theresa_Leisgang_Institute_of_Environmental_Justice
name: Theresa Leisgang & Institute of Environmental Justice
place: Unser aller Wald
sample: ''
subtitle: Buchlesung und anschließende Diskussion
summary_text: ''
taxonomy:
  tag:
  - lesung
  - klimagerechtigkeit
time_info: So 08.08. | 19:30 - 21:00
title: Geschichten über Klimagerechtigkeit

---

Mit Theresa Leisgang mit ihrem Buch „Zwei am Puls der Erde“ und Kathrin Henneberger mit „Realität Klimakrise“
