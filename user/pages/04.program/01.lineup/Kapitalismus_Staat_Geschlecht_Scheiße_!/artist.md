---
category: education
ext_url: ''
id: Gegengrau
name: Gegengrau
place: Klimacamp Rheinland | WSZ 02
sample: ''
subtitle: Workshop
summary_text: ''
taxonomy:
  tag:
  - workshop
  - queerfeminismus
  - kritik
time_info: Mo 09.08. | 14:30 - 16:30
title: Kapitalismus/Staat + Geschlecht = Scheiße?!

---

Es ist ein partizipativer Workshop rund um die Frage "Warum brauchen kapitalistische Staaten das Konstrukt "Geschlecht" bzw. eine binäre Geschlechtertrennung?". Zusammen betrachten wir historische Ereignisse und Gegebenheiten die veranschaulichen, wie diese Frage vielleicht beantwortet werden kann. Dafür braucht ihr kein besonderes Vorwissen. Eure persönlichen Erfahrungen reichen aus. Wir sehen uns dabei nicht als "DIE Expert*innen" sondern eher als Rahmengebende für einen gemeinsamen Brainstorming- und Austauschprozess.     Teilnehmer*innen Anzahl: max. 20
