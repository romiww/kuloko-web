---
category: music
ext_url: https://www.lebenslaute.net/
id: Lebenslaute
place: Markt- und Theaterwiese
sample: https://www.youtube.com/watch?v=wsK6wqoh_q0
subtitle: Klassische Musik, klassische und internationale Lieder
summary_text: Lebenslaute ist ein Netzwerk von Musiker*innen das klassische Musik
  an Orten aufführt, von denen Menschenrechtsverletzungen und/oder Umweltzerstörung
  ausgehen. Vom 10-17 August 2021 unterstützen wir "Alle Dörfer Bleiben" und sind
  am und im Tagebau Garzweiler gegen die weitere Kohleverstromung aktiv.
taxonomy:
  tag:
  - klassik
  - chor
  - orchester
time_info: 11.08. 15:30 - 16:30 & 12.08. 13:30-14:30 Im Kleinensemble auf der Theaterwiese  |
  13:08. 19:00 - 22:00 In Lützerath
title: Lebenslaute

---


