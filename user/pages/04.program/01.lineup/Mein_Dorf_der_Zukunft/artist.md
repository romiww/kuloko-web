---
category: education
ext_url: ''
id: Klima_Allianz
name: Klima-Allianz
place: Schlosswiese | WSZ 04
sample: ''
subtitle: Eine Wandelwerkstatt
summary_text: ''
taxonomy:
  tag:
  - lokales
  - workshop
time_info: So 08.08. | 10:00 - 13:00
title: Mein Dorf der Zukunft

---

Dieser Workshop bietet Impulse, Raum und Zeit zur (co-)kreativen Auseinandersetzung mit den Fragen: Wie soll mein Dorf 2030 aussehen? Wie die nähere Umgebung? Was oder wen brauche ich für mein gutes Leben? Was ist aktuell wichtig für das gute Leben im Allgemeinen? Was vermisse ich? Der Workshop gliedert sich in 3 Phasen. Zunächst nehmen wir euch mit auf eine Traumreise ins Jahr 2030, gespickt mit Bildern der Zukunft, die wir von Menschen aus anderen Orten des Wandels mitbringen. Nach diesem Impuls entlassen wir euch mit einem Leitfaden in eine kreative Phase: allein oder in einer Zweiergruppe, könnt ihr euch zurückziehen, spazieren gehen, malen, schreiben, dichten, Gespräche oder Videos aufzeichnen. Eurer Fantasie sind keine Grenzen gesetzt. Im Anschluss laden wir euch ein, eure Ideen, Bilder, Träume, Texte, Gedankenfetzen, Wünsche … Teil einer Wandelausstellung werden zu lassen. Dabei ist jeder Beitrag wertvoll und ohne Anspruch auf Perfektion. Zum Abschluss besteht die Möglichkeit zum offenen Austausch. Herzliche Einladung – wir freuen uns auf euch! Es begleiten euch Jasmin Ziemacki, Hans Kühnl und Antje Grothus (Klima-Allianz Deutschland)
