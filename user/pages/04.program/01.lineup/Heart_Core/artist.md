---
category: music
ext_url: https://www.heart-core-moenchengladbach.de/
id: Heart_Core
place: Markt- und Theaterwiese
sample: ''
subtitle: A cappella Chormusik
summary_text: Seit 26 Jahren singen im Heart Core Frauen im Alter von 20-70 Jahren
  mehrstimmige Lieder mit selbstgemachten feministischen Texten. Die Chorleiterinnen,
  die nach den Prinzipien des NVN (natural voice network) arbeiten, arrangieren die
  Stücke größtenteils selber zu bekannten Melodien.
taxonomy:
  tag:
  - chor
time_info: Sa 14.08. | 16:30 - 17:30
title: Heart Core

---


