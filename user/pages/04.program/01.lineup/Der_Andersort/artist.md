---
category: others
ext_url: https://actforutopia.org/der-andersort/
id: Der_Andersort
place: MaWaLü
sample: ''
subtitle: Interaktives Hörstück
summary_text: Der Andersort verwandelt Plätze, Brachen und ungenutzte Flächen in Bühnen.
  Als Teilnehmer:in des interaktiven Hörspiels erhältst du über Kopfhörer Regieanweisungen
  und wirst ganz nebenbei zur Darsteller:in des Stücks. Du wandelst zwischen Menschen
  und Bäumen und veränderst Orte durch deine Ideen.
taxonomy:
  tag:
  - performance
  - interaktiv
  - klangwelten
  - storytelling
time_info: 08.08. 16:00 | 10.08. 19:00
title: Der Andersort

---

Wir haben ein interaktive Hörstück mit dem Namen „der Andersort“ entwickelt, welches sich mit der Gestaltung von ungenutzten Orten beschäftigt. Bei dem coronakonformen Hörstück bekommen pro Slot maximal 30 Teilnehmer:innen eine Geschichte über Funkkopfhörer erzählt. Die Inhalte sind wie Regieanweisungen aufgebaut und die Teilnehmenden werden zu Dasteller:innen des Stücks. 
Thematisch wird die Gestaltung eines Ortes aus verschiedenen Perspektiven betrachtet. Die Ideen der Teilnehmenden werden in die Geschichte integriert, der Ort wird umgenutzt und gestaltet. Auf magische Weise werden Menschen zu Bäumen, die Rolle von Freiraum und Mitsprache wird thematisiert und die Misere von Veränderung und Zerstörung ist Teil der Handlung.
Im Anschluss an das Stück wird ein Nachgespräch angeboten, bei dem Eindrücke und Erfahrungen ausgetauscht und Ideen diskutiert und angestoßen werden können.
Wir das sind drei Personen, die sich für das Stück zusammengefunden haben. Kevin Kurmann (Technik), Lajos Groffmann (Audio und Technik) und Mona Hofmann (Story und Regie).
