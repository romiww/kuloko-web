---
category: music
ext_url: ''
id: Jooles_Nick
place: Hofbühne
sample: ''
subtitle: Singer-Songwriter
summary_text: Jooles & Nick  Zwei Singer-Songwriter, zwei Freunde, zwei Träumer unterwegs
  mit Klavier, Gitarre und Stimme. Mit im Gepäck, die neuen Songs vom Jooles & The
  Hidden Tracks Album.
taxonomy:
  tag:
  - singer-songwriter*in
time_info: Sa 07.08. | 16:00 - 16:45
title: Jooles & Nick

---

Zwei Singer-Songwriter, zwei Freunde, zwei Träumer:
Jooles schreibt von lauten Gefühlen und leisen Gedanken, singt mit vorlautem Rhythmus über barfüßige Wahrheit und lässt an den richtigen Stellen das Gefühl von Zuhause im Herzen aufflackern.
Jooles überzeugt durch ihre eindrucksvolle Stimmgewalt sowie ihr unverblümtes Songwriting. Beides verarbeitet sie zu brachialen Balladen und Liebeserklärungen. Irgendwie ist alles authentisch, unverwechselbar und tief.
Eine treibende, aber warme Gitarre bietet Grund und Boden für Jooles Stimme, mit der sie in verschiedenen Klangfarben bunte Szenarien auf die Leinwände in unseren Köpfen malt.
Nick Takvorian bietet Hoffnungen, Zweifel, Träume und Ideen. 
Klare Vocals, reduzierte Gitarre und melancholisches Klavier weben einen Moment der Stille - aber Stille kann laut sein.
