---
category: music
ext_url: htttp://merlecello.de
id: Merle
place: Wiesenbühne
sample: https://youtu.be/r-BcjZTLTZU
subtitle: Cello-Liedermacherin
summary_text: Ich singe von Kirschblütenstaubsaugern, Rissen im Asphalt und dem Drunter
  und Drüber im nächtlichen Gewühl. Doch ab heute frisst der Elefanten in meinem Bauch
  nur noch Rosinen mit Himbeerpudding...
taxonomy:
  tag:
  - singer-songwriter*in
  - akustik
time_info: Sa 07.08. | 19:30 - 20:30
title: Merle

---

Ich singe von Kirschblütenstaubsaugern, Rissen im Asphalt und dem Drunter und Drüber im nächtlichen Gewühl. Doch ab heute frisst der Elefanten in meinem Bauch nur noch Rosinen mit Himbeerpudding…

Immer wieder zwischen vielen Lebenswelten zerissen, trete ich seit 2013 mit meinem Soloprojekt (Cello/Klavier/Gesang) auf. Inspiriert dazu hat mich u.a. die Rotzfreche Asphaltkultur (RAK), ein seit über dreißig Jahren bestehender Zusammenschluß von linken (Straßen)musiker*innen und Kleinkünstler*innen.

Zusammen mit u.a. Konny, Früchte des Zorns und Yok Quetschenpaua organisiere ich mich im DIY Label AbDafürRecords. Die Bands verbindet nicht nur Freundschaft, sondern sie verstehen ihre Musik auch als musikalische Intervention gegen eine Gesellschaft, die immer weiter nach rechts rückt. Sie verstehen ihre Musik aber auch als Kampf für eine Welt mit Blaubeermuffins für Alle!

Mit meinem Album „Zwischen drunter und drüber“ (2015) unterstütze ich die Bereitstellung von Wohnraum für Geflüchtete auf dem Projektehof Wukania nördlich von Berlin.
