---
category: music
ext_url: https://missratenetoechter.noblogs.org/missratene-toechter/
id: missratene_Tochter
place: Wiesenbühne
sample: ''
subtitle: Anarchaschlager
summary_text: Verpeilt und irgendwie trotzdem am Start trällern sie immer mal wieder
  ihre Lieder. Die Masse findet sich dann im anarchistischen Stadl. Dann heißt es
  endlich Ohrwürmer trällern ohne schlechtes Gewissen.
taxonomy:
  tag:
  - akustik
  - punk
  - anarchaschlager
  - anarchie
time_info: Fr 13.08. | 22:30 - 23:30
title: missratene Töchter

---


