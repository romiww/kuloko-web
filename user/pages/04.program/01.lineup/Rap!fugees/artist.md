---
category: music
ext_url: https://www.rapfugees.org/
id: Rap!fugees
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=kV_gl-e4eqc
subtitle: HipHop
summary_text: ''
taxonomy:
  tag:
  - hiphop
  - rap
time_info: So 15.08. | 19:00 - 20:00
title: Rap!fugees

---

Die Rapfugees sind ein Kreativ-Kollektiv, dass die Hip-Hop Kultur lebt, tanzt, rappt, beatboxt & scracht. Menschen mit Geschichten aber ohne Ego-Musik. 

2017 im Rahmen der G20 Proteste als loser Zusammenschluss gegründet, wurden sie mehr, wuchsen zusammen und stehen inzwischen im ganzen Land auf kleinen & großen Bühnen. Immer mitten drin, aber dauernd Dazwischen, Oldschool aber Trap, Miami meets Teheran in Hamburg.

Live – ob auf Demos, in Clubs, auf Festivals wie der Fusion oder im renommierten Thalia Theater - setzen die Künstler*innen alle ihre Talente ein: Die Zuschauer erwartet eine spektakuläre Show: Hin & hergerissen zwischen aktionsgeladenen Breakdance-Moves und berührender Pantomime, Improvisationen mit Loopstation & Beatbox, Scratches der alten Schule, Texten & Beats die mal unter die Haut und mal direkt in die Hüften gehen. Der Wu-Tang Clan der Globalisierung: 
In rap we find refuge!
