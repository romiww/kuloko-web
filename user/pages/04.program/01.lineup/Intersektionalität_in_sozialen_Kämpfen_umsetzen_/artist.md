---
category: education
ext_url: ''
id: Riv
name: Riv
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: Ein diskriminierungskritischer Blick auf Klima- und Tierrechtsbewegung
summary_text: ''
taxonomy:
  tag:
  - workshop
  - kritik
time_info: Di 10.08. | 14:30 - 16:30
title: 'Intersektionalität in sozialen Kämpfen umsetzen:'

---

In diesem Workshop nehmen wir genauer in den Blick, wie soziale Bewegungen diskriminierend wirken können, am Beispiel von Klima- und Tierrechtsbewegung. Dabei ist das Ziel den eigenen Privilegien bewusst zu werden, einen diskriminierungskritischeren Blick in unserem Aktivismus zu entwickeln, sowie über diesen intersektionaler nachzudenken und neue Formen eines intersektionalerem Aktivismus zu schaffen.     Dieser Workshop ist offen für alle und daher kein BIPoC und Jüd_Innen, sowie FLINTA* only Raum.
