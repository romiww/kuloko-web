---
category: others
ext_url: ''
id: Julia_Pesch
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: Wie kann Klimagerechtigkeit in Bildungsräumen aufgegriffen werden?
summary_text: ''
taxonomy:
  tag:
  - workshop
  - klimagerechtigkeit
time_info: So 08.08. | 10:30 - 12:30
title: Julia Pesch - Klima.Gerechtigkeit.Bildung?!

---

Methodenworkshop zur Broschüre “Klimagerechtigkeit Jetzt - Was ist Klima(un-)gerechtigkeit?"
Du willst Klimagerechtigkeit nicht nur auf die Straße, sondern auch in Bildungsräume reintragen? Dann bist du hier im Workshop genau richtig! 

Denn ob in außerschulischer oder schulischer Bildung: Die Klimakrise wird immer häufiger in Bildung thematisiert. Neben den naturwissenschaftlichen Fakten und Tipps für individuelle Handlungsoptionen kommt das Thema Klimagerechtigkeit allerdings häufig zu kurz oder wird gar nicht diskutiert. Wie also kann “Climate Justice NOW!” auch in Bildungsräumen aufgegriffen werden? Entlang der Informationsbroschüre “Klimagerechtigkeit JETZT - Was ist Klima(un-)gerechtigkeit?” (siehe http://klima-gerechtigkeit.info/) stellen wir im Workshop Methoden aus der praktischen Bildungsarbeit vor, die wir exemplarisch ausprobieren und diskutieren. 

