---
category: others
ext_url: ''
id: Alrescha
place: MaStaMo | Zirkuszelt
sample: ''
subtitle: Bewegungsangebot
summary_text: ''
taxonomy:
  tag:
  - bewegung
time_info: Mo 09.08. - So 15.08. | immer 08:00 - 08:45
title: Alrescha - Yoga

---

Morgenyoga, um in den Tag zu starten.
