---
category: music
ext_url: http://www.johannazeul.de
id: Johanna_Zeul
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=B5_c7AVcwZU
subtitle: wild crazy singer songwriter
summary_text: '"so heiß, so direkt und so gut."  Musikexpress   "Ein lebensbejahender
  Arschtritt für eine problembeladene Welt !"  Westzeit   „Echt toll! Weil es so klar
  ist.“  Smudo, Texter/Rapper   aktuelle JOHANNA ZEUL Single: LOVEFONE (30.7.)  http://www.johannazeul.de'
taxonomy:
  tag:
  - singer-songwriter*in
  - akustik
  - storytelling
  - kinder
time_info: So 15.08. | 17:00 - 18:00
title: Johanna Zeul

---

Wessen Karriere als selbstbestimmte Popmusikerin startete mit dem Rio Reiser Songpreis 2008 ? Wer erhielt den Udo Lindenberg Panikpreis 2010 ? Wer ist die deutsche Synchronstimme des singenden Bettelmädchens im Film „April und die außergewöhnliche Welt“ (arte) ? Wer schlug den Raab (Stefan) – zum Ritter ? Die Antworten lauten Johanna Zeul, Johanna Zeul, Johanna Zeul und Johanna Zeul. 

"so heiß, so direkt und so gut." 
Musikexpress 

"Ein lebensbejahender Arschtritt für eine problembeladene Welt !" 
Westzeit 

„Echt toll! Weil es so klar ist.“ 
Smudo, Texter/Rapper 

aktuelle JOHANNA ZEUL Single: LOVEFONE (30.7.)

http://www.johannazeul.de
