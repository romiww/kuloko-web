---
category: music
ext_url: ''
id: Piece_of_Peace
place: Markt- und Theaterwiese
sample: ''
subtitle: Vocal Ensemble
summary_text: Piece of Peace singen größtenteils selbst arrangierte acappella Stücke
  aus Rock und Pop. Weil Widerstand Soundtrack braucht, haben sie für die Kuloko thematisch
  passende Stücke aus verschiedenen Jahrzehnten zusammengestellt von Tracy Chapman
  über Carly Simon bis Mamuse.
taxonomy:
  tag:
  - ''
time_info: Fr 13.08. | 20:30 - 21:30
title: Piece of Peace

---


