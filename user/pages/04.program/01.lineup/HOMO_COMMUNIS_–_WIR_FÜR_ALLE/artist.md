---
category: education
ext_url: ''
id: ''
name: ''
place: Scheunenkino
sample: ''
subtitle: Dokufilm mit anschließendem Gespräch
summary_text: ''
taxonomy:
  tag:
  - film
  - transformation
time_info: Mi 11.08. | 19:30 - 22:00
title: HOMO COMMUNIS – WIR FÜR ALLE

---

Der Dokufilm "HOMO COMMUNIS – WIR FÜR ALLE“ porträtiert Akteure in Venezuela und in Deutschland, die versuchen bereits eine andere Gesellschaft zu leben oder aufzubauen. Er begleitet Menschen, die den Mut haben sich gemeinsam für einen Systemwandel stark zu machen – ziviler Ungehorsam nicht ausgeschlossen. Sie leben ihre Vision von Kooperation und teilen. Im Jahr 2019 begleitete der Film unter anderem auch die Aktion von Ende Gelände gegen die Tagebaue im Rheinland. Der Hambacher Forst und Alle Dörfer Bleiben kommen natürlich auch vor. Anschließend Diskussion mit der Filmemacherin Carmen Eckhardt. https://www.homocommunis.de/
