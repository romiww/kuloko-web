---
category: others
ext_url: ''
id: Feline_Przyborowski
place: Markt- und Theaterwiese
sample: ''
subtitle: Installation frei nach Anton Tschechow
summary_text: HEUL NICHT! Der Kirschgarten wird am 22.08. verkauft.  Und dann ist
  er weg.  (Oder?) Die Installation gibt Raum für Abschied, Konfrontation und Aufbruch.
  Denn wer sind wir, ohne Zuhause? Wer beraubt hier wen? Und wozu weinen?
taxonomy:
  tag:
  - installation
  - kunst
time_info: Mo 09.08. - So 15.08.
title: Feline Przyborowski - Kirschgärten

---

»Oh mein zärtlich geliebter herrlicher Kirschgarten! Mein Leben, meine Jugend, mein Glück, leb wohl! Leb wohl!«
HEUL NICHT! Der Kirschgarten wird am 22.08. verkauft.
Und dann ist er weg.
(Oder?)

Basierend auf Anton Tschechows Stück »Der Kirschgarten« und anderen Fremd- und Eigentexten lädt die Installation zum Verweilen und Erleben ein, stets balancierend zwischen Realität und Fiktion.
Sie gibt Raum für Abschied, Konfrontation und Aufbruch.
Denn wer sind wir, ohne Zuhause? Wer beraubt hier wen? Und wozu weinen?
» Das Leben ist vergangen, als ob ich gar nicht gelebt hätte.«
