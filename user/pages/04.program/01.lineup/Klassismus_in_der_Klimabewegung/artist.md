---
category: education
ext_url: ''
id: Kollektiv_Bildung_fur_utopischen_Wandel
name: Kollektiv Bildung für utopischen Wandel
place: Klimacamp Rheinland | WSZ 03
sample: ''
subtitle: Workshop
summary_text: ''
taxonomy:
  tag:
  - klassismus
  - organisierung
  - workshop
time_info: Fr 13.08. | 14:30 - 16:30
title: Klassismus in der Klimabewegung

---

Klimagerechtigkeit bedeutet, soziale und Klima-Themen zu verbinden. Allerdings sind Arbeiter*innen und Menschen mit wenig Geld in der Klimabewegung stark untervertreten. Das liegt an kapitalistischer Ausbeutung und auch an klassistischen Strukturen in unserer Bewegung. Klassismus bezeichnet dabei die Diskriminierung und Unterdrückung von Menschen wegen ihrer sozialen Herkunft, ihres Einkommens oder Berufs. Im Workshop geht es darum, wie die Klimakrise Menschen im Kapitalismus unterschiedlich trifft, wo in der Klimabewegung Klassismus zu sehen ist und wie wir unsere Gruppen verändern und neue Bündnisse gegen Klassismus aufbauen können. Wir arbeiten interaktiv mit Methoden der non-formalen Bildung. 

Über die Referent*innen: Wir, das neugegründete Kollektiv „Bildung für utopischen Wandel“ (BuWa), machen politische Bildungsarbeit zu den Themen Klimagerechtigkeit, Erinnerung und Antidiskriminierung. Wir sind fünf Menschen, die seit 5 bis 10 Jahren in der politischen Bildungsarbeit und in der Klimagerechtigkeitsbewegung aktiv sind. 

