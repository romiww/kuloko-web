---
category: others
ext_url: facebook.com/Cabaretgoodtimes
id: Goodtimes_Cabaret
place: Markt- und Theaterwiese
sample: https://www.youtube.com/watch?v=E2DDMtCPoSE&t=2s
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - theater
  - performance
  - zirkus
time_info: ''
title: Goodtimes Cabaret - Zirkus, Musik, Comedy

---

Das Goodtimes Cabaret ist ein reisendes Spektakel und ein Gemisch aus Kabaret, Zirkus, Musik und Freakshow Elementen.
