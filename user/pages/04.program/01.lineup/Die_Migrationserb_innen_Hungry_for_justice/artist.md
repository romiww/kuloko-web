---
category: others
ext_url: https://www.prismaqueer.de/
id: Die_Migrationserb_innen
place: Wiesenbühne
sample: ''
subtitle: Postmigrantische & Postkoloniale Performance
summary_text: Klimagerechtigkeit ist ein zentrales Thema der letzten Jahre, aber aus
  welcher Position werden Forderungen gestellt? Wer steuert das Boot, in dem wir alle
  sitzen?
taxonomy:
  tag:
  - theater
  - performance
time_info: Di 10.08. | 20:00 - 21:00
title: Die Migrationserb:innen - Hungry for justice

---

Die Migrationserb:innen ist ein Projekt von Prisma Queer Migrants e.V. Die Strukturen der sogenannten „Hochkultur“ -so auch des Theaters- sind weiß und heteronormativ dominiert. Queere Theatermachende und/oder Theathermachende of Color werden häufig im Kontext eines oberflächlichen Diversity-Ansatzes einbezogen, ohne dass ihre Perspektiven grundlegend Einfluss auf die bestehenden Strukturen haben, bzw. die intersektionale Verschränkung von Diskriminierungserfahrungen bedacht werden. Mit den Migrationserb:innen möchte Prisma Queer Migrants e.V. Sehgewohnheiten auf der Bühne verändern. Wir glauben daran, dass die Bühne wesentlich intersektionaler gestaltet werden und insgesamt stärker demokratisiert werden muss. Dies kann nur über echte Powersharing geschehen. Aktuell finden 14 Schauspielende of Color bzw. mit Migrationserbe hier ein safer/braver Space und die Gruppe ist offen für Neuzugänge von Queeren Personen of Color, bzw. Straight Allies of Color.
