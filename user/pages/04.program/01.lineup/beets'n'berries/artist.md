---
category: music
ext_url: https://beets.de/
id: beets'n'berries
place: Hofbühne
sample: https://youtu.be/aYNJRpaaBD0
subtitle: Akustik-Pop
summary_text: Die Akustikband direkt aus dem bedrohten Keyenberg. Nur 10 m von Proberaum
  bis Bühne.
taxonomy:
  tag:
  - pop
  - akustik
time_info: So 08.08. | 18:45 - 20:00
title: beets'n'berries

---


