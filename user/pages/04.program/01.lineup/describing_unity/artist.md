---
category: music
ext_url: https://describingunity.bandcamp.com
id: describing_unity
place: Wiesenbühne
sample: ''
subtitle: synth folk punk
summary_text: punk music with ukulele, synth, and a buncha disdain for cops and authority
taxonomy:
  tag:
  - punk
  - singer-songwriter*in
  - psychedelic
time_info: Fr 13.08. | 14:00 - 15:00
title: describing unity

---


