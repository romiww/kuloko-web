---
category: music
ext_url: https://www.instagram.com/lisaweddings_/
id: Lisa_Schlipper_Ralf_Hintzen
place: Hofbühne
sample: ''
subtitle: Balladen, Pop, Aktuelles
summary_text: Bekannte Songs neu interpretiert als Pianoversionen. Lisas Familie stammt
  aus dem bedrohten Dorf Unterwestrich.
taxonomy:
  tag:
  - balladen
  - pop
  - lokales
time_info: So 08.08. | 15:00 - 15:45
title: Lisa Schlipper & Ralf Hintzen

---


