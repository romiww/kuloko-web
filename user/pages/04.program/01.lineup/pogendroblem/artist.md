---
category: music
ext_url: https://pogendroblem.bandcamp.com/
id: pogendroblem
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=s6GOUjtfAlc
subtitle: punk
summary_text: pogendroblem klingen nach 80s-Punk mit Garage-Einflüssen und poppiger
  Niedlichkeit. Inhaltlich changiert alles zwischen Angepisstheit, utopischem Begehren
  und kühler Hoffnungslosigkeit.
taxonomy:
  tag:
  - punk
time_info: Fr 13.08. | 16:00 - 17:00
title: pogendroblem

---

Wie wollen wir miteinander umgehen? – Anders! So viel steht fest für pogendroblem, die sich nun fast vollends von provinziellen Jugendlichen aus Bergisch Gladbach zu urbanen Punx transformiert haben.  Jetzt geht es darum das Potential, von dem immer alle reden, schneller durch falsche Entscheidungen, unüberlegte Veröffentlichungen, schlechte Ernährung und zu viele Konzerte zu verballern als Nicolas Cage Rollen zusagt // eure Miete steigt // die Polarkappen schmelzen (you get it), bis dass Lohnarbeit und Kleinfamilie sie scheiden. pogendroblem klingen nach 80s-Punk mit Garage-Einflüssen und poppiger Niedlichkeit. Inhaltlich changiert alles zwischen Angepisstheit, utopischem Begehren und kühler Hoffnungslosigkeit. Privat sind sie manchmal etwas sassy aber voll nett – außer ihr seid Nazis oder Macker.
