---
category: education
ext_url: ''
id: Adrian_Blount
name: Adrian Blount
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - intersektionalitaet
  - intersectionality
  - empowerment
  - bpoc-only
time_info: Sa 14.08. | 14:30 - 16:30
title: Intersectionality through movement

---

Intersectionality Through Movement for Activists and Organizers. For QTBIPOC only. This workshop centers QTBIPOC safety and relaxation.  

A practice in holding space for each other through movement by ADRIAN BLOUNT.

Through breathing and affirmation, we will discuss intersectionality, what it means and looks like to hold space for our varying experiences with intersecting/differing identities, how/what it looks like to practice that in activist and organizational work, and how we can effectively interpret our conversation into collective movement thereby reinforcing community work and collective nurturing. This workshop references somatic body work.

*The workshop requires person to person contact. Those who are not comfortable with person to person contact can notify the instructor beforehand to discuss. 

