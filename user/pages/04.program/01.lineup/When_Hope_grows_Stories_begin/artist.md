---
category: others
ext_url: ''
id: When_Hope_grows_Stories_begin
place: ''
sample: ''
subtitle: Eine Reise durch Geschichten des Widerstands
summary_text: Wo beginnt Widerstand? Wer gibt uns die Kraft für den ersten Schritt?
  Und wer den Mut weiterzumachen? Lasst euch von den Geschichtenerzähler*innen Annika
  & Alex auf eine Reise zu den Ursprüngen unseres Widerstands & zur Kraft widerständiger
  Gemeinschaften, zu Zweifeln, Wut & Hoffnung mitnehmen.
taxonomy:
  tag:
  - storytelling
time_info: ''
title: When Hope grows, Stories begin

---


