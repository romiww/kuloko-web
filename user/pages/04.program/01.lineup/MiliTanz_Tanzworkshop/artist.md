---
category: others
ext_url: ''
id: MiliTanz
place: Markt- und Theaterwiese
sample: ''
subtitle: Workshop
summary_text: ''
taxonomy:
  tag:
  - ''
time_info: Sa 14.08. | 11:00 - 14:00
title: MiliTanz - Tanzworkshop

---

Wir wollen politische Messages durch Tanz und Performances emotional zugänglich machen, außerdem auch Aktivistis ein Tool geben sich während Aktionen zu empowern und zusammen Spaß zu haben! Wir wollen eine Performance zum Thema "Körper im Kapitalismus" kreieren und mit euch auf die Bühne bringen.
