---
category: education
ext_url: ''
id: Dorothee_Haussermann
name: Dorothee Häußermann
place: Schlosswiese | WSZ 04
sample: ''
subtitle: Die Klimakrise im Roman
summary_text: ''
taxonomy:
  tag:
  - lesung
  - roman
  - aktivismus
time_info: Di 10.08. | 20:00 - 21:30
title: „Wind aus Nord-Süd“

---

Eine namenlose Gruppe sprengt im Namen des Klimaschutzes ein Flugzeug nach Bangkok in die Luft. Weitere ökologisch motivierte Brandanschläge folgen. Im Schatten dieser Ereignisse erzählt der Roman von der Freundschaft dreier Frauen, die politisch aktiv sind und dabei äußerst unterschiedliche Herangehensweisen wählen. Ihre Lebenswege bringen ein Auf und Ab von Nähe und Entfremdung – und eine von ihnen verschwindet eines Tages völlig.
Die Autorin wird einige Stellen aus dem Roman vorlesen und freut sich dann über einen Austausch mit euch.
Zur Autorin: Dorothee Häußermann organisiert zusammen mit ihrer Politgruppe ausgeCohlt Kampagnen für Kohleausstieg und Klimagerechtigkeit; gibt Workshops und Pressetrainings, und findet, dass zwischendrin Zeit sein muss zum Schreiben, Lesen, Kaffeetrinken.
