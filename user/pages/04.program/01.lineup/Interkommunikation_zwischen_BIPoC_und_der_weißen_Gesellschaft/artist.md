---
category: education
ext_url: ''
id: Mamadou_Bah
name: Mamadou Bah
place: Klimacamp Rheinland | WSZ 02
sample: ''
subtitle: 'Communication between Bipoc and the white (dominated) society '
summary_text: ''
taxonomy:
  tag:
  - workshop
  - rassismus
time_info: Sa 14.08. | ? - 16:30
title: Interkommunikation zwischen BIPoC und der weißen Gesellschaft

---

In dem Workshop wird es um die Probleme zwischen diesen zwei Gruppen gehen und wie man diese lösen kann. Ich möchte sowohl die Perspektive von weißen Menschen als auch die von Menschen, die Rassismuserfahrungen machen beleuchten und untersuchen. Eines der wichtigsten Dinge am Menschsein ist die kommunikation. Ich möchte dass wir uns zusammen setzen und sowohl miteinander sprechen als und auch darüber austauschen, wie wir Wege finden können Diskriminierung abzubauen und uns gegenseitig zu verstehen.

The workshop deals with the communication problems between Bipoc and the white privilege society and also finding better ways to resolve these issues among the two groups. My goal is to examin and enlighten the Bipoc and white(dominated society) perspectives. The most important characteristic of humankind, is communication. I want us to sit together on the same table, exchange ideas about how to find the best possible way to understand each other, and to fight against racism, discrimination and stereotypes for a better society.
