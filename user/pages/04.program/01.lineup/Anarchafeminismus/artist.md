---
category: education
ext_url: ''
id: Marie_Maru
name: Marie & Maru
place: MaStaMo
sample: ''
subtitle: Workshop
summary_text: ''
taxonomy:
  tag:
  - feminismus
  - workshop
time_info: Di 10.08. | 11:45 - 13:15
title: Anarchafeminismus

---

Wir stellen euch verschiedene feministische Strömungen vor &
      leiten anhand diesen den Anarchafeminismus her, anschließend
      erläutern wir die Notwendigkeit einer anarchafeministischen Praxis
      &
      betrachten gemeinsam die feministische Bewegung im
      deutschsprachigen
      Raum. Zum Abschluss schauen wir uns die Kampfstrategie einer
      südamerikanischen anarchistischen Organisation, in Form eines
      Ringmodells an & brainstormen gemeinsam was wir davon für
      unsere
      lokalen feministischen Bewegungen mitnehmen können. 

    
    

    
