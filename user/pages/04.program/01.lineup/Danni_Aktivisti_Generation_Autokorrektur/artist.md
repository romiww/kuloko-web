---
category: others
ext_url: ''
id: Danni_Aktivisti
place: Scheunenkino
sample: ''
subtitle: Filmvorführung mit Filmgespräch in deutscher Sprache mit englischen Untertiteln
summary_text: 'Ein musikalischer Vagabund möchte im Dannenröder Wald eigentlich nur
  einen kurzen Stopp seiner Fahrradtour einlegen – entscheidet dann aber, sich der
  Polizei und den Maschinen zusammen mit einer Trommelgruppe im Baumhaus entgegenzustellen.
  FILMGESPRÄCH mit Aktivist:innen aus dem Danni nach der Doku. '
taxonomy:
  tag:
  - ''
time_info: Mi 11.08. | 17:00 - 19:00
title: 'Danni Aktivisti - Generation Autokorrektur '

---

Stets begleitet von Rhythmen, Harmonien und Poesie direkt und inspiriert
 von den Protesten nimmt uns die bunt gepinselte Doku mit auf eine Reise
 in den Wald, wo durch endothermes Bestreben einiger weniger Menschen 
eine politische Oase entstand, in der sich marxistisch-anarchistischer 
Freiheitskampf auf wilde Art und Weise mit Thunbergschem Klimaaktivismus
 verband. Wer hier welchen Film schiebt wird erst richtig klar, als 
Rodungstrupps auf Bäume, Polizisten auf Aktivisten und Politiker auf 
Bürger treffen…
