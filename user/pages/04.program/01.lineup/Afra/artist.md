---
category: music
ext_url: https://aframusik.de
id: Afra
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=zOgfgWfbU7Q
subtitle: Soul/Pop
summary_text: ''
taxonomy:
  tag:
  - soul
  - pop
time_info: Mi 11.08. | 22:00 - 23:00
title: Afra

---

Afra`s Musik ist ein Mix aus Soul, Jazz, Pop, HipHop und Afrobeat. Ihre deutschsprachigen Texte sind nachdenklich, empowernd, sozialkritisch und versprühen doch immer eine große Portion Optimismus. Mit ihrer Liveband spielt sie seit 2017 Konzerte und Festivals. Ihr 1. Album kommt im Frühjahr 2022.
