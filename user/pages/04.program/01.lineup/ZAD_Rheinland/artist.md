---
category: education
ext_url: ''
id: ''
name: ''
place: Klimacamp Rheinland | Treffen am Infopunkt
sample: ''
subtitle: Hüttenbauworkshop
summary_text: ''
taxonomy:
  tag:
  - ''
time_info: Sa 14.08. - So 15.08. | immer 10:30 - 16:30
title: ZAD Rheinland

---

Damit wir im Herbst in der ZAD Rheinland gut vorbereitet sind wollen wir mit euch Hütten bauen, in denen man widerständig leben und blockieren kann. Dafür ist dieser Workshop. Ihr braucht keinerlei vorerfahrung im bauen um teilzunehmen.
