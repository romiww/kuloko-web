---
category: music
ext_url: http://byggesett-orchestra.de
id: Byggesett_Orchestra
place: Hofbühne
sample: https://youtu.be/etcDN0ecuTk
subtitle: Electronic Groove Trance Drone
summary_text: Tiefe Bässe, treibende Grooves auf sphärischen Soundscapes.
taxonomy:
  tag:
  - electronic
  - pop
  - klangwelten
time_info: Fr 06.08. | 22:00 - 23:00
title: Byggesett Orchestra

---

Das niederrheinische Drone-Groove-Electronic Projekt kommt als Trio in die Dörfer. Ohne den im Urlaub verweilenden Trompeter werden die Herren Körfer, Hasselmann und Sehrbrock DeepGrooves und TribalElectronics präsentieren. Liveshows und Alben des Byggesett Orchestra gelten seit Jahren als Garant für Musik, die Kopf und Beine gleichermaßen beansprucht.
