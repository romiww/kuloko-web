---
category: education
ext_url: ''
id: Alina_Lukas
name: Alina & Lukas
place: Schlosswiese | WSZ 04
sample: ''
subtitle: (De-)konstruktion von Dörflichkeit
summary_text: ''
taxonomy:
  tag:
  - utopie
  - workshop
time_info: Fr 13.08. | 10:00 - 12:30
title: Ländliche Utopien 1

---

Welche Bilder des ländlichen Raums werden durch Kinderbücher, und klischeeüberladene Fernsehschmonzetten und Landlust-Zeitschriften (re)produziert? Wie können wir neue Bilder des ländlichen Raums erdenken und positive Möglichkeitsräume von Ländlichkeit konstruieren? Im Workshop schauen wir uns gemeinsam mediale Darstellungen von Ländlichkeit an – danach zeichnen wir in einem Wunschkonzert neue Bilder des ländlichen Raums.
/KOMPOST – Konglomerat für den sozial-ökologischen Umbau ländlicher Regionen
instagram.com/hey_kompost
kompost_vernetzt@lists.riseup.net (lists.riseup.net/www/info/kompost_vernetzt) 
/ Alina konnte es kaum erwarten, die Engstirnigkeit ihres 300-Einwohner-Dorfes in Nordhessen zu verlassen. Aktuell versucht sie in Bonn kritische, feministische und antirassistische Perspektiven in die Agrarwissenschaften zu integrieren. 
/ Lukas wäre gerne Anarchist, interessiert sich für Commons und aktiviert sich hier und da für die Degrowth und Klimagerechtigkeits Bewegung. Zusammen mit Corona kam allerdings auch eine Lohnarbeit für ländliche Entwicklung um die Ecke. 
