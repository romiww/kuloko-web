---
category: music
ext_url: https://claraclasen.net/
id: Clara_Clasen
place: Hofbühne
sample: https://www.youtube.com/watch?v=jWTd0IndNr8
subtitle: Acoustic Rock
summary_text: Clara Clasen bringt mit ihrer Gitarre düster-theatralen Akustikrock
  auf die Bühne und verhandelt in ihren Songs Themen wie Macht, Trauma, Queerness
  und Fuckbois.
taxonomy:
  tag:
  - akusitk
  - rock
time_info: So 08.08. | 17:30 - 18:15
title: Clara Clasen

---

Clara Clasen ist die Gründerin einer kleinen, aber energischen Acoustic-Rock-Manufaktur mit Sitz in Bonn. Ihre Arbeit zeichnet sich durch ihren effektvoll variablen Gesang und ihre starke Stimme sowie durch ihr spannungsvolles Songwriting aus, das mit Einflüssen aus Blues, Punk und Pop spielt. Meist ist sie mit ihrer Westerngitarre anzutreffen, wagt sich zuweilen aber auch an die Ukulele oder ans Klavier. 2018 erblickte ihr erstes Studioalbum im Band-Arrangement, „Sugar & Morphine“ das Licht der Welt, 2020 folgte "Cyanide & Cream" beim Bonner Label Disentertainment.
