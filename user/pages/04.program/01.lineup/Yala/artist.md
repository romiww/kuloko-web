---
category: music
ext_url: ''
id: Yala
place: Markt- und Theaterwiese
sample: ''
subtitle: Singer/Songwriter*in
summary_text: ''
taxonomy:
  tag:
  - singer-songwriter*in
time_info: Do 12.08. | 15:00 - 15:45
title: Yala

---

Meine Musik ist ruhig, nachdenklich und schön. Sie kann Menschen ins Herz sprechen wenn sie sich darauf einlassen. Sie handelt  vom fragen und finden
vom zweifeln, und sich haltlos fühlen 
anpacken und loslassen, 
vom Wellen schlagen 
vom verwandeln 
vom träumen, vom lebendig sein, 
vom Licht das durchschimmert. Naja und eben auch  davon wie mensch sich im Kapitalismus so durchschlägt und die Hoffnung nicht verliert.
