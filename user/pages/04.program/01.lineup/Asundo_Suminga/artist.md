---
category: music
ext_url: https://open.spotify.com/artist/09obEKlzItlNAOD4MGQbs0?si=wvKQWp-JTr6TRZWqfC4P5w&dl_branch=1
id: Asundo_Suminga
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=jOvETPRyMiA
subtitle: Rap/ Hip-Hop/ African Dancehall
summary_text: 'Vielfältige Musik: Mal Rap, mal African Dancehall. Manchmal ernst,
  häufig verpsielt und mit einem Augenzwinkern. Alles mit viel Liebe selbstgemacht
  und mit ordentlich Power dahinter!'
taxonomy:
  tag:
  - rap
  - hiphop
  - dancehall
time_info: Mi 11.08. | 20:00 - 20:40
title: Asundo Suminga

---


