---
category: education
ext_url: ''
id: ''
name: ''
place: Klimacamp Rheinland | Zirkuszelt
sample: ''
subtitle: A decolonial, feminist perspective  - with Paulina Trejo Mendez
summary_text: ''
taxonomy:
  tag:
  - podium
  - organisierung
time_info: Fr 13.08. | 17:30 - 19:00
title: How to make our actvist places more inclusive?

---

Facilitated by Lea Latoya
