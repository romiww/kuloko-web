---
category: others
ext_url: https://oekoterroristin.wordpress.com/
id: Ronan_Winter
place: Unser aller Wald
sample: ''
subtitle: Ein Jugendroman über Klimaaktivismus von Ronan Winter
summary_text: ''
taxonomy:
  tag:
  - lesung
  - aktivismus
time_info: Mi 11.08. | 19:30 - 20:30
title: 'Ronan Winter - Lesung und Gespräch: "Ökoterroristin"'

---

"Ökoterroristin" ist ein queerfeministischer Abenteuerroman, eine Coming-of-Age Geschichte und ein Handbuch zum aktiv werden.

Mika ist eine ganz normale Siebzehnjährige: Sie hasst Schule, mag Mangoeis und will den Kapitalismus stürzen.
Als die Polizei eine Hausdurchsuchung bei ihr macht, weil sie verdächtigt wird, Maschinen der Firma Kerogreen sabotiert zu haben, platzt Mika der Kragen: Der Energiekonzern hat eine Genehmigung erlangt, in der Nähe der Stadt nach Schiefergas zu bohren und schreckt dabei vor keiner Umweltzerstörung zurück. Zusammen mit ihren Freundinnen Nova und Sana nimmt Mika den Kampf gegen den Klimawandel auf.
Und dann ist da auch noch Aaron, mit dem Mika sich stundenlang über Anarchie unterhalten kann, und ein Sommer, der nach Revolution schmeckt. 

Taschenbuch: black-mosquito.org/de/okoterroristin-der-klimawandel-wartet-nicht-bis-du-die-schule-fertig-hast.html
Instagram: oeko_terroristin 
