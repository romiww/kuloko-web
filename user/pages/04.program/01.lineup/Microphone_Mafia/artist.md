---
category: music
ext_url: https://www.microphone-mafia.com
id: Microphone_Mafia
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=R6WlsmM5p9Q
subtitle: Hip-Hop
summary_text: Die einzige Mafia, die die Welt braucht! Raps in Italienisch und Neapolitanisch,
  in Türkisch, Englisch und Deutsch. "International, multilingual, multicultural"
  – wie es in einem ihrer ersten Songs so schön heißt. Bekannt durch ihren unermüdlichen
  Einsatz gegen Rassismus und rechte Gewalt.
taxonomy:
  tag:
  - hiphop
  - rap
time_info: Sa 14.08. | 19:30 - 21:00
title: Microphone Mafia

---

Die einzige Mafia, die die Welt braucht !

Wie die Zeit vergeht: mittlerweile sind wir tatsächlich einer der ältesten aktiven Hiphop-Acts in Deutschland. 1989 wurde Microphone Mafia von einem Haufen 16- und 17-jähriger Kids in Köln-Flittard gegründet. Das ist der Stadtteil im Kölner Nordosten im Schatten der Bayer-Werke, wo wir alle herkommen und wo wir immer noch verwurzelt sind. Das ist unsere Heimat, ein Arbeiterstadtteil, kein Asi-Viertel, aber Reichtum wirst Du hier selten sehen. Wir sind keine Bürgerkinder, wir sind Proletenkinder, und wir sind sogar stolz darauf, denn wir mussten uns alles im Leben erkämpfen.

In Flittard, unserer home base, spricht man deutsch, türkisch, italienisch, jede Menge andere Sprachen – und natürlich Kölsch. Was für andere sensationell war oder auch revolutionär – für uns war es fast selbstverständlich, dass wir in den Sprachen rappen, mit denen wir aufgewachsen sind. Schon allein deshalb, weil keiner von uns so gut Englisch konnte, dass wir die Amis hätten nachahmen können. Also gab es bei uns von Anfang an Raps in Italienisch und Neapolitanisch, in Türkisch, Englisch und Deutsch. International, multilingual, multicultural – wie es in einem unserer ersten Songs so schön heißt.

Wir waren auch nie damit zufrieden, die jeweils gängigen Beats des amerikanischen Hiphop zu übernehmen. Wenn man aus verschiedenen Kulturen kommt, dann hat man einen reichen musikalischen Schatz, aus dem man seine eigene Musik entwickeln kann. Warum nicht eine türkische Zurna in den Hiphop einführen?? Oder eine neapolitanische Mandoline?? Oder eine Blasorchester vom Balkan?? So sind wir von Anfang an vorgegangen, und so war schon unser erstes Album „Vendetta“ aus dem Jahre 1996. Inzwischen gibt es 7 Mafia-Alben. Und wer wissen will, was wir außerdem noch veröffentlicht haben, auf wessen Alben wir noch mitgewirkt haben, der möge sich auf unserer website www.microphone-mafia.com
die Rubrik „Diskografie“ ansehen. Von Anfang an war uns klar, dass wir mehr zu sagen haben als ihn reine Battle Rhymes rein passt. Wir haben uns in unseren Texten immer mit dem Leben befasst, mit Hoffnungen, Träumen, aber auch Enttäuschungen. Und plötzlich war die Mafia in Deutschland das Aushängeschild im Kampf gegen Rassismus und rechte Gewalt. Und das lebende Beispiel dafür, dass verschiedene Kulturen in diesem Land zusammen leben können. Wir haben uns diese Rolle nicht ausgesucht, aber wir akzeptieren sie gern.

Am Anfang waren wir 6, aber im Laufe der Jahre ist die Mafia kleiner geworden, seit 2002 gibt es 3 Mafiosi: Signore Rossi, den Kölschen Italiener, Asia und DJ Ra, die kölschen Türken. Zu dritt ziehen wir durch die Welt. Recording ist die eine Sache, aber vor allem gehört Hiphop auf die Bühne. Und deshalb sind wir auch unterwegs, so oft es uns möglich ist. Die Mafia auf Tour: es gibt kaum einen Ort in Deutschland, wo wir nicht schon aufgetreten sind. Und im Rest der Welt ist auch sehr schön: in Tschechien und der Türkei, in Österreich und der Schweiz, in Frankreich, Ialien, Venezuela und Bolivien. Wir waren schon fast überall und bald fahren wir auf Europa Tour verbunden mit einer Deutschland Tour – Die Mafia keeps rollin’ und das schon seit 30 Jahren!!
