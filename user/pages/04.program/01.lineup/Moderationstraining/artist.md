---
category: education
ext_url: ''
id: Nisha_Noura
name: Nisha & Noura
place: Klimacamp Rheinland | WSZ 02
sample: ''
subtitle: 'Moderation ist wichtig für die Revolution! '
summary_text: ''
taxonomy:
  tag:
  - skill-share
  - workshop
  - organisierung
time_info: Do 12.08. | 10:30 - 12:30
title: Moderationstraining

---

Plenum ist wichtig für die Revolution! Plenum ist aber auch viel zu oft viel zu anstrengend und langwierig. Was helfen kann: eine gute Gesprächsleitung. Und genau damit wollen wir uns in diesem Workshop beschäftigen. Es wird um die Rolle der Moderator*in gehen, ihre Aufgaben, mögliche Methoden, Tipps und Tricks für Treffen, die uns und unsere Gruppen weiterbringen und stärken. Dabei werden wir auf unsere Erfahrungen zurückgreifen, learnings zusammen sammeln und direkt ein bisschen üben.
