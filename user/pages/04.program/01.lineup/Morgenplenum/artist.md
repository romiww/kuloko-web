---
category: others
ext_url: ''
id: Morgenplenum
place: MaStaMo | Zirkuszelt
sample: ''
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - skill-share
  - organisierung
  - partizipation
time_info: Mo 09.08. - Sa 14.08. | immer 09:30 - 10:30
title: Morgenplenum

---

Im Plenum organisieren wir unser Tagesprogramm und Du bist eingeladen, Dich mit deinen Skills, Themen und Angeboten einzubringen.
