---
category: education
ext_url: https://www.glokal.org/projekte/klimadesol-klima-dekolonial-und-solidarisch/
id: Tahir_Della
name: Tahir Della
place: Klimacamp Rheinland | WSZ 02
sample: ''
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - dekolonial
  - intersektionalitaet
  - antirassismus
time_info: Fr 13.08. | 14:00 - 17:00
title: Klimabewegung und die koloniale Kontinuität (digital)

---

Alle reden von der Dekolonialisierung, was bedeutet sie im Zusammenhang mit der Klimabewegung? Wie lässt sich die deutsche Bewegung zur Klimagerechtigkeit mit antirassistischen und postkolonialen Perspektiven zusammendenken? Organisiert von KlimaDeSol -Projekt Klima Dekolonial und Solidarisch: Die bundesdeutsche Klimabewegung -so wie wir sie seit vielen Jahren kennen, aber auch jüngere Bewegungen wie die fridays for future- hat sich als eine Bewegung herausgestellt, in denen weiße Debatten und Deutungshoheiten dominieren, die gleichzeitig "unter sich" bleibt und die Kämpfe der BPOC, diasporische Perspektiven und deren Vertreter*innen an den Rand drängt.Unser Ziel ist es, der Klimabewegung wichtige Impulse zu einer demokratischen, dekolonialen und solidarischen Bewegung um Klimagerechtigkeit zu geben, als auch dem bisher weiß – elitär und westlich dominierten Konzept von Klima eine dekoloniale, ganzheitliche und intersektionale Narrative entgegenzusetzen.
