---
category: music
ext_url: https://www.instagram.com/morpho.music
id: Morpho
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=fjVIw8esUIg
subtitle: Electronic Art Pop
summary_text: ''
taxonomy:
  tag:
  - electronic
  - pop
  - klangwelten
time_info: Fr 06.08. | 19:00 - 20:30
title: Morpho

---

morpho verschmilzt auf eigensinnige Weise akustische mit elektronischen Klängen. Es entsteht ein musikalischer Mix der sowohl eingängig als auch originell ist. Seit 2019 schreiben sie Songs über das Augenverschließen vor der Klimakrise, männliche Dominanz in Beziehungen, Vereinsamung durch Social Distancing und alles was ihnen sonst am Herzen liegt. Miriam Braun (vox, bass) und Luka Kleine (drums, synth) verbindet ihr breites musikalisches Interesse für Pop, Folk, Jazz und Electro. Was daraus entsteht nennen sie Electronic Art Pop
