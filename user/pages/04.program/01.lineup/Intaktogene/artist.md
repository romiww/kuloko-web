---
category: music
ext_url: https://www.instagram.com/intaktogene
id: Intaktogene
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=wRZNywLdlcA&list=PLx2Y4w9tXU3aNy3C6O2SKh_gY2-7hKrQw&index=6
subtitle: Melodic Techno/Progressive House
summary_text: Intaktogene kommt aus Berlin und spielt irgendwas zwischen melodischem
  Techno und Progressive House. Herz und Wumms muss es haben.
taxonomy:
  tag:
  - techno
  - house
  - dj
time_info: Fr 06.08. | 23:30 - 01:30
title: Intaktogene

---

Was macht man wenn man Gitarre, Cello und Schlagzeug spielt? Ganz genau Techno!
Den musikalischen Anfängen zum Trotz fand sich Intaktogene statt am Instrument durch Zufall hinterm DJ Pult. Nur zum Spaß. Doch aus Spaß wurde Leidenschaft und die Elektro Götter hatten andere Pläne!
Intakto… was genau eigentlich? „Entaktogen“, das Innere berührend - Musik mit Melodien die das Herz bewegen und ein Bass der bis in die Knochen wummert. Intaktogene liebt Techno und Techno liebt Intaktogene, Topf und Deckel, die große Liebe auf den ersten Bass!
