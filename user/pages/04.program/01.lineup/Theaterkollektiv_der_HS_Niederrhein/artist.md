---
category: others
ext_url: https://www.asta.hn/theater
id: Theaterkollektiv_der_HS_Niederrhein
place: Wiesenbühne (Fr&Sa), Theaterwiese (So)
sample: ''
subtitle: How to kill your Planet slowly
summary_text: Die intergalaktische Sendung „How to kill you Planet slowly“ zeigt die
  dümmsten Zivilisationen aller bekannten Galaxien. Viele extraterrestrische Gesellschaften
  scheitern schon an den einfachsten Herausforderungen. Bedauerlich - aber umso lustiger
  zum Zuschauen!
taxonomy:
  tag:
  - theater
time_info: 13.08. 13:00 & 14.08. 15:00 auf der Wiesenbühne | 15:08. 19:30 auf der
  Theaterwiese
title: Theaterkollektiv der HS Niederrhein

---

Die intergalaktische Sendung „How to kill you Planet slowly“ zeigt wie immer die dümmsten Zivilisationen aller bekannten Galaxien. Viele extraterrestrische Gesellschaften schaffen es nicht mal über die ersten Entwicklungsstufen, sondern scheitern schon an den einfachsten Herausforderungen. So bedauerlich das auch ist, umso lustiger ist es dabei zu zuschauen. Diesmal präsentieren unsere beliebten Moderations-Entitäten, die wohl seit vielen Sendungen die witzigste aller Zivilisationen. Ein Planet voller Aberglauben, voller verwirrender Rituale und Absurditäten. Diese Zweibeinigen Kohlenstoffbeutel nennen sich selbst „Menschen“ und ihren Planeten passend „Dreck“.
Ob diese „Menschen“ der Selbstauslöschung entgehen können, steht nicht in den Sternen, sondern erfahrt ihr in der nächsten Sendung.
