---
category: others
ext_url: ''
id: MaStaMo
place: MaStaMo
sample: ''
subtitle: Vernetzung
summary_text: ''
taxonomy:
  tag:
  - vernetzung
time_info: Sa 14.08. | 15:00 - 16:30
title: MaStaMo - Vernetzung, Ausblick, Andockmöglichkeiten

---

Du fandest es schön auf dem MaStaMo? Du möchtest Teil der MaStaMeute werden? Dann komm zu unserem Vernetzungsworkshop, in dem wir unsere Strukturen und Mitmachmöglichkeiten über die Kuloko hinaus vorstellen.
