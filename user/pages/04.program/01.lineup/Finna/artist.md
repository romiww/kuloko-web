---
category: music
ext_url: https://audiolithbooking.net/artist/finna/
id: Finna
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=yoVi1Cy8icc
subtitle: queerfeministischer Rap
summary_text: Liebe, Wut, Rotz und Glitzer treffen auf klare Statements und queerfeministisches
  Empowerment.
taxonomy:
  tag:
  - hiphop
  - rap
time_info: Fr 06.08. | 21:30 - 22:30
title: Finna

---

Rapperin Finna ist eine grinsende Rebellin mit Riesenstimme, die sich für sexuelle Selbstbestimmung, gegen Homophobie und Bodyshaming stark macht. Eine Powerfrau, die durch starke Softness und bestechende Ehrlichkeit nicht nur auffällt, sondern sich als bleibender Eindruck in die Herzen spielt.
