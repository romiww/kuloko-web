---
category: music
ext_url: https://linktr.ee/karmakind
id: Karmakind
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=d3oVTN6SefY
subtitle: Electronic Live Act
summary_text: KARMAKIND bewegen sich in den Zwischenwelten von Downtempo & Dub, tauchen
  in psychedelische elektronische Sphären ein und lassen immer wieder Raum für Improvisation.
  Sie nehmen dich mit auf eine intensive Reise durch experimentelle, fantasievolle
  und tanzbaren Sets.
taxonomy:
  tag:
  - electronic
  - klangwelten
  - dub
time_info: Do 12.08. | 23:00 - 00:00
title: Karmakind

---

Wie ein neugieriges Kind erforscht das in Bochum gegründete Projekt KARMAKIND die Welt der elektronischen Musik. Diese Herangehensweise bricht technoide Konventionen und öffnet neue Türen. Es entsteht eine lebendige Fusion zwischen Gesang, Querflöte, Gitarre und elektronischen Beats. Mal meditativ, mal verspielt, mal druckvoll und energetisch – alles kann passieren.
KARMAKIND bewegt sich frei zwischen den Welten des Downtempo und Dub, in psychedelischen Sphären von House und immer wieder im Überraschungsmoment der Improvisation. Mit Texten auf Arabisch, Kurdisch, Deutsch, Englisch und Spanisch entstehen experimentelle, tanzbare und fantasievolle Sets, die das Publikum unmittelbar auf eine Reise mitnehmen.
