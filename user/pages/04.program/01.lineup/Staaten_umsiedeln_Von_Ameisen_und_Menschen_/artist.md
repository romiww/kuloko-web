---
category: education
ext_url: https://www.instagram.com/golfpolokok/
id: Hanna_und_Lisa
name: Hanna und Lisa
place: ''
sample: ''
subtitle: Pecha-Kucha-Vortrag
summary_text: ''
taxonomy:
  tag:
  - workshop
  - Umsiedlung
time_info: ''
title: Staaten umsiedeln. Von Ameisen und Menschen.

---

Bei Bauprojekten oder anderen menschlichen Großprojekten werden oft ganze Dörfer aber auch komplette Staaten umgesiedelt. Immer wenn z. B. bei einem Autobahnbau oder einem Tagebau eine Ameisensiedlung dem Bauvorhaben im Weg ist, müssen die Nester gesetzlich geschützter Ameisenarten mit großer Aufregung der Betroffenen umgesiedelt werden. Wir wollen den Blick dafür öffnen, wie diese Umsiedlungen vonstattengehen und was dies für unsere nicht-menschlichen Mitbewohner:innen bedeutet. Die Ameisen, als Spezialistinnen für Siedlungsbau und Verteidigung, zeigen uns Mechanismen sich kollektiv, strategisch und selbstorganisiert der Umsiedlung entgegenzustellen. Darüber hinaus zeigen sie uns, wie solidarisch ein Wiederaufbau der Siedlung möglich ist.
