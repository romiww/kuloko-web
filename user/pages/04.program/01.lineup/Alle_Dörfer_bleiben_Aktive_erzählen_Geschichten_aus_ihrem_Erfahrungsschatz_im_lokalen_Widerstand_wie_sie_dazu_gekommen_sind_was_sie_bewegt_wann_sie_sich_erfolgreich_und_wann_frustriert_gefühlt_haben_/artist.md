---
category: others
ext_url: ''
id: ''
place: MaWaLü
sample: ''
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - storytelling
  - lokales
time_info: Mo 09.08. | 20:00 - 22:00
title: Alle Dörfer bleiben - Aktive erzählen Geschichten aus ihrem Erfahrungsschatz
  im lokalen Widerstand - wie sie dazu gekommen sind, was sie bewegt, wann sie sich
  erfolgreich und wann frustriert gefühlt haben.

---

Aktive erzählen Geschichten aus ihrem Erfahrungsschatz im lokalen Widerstand - wie sie dazu gekommen sind; was sie bewegt; wann sie sich erfolgreich und wann frustriert gefühlt haben.
