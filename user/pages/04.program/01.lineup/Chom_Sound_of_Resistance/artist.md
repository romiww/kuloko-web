---
category: music
ext_url: ''
id: Chom
place: Wiesenbühne
sample: ''
subtitle: Akkustik Gitarre & Gesang
summary_text: '* Eine musikalische Reise durch die Geschichte von Widerstand & Revolution
  * Musik ist eine besondere Form des politischen Ausdrucks, aber auch des kollektiven
  Erinnerns. Chom singt alte und neue widerständige Lieder und erzählt die historischen
  Kontexte.'
taxonomy:
  tag:
  - singer-songwriter*in
  - akustik
  - storytelling
time_info: So 08.08. | 17:00 - 18:00
title: Chom - Sound of Resistance

---

Eine musikalische Reise durch die Geschichte von Widerstand & Revolution. Von Arbeiter\*innenliedern bis zu Liedern gegen Kolonialherrschaft oder für feministische Kämpfe - Musik ist eine besondere Form des politischen Ausdrucks, aber auch des kollektiven Erinnerns. Machtzentren haben große Ressourcen, um die eigenen Deutungen der Welt zu verewigen - mit der Geschichte der Unterdrückten verhält es sich schwieriger. Chom singt alte und neue widerständige Lieder und erzählt die historischen Kontexte.
