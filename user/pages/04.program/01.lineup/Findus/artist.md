---
category: music
ext_url: ''
id: Findus
place: Wiesenbühne
sample: ''
subtitle: ''
summary_text: meine Musik soll Menschen berühren und verbinden
taxonomy:
  tag:
  - akustik
  - singer-songwriter*in
time_info: Sa 07.08. | 15:30 - 16:30
title: Findus

---

Ich bin Findus und schreibe Lieder über Dinge,die mich bewegen. Dabei bewegen sich die meisten Texte irgendwo zwischen persönlichen Beziehungen, meinen Wünschen für die Welt und meinem Entsetzen über ihren derzeitigen Zustand. Zurzeit verarbeite ich mit meinen Liedern vor allem die Erfahrungen, die ich in 7 Monaten inspirierenden Lebens & schmerzhafter Räumung im Dannenröder Wald gesammelt habe. Ich singe und begleite mich selbst auf der Gitarre.
