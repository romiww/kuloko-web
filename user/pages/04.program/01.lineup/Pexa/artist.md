---
category: music
ext_url: https://www.instagram.com/pexa_161/
id: Pexa
place: Wiesenbühne
sample: ''
subtitle: QueerRap, Hip Hop, Trap
summary_text: ''
taxonomy:
  tag:
  - rap
  - hiphop
  - trap
  - dj
time_info: Sa 14.08. | 23:30 - 01:30
title: Pexa

---

Pexa bringt einen Mix aus queeren und emanzipatorischen Statements mit tiefen Bassdrums und
schnellen Hi- Hats an den Decks. Der Rhythmus der Musik soll euch dazu animieren den Body mal
wieder richtig durch zu shaken und die Tanzfläche zum schwofen zu bringen, dabei soll eine
Atmoshäre geschaffen werden wo sie jede*r wohl und wollkommen fühlt.
