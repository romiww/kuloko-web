---
category: others
ext_url: ''
id: ''
place: MaStaMo | Zirkuszelt
sample: ''
subtitle: Open Stage
summary_text: ''
taxonomy:
  tag:
  - jamsession
  - open-stage
time_info: Mi 11.08. | 20:00 - 23:00
title: Danni/Gäst_innenhaus - Offene Lese- & Musikbühne

---

Komm gerne vorbei, bring deine Texte und Instrumente mit und lass unser Zirkuszelt zu einem kreativen Raum werden.
