---
category: music
ext_url: https://eze-warcenciel.com
id: Eze_Wendtoin
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=H_lfVO2r0Ps
subtitle: Chanson, Afro-Folk, Latino & westafrikanische Rhythmen
summary_text: Am Liebsten dichtet, komponiert und singt Ezé auf Deutsch, aber auch
  auf Französisch und in seiner Muttersprache Mooré. Er spielt Schlagzeug, Gitarre
  und andere burkinische Instrumente und schreibt humorvolle, poetische und oft auch
  gesellschaftskritische Texte.
taxonomy:
  tag:
  - chanson
  - folk
  - pop
  - ''
time_info: Sa 07.08. | 21:00 - 22:00
title: Ezé Wendtoin

---

Ezé Wendtoin,

ist ein Künstler, der viele Eisen im Feuer hat. Er ist sowohl Liedermacher und Musiker als auch Schauspieler, Moderator und Märchenerzähler. Ursprünglich stammt er aus einer Trommler-, Pfarrer- und Schmiedefamilie in Burkina Faso. Dort verliebt er sich in die Sprache von Goethe und beginnt mit großer Leidenschaft ein Germanistikstudium, das er ab 2016 im Master an der TU Dresden fortsetzt. Trotz zahlreicher Auftritte und Workshops neben her schließt er schon im Jahr 2018 sein Studium sehr erfolgreich ab.

Am Liebsten dichtet, komponiert und singt Ezé auf Deutsch, aber auch auf Französisch und in seiner Muttersprache Mooré. Er spielt Schlagzeug, Gitarre und andere burkinische Instrumente, und bewegt sich mit seiner Musik zwischen Chanson, Afro-Folk, Latino und westafrikanischen Rhythmen. Seine Texte sind humorvoll, poetisch und oft auch gesellschaftskritisch.

Ezés Musik verbindet Menschen und bringt Kulturen zusammen. Mit seiner Musik und seinen Workshops setzt er sich gegen jede Art von Diskriminierung ein und kämpft gegen Vorurteile – und das mit einer ansteckend guten Laune und einer schier unendlichen Energie.

Mit seinem in Burkina Faso gegründeten Verein APECA setzt sich Ezé für den Bau einer Schule für benachteiligte Kinder und junge Frauen in der Umgebung Ouagadougous, der Hauptstadt Burkina Fasos, ein.
