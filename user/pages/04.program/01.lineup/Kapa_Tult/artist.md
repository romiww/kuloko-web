---
category: music
ext_url: https://kapa-tult.de/
id: Kapa_Tult
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=2nFtppeFwcE
subtitle: Indie-Pop
summary_text: 'An alle, die rastlos zwischen Alltag und Revolte pendeln: Have a Kapa
  Tult, es ist sehr gut und klingt schön.  Obwohl es auf Deutsch ist!'
taxonomy:
  tag:
  - pop
time_info: Sa 14.08. | 18:00 - 19:00
title: Kapa Tult

---

Die Band Kapa Tult besteht seit 2018 aus Inga (Gitarre/Gesang), Paul (Bass) und Angi (Schlagzeug). Neu dabei ist Robin am Keyboard. Seit ihrer Gründung haben Kapa Tult zusammen über 40 Konzerte gespielt, u.a. auf diversen Festivals (z.B. Musikschutzgebiet, Kulturzelt Kassel, Mind The Gap Open Air, Feministischer Streik Leipzig) im Kasseler Umland sowie in Leipzig, wo die Band lebt und arbeitet. Beim FM4 Protestsongcontest 2018 in Wien erreichten Kapa Tult mit dem Song „Priority Lane“ den 2. Platz. Im Januar 2022 erscheint Kapa Tults Debüt-EP. Auf „Meinten Sie Katapult?“ rechnen sie mit dem Bösen ab: Schulsystem, Klimakatastrophe, Schöheitsideale und der Mutter, die darauf wartet, dass man endlich mal eine ernsthafte Liebesbeziehung führt.
