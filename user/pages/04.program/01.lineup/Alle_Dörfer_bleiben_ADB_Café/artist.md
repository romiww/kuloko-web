---
category: others
ext_url: ''
id: Alle_Dorfer_bleiben
place: Hofbühne
sample: ''
subtitle: auf dem Winzenhof
summary_text: ''
taxonomy:
  tag:
  - café
time_info: So 08.08. | 14:00 - 18:00
title: Alle Dörfer bleiben - ADB Café

---

Bei lecker selbstgemachtem Kuchen und frisch gebrühtem Kaffee gibt es die Möglichkeit noch vor dem Bühnenprogramm ins Gespräch zu kommen - mit Menschen aus dem lokalen Widerstand und allen die sich noch auf dem Winzenhof einfinden. Alle Dörfer bleiben
