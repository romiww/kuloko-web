---
category: education
ext_url: ''
id: Lexy_vom_Bildungskollektiv_Herrschaftsfreie_Welt_Beziehungen"
name: Lexy vom Bildungskollektiv „Herrschaftsfreie Welt-/Beziehungen“
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: Die Lust am Zweifeln
summary_text: ''
taxonomy:
  tag:
  - feminismus
  - workshop
time_info: Fr 13.08. | 14:30 - 16:30
title: Kritische Männlichkeit*en

---

Die vorherrschenden Vorstellungen von Männlichkeit verursachen Gewalt gegenüber Frauen* und queeren Menschen. Gleichzeitig werden auch Männer* von allzu engen Geschlechternormen eingeschränkt. Dieser Workshop möchte dazu einladen, beide Perspektiven miteinander zu verbinden: die Kritik an (der eigenen Mitwirkung in) patriarchalen Strukturen und das lustvolle Experimentieren im Bezug auf Geschlechtsidentitäten. Mithilfe von auto-biographischer Reflexion, kurzen Impulsen und Rollenspielen vermittelt der Workshop ein Grundverständnis davon, was Männlichkeit eigentlich ist, wie sie wirkt und wie wir uns ihrer Macht widersetzen können. Der Workshop ist offen für alle Geschlechter.
