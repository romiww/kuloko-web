---
category: others
ext_url: ''
id: Kurt_Lehmkuhl
place: Markt- und Theaterwiese
sample: ''
subtitle: Literatur
summary_text: Die beiden Kriminalromane "Begraben in Garzweiler II" aus dem Jahr 1996
  und "Kohlegier" von 2016 stellen die Situation im Tagebaugebiet von Erkelenz und
  im Rheinischen Revier dar. Nach wie vor sind die Texte hochaktuell und dokumentieren
  die Entwicklung des Widerstands.
taxonomy:
  tag:
  - lesung
time_info: Mi 11.08. | 17:00 - 18:00
title: Kurt Lehmkuhl

---

haben nichts von ihrer Aktualität verloren. Sie
