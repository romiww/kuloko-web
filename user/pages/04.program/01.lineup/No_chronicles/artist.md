---
category: music
ext_url: https://bandcamp.nochronicles.com
id: No_chronicles
place: Wiesenbühne
sample: ''
subtitle: Riot Jazz punk
summary_text: No chronicles sind ein female bass schlagzeug duo mit 2 stimmigen Gesang.
  Eine Schublade ist für Ihre Musik nicht zu finden... Da braucht es eher ein großes
  Regal um die fasettenreiche Musik von lieblich über schräg bis rau zu beschreiben.
taxonomy:
  tag:
  - jazz
  - punk
time_info: Do 12.08. | 20:00 - 21:00
title: No chronicles

---


