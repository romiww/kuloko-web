---
category: others
ext_url: https://defend-kurdistan.com/de/
id: Gemeinsame_Anreise_"Defend_Kurdistan_Demo"
place: MaWaLü
sample: ''
subtitle: ''
summary_text: Gemeinsame Busanreise zur Defend Kurdistan Demo in Düsseldorf, gegen
  die türkische Invasion in Südkurdistan
taxonomy:
  tag:
  - demo
  - aktivismus
  - internationalismus
time_info: Sa 14.08. - Sa 14.08. | immer 10:00 - 18:00
title: Gemeinsame Anreise "Defend Kurdistan Demo"

---

„Stell dir vor, es ist Krieg und keiner schaut hin“ – diese Abwandlung des Spruchs der alten
Friedensbewegung
 könnte Südkurdistan (Nordirak) meinen. Dort greift der türkische Staat 
seit dem 23. April die Regionen Zap, Metîna und Avaşîn ununterbrochen 
aus der Luft und mit tausenden Bodentruppen an. Die Türkei argumentiert 
bei ihrem Vorgehen mit einer angeblichen Gefahr für „ihr Territorium“ 
durch die Guerilla der PKK (Arbeiterpartei Kurdistans), die seit 
Jahrzehnten einen Großteil der südkurdischen Grenzgebiete kontrolliert 
und mit der Ankara ebenso lange in Konflikt steht – aufgrund der 
politisch ungelösten kurdischen Frage. Dabei werden offensichtlich auch 
international geächtete chemische Kampfstoffe eingesetzt, Wälder 
niedergebrannt oder abgeholzt und in den besetzten Gebieten neue 
Militärstützpunkte errichtet, um eine türkische Besatzung im Nordirak zu
 verstetigen. Die Angriffe richten sich auch systematisch gegen die 
dortige Zivilbevölkerung, deren Siedlungsgebiete und Anbauflächen 
täglich bombardiert werden.
