---
category: music
ext_url: https://www.instagram.com/pvc_provinzcrew/
id: Provinzcrew
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=T0NA5j7YQJM
subtitle: (Theater-) Rap
summary_text: Fette Beats, stimmungsvoller Rap und eine ganze Crew am Start. Die Texte
  der PVC sind sinnbefreit und witzig bis tiefgründig und gesellschaftskritisch. Die
  Musik ist vielfältig. Der Kern der Crew besteht aus den vier Rappis „Averell, Flokii,
  MC Purzelbaum, Mo Nita“ und einer DJ* „Intaktogene“.
taxonomy:
  tag:
  - rap
  - theater
time_info: Fr 06.08. | 17:00 - 18:30
title: Provinzcrew

---

Keiner kann so recht sagen was es ist, aber es sprudelt!
Die Provinzcrew entstammt der Erkenntnis, dass in ein Rapsong mehr Text passt, als in das konventionelle Lagerfeuerlied. Also hingen A&B die Gitarren an den Nagel, gingen in die Strahlenkammer und mutierten zu Rappern, Averell und Flokii. Eines Abends kam Gin aus einer verkorkten Flasche und sie wünschten sich eine Crew.
In jener Nacht, gab es einen Knall. Kein Erschreckender, Unheimlicher - Viel mehr ein liebkosender, erquickender Knall voll trockener Süße. Ein Korken flog durch die Luft und MC Purzelbaum war da.
Mo Nita tauchte der Geschichte nach einfach plötzlich auf. Wobei niemand mehr weiß, wo oder wann, und schon gar nicht mehr wie. Doch PVC ist sich einig, Mo führt zu mehr Sprudel.
Mehr Sprudel = Mehr Gut!
Nach einem Jahr Findungs- und Kennenlernphase, Konzerten auf Festivals, Berliner Studipartys, Fridays for Future, und in der Clubszene, released die Provinzcrew ihr erstes Album ”Nemo”.
Die Vielseitigkeit ist krass und jeder Track ist ein Kunstwerk. Noch krasser: Bald kommt die nächste Single "Demian"!
Und die Crew? Wächst! Ob L’Andrénalin im HQ ein Video schneidet, Luks ein Backdrop zaubert, Mighty Rehab zum Freidrehen einlädt oder Naladin, seinem Kater musikalisch Ausdruck verleiht. Hier haben alle ihren Platz.
Den Stil bezeichnen sie als Theater Rap. Sie sind gespannt in welche Schubladen Du sie steckst. Hoffentlich in eine Große, mit Kühlung und viel Platz für sprudelige, schaumweinartige Kaltgetränke.
