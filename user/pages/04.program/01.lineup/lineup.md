---
blog_url: program/lineup
content:
  items:
  - '@self.children'
  order:
    by: date
    dir: desc
  pagination: false
  url_taxonomy_filters: true
header:
- category: music
  id: Ami
  name: Ami
- category: others
  id: apgw
  name: apgw
- category: music
  id: Yala
  name: Yala
- category: music
  id: Piece_of_Peace
  name: Piece of Peace
- category: music
  id: Byggesett_Orchestra
  name: Byggesett Orchestra
- category: music
  id: Finna
  name: Finna
- category: music
  id: Intaktogene
  name: Intaktogene
- category: others
  id: kollektiv_eigenklang
  name: kollektiv eigenklang
- category: music
  id: Mariybu
  name: Mariybu
- category: music
  id: Mascha
  name: Mascha
- category: music
  id: Morpho
  name: Morpho
- category: music
  id: Provinzcrew
  name: Provinzcrew
- category: music
  id: ROB_SURE
  name: ROB SURE
- category: music
  id: Sascha_Katterbach
  name: Sascha Katterbach
- category: music
  id: BoringPain
  name: BoringPain
- category: music
  id: Eze_Wendtoin
  name: Ezé Wendtoin
- category: music
  id: Findus
  name: Findus
- category: music
  id: Jooles_Nick
  name: Jooles & Nick
- category: others
  id: Theaterwerkstatt_Bethel
  name: Theaterwerkstatt Bethel
- category: music
  id: The_Punkelelics
  name: The Punkelelics
- category: music
  id: Stefan_Pieck
  name: Stefan Pieck
- category: music
  id: Julie_and_me
  name: Julie and me
- category: others
  id: Wild_Roots_Performance_Collective
  name: Wild Roots Performance Collective
- category: music
  id: Sonori_b2b_Luna_X_Tatsachen_Jane
  name: Sonori b2b Luna X // Tatsachen & Jane
- category: education
  id: Klima_Allianz
  name: Klima-Allianz
- category: others
  id: Julia_Pesch
  name: Julia Pesch
- category: education
  id: Nisha
  name: Nisha
- category: others
  id: Armin_Wuhle
  name: Armin Wühle
- category: music
  id: Lisa_Schlipper_Ralf_Hintzen
  name: Lisa Schlipper & Ralf Hintzen
- category: music
  id: speechless
  name: speechless
- category: music
  id: Chom
  name: Chom
- category: music
  id: Clara_Clasen
  name: Clara Clasen
- category: music
  id: beets'n'berries
  name: beets'n'berries
- category: others
  id: Horsta
  name: Horsta
- category: education
  id: Theresa_Leisgang_Institute_of_Environmental_Justice
  name: Theresa Leisgang & Institute of Environmental Justice
- category: music
  id: Diva_Daneben
  name: Diva Daneben
- category: education
  id: Sergen_Canoglu
  name: Sergen Canoglu
- category: education
  id: Charlotte_Peltre_Philipp_Hofmann
  name: Charlotte Peltre & Philipp Hofmann
- category: education
  id: Eva_von_Redecker
  name: Eva von Redecker
- category: others
  id: Kaffee_Kuchen
  name: Kaffee&Kuchen
- category: others
  id: Daniel_Chatard
  name: Daniel Chatard
- category: education
  id: Fabian_Flues_Kathrin_Henneberger_Niels_Jongerius
  name: Fabian Flues, Kathrin Henneberger & Niels Jongerius
- category: education
  id: Camila_Schmid
  name: Camila Schmid
- category: education
  id: Zucker_im_Tank
  name: Zucker im Tank
- category: education
  id: Marie_Maru
  name: Marie & Maru
- category: education
  id: Alexander_Neupert_Doppler
  name: Alexander Neupert-Doppler
- category: education
  id: Riv
  name: Riv
- category: education
  id: Luca
  name: Luca
- category: education
  id: Melchi_Vepouyoum_Christoph_Ebel
  name: Melchi Vepouyoum & Christoph Ebel
- category: education
  id: KLARA_KOLLEKTIV
  name: KLARA KOLLEKTIV
- category: education
  id: Susanne_Fasbender_Peter_Emorinken_Donatus
  name: Susanne Fasbender & Peter Emorinken Donatus
- category: others
  id: Die_Migrationserb_innen
  name: Die Migrationserb:innen
- category: education
  id: Dorothee_Haussermann
  name: Dorothee Häußermann
- category: music
  id: Virus_Mensch
  name: Virus:Mensch
- category: others
  id: Autoren_im_Team
  name: Autoren im Team
- category: education
  id: Simon_Sutterlutti
  name: Simon Sutterlütti
- category: education
  id: Mihir_Sharma
  name: Mihir Sharma
- category: education
  id: _ausgestrahlt
  name: .ausgestrahlt
- category: others
  id: Sinja_und_Helene
  name: Sinja und Helene
- category: music
  id: Lebenslaute
  name: Lebenslaute
- category: education
  id: ''
  name: ausgeco2hlt
- category: others
  id: Kurt_Lehmkuhl
  name: Kurt Lehmkuhl
- category: others
  id: Ronan_Winter
  name: Ronan Winter
- category: music
  id: Asundo_Suminga
  name: Asundo Suminga
- category: music
  id: Arbeitstitel_Tortenschlacht
  name: Arbeitstitel Tortenschlacht
- category: music
  id: Afra
  name: Afra
- category: education
  id: KanguruKollektiv
  name: KänguruKollektiv
- category: education
  id: Initiative_Demokratischer_Konfoderalismus
  name: Initiative Demokratischer Konföderalismus
- category: education
  id: Kollektiv_Bildung_fur_utopischen_Wandel
  name: Kollektiv Bildung für utopischen Wandel
- category: education
  id: Jojo_Klick
  name: Jojo Klick
- category: education
  id: Antonia_Charlotte
  name: Antonia & Charlotte
- category: education
  id: Still_Burning_Network
  name: Still Burning Network
- category: music
  id: No_chronicles
  name: No chronicles
- category: education
  id: Karin_de_Miguel
  name: Karin de Miguel
- category: others
  id: Annika_Alex
  name: Annika & Alex
- category: music
  id: Fartuuna
  name: Fartuuna
- category: music
  id: Karmakind
  name: Karmakind
- category: education
  id: Autonome_feministische_Sommerschule
  name: Autonome feministische Sommerschule
- category: education
  id: Alina_Lukas
  name: Alina & Lukas
- category: education
  id: Mamadou_Bah
  name: Mamadou Bah
- category: music
  id: describing_unity
  name: describing unity
- category: music
  id: pogendroblem
  name: pogendroblem
- category: music
  id: Pappnasen_Rotschwarz
  name: Pappnasen Rotschwarz
- category: education
  id: Emergentes_Coopar_Comunicacion_Graswurzel_tv_
  name: 'Emergentes, Coopar Comunicación & Graswurzel.tv. '
- category: music
  id: HerrinGedeck
  name: HerrinGedeck
- category: education
  id: Sara_Huther_Sita_Scherer
  name: Sara Hüther & Sita Scherer
- category: music
  id: Kollektiv_Abschaum
  name: Kollektiv Abschaum
- category: others
  id: Riadh_Ben_Ammar
  name: Riadh Ben Ammar
- category: music
  id: missratene_Tochter
  name: missratene Töchter
- category: others
  id: Goodtimes_Cabaret
  name: Goodtimes Cabaret
- category: education
  id: Tobi_Rosswog
  name: Tobi Rosswog
- category: education
  id: Zade_Abdullah_Noura_Hammouda
  name: Zade Abdullah & Noura Hammouda
- category: music
  id: Daizen
  name: Daizen
- category: education
  id: Tahir_Della
  name: Tahir Della
- category: education
  id: Adrian_Blount
  name: Adrian Blount
- category: education
  id: Feuerquallen_Kollektiv
  name: Feuerquallen Kollektiv
- category: others
  id: Militanz
  name: Militanz
- category: music
  id: Heart_Core
  name: Heart Core
- category: education
  id: Philip_Noack
  name: Philip Noack
- category: music
  id: Kapa_Tult
  name: Kapa Tult
- category: music
  id: T_r_anzgeschichten
  name: T_r_anzgeschichten
- category: music
  id: Microphone_Mafia
  name: Microphone Mafia
- category: music
  id: FaulenzA
  name: FaulenzA
- category: music
  id: Pexa
  name: Pexa
- category: others
  id: Bildungskollektiv_Bonn
  name: Bildungskollektiv Bonn
- category: music
  id: be_la
  name: be là
- category: education
  id: Jani
  name: Jani
- category: music
  id: Bonbonlegerin
  name: Bonbonlegerin
- category: music
  id: Johanna_Zeul
  name: Johanna Zeul
- category: music
  id: Fold
  name: Föld
- category: music
  id: Rap!fugees
  name: Rap!fugees
- category: others
  id: Lady_Kitty's_Hell's_Belles
  name: Lady Kitty´s Hell´s Belles
- category: music
  id: Seasoul
  name: Seasoul
- category: education
  id: Gegengrau
  name: Gegengrau
- category: others
  id: Theaterkollektiv_der_HS_Niederrhein
  name: Theaterkollektiv der HS Niederrhein
- category: others
  id: Feline_Przyborowski
  name: Feline Przyborowski
- category: education
  id: Aie_Al_Khaiat_Gornig
  name: Aie Al Khaiat-Gornig
- category: others
  id: Ende_Gelande
  name: 'Ende Gelände

    '
- category: others
  id: Ende_Gelande_Bochum
  name: Ende Gelände Bochum
- category: music
  id: Die_schon_wieder
  name: Die schon wieder
- category: education
  id: Moko_Zui
  name: Moko & Zui
- category: others
  id: Unser_Aller_Wald
  name: Unser Aller Wald
- category: others
  id: Danni_Aktivisti
  name: Danni Aktivisti
- category: education
  id: Lexy_Bildungskollektiv_Herrschaftsfreie_Welt_Beziehungen"
  name: Lexy, Bildungskollektiv „Herrschaftsfreie Welt-/Beziehungen"
- category: others
  id: Bazon_Brock
  name: Bazon Brock
hero_background: /user/pages/images/opencall_music.jpeg
hero_foreground: /user/pages/images/fist-with-mic-bar.svg
hero_title_color: light
menu: Lineup
show_breadcrumbs: false
show_pagination: false
show_sidebar: false
title: Lineup

---
