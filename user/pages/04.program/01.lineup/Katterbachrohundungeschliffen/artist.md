---
category: music
ext_url: ''
id: Katterbachrohundungeschliffen
place: 'Wiesenbühne '
sample: ''
subtitle: 'Singer Songwriter '
summary_text: Lieder aus roher Kehle und ungeschliffener Seele auf kraftvollem Akustikgitarrenfundament
taxonomy:
  tag:
  - singer-songwriter*in
time_info: ''
title: Katterbachrohundungeschliffen

---

Sascha Katterbach singt Lieder aus roher Kehle und ungeschliffener Seele. Atmosphärisch, kraftvoll, echt. Der aus Aachen stammende Gitarrist und Sänger, der dieses Jahr sein Debutalbum „farbenbrauchenlicht“ veröffentlichen wird, nimmt einerseits kein Blatt vor den Mund, wenn es um gesellschaftskritische Themen geht. Andererseits geht er feinfühlig und liebevoll mit Themen des Alltags um, läßt dabei immer Raum, zwischen den Zeilen zu lesen und macht sie durch seinen starken harmonischen Unterbau fühlbar.
