---
category: music
ext_url: http://www.robsure.com/
id: ROB_SURE
place: Hofbühne
sample: ''
subtitle: Pop
summary_text: ROB SURE - in the air tonight...  Mitreißende Akustik-Interpretationen
  von Pop-Klassikern mit bisweilen mehr instrumentaler Präsenz und Lebendigkeit als
  eine ganze Band.  www.robsure.com
taxonomy:
  tag:
  - pop
  - cover
time_info: Fr 06.08. | 20:30 - 21:30
title: ROB SURE

---

ROB SURE - in the air tonight...

Wenn ein Musiker mit viel Stimme und perkussivem Drive der Akustik-Gitarre seine eigenen Interpretationen von Pop-Klassikern live auf der Bühne präsentiert, klingen (alt)bekannte Hits auf eine mitreißende Art neu und aktueller denn je.
Angereichert mit Blues-Harp, Foot-Stomp, Electro-Percussion und Live-Loops bietet Rob bisweilen mehr instrumentale Präsenz und Lebendigkeit als eine ganze Band. Vom ersten Stück bis zur dritten Zugabe gibt's Musik für die Seele, den Bauch und die Beine mit einer gehörigen Portion Gänsehautgefühl.

All night long… breaking the habit!

www.robsure.com
