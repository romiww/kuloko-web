---
category: others
ext_url: ''
id: Feldarbeit_in_der_Praxis
place: Klimacamp Rheinland
sample: ''
subtitle: Gemeinsame Anfahrt zum Feld in MG
summary_text: Woher kommt das Gemüse, das hier verkocht wird? Ein Bio Bauer in Mönchengladbach
  braucht eure Unterstützung auf dem Feld.
taxonomy:
  tag:
  - ''
time_info: Di 10.08. - Fr 13.08. | immer 11:00 - 16:00
title: Feldarbeit in der Praxis

---


