---
category: music
ext_url: ''
id: Daizen
place: Wiesenbühne
sample: https://m.youtube.com/watch?v=fwdGdOBW9-Y&feature=emb_title
subtitle: Psychedelische Weltmusik
summary_text: Der Kölner Straßenmusiker Daizen verwebt Didgeridoo, Gitarre und Perkussion
  mittels einer Loop-Maschine zu psychedelischen Weltmusik-Symphonien.
taxonomy:
  tag:
  - klangwelten
  - psychedelic
  - ambient
time_info: Sa 14.08. | 13:00 - 14:00
title: Daizen

---

„Daizen“ ist ein Straßen- und Loopkünstler aus Köln. Als One-Man-Band verwebt er Melodien und Rhythmen mit seiner Didgeridoo, Gitarre und einer Loopstation zu psychedelischen Weltmusik Kompositionen.
2017 aus Australien von einer Straßenmusiktour zurückgekehrt, hat er die inspirierende Stimmung dort aufgesogen, um sie in den grauen Alltag der Leute in diesem Lande entgegen zu speien. Musikalisch eine Synthese zwischen Tash Sultana, FKJ und Santana.
