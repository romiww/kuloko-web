---
category: music
ext_url: ''
id: T_r_anzgeschichten
place: MaWaLü
sample: ''
subtitle: Queerfeministische installative Soundperformance & Tanzveranstaltung
summary_text: Die Performance ist eine 2-3 stündige Bewegung durch politisch-persönliche
  Gedanken zu sattem Beat und Synthi-Sounds - trans_mittet aus einem trans_formierten
  Bus.  Komm als was du gerne kommen willst - wir kommen in Drag und wollen mit euch
  tanzen!
taxonomy:
  tag:
  - performance
  - tanz
  - interaktiv
time_info: Sa 14.08. | 18:00 - 20:30
title: T_r_anzgeschichten - Inge hat eine Leiter

---


