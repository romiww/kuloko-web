---
category: music
ext_url: http://www.rohundungeschliffen.de/
id: Sascha_Katterbach
place: Hofbühne
sample: https://www.youtube.com/watch?v=4iVIoWm-oxo&list=PLKMKHCKMfWAqV1oVAqFpj1G6B1s0dTZWa
subtitle: singer/songwriter*in
summary_text: Lieder aus roher Kehle und ungeschliffener Seele auf kraftvollem Akustikgitarrenfundament
taxonomy:
  tag:
  - singer-songwriter*in
  - pop
time_info: ''
title: Sascha Katterbach

---

Sascha Katterbach singt Lieder aus roher Kehle und ungeschliffener Seele. Atmosphärisch, kraftvoll, echt. Der aus Aachen stammende Gitarrist und Sänger, der dieses Jahr sein Debutalbum „farbenbrauchenlicht“ veröffentlichen wird, nimmt einerseits kein Blatt vor den Mund, wenn es um gesellschaftskritische Themen geht. Andererseits geht er feinfühlig und liebevoll mit Themen des Alltags um, läßt dabei immer Raum, zwischen den Zeilen zu lesen und macht sie durch seinen starken harmonischen Unterbau fühlbar.
