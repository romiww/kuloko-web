---
category: education
ext_url: ''
id: Ailis
name: Ailis
place: MaStaMo | FLINTA* Zelt
sample: ''
subtitle: Workshop, Kunst
summary_text: ''
taxonomy:
  tag:
  - workshop
  - kunst
time_info: Fr 13.08. | 11:00 - 12:30
title: Akt stehen - eine feministische Perspektive

---

Ein Raum für FLINTA*, indem gemeinsam Akt gezeichnet und gestanden wird. Ein Austauschraum, um die seuxualisierte Künstler:innen-Musen Beziehung zu dekonstruieren.
