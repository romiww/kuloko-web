---
category: others
ext_url: ''
id: MaStaMo_Abschlussplenum
place: MaStaMo | Zirkuszelt
sample: ''
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - ''
time_info: So 15.08. | 09:30 - 10:30
title: MaStaMo-Abschlussplenum

---

Wir wollen gemeinsam das MaStaMo-Camp auf der Kuloko beenden, uns vernetzen und Zukunftspläne schmieden. Komm vorbei und mach mit.
