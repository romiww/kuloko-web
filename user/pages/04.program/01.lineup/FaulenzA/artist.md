---
category: music
ext_url: https://faulenza.com
id: FaulenzA
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=sEa-v0zzpc0
subtitle: Trans*female Rap
summary_text: ''
taxonomy:
  tag:
  - rap
time_info: Sa 14.08. | 22:00 - 23:00
title: FaulenzA

---

FaulenzA ist ist Musikerin, Trans*Aktivistin und Buchautorin. Sie fetzt die Normalität weg mit
fetten tanzbaren Rap-Beatz, oder nimmt die Gitarre und begleitet ihre Songs selbst. FaulenzA
möchte dich zum tanzen, zum lachen und weinen bringen. Sie versucht sich selbst stark zu
machen und freut sich in den Songs ihre Gefühle und Erfahrungen mit dir zu teilen. Ihre Musik ist
ehrlich, intim und einfühlsam. Mal quatschig und lustig, mal melancholisch und warm, schön und
zärtlich, oder sie geht wütend nach vorne. Sie ist FaulenzA’s Herzblut und Liebe. Sie ist ihre Wut,
ihre Traurigkeit und Hoffnung. Sie ist ihre Welt aus Zuckerguss und Marzipan, mit Einhörnen und
autonomen Mäusen. Komm mit! Wenn du magst, reist FaulenzA mit dir dorthin.
Neben der Musik bietet FaulenzA auch Workshops und Vorträge zu den Themen
„Trans*misogynie“, „Mad Pride“ und „Selbstverteidigung/Selbstbehauptung“ an.
