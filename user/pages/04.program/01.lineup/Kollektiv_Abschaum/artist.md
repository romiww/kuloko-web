---
category: music
ext_url: https://kollektivabschaum.bandcamp.com/
id: Kollektiv_Abschaum
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=HbhwCiRNhw8&t=10s
subtitle: Pop-Punk
summary_text: Gute Band aus Köln. Machen sowas wie Musik. Haben gute Texte (linksradikal
  dies das). Sehen sehr gut aus.
taxonomy:
  tag:
  - pop
  - punk
time_info: Fr 13.08. | 21:00 - 22:30
title: Kollektiv Abschaum

---

Lol Pressetext, sind wir nen verkacktes Start-up oder was?

Ey, wenn du das hier liest! Ja genau du! Zünd mal lieber was an, statt so scheiß Pressetexte hier zu lesen!
