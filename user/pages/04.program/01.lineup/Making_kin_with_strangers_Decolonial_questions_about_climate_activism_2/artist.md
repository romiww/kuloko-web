---
category: education
ext_url: ''
id: ''
name: Mihir Sharma
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: 'Mit fremden verwandt werden: dekoloniale Fragen zu Klimaaktivismus'
summary_text: ''
taxonomy:
  tag:
  - workshop
  - kolonialismus
  - rassismus
  - organisierung
time_info: Mi 11.08. | 14:30 - 16:30
title: 'Making kin with strangers: Decolonial questions about climate activism 2'

---

Warum werden Klimaaktivist*innen in Manila, in den Wäldern Kolumbiens, und in Chhattisgarh umgebracht, und nicht in Köln, Einbeck, oder Plauen? Wie kann eine Verwandtschaft mit Menschen anderswo zu solidarischen Aktionen und Praxis beitragen? Hier wird ein Raum aufgemacht, mit einer dekoloniale Perspektive seine Praxis zu reflektieren, und konstruktive Ideen, textempfehlungen, und auch potenzielle Genoss*innen, mitzunehmen.
