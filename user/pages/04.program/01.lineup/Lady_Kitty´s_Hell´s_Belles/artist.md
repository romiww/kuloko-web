---
category: others
ext_url: http://serpent-rouge.com/belles.html
id: Lady_Kitty's_Hell's_Belles
place: Klimacamp Rheinland | Zirkuszelt
sample: http://serpent-rouge.com/HB_Pole_Kitty.mp4
subtitle: die Schönheiten des Ödlands
summary_text: Akrobatik, Erotik, Tanz, Feuer, Bühnenshow! Wir sind eine Gruppe hübscher
  Frauen und Männer, die erstaunliche Dinge mit ihren Körpern anstellen können. In
  unserer Show feiern wir die Bewegung, das Leben, die Liebe und (manchmal) die Lust.
taxonomy:
  tag:
  - performance
  - tanz
  - akrobatik
time_info: So 15.08. | 21:00 - 22:00
title: Lady Kitty´s Hell´s Belles

---

Lady Kitty´s Hell´s Belles, die Höllenschönheiten aus dem Ödland, haben mit ihrer sexy Akrobatikshow bereits Festivals wie Wacken, Castle Rock, Autumn Moon, M´era Luna und das Wave Gotik Treffen gerockt. Wandelbar und elegant feiern sie ihre Körper in Bewegung - ob mit Showglas, Federfächern, Tanzstange, Luftring, Vertikaltuch oder Feuer. 
