---
category: education
ext_url: ''
id: Antonia_Charlotte
name: Antonia & Charlotte
place: Schlosswiese | WSZ 04
sample: ''
subtitle: Workshop
summary_text: ''
taxonomy:
  tag:
  - transformation
  - workshop
time_info: Do 12.08. | 16:30 - 18:30
title: Selbstbestimmte Bildung und gesellschaftlicher Wandel

---

Welche Rolle spielt Bildung im gesellschaftlichen Wandel? Warum bereitet uns das heutige Bildungssystem nicht auf die Herausforderungen unserer Zeit vor? Wir berichten vom Aufbau unseres eigenen Studiengangs in Philosophie und Gesellschaftsgestaltung, von unseren Anliegen und Hürden und wollen im Anschluss mit euch über zukunftsfähige Bildung ins Gespräch kommen. Referent*innen: Antonia und Charlotte studieren beide Philosophie und Gesellschaftsgestaltung. Beim Aufbau ihres eigenen Studiengangs kommen sie immer wieder mit der Frage, wie zukunftsfähige Bildung aussehen kann, in Kontakt. Neben dem Studium pflegt Antonia gerettete Katzenbabys und Hühner, während Lotte sich für die Klimagerechtigkeitsbewegung engagiert.
