---
category: education
ext_url: ''
id: Simon_Sutterlutti
name: Simon Sutterlütti
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: Jenseits des grünen Kapitalismus
summary_text: ''
taxonomy:
  tag:
  - kapitalismus
  - workshop
time_info: Mi 11.08. | 14:30 - 16:30
title: Caring Commonism

---

Die Politik versagt, und (höchstwahrscheinlich) wird sie weiter versagen. Denn sie versucht eine Lebens- und Wirtschaftsweise zu begrünen, die auf Konkurrenz, Profit und Wachstum aufbaut, und Care & Sorge für Mensch und Umwelt patriarchal-kolonial ignoriert. Wir beschäftigen uns kurz mit den Utopien einer sozialökologischen Marktwirtschaft, eines Marktsozialismus und demokratischen Staatssozialismus. Dann diskutieren wir mit feministischer Ökonomiekritik, Commons, Care und antiautoritären Strömungen die Grundzüge einer bedürfnisorientierten, machtkritischen, ökosozialen Gesellschaft. Abschließend stellt sie die Frage, was diese utopische Überlegungen für unsere Praxis, für unseren Bezug auf Reformen, Staaten, kleine Projekte gelebter Utopie und soziale Bewegungen heißt.
