---
title: 'Lineup (GER)'
menu: 'Lineup (GER)'
hero_background: /user/pages/images/opencall_music.jpeg
hero_foreground: /user/pages/images/fist-with-mic-bar.svg
hero_title_color: light
blog_url: program/lineup
content:
    items:
        - '@self.children'
    order:
        by: date
        dir: desc
    pagination: false
    url_taxonomy_filters: true
show_sidebar: false
show_pagination: false
show_breadcrumbs: false
header:
    -
        category: education
        id: Adrian_Blount
        name: 'Adrian Blount'
    -
        category: music
        id: Afra
        name: Afra
    -
        category: music
        id: Arbeitstitel_Tortenschlacht
        name: 'Arbeitstitel Tortenschlacht'
    -
        category: others
        id: Armin_Wuhle
        name: 'Armin Wühle'
    -
        category: music
        id: Asundo_Suminga
        name: 'Asundo Suminga'
    -
        category: others
        id: Autoren_im_Team
        name: 'Autoren im Team'
    -
        category: music
        id: be_la
        name: 'be là'
    -
        category: music
        id: 'beets''n''berries'
        name: 'beets''n''berries'
    -
        category: music
        id: Bonbonlegerin
        name: Bonbonlegerin
    -
        category: music
        id: BoringPain
        name: BoringPain
    -
        category: music
        id: Byggesett_Orchestra
        name: 'Byggesett Orchestra'
    -
        category: music
        id: Chom
        name: Chom
    -
        category: music
        id: Clara_Clasen
        name: 'Clara Clasen'
    -
        category: music
        id: Daizen
        name: Daizen
    -
        category: others
        id: Daniel_Chatard
        name: 'Daniel Chatard'
    -
        category: music
        id: describing_unity
        name: 'describing unity'
    -
        category: others
        id: Die_Migrationserb_innen
        name: 'Die Migrationserb:innen'
    -
        category: music
        id: Diva_Daneben
        name: 'Diva Daneben'
    -
        category: music
        id: Eze_Wendtoin
        name: 'Ezé Wendtoin'
    -
        category: music
        id: Fartuuna
        name: Fartuuna
    -
        category: music
        id: FaulenzA
        name: FaulenzA
    -
        category: music
        id: Findus
        name: Findus
    -
        category: music
        id: Finna
        name: Finna
    -
        category: music
        id: Fold
        name: Föld
    -
        category: others
        id: Goodtimes_Cabaret
        name: 'Goodtimes Cabaret'
    -
        category: music
        id: Heart_Core
        name: 'Heart Core'
    -
        category: music
        id: HerrinGedeck
        name: HerrinGedeck
    -
        category: others
        id: Horsta
        name: Horsta
    -
        category: music
        id: Intaktogene
        name: Intaktogene
    -
        category: music
        id: Johanna_Zeul
        name: 'Johanna Zeul'
    -
        category: music
        id: Jooles_Nick
        name: 'Jooles & Nick'
    -
        category: music
        id: Julie_and_me
        name: 'Julie and me'
    -
        category: music
        id: Kapa_Tult
        name: 'Kapa Tult'
    -
        category: music
        id: Karmakind
        name: Karmakind
    -
        category: others
        id: Kirschgarten
        name: Kirschgärten
    -
        category: music
        id: Kollektiv_Abschaum
        name: 'Kollektiv Abschaum'
    -
        category: others
        id: kollektiv_eigenklang
        name: 'kollektiv eigenklang'
    -
        category: others
        id: Kurt_Lehmkuhl
        name: 'Kurt Lehmkuhl'
    -
        category: others
        id: 'Lady_Kitty''s_Hell''s_Belles_-_die_Schonheiten_des_Odlands'
        name: 'Lady Kitty´s Hell´s Belles - die Schönheiten des Ödlands'
    -
        category: music
        id: Lebenslaute
        name: Lebenslaute
    -
        category: music
        id: Lisa_Schlipper_Ralf_Hintzen
        name: 'Lisa Schlipper & Ralf Hintzen'
    -
        category: music
        id: Mariybu
        name: Mariybu
    -
        category: music
        id: Mascha
        name: Mascha
    -
        category: music
        id: Merle
        name: Merle
    -
        category: music
        id: Microphone_Mafia
        name: 'Microphone Mafia'
    -
        category: others
        id: Militanz
        name: Militanz
    -
        category: music
        id: missratene_Tochter
        name: 'missratene Töchter'
    -
        category: music
        id: Morpho
        name: Morpho
    -
        category: music
        id: No_chronicles
        name: 'No chronicles'
    -
        category: music
        id: Pappnasen_Rotschwarz
        name: 'Pappnasen Rotschwarz'
    -
        category: music
        id: Pexa
        name: Pexa
    -
        category: music
        id: pogendroblem
        name: pogendroblem
    -
        category: music
        id: Provinzcrew
        name: Provinzcrew
    -
        category: music
        id: Rap!fugees
        name: Rap!fugees
    -
        category: others
        id: Rhiad_Ben_Anmar
        name: 'Rhiad Ben Anmar'
    -
        category: music
        id: ROB_SURE
        name: 'ROB SURE'
    -
        category: others
        id: ROYAL_PHOTO_SERVICE
        name: 'ROYAL PHOTO SERVICE'
    -
        category: music
        id: Katterbach_rohundungeschliffen
        name: 'Katterbach rohundungeschliffen'
    -
        category: music
        id: Seasoul
        name: Seasoul
    -
        category: music
        id: Sonori_b2b_Luna_X_Tatsachen_Jane
        name: 'Sonori b2b Luna X // Tatsachen & Jane'
    -
        category: music
        id: speechless
        name: speechless
    -
        category: music
        id: Stefan_Pieck
        name: 'Stefan Pieck'
    -
        category: music
        id: T_r_anzgeschichten
        name: T_r_anzgeschichten
    -
        category: music
        id: The_Punkelelics
        name: 'The Punkelelics'
    -
        category: others
        id: Theaterkollektiv_der_HS_Niederrhein
        name: 'Theaterkollektiv der HS Niederrhein'
    -
        category: others
        id: Theaterwerkstatt_Bethel
        name: 'Theaterwerkstatt Bethel'
    -
        category: music
        id: Virus_Mensch
        name: 'Virus:Mensch'
    -
        category: others
        id: Wild_Roots_Performance_Collective
        name: 'Wild Roots Performance Collective'
    -
        category: education
        id: Sergen_Canoglu
        name: 'Sergen Canoglu'
    -
        category: education
        id: Tobi_Rosswog
        name: 'Tobi Rosswog'
    -
        category: education
        id: KanguruKollektiv
        name: KänguruKollektiv
    -
        category: education
        id: Zade_Abdullah_Noura_Hammouda
        name: 'Zade Abdullah & Noura Hammouda'
    -
        category: others
        id: Alle_Dorfer_bleiben
        name: 'Alle Dörfer bleiben'
    -
        category: education
        id: Klima-Allianz
        name: Klima-Allianz
    -
        category: education
        id: Charlotte_Peltre_Philipp_Hofmann
        name: 'Charlotte Peltre & Philipp Hofmann'
    -
        category: education
        id: Fabian_Flues_Kathrin_Henneberger_Niels_Jongerius
        name: 'Fabian Flues, Kathrin Henneberger & Niels Jongerius'
    -
        category: education
        id: Dorothee_Haussermann
        name: 'Dorothee Häußermann'
    -
        category: education
        id: ausgeco2hlt
        name: ausgeco2hlt
    -
        category: education
        id: Antonia_Charlotte
        name: 'Antonia & Charlotte'
    -
        category: education
        id: Still_Burning_Network
        name: 'Still Burning Network'
    -
        category: education
        id: Alina_Lukas
        name: 'Alina & Lukas'
    -
        category: education
        id: 'Lexy_Bildungskollektiv_Herrschaftsfreie_Welt-_Beziehungen"'
        name: 'Lexy, Bildungskollektiv „Herrschaftsfreie Welt-/Beziehungen"'
    -
        category: education
        id: Simon_Sutterlutti
        name: 'Simon Sutterlütti'
    -
        category: education
        id: Initiative_Demokratischer_Konfoderalismus
        name: 'Initiative Demokratischer Konföderalismus'
    -
        category: education
        id: Jojo_Klick
        name: 'Jojo Klick'
    -
        category: education
        id: Alexander_Neupert-Doppler
        name: 'Alexander Neupert-Doppler'
    -
        category: education
        id: Eva_von_Redecker
        name: 'Eva von Redecker'
    -
        category: education
        id: Kollektiv_Bildung_fur_utopischen_Wandel
        name: 'Kollektiv Bildung für utopischen Wandel'
    -
        category: education
        id: Autonome_feministische_Sommerschule
        name: 'Autonome feministische Sommerschule'
    -
        category: education
        id: Theresa_Leisgang_Institute_of_Environmental_Justice
        name: 'Theresa Leisgang & Institute of Environmental Justice'
    -
        category: others
        id: Kaffee_Kuchen
        name: 'Kaffee&Kuchen'
    -
        category: education
        id: Camila_Schmid
        name: 'Camila Schmid'
    -
        category: education
        id: Riv
        name: Riv
    -
        category: education
        id: Zucker_im_Tank
        name: 'Zucker im Tank'
    -
        category: education
        id: Susanne_Fasbender_Peter_Emorinken_Donatus
        name: 'Susanne Fasbender & Peter Emorinken Donatus'
    -
        category: education
        id: Mihir_Sharma
        name: 'Mihir Sharma'
    -
        category: education
        id: .ausgestrahlt
        name: .ausgestrahlt
    -
        category: education
        id: Karin_de_Miguel
        name: 'Karin de Miguel'
    -
        category: education
        id: Sara_Huther_Sita_Scherer
        name: 'Sara Hüther & Sita Scherer'
    -
        category: education
        id: Mamadou_Bah
        name: 'Mamadou Bah'
    -
        category: education
        id: Feuerquallen_Kollektiv
        name: 'Feuerquallen Kollektiv'
    -
        category: education
        id: Philip_Noack
        name: 'Philip Noack'
    -
        category: education
        id: Emergentes_Coopar_Comunicacion_Graswurzel.tv.
        name: 'Emergentes, Coopar Comunicación & Graswurzel.tv. '
    -
        category: others
        id: Annika_Alex
        name: 'Annika & Alex'
    -
        category: others
        id: Bildungskollektiv_Bonn
        name: 'Bildungskollektiv Bonn'
    -
        category: education
        id: Luca
        name: Luca
    -
        category: education
        id: KLARA_KOLLEKTIV
        name: 'KLARA KOLLEKTIV'
    -
        category: education
        id: Nisha
        name: Nisha
    -
        category: education
        id: Marie_Maru
        name: 'Marie & Maru'
---

