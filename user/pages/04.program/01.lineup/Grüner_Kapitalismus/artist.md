---
category: education
ext_url: ''
id: Sergen_Canoglu
name: Sergen Canoglu
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: Antwort auf die Klimakrise?
summary_text: ''
taxonomy:
  tag:
  - workshop
  - kapitalismus
  - klimagerechtigkeit
time_info: Mo 09.08. | 10:30 - 12:30
title: Grüner Kapitalismus

---

Viele Menschen sind wegen der Klimakrise beunruhigt, sie möchten handeln, gehen auf die Straße und setzen sich für eine Lösung aus der Katastrophe ein. Die meisten Parteien sehen in einem "grünen" Kapitalismus die Lösung, doch kann dieser eine Lösung sein?
