---
category: education
ext_url: ''
id: Schnock
name: Schnock
place: MaStaMo
sample: ''
subtitle: Austauschraum für Hauptbezugspersonen von Kindern
summary_text: ''
taxonomy:
  tag:
  - austausch
time_info: Sa 14.08. | 16:45 - 18:15
title: Links leben mit Kindern. Zwischen Utopie und Realität.

---

Dieser Raum richtet sich an alle Menschen, die Hauptbezugspersonen für ein oder mehrere Kinder sind. Wir wollen uns darüber austauschen, wie es geht unsere Gesellschaftskritik im Alltag mit Kindern umzusetzen, woran wir scheitern, was uns Hoffnung gibt und wo wir uns gegenseitig unterstützen können. Leider kann keine Kinderbetreuung angeboten werden.
