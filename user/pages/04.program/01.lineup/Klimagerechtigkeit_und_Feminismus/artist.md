---
category: education
ext_url: ''
id: Institute_of_Environmental_Justice
name: Institute of Environmental Justice
place: Klimacamp Rheinland | Zirkuszelt
sample: ''
subtitle: Podiumsdiskussion
summary_text: ''
taxonomy:
  tag:
  - feminismus
  - klimagerechtigkeit
  - podiumsdiskussion
time_info: So 08.08. | 16:00 - 17:30
title: Klimagerechtigkeit und Feminismus

---

Ohne die Gleichberechtigung aller Geschlechter und ein Ende von Strukturen die unterdrücken, werden wir die Klimakrise nicht aufhalten können. Auf unserem Podium „Klimagerechtigkeit & Feminismus“ wollen wir den Perspektiven und Forderungen von Frauen zuhören und gemeinsam diskutieren, wie wir unseren Streit für Klima- und Umweltschutz verbinden mit unserem queerfeministischen Streit für Gleichberechtigung, soziale Gerechtigkeit und Antirassismus, um eine bessere… eine solidarische Weltgemeinschaft aufzubauen.
