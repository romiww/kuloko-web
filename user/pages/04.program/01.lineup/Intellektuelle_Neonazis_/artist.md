---
category: education
ext_url: ''
id: Kante
name: Kante
place: Schlosswiese | WSZ 04
sample: ''
subtitle: Narrative der Neuen Rechten
summary_text: ''
taxonomy:
  tag:
  - workshop
  - vortrag
  - rassismus
time_info: Do 12.08. | 14:30 - 16:30
title: Intellektuelle Neonazis?

---

Was haben Identitäre Instergrammerinnen, die AFD, französische Intellektuelle, erzkatholische Blogger und Burschis mit Russia Today zu tun? Narrative und Framing der neuen Rechten.
