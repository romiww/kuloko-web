---
category: others
ext_url: ''
id: Ende_Gelande
place: Klimacamp Rheinland
sample: ''
subtitle: Infostand
summary_text: ''
taxonomy:
  tag:
  - ''
time_info: Mi 11.08. - So 15.08.
title: "Ende Gelände\n - EG goes Lützerath"

---

Im Herbst heißt es für uns Rodungen stoppen und Lützerath unräumbar machen! Jetzt schon wollen wir an Bauwochenenden Strukturen aubauen und im Herbst Lützerath widerständig und bestimmt verteidigen. Auf der Kuluko findet ihr unseren Infostand. Hier könnt ihr Mobimaterial für euch und eure Friends &Städte abholen. In der Zeit von 16:30-17:30 sind wir täglich vor Ort, kommt vorbei und bereitet euch mit uns auf einen kämpferischen Herbst vor! (Mobimaterial könnt ihr rund um die Uhr abholen) Ab November Lützerath unräumbar machen - System Change not Climate Change
