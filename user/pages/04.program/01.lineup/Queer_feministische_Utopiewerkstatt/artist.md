---
category: education
ext_url: ''
id: Lexy_Bildungskollektiv_Herrschaftsfreie_Welt_Beziehungen"
name: Lexy, Bildungskollektiv „Herrschaftsfreie Welt-/Beziehungen"
place: Klimacamp Rheinland | WSZ 02
sample: ''
subtitle: Workshop, Austausch
summary_text: ''
taxonomy:
  tag:
  - ''
time_info: Mi 11.08. | 10:30 - 12:30
title: Queer_feministische Utopiewerkstatt

---

Wie könnte eine post-patriarchale Gesellschaft aussehen? In der Utopienwerkstatt wollen wir gemeinsam versuchen, uns Bildern einer geschlechtergerechten Zukunft anzunähern. Dabei bewegen wir uns que(e)r zu den Themenfeldern Fürsorge & Lohnarbeit, Beziehungen, Elternschaft und Sexualität.Nach einem gemeinsamen Einstieg gibt es parallel verschiedene Methodenangebote, um sich mit bestehenden feministischen Utopien auseinander zu setzen oder selbst utopische Skizzen zu entwerfen. Am Ende des Workshops werden wir die Ergebnisse zu einem Mosaik zusammenführen und uns über Gemeinsamkeiten bzw. Unterschiede in unseren Vorstellungen von Utopie austauschen. Der Workshop ist offen für alle Geschlechter. 
