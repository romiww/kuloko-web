---
category: music
ext_url: https://foeld.bandcamp.com/album/f-ld
id: Fold
place: Markt- und Theaterwiese
sample: ''
subtitle: Experimental Ambient Psychedelic
summary_text: ''
taxonomy:
  tag:
  - ambient
  - psychedelic
  - klangwelten
time_info: So 15.08. | 17:30 - 18:00
title: Föld

---

Mensch, Gitarre, Repetition.
