---
category: education
ext_url: ''
id: ''
name: ''
place: Klimacamp Rheinland | Zirkuszelt
sample: ''
subtitle: Podiumsdiskussion
summary_text: ''
taxonomy:
  tag:
  - podiumsdiskussion
time_info: Do 12.08. | 20:00 - 21:30
title: (Wen) wählt die Klimagerechtigkeit?

---

"Diese Wahl ist eine Klimawahl" ist gerade häufig zu hören. Aber was heißt das? Wen sollen wir als Klima(gerechtigkeits)bewegung wählen? Können Wahlen überhaupt die Welt verändern oder ist mein Stimmzettel nur Papierverschwendung? In welchem Verhältnis steht der Protest auf der Straße zu der Arbeit im Parlament? Stehen Basisdemokratie und Parlamentarismus im Widerspruch zueinander? Keine zwei Monate vor der Bundestagswahl diskutieren wir diese Fragen mit einem Vertreter der LINKEN, einer der GRÜNEN und einer Anarchistin. Statt Wahlkampfparolen zu hören, wollen wir uns darüber austauschen, an welchen Punkten wir innerhalb und außerhalb der Institutionen die Möglichkeit für Veränderung sehen, wie diese sich ergänzen oder im Wege stehen, und wie wir am Ende zu einer gerechten Gesellschaft finden. Die Veranstaltung findet auf Deutsch statt und wird nicht aufgezeichnet.
Referent*innen: 
Lorenz Gösta Beutin, Klima- und Energiepolitischer Sprecher der Linksfraktion im Bundestag
Johanna Frei, ausgeCO2hlt
Weitere Referent*innen angefragt.
Moderation:Daniel Hofinger und Dorothee Häußermann (Ende Gelände und ausgeCO2hlt)
