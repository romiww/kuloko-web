---
category: others
ext_url: https://arminwuehle.com/
id: Armin_Wuhle
place: Wiesenbühne
sample: https://www.instagram.com/p/CPi1gKtKrhN/
subtitle: ''
summary_text: 'Widerstand, Haltung und Ziviler Ungehorsam: Lesung aus Armin Wühles
  »Getriebene«'
taxonomy:
  tag:
  - lesung
time_info: So 08.08. | 15:00 - 16:00
title: Armin Wühle

---


