---
category: music
ext_url: https://soundcloud.com/bellabela
id: be_la
place: Wiesenbühne
sample: ''
subtitle: Techno
summary_text: be là ist Teil des queer-feministischen DJ-Kollektivs Non+Ultras. Ihre
  Sets lassen sich den labels deep, dark, dystopian und ethereal versehen, die zur
  industriellen und urbanen Atmosphäre des Autonomen Zentrums Köln passen, wo sie
  regelmäßig auftritt.
taxonomy:
  tag:
  - techno
  - dj
time_info: 15.08. | 01:30 - 03:30
title: be là

---


