---
category: others
ext_url: ''
id: women*_performing_for_feminism_KAOS
place: ''
sample: ''
subtitle: Antikapitalistische Mitmachperformance
summary_text: ''
taxonomy:
  tag:
  - theater
  - performance
  - interaktiv
time_info: ''
title: women* performing for feminism KAOS

---

Hast du Lust dich in Bewegung mit dem Thema "Körper im
      Kapitalismus" auseinanderzusetzen, hast aber noch keine
      Tanzerfahrung ? Dann bist du hier genau richtig! Es geht in der
      Performance nicht um "schöne" Bewegungen, sondern darum zu
      verstehen, was das kapitalistische System mit unseren Körpern
      macht**. Welche Zwänge, Erwartungen oder Normen legen sich auf
      Körper durch Leistungsdruck, Effizienzlogiken und
      Individualisierung? Welche Schmerzen ertragen, ja normalisieren
      wir tagtäglich? Und wie fühlt und bewegt sich letztendlich ein
      befreiter und widerständiger Körper? Wir wollen gemeinsam die
      Brüche suchen, die Grautöne und Widersprüche, in denen wir mehr
      sind als nur disziplinierte, arbeitende Körper.

      

      Ganz konkret bieten wir einen 3-stündigen Workshop an, in dem wir
      diesen Fragen praktisch nachgehen und Teile der Mitmachchoreo
      Schritt für Schritt gemeinsam entwickeln. Anschließend gibt es
      eine Werkstattaufführung am Samstag, den 14.08. um 16 Uhr.

      

      ** Wir wollen darauf hinweisen, dass wir nur unsere eigenen, ganz
      persönlichen Erfahrungen mit dem Thema Körper im Kapitalismus
      durch Bewegung nahbar und erfahrbar machen können und sind uns
      dabei unserer privilegierten Position und Sichtweise auf globaler
      Ebene bewusst.
