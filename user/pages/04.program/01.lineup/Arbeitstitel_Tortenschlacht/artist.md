---
category: music
ext_url: https://arbeitstiteltortenschlacht.bandcamp.com/
id: Arbeitstitel_Tortenschlacht
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=7xAT1eTf00M
subtitle: folk-hop, trafo-kitsch, singer-songwriter*in
summary_text: Arbeitstitel Tortenschlacht basteln Zukunftsmusik, machen Aufmucke und
  kochen Weltschmerzmedizin.
taxonomy:
  tag:
  - folk
  - singer-songwriter*in
  - pop
  - akustik
time_info: Mi 11.08. | 21:00 - 22:00
title: Arbeitstitel Tortenschlacht

---


