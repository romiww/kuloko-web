---
category: education
ext_url: ''
id: ''
name: Danni/Gäst_innenhaus
place: MaStaMo
sample: ''
subtitle: Austausch und Diskussion
summary_text: ''
taxonomy:
  tag:
  - aktivismus
time_info: Do 12.08. | 15:00 - 16:30
title: Erfahrungen aus der Danni-Besetzung - Part !

---

Besetzung, Räumung und die weiterhin aktive KeineA49-Kampagne
