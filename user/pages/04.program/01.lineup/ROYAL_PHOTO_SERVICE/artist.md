---
category: others
ext_url: http://www.historische-fotografen.de
id: ROYAL_PHOTO_SERVICE
place: ''
sample: https://consent.youtube.com/m?continue=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Dt3ir_7-iNCk%26feature%3Dyoutu.be&gl=DE&m=0&pc=yt&uxe=23983172&hl=de&src=1
subtitle: Foto-Show
summary_text: Fotografie im Stil der Jahrhundertwende mit einer alten Plattenkamera
taxonomy:
  tag:
  - ''
time_info: ''
title: ROYAL PHOTO SERVICE

---

Zwanziger Jahre Foto-Show: Charleston, Prohibition, Swing und Mafia

Al Capone at its Best- Parties werden aktuell gern im Stil der 20er-Jahre ausgerichtet. Mit dabei ist dort oft eine Fotobox. Hierdurch entstand die Idee unserer einzigartigen Foto-Show. Seitdem begleiten wir exklusive Veranstaltungen mit dieser unvergleichlichen Eventkation.

Erleben Sie unsere Live-Event-Fotoshow im Stil der 20er-Jahre bzw. der golden Twenties!

Mit unserer alten Nostalgie-Kamera aus der Zeit der Jahrhundertwende sind wir deswegen seit über sieben Jahren auf den unterschiedlichsten Events gebucht. Wir stellen mit unserem Event-Fototeam eine komplette Kulisse mit Hintergrund, Beleuchtung, historischer Kamera, Fotograf und bezaubernden Assistentinnen. The Great Gatsby, Charleston, Prohibition und Swing. Die Fotografie der 20er Jahre setzen wir für Ihren Event um! Die goldenen 20er oder die „Roaring Twenties“ stehen ausserdem für die Blütezeit der Kunst, Kultur und Wissenschaft der damaligen Zeit. New York mit seinen Limousinen genauso wie The Great Gatsby, Menschen in schicken Anzügen oder Al Capone mit Thommygun Maschinengewehr. Die goldenen Zwanziger in Berlin mit Klärchens Ballhaus oder das Aufleben der Kunst und dem Flair der Boheme.

Wir stellen hierfür unser 20er-Jahre Fotoshooting zusammen. Unser Theatherfundus umfasst historische Accessoires, alte Waffen, Kleidungsstücke und Speialanfertigungen. Hierdurch entsteht die einzigartige Atmosphäre der golden Twenties.
