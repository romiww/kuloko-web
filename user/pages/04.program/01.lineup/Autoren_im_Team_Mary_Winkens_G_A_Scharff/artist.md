---
category: others
ext_url: https://www.autoren-im-team.de
id: Autoren_im_Team
place: Markt- und Theaterwiese
sample: https://www.youtube.com/watch?v=Mk-nBbZgjqQ&t=194s
subtitle: ''
summary_text: Ein Autorenteam, das mehr oder weniger direkt am Grubenrand zu Hause
  ist
taxonomy:
  tag:
  - storytelling
  - lesung
  - kinder
  - lokales
time_info: 16:00 & 19:00
title: Autoren im Team - Mary Winkens & G. A. Scharff

---


