---
category: others
ext_url: ''
id: Florencia
place: Unser aller Wald
sample: ''
subtitle: ''
summary_text: Im Haus der Geschichten wird überliefert was nicht in die Geschichte
  eingehen soll. Wer diese Geschichten erfahren will ist herzlich zum hören, riechen,
  trinken, spüren und teilen eingeladen.
taxonomy:
  tag:
  - storytelling
  - interaktiv
  - installation
  - performance
time_info: Fr 06.08. - Mi 11.08. | immer 18:00 - 19:00
title: Florencia - Geschichten überliefern

---

Bei "Geschichten überliefern" werden im Haus der Geschichten Erfahrungen erzählt, die vom verborgenen ans Licht kommen werden. 
Die Geschichten werden nicht nur durch lesen und erzählen überliefert, sonden durch Gegenstände, Gerüche, Musik und Anderes was in den Geschichten vorkommt und vor Ort auftaucht. Anschließend und währenddessen können Fragen gestellt werden oder weitere Geschichten erzählt werden, oder wir können miteinander ins Gespräch, bei einer Tasse Tee oder ein anderen Getränk, kommen. 

Das Haus der Geschichten kann auch außerhalb der Vorstellungszeiten als Rückzugsort zum mit sich selbst und anderen im Kontakt kommen genutzt werden.

Ich freue mich auf euch und die Geschichten.

Wo: Haus der Geschichten                                     
Wann: Jeden Tag vom 6. bis 11. August 
            ab 18 Uhr
Dauer: 15-20 Minuten Erzählung 
            Anschließend kann dort verweilt werden oder weitergegangen werden.


