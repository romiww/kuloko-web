---
category: music
ext_url: ''
id: speechless
place: Hofbühne
sample: ''
subtitle: akustische Cover-Musik
summary_text: speechless (Christiane Apeldorn, Martin Berger, Elisa Weidemann) aus
  Erkelenz ist ein Akustik-Trio mit mehrstimmigen Gesang. Sie arrangieren Coversongs
  mit Piano und/oder akustischer Gitarre auf ihre Weise und laden zum Zuhören und
  Chillen ein.
taxonomy:
  tag:
  - akustik
  - cover
  - lokales
time_info: So 08.08. | 16:15 - 17:00
title: speechless

---

speechless (Christiane Apeldorn, Martin Berger, Elisa Weidemann) aus Erkelenz schaffen mit ihrem mehrstimmigen Gesang immer wieder eine einzigartige Atmosphäre, die zum Zuhören und Chillen einlädt. Dass ihr „mehrstimmiger Gesang unter die Haut geht“ und wie „gesungene Lyrik klingt“, wurde in mehrere Pressestimmen beschrieben. Sie arrangieren Coversongs mit wechselnder Piano-und/oder akustischer Gitarrenbegleitung auf ihre Weise.
