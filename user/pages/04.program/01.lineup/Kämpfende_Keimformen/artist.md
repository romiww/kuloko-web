---
category: education
ext_url: ''
id: Jojo_Klick
name: Jojo Klick
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: 'Durch Commons und soziale Bewegungen den Kapitalismus

  aufheben'
summary_text: ''
taxonomy:
  tag:
  - kapitalismus
  - workshop
time_info: Do 12.08. | 14:30 - 16:30
title: 'Kämpfende Keimformen

  '

---

Eine neue Gesellschaftsform fällt nicht vom Himmel, sondern existiert in
Keimform bereits in der alten. In diesem Workshop wollen wir dazu
einladen, darüber nachzudenken wie Keimformen einer
herrschaftsfreien, solidarischen und ökologischen Gesellschaft
aussehen und wie sich diese verallgemeinern könnten. Wir sehen
solche Keimformen überall dort, wo Menschen jenseits der Formen von
Markt, Staat und Herrschaft miteinander kooperieren: In Commons bzw.
durch Commoning, also durch gemeinsame bedürfnisorientierte
Selbstorganisation. Wir diskutieren, wie sich Commoning durch soziale
Bewegungen und Aneignungskämpfe ausweiten und wie ein
gesamtgesellschaftlicher Bruch aussehen kann, der nicht die ganze
alte Scheiße in Form von Lohnarbeit und Herrschaft reproduziert,
sondern Verhältnisse schafft, in denen die Freiheit der Einzelnen
Bedingung für die Freiheit aller ist.

Über die Referent*in: Jojo Klick fühlt sich der Klimagerechtigkeits- und
anderen emanzipatorischen Bewegungen verbunden, bloggt auf
keimform.de und schreibt zur Zeit gemeinsam mit anderen Menschen ein
Buch über gesellschaftliche Transformation.
