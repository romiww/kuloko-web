---
category: education
ext_url: ''
id: ''
name: Alina & Lukas
place: Schlosswiese | WSZ 04
sample: ''
subtitle: Wie wird eine Vision vom guten Landleben für alle real?
summary_text: ''
taxonomy:
  tag:
  - utopie
  - workshop
time_info: Fr 13.08. | 14:30 - 16:30
title: Ländliche Utopien 2

---

Wir brauchen ländliche Regionen für den sozial-ökologischen Umbau. Gemeinsam möchten wir unseren Ängsten, strukturellen Hürden und emanzipatorischen Möglichkeiten begegnen und somit eine gemeinsame Vision für den ländlichen Umbau skizzieren. Welche theories of change führen uns dorthin?
/KOMPOST – Konglomerat für den sozial-ökologischen Umbau ländlicher Regionen
instagram.com/hey_kompost
kompost_vernetzt@lists.riseup.net (lists.riseup.net/www/info/kompost_vernetzt) 
/ Alina konnte es kaum erwarten, die Engstirnigkeit ihres 300-Einwohner-Dorfes in Nordhessen zu verlassen. Aktuell versucht sie in Bonn kritische, feministische und antirassistische Perspektiven in die Agrarwissenschaften zu integrieren. 
/ Lukas wäre gerne Anarchist, interessiert sich für Commons und aktiviert sich hier und da für die Degrowth und Klimagerechtigkeits Bewegung. Zusammen mit Corona kam allerdings auch eine Lohnarbeit für ländliche Entwicklung um die Ecke. 
