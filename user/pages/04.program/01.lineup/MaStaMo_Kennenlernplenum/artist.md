---
category: others
ext_url: https://www.bundjugend-nrw.de/projekte/mastamo/
id: MaStaMo_Kennenlernplenum
place: MaStaMo | Zirkuszelt
sample: ''
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - skill-share
  - organisierung
  - partizipation
time_info: So 08.08. | 09:30 - 11:30
title: MaStaMo-Kennenlernplenum

---

MaStaMo stellt sich vor und wir werden gemeinsam über die Campstrukturen und das offene Skillshare hier im Camp sprechen, Verantwortlichkeiten und Programmpunkte für die nächsten Tage festlegen. Komm gerne vobrei und mach mit.
