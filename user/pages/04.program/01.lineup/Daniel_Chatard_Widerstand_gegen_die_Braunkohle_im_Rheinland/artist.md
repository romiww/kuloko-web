---
category: others
ext_url: http://chatard.de
id: Daniel_Chatard
place: Scheunenkino
sample: ''
subtitle: Dokumentarfotografie
summary_text: Daniel Chatard (*1996 in Heidelberg) ist ein deutsch-französischer Dokumentarfotograf.
  Wiederkehrende Themen seiner Recherche sind kollektive Identitäten, Grenzen und
  Umwelt.
taxonomy:
  tag:
  - fotografie
time_info: Mo 09.08. - So 15.08.
title: Daniel Chatard - Widerstand gegen die Braunkohle im Rheinland

---

Daniel Chatard (*1996 in Heidelberg) ist ein deutsch-französischer Dokumentarfotograf.
In seiner Arbeit untersucht Chatard soziale Dynamiken und Konflikte. Dabei interessiert er sich besonders dafür, wie diese sich im physischen Raum manifestieren. Wiederkehrende Themen seiner Recherche sind kollektive Identitäten, Grenzen und Umwelt.
