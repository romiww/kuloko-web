---
category: education
ext_url: ''
id: Initiative_Demokratischer_Konfoderalismus
name: Initiative Demokratischer Konföderalismus
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: Demokratischer Konföderalismus als Lösungansatz
summary_text: ''
taxonomy:
  tag:
  - workshop
  - organisierung
time_info: Do 12.08. | 10:30 - 12:30
title: Revolution in der BRD?!

---

Wie kann ein revolutionärer Prozess in der BRD aussehen? Was bedeutet Revolution überhaupt? Was brauchen wir dafür?

Die Initiative Demokratischer Konföderalismus möchte in dem Workshop
ihre Perspektive ausgehend von den Ideen und der Praxis der
Kurdischen Freiheitsbewegung auf die Fragen teilen. Der
Demokratischer Konföderalismus betont die Vielfalt der Gesellschaft
und hat das Ziel durch eine Organisierung von Selbstverwaltung
außerhalb von Staat, nach und nach zu einer basisdemokratischen,
geschlechterbefreiten und ökologischen Organisierung der
Gesellschaft zu gelangen. Wir wollen in dem Vortrag nicht zu
theoretisch bleiben: Der Demokratischen Konföderalimus wird durch
den Aufbau von Kommunen, Räten, Kooperativen und Akademien
Wirklichkeit. Dabei ist wichtig, dass diese nicht als Inseln im
System entstehen, sondern in all ihrer Vielfalt zu einer
anti-systemischen Kraft werden. Das Paradigma der Kurdischen
Freiheitsbewegung bietet uns eine Grundlage uns selber und das System zu analysieren, und darüber hinaus der Kapitalischen Moderne eine konkrete Alternative die Demokratische Moderne entgegenzustellen. Entscheidend und unser Analyse des Patriarchats folgend ist dabei die autonome FLINT-Organisierung auf allen Ebenen. Darüberhinaus gibt es viele weitere Punkte, die wir aus den Erfahrungen der Revolution in
Kurdistan, lernen können. Diese möchten wir ebenfalls zur Sprache
bringen und dann in eine gemeinsame Diskussion kommen.
