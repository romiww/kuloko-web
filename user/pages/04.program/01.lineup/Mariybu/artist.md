---
category: music
ext_url: https://www.instagram.com/mariybu_/
id: Mariybu
place: Wiesenbühne
sample: https://www.youtube.com/watch?v=KZH7Taq_UAo
subtitle: HipHop
summary_text: ''
taxonomy:
  tag:
  - hiphop
  - rap
time_info: Fr 06.08. | 22:30 - 23:30
title: Mariybu

---

Macker kriegen Schiss, wenn die Hamburger Rapperin Mariybu das Mic
auspackt - und die Bühne brennt, wenn sie ihre Show abliefert. Basslastige
Beats, unmissverständliche Ansagen und queerfeministisches Empowerment
- alles Marke Mariybu. Mit starker Meinung, progressiven Inhalten und einer
kräftigen Stimme trifft sie toxische wie sexistische Dynamiken genau da, wo
es weh tun soll. Gleichzeitig beweist die talentierte Künstlerin, dass es im
Kampf gegen Patriarchat und Machtgefälle auch Zeit für Pausen,
Emotionalität und Verletzlichkeit braucht. Kürzlich beim All Female Rap-Label
365XX gesignt, arbeitet Mariybu gerade an ihrer zweiten EP, die sie im Herbst
diesen Jahres releast. Die erste Single-Auskopplung “Toxic” ist Ende Juni
erschienen und hat in der Deutschrap-Szene bereits für ordentlich Welle
gesorgt.
