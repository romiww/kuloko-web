---
category: education
ext_url: ''
id: Jani
name: Jani
place: Klimacamp Rheinland | WSZ 02
sample: ''
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - transformative-justice
  - intersektionalität
time_info: Sa 14.08. | 10:30 - 12:30
title: Transformative Konfliktbearbeitung-Straflogiken überwinden

---

Institutionell und strukturell wird "sozial inadäquates" Verhalten mit Strafe begegnet. Kriminalisierung und Gefängnisse sind Teil eines Systems, das durch Gewalt und Herrschaftsverhältnisse geprägt ist. Es geht wenig darum welche Umstände zu dem Verhalten geführt haben und wie ein konstruktiver Umgang mit Gewalt zwischenmenschlichen Auseinandersetzungen aussehen kann. Im Workshop werden wir darüber sprechen, wo das Strafjustizsystem scheitert auf zwischenmenschliche Konflikte und Beteiligte einzugehen und im Gegensatz vielmehr soziale Ungleichheiten aufrechterhält und fördert. Zudem wollen wir der Frage nachgehen was das mit uns und einer Gesellschaft macht, die diese Straflogiken über Jahrhunderte verinnerlicht hat. Im zweiten Teil schauen wir uns an welche Möglichkeiten, Ansätze und Methoden es gibt Konflikte zwischenmenschlich und nachhaltig zu bearbeiten. Im Sinne des transformative justice Ansatzes möchten wir diskutieren welches transformative Potenzial in diesen Alternativen steckt und welche Anwendung wir kurzfristig und langfristig auch in unseren Projekten und Gruppen sehen. Im dritten Teil möchten wir gerne mit euch diskutieren welche Zweifel, Ideen und Anwendungsgebiete es gibt und vielleicht auch an der ein oder anderen Vision herumbasteln.
