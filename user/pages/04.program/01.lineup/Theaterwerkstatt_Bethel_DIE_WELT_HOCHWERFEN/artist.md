---
category: others
ext_url: ' www.theaterwerkstatt-bethel.de'
id: Theaterwerkstatt_Bethel
place: MaWaLü
sample: https://www.youtube.com/watch?v=JU1RNtuk9Ns
subtitle: ''
summary_text: 'Die Performer:innen der Theaterwerkstatt Bethel greifen mit ihrer Performance
  das Spannungsverhältnis zwischen Maßlosigkeit und Verzicht, zwischen Ignoranz und
  Wertschätzung im Umgang mit der uns umgebenden Natur auf. '
taxonomy:
  tag:
  - theater
  - performance
  - storytelling
  - klangwelten
time_info: Sa 07.08. | 16:30 - 17:00
title: Theaterwerkstatt Bethel - DIE WELT HOCHWERFEN

---

Warum verhalten wir uns so maßlos? Und warum arbeiten wir so fleißig an unserem eigenen Aussterben? Zeugt nicht schon unser Umgang mit Pflanzen, deren sprachliche Bezeichnung als „Grünzeug“ oder „Unkraut“ von unserer Geringschätzung der Natur? Wir haben uns so sehr ins Wachsen, Gedeihen, Weiterentwickeln, Vorantreiben, Überschreiten, Wirtschaften, Produzieren und Konsumieren verliebt, dass wir vergessen haben, Grenzen zu setzen. Aber das kann man ändern!

Die Performer:innen der Theaterwerkstatt Bethel greifen mit ihrer Performanceproduktion DIE WELT HOCHWERFEN, das Spannungsverhältnis zwischen Maßlosigkeit und Verzicht, zwischen Ignoranz und Wertschätzung mit der uns umgebenden Natur auf.


Spieler:innen: Sven Bußmann, Pauline Elges, Irmgard Kreutz, Berit Meiners, Patrick Meiners, Sigrid Polanski, Ralf Strehl, Shiwa H. Roudsari, Dietmar Teich, Bettina Weber, Isabel Weber

Künstlerische Leitung: Mitja Brinkkötter, Lisa Saal, Nicole Zielke

Sounds & Musikalische Leitung: Luka Kleine

 

Das Projekt wird gefördert vom Sonderprogramm Neustart Kultur #takeaction​, Soziokultur NRW und vom Ministerium für Kultur und Wissenschaft des Landes NRW.

 

Veranstaltungshinweis:

Zur Liveperformance DIE WELT HOCHWERFEN gibt es vom 08.08.-31.08. die multimediale Naturinstallation UNKRAUT in der Kirche für Köln (St. Michael) am Brüsseler Platz zu sehen.  UNKRAUT lädt die Besucher:innen ein, einzutauchen - in ein Kleinod aus Kräutern, Blumen, Pflanzen, Geräuschen, Gesängen und Klängen. Die Seele baumeln zu lassen. Und dem Flüstern der Bäume, den warmen Tropfen von Wasser oder dem donnernden Wolkenbruch zu lauschen. Ganz leise, still und heimlich…

Die Sound- und Naturinstallation ist in Kooperation mit der Kirche für Köln (St. Michael), dem Gartenverein Querbeet und dem Kulturfestival Sommerblut entstanden. Sie steht symbolisch für die Wertschätzung von Naturoasen im Stadtraum.
