---
category: education
ext_url: ''
id: Fabian_Flues_Kathrin_Henneberger_Niels_Jongerius
name: Fabian Flues, Kathrin Henneberger & Niels Jongerius
place: Klimacamp Rheinland | WSZ 03
sample: ''
subtitle: Workshop
summary_text: ''
taxonomy:
  tag:
  - workshop
  - klimagerechtigkeit
time_info: Di 10.08. | 10:30 - 12:30
title: Energiecharta-Vertrag

---

Der Energiecharta-Vertrag ist die Geheimwaffe der fossilen Konzerne, um die Energiewende zu sabotieren. Er ermöglicht es Unternehmen wie RWE Klagen gegen Klimamaßnahmen einzureichen und hohe Entschädigungen einzufordern. In diesem Workshop wollen wir hinter die Kulissen des anti-Klima-Vertrags schauen und gemeinsam überlegen, was wir gegen das Abkommen tun können. 
Fabian arbeitet bei der Nichtregierungsorganisation PowerShift eine Kampagne für einen Ausstieg aus dem Energiecharta-Vertrag.Niels Jongerius arbeitet beim Transnational Institute in den Niederlanden und organisiert dort eine Kampagne gegen den Energiecharta-Vertrag. Kathrin ist vom Institute of Environmental Justice.
