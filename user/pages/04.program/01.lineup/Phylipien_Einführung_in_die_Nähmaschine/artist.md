---
category: others
ext_url: ''
id: ''
place: MaStaMo | Meisennest
sample: ''
subtitle: Skillshare
summary_text: ''
taxonomy:
  tag:
  - skillshare
time_info: Sa 14.08. | 10:30 - 11:30
title: Phylipien - Einführung in die Nähmaschine

---

Du wolltest schon immer reparieren, nähen, kreieren können? Dann komm zur Einführung in unserem Nähspace
