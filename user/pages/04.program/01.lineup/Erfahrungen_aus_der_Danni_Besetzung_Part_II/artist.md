---
category: education
ext_url: ''
id: ''
name: Danni/Gäst_innenhaus
place: MaStaMo
sample: ''
subtitle: Austausch und Diskussion - Part II
summary_text: ''
taxonomy:
  tag:
  - aktivismus
time_info: Do 12.08. | 16:45 - 18:15
title: Erfahrungen aus der Danni-Besetzung - Part II

---

Wie geht es weiter in der Klimagerechtigkeitsbewegung? Das Danni Gäst_innenhaus stellt seine Projekte vor.
