---
category: others
ext_url: https://klimakollektiv.org/
id: Klimakollektiv
place: Lützerather Wald
sample: ''
subtitle: Tanztheater
summary_text: ''
taxonomy:
  tag:
  - performance
  - theater
  - tanz
time_info: Do 12.08. 15:00 - 15:45, Fr 13.08. 15:00 - 15:45 & 19:00 - 19:30
title: Klimakollektiv - Haltestelle Irrweg

---

Aufgepasst: Die Performance findet NICHT im Lützerather Wald statt, sondern an der Bushaltestelle "Utopie" im Süden des Dorfes.  |||  Warten Sie mit uns an der Haltestelle Irrweg auf bessere Zeiten. Ein Duo zwischen Tanz & Theater nimmt uns mit auf eine bewegte Reise in mögliche Zukünfte des Rheinischen Braunkohlereviers. Humorvoll und poetisch lassen sie doch die Hoffnung nicht sterben und zelebrieren den Wahnsinn.
