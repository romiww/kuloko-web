---
category: education
ext_url: ''
id: ''
name: Phylipien
place: MaStaMo
sample: ''
subtitle: Austausch
summary_text: ''
taxonomy:
  tag:
  - diskussion
time_info: Fr 13.08. | 15:00 - 16:30
title: Flugreisen

---

Austausch- und Diskussionsraum zum Thema Flugreisen aus ökologischer Perspektive.
