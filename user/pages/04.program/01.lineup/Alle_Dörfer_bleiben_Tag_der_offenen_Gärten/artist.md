---
category: others
ext_url: ''
id: ''
place: ''
sample: ''
subtitle: In den Gärten der Dörfer - mehr Infos @AlleDoerfer
summary_text: ''
taxonomy:
  tag:
  - lokales
time_info: Sa 14.08. | 15:00 - 18:00
title: Alle Dörfer bleiben - Tag der offenen Gärten

---

Anwohnende öffnen ihre Gärten, um Menschen Einblicke zu gewähren, welche "Oasen" sich dort verbergen, die mensch sonst nicht zu Gesicht bekommt. Sie laden zum wandeln, verweilen und miteinander ins Gespräch kommen ein. All diese Plätze müssen erhalten bleiben! Sicherlich gibt unter dem ein oder anderen alten Obstbaum (oder an anderen lauschigen Plätzchen im Schatten) auch ein kühles Getränk oder einen Kaffee ;-). Die genauen Orte findet ihr vorher am Imfopunkt heraus.
