---
category: others
ext_url: https://twitter.com/tanzmili?lang=de
id: Militanz
place: Markt- und Theaterwiese
sample: ''
subtitle: antikapitalistische Mitmachperformance
summary_text: Wir wollen politische Messages durch Tanz und Performances emotional
  zugänglich machen, außerdem auch Aktivistis ein Tool geben sich während Aktionen
  zu empowern und zusammen Spaß zu haben! Wir wollen eine Performance zum Thema "Körper
  im Kapitalismus" kreieren und mit euch auf die Bühne bringen.
taxonomy:
  tag:
  - tanz
  - performance
time_info: Sa 14.08. | 16:00 - 16:30
title: Militanz

---


