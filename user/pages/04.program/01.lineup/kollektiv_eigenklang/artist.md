---
category: others
ext_url: https://soundcloud.com/eigenklang
id: kollektiv_eigenklang
place: Unser aller Wald
sample: https://www.youtube.com/watch?v=ML980zFGmvs
subtitle: INTERACTIVE AUDIOVISUAL EXPERIENCE
summary_text: ''
taxonomy:
  tag:
  - installation
  - interaktiv
  - klangwelten
time_info: Fr 06.08. 16:00 - So 08.08. 17:30
title: kollektiv eigenklang

---

Kollektiv Eigenklang is an audio-visual performance collective exploring the unlimited posibilities of improvised and interactive live producing.
