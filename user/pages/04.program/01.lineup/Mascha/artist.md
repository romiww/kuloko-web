---
category: music
ext_url: https://www.maschamusic.de
id: Mascha
place: Hofbühne
sample: ''
subtitle: Singersongwriter/ Pop Musik
summary_text: Von bekannten Klassikern, neuen Songs aus dem Radio, Balladen oder Popnummern,
  bis zu eigenen Liedern in deutscher oder englischer Sprache. Mascha singt ihre Songs
  unplugged in Gitarren- oder Klavierbegleitung. Wer Lust auf ein Set aus ruhiger,
  poppiger und Mitsingmusik hat, ist hier genau richtig.
taxonomy:
  tag:
  - singer-songwriter*in
  - akustik
  - cover
  - lokales
time_info: Fr 06.08. | 19:15 - 20:00
title: Mascha

---

Hi! Ich bin Mascha - 22-jährige Musikerin aus dem Kreis Heinsberg. Musik ist meine Leidenschaft und es macht mich glücklich, für und mit euch zu singen. Musikalisch ordne ich mich zwischen Singersongwriter und Popmusik ein. Ich stehe genauso auf gefühlvolle Balladen wie auch auf rockige und poppige Songs. Das spiegelt sich auch in meinen eigenen Songs wieder. Live performe ich meistens unplugged mit Klavier und Gitarre. Ich freue mich auf euch!
