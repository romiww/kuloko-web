---
category: education
ext_url: ''
id: Solidarnetz
name: Solidarnetz
place: Schlosswiese | WSZ 04
sample: ''
subtitle: Workshop
summary_text: Warum hören so viele Menschen auf, politisch aktiv zu sein? Warum bleibt
  politische Veränderung oft eine Nebensache? Vereinzelt stehen wir den ausbeuterischen
  Systemen scheinbar machtlos gegenüber. Es ist höchste Zeit, sich grundlegend anders
  zu organisieren. Mit Blümchen und Nia.
taxonomy:
  tag:
  - workshop
  - organisierung
time_info: Mo 09.08. | 10:30 - 12:30
title: Wie wir uns organisieren können

---


