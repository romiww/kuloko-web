---
category: education
ext_url: ''
id: ''
name: ''
place: Klimacamp Rheinland
sample: ''
subtitle: Mehr Infos vor Ort
summary_text: ''
taxonomy:
  tag:
  - workshop
time_info: Täglich von 10:30 -12:30 und von 14:30-16:30 Uhr.
title: Workshops im Open Space

---

Täglich von 10:30 -12:30 und von 14:30-16:30 Uhr.
