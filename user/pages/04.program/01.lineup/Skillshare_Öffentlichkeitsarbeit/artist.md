---
category: education
ext_url: ''
id: Kathrin_Henneberger
name: Kathrin Henneberger
place: Schlosswiese | WSZ 04
sample: ''
subtitle: Workshop
summary_text: ''
taxonomy:
  tag:
  - klimagerechtigkeit
  - kommunikation
  - workshop
time_info: Sa 14.08. | 10:30 - 12:30
title: Skillshare Öffentlichkeitsarbeit

---

Ihr überlegt aktiv zu werden für Klimagerechtigkeit, habt vielleicht schon Ideen oder Konzepte für Projekte? Ihr wollt beginnen für Klimagerechtigkeit das Mikro zu ergreifen? Wollt vielleicht mit euren eigenen Projekten für Klimagerechtigkeit durchstarten und sucht Tipps für die Presse- und Öffentlichkeitsarbeit? Hier seid ihr richtig! In unserem Workshop geben wir euch eine Einführung und beraten auch gerne eure Öffentlichkeitsarbeit für Klimagerechtigkeitsprojekte.
