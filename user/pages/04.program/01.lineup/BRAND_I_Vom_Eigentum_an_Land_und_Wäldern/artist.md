---
category: education
ext_url: ''
id: Susanne_Fasbender_Peter_Emorinken_Donatus
name: Susanne Fasbender & Peter Emorinken Donatus
place: Scheunenkino
sample: ''
subtitle: Anschließend Gespräch zu neokolonialem Klimaschutz mit Filmemacherin Susanne
  Fasbender und Peter Emorinken Donatus
summary_text: ''
taxonomy:
  tag:
  - film
  - kolonialismus
time_info: Di 10.08. | 17:00 - 20:00
title: BRAND I - Vom Eigentum an Land und Wäldern

---

In Zusammenarbeit mit der Gesellschaft für bedrohte Völker.
