---
category: music
ext_url: https://bonbonlegerin.bandcamp.com
id: Bonbonlegerin
place: Wiesenbühne
sample: ''
subtitle: Folk Punk
summary_text: ein Banjo-lastiger Bonbonanschlag!
taxonomy:
  tag:
  - punk
  - folk
  - akustik
time_info: So 15.08. | 15:00 - 16:00
title: Bonbonlegerin

---


