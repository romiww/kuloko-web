---
category: education
ext_url: ''
id: ''
name: ''
place: Klimacamp Rheinland | Zirkuszelt
sample: ''
subtitle: BIPoC only panel
summary_text: ''
taxonomy:
  tag:
  - podium
  - organisierung
time_info: Sa 14.08. | 17:30 - 19:00
title: Kämpfe verbinden

---

Panel with Adrian Blount, Yi Yi Prue and more speakers to be announced. Facilitated by Lea Latoya.
