---
category: others
ext_url: ''
id: Riadh_Ben_Ammar
place: Markt- und Theaterwiese
sample: https://www.hor-dresden.de/die-falle-von-riadh-ben-ammar
subtitle: ''
summary_text: ''
taxonomy:
  tag:
  - theater
  - performance
  - storrytelling
time_info: Sa 14.08. | 21:00 - 22:00
title: Riadh Ben Ammar - Die Falle

---

Die »Die Falle« ist eine Geschichte über die geschlossene EU-Außengrenze und ihre Missverständnisse. Man kann nicht an der Küste leben, ohne die andere Seite wenigstens einmal gesehen zu haben. In Tanger, Algier oder Tunis sitzen selbst die Katzen im Hafen und schauen auf die andere Seite. Alle wollen dahin. Die meisten träumen davon. Die jungen Leute, die es schaffen in Europa zu landen, versuchen alles, um nicht wieder mit leeren Händen zurückzukehren. Illegalität, Kriminalität und die ständige Angst abgeschoben zu werden sind ihr Alltag.
| Im Anschluss wird ein Nachgespräch stattfinden |
