---
category: education
ext_url: ''
id: Camila_Schmid
name: Camila Schmid
place: Klimacamp Rheinland | WSZ 01
sample: ''
subtitle: Und was hat das mit uns zu tun?
summary_text: ''
taxonomy:
  tag:
  - workshop
  - kolonialismus
  - rassismus
time_info: Di 10.08. | 10:30 - 12:30
title: Kolonialismus, Rassismus...

---

Anti-Rassismus und Dekolonialisierung in sozialen Bewegungen umsetzen: Ein kritischer Blick auf Klima- und Tierrechtsbewegungen 

Durch diesen Workshop sollen Teilnehmende an die Themen Kolonialismus und Rassismus eingeführt werden, um danach Klima- und Tierrechtsbewegungen in eben diesen Unterdrückungssystemen einordnen zu können. Es sollen proaktive Umgangsweisen aufgezeigt werden die helfen sollen, den eigenen Aktivismus rassismuskritisch(er) und dekolonial(er) zu gestalten. 

Dieser Workshop ist in erster Linie für weiße Allies und jene die es noch werden möchten konzipiert. Trotzdem sind alle herzlich willkommen.
