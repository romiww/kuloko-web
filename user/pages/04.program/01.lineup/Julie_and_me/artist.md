---
category: music
ext_url: https://www.facebook.com/julieandme.music/
id: Julie_and_me
place: Hofbühne
sample: https://youtu.be/2gX3kPoLDrI
subtitle: Akustische Zwei-Klang-Band
summary_text: Gitarre und Posaune, Gesang
taxonomy:
  tag:
  - akustik
  - singer-songwriter*in
time_info: Sa 07.08. | 18:30 - 19:15
title: Julie and me

---

minimalistisch, emotional, intim
