---
category: others
ext_url: ''
id: Die_Kraft_der_Schmetterlinge
place: Scheunenkino
sample: ''
subtitle: Dokumentation
summary_text: ''
taxonomy:
  tag:
  - film
time_info: Mo 09.08. | 20:00 - 21:30
title: Die Kraft der Schmetterlinge

---

Im Rahmen der Chaostheorie spricht man vom „Schmetterlingseffekt“, was bedeutet, dass kleinste Abweichungen langfristig ein ganzes System vollständig und unvorhersagbar verändern können... Politiker:innen verschiedener Länder betonen immer wieder, dass es derzeit keine Alternativen zum neoliberalen Wirtschafts- und Lebenskonzept gibt. “Die Kraft der Schmetterlinge” ist 2011 auf einer Reise von Mexiko nach Panama entstanden und handelt von Menschen in Mittelamerika, die statt auf Lösungen seitens ihrer Regierungen zu hoffen – sich zusammentun, sich organisieren und so eigene Lösungen und Alternativen schaffen.
