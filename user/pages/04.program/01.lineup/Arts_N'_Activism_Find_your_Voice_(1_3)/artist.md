---
category: education
ext_url: https://www.bundjugend-nrw.de/projekte/arts-activism/
id: Melchi_Vepouyoum_Christoph_Ebel
name: Melchi Vepouyoum & Christoph Ebel
place: MaStaMo
sample: ''
subtitle: Workshopreihe
summary_text: 'Meine Stimme und Ich: Stimmbildung, meine Stimme gesund halten und
  Selbstbewusst einsetzen'
taxonomy:
  tag:
  - workshop
  - kunst
  - ''
time_info: Di 10.08. | 15:00 - 18:15
title: Arts N' Activism - Find your Voice (1/3)

---

Wie können wir Kunst und Aktivismus verbinden? Wie kann ich meine Stimme
 gesund halten und kraftvoll für eine Veränderung in der Welt erheben? 
Wie kann Musik im Protest kreativ eingesetzt werden? Diesen Fragen 
widmen sich die drei Workshops, die unabhängig voneinander besucht 
werden können.
Methoden und Inhalte: Singen, Stimmbildung, Körperarbeit (Alexandertechnik), Bodypercussion, gemeinsames Musizieren
