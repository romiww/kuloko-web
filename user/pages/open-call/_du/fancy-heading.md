---
title: Du...
heading_colors: yellow-bg
---

+ bist Teil eines **Wanderzirkusses**?  
+ schreibst auf Arbeit heimlich **rassismuskritische Gedichte**?  
+ spielst mit deinen Kindern in einer **Band**?  
+ willst mit deinen **Drag Skills** mal wieder alle verzaubern?  
+ wartest nur auf eine Gelegenheit, mit deiner **Theatergruppe** mal aufzutreten?  
+ hast Lust tolle Menschen kennen zu lernen, die Hobbies, Politik oder bald **Freundschaften** mit dir teilen?  
+ hast vom **06. bis 15. August noch nichts vor**?   
+ und hast **Lust bei der KuloKo dabei zu sein?**
