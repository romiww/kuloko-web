---
title: 'Schickt uns eine Mail'
body_classes: modular
background:
    image: /user/pages/images/guitars-light.png
    repeat: true
url: 'mailto:booking@kultur-ohne-kohle.de'
paragraph_styles: light
---

+ mit dem Betreff « Open Call » und 2-3 Sätzen über euch,
+ einer Hör-, Seh- oder Fühlprobe und
+ wenn ihr habt einen Link zu eurer Website oder Social-Media-Accounts  
An <span class= "light"> [booking@kultur-ohne-kohle.de](mailto:booking@kultur-ohne-kohle.de) </span>