---
title: discrimination
---

####ACH JA, LAST BUT NOT LEAST – EINE SACHE IST UNS FURCHTBAR WICHTIG

Schreib uns besonders gern, wenn du dich als **FINTA<span class="popover">(?)<span class="popover-container">FINTA – Frau, Inter, Non-binary, Trans, Agender</span></span>**, **BIPoC<span class="popover">(?)<span class="popover-container">BIPoC (Black Indigenous People of Colour) <span class="montserrat">ist eine selbstgewählte Bezeichnung von verschiedensten Menschen, die sich als nicht-weiß definieren. In der Mehrheitsgesellschaft gilt weiß nach wie vor als Norm und nicht-weiß als Abweichung davon. Was BIPoC miteinander verbindet, sind geteilte Rassismuserfahrungen, Ausgrenzung von der Mehrheitsgesellschaft und kollektive Zuschreibungen des « Andersseins ». In weiß dominierten Gesellschaften sind nicht-weiße Menschen seit der Kolonialzeit von Rassismus betroffen – bis heute. (Quelle : Missy Magazine)</span></span></span>**, von der Gesellschaft be_hindert, nicht-akademisch, LGBTQIA+ <span class="popover"> (?) <span class="popover-container"> LGBTQIA+ ist ein Akronym und steht für Lesbian, Gay, Bisexual, Transgender, Queer/Questioning, Intersex, Asexuell/Aromantisch und alle anderen, sich selbst der queeren Community zugehörig fühlenden Menschen. </span> </span> und/oder Refugee indentifizierst !

Die Gesellschaft, in der wir leben, gibt _weißen_<span class="popover">(?)<span class="popover-container">weiß ist keine ermächtigende Selbstbezeichnung, sondern eine privilegierte Position innerhalb eines rassistischen Systems. Deshalb wird weiß klein und kursiv geschrieben.
(Quelle: Missy Magazine)</span></span> cis<span class="popover">(?)<span class="popover-container">cis – Menschen, die sich mit dem Geschlecht wohlfühlen, das ihnen bei der Geburt zugewiesen wurde</span></span>-Männern viel zu oft eine Bühne. Vielen Menschen wird dadurch Sichtbarkeit verwehrt und sie werden strukturell in und an dem, was sie gern tun möchten, gehindert. Um dem entgegenzuwirken, haben wir entschieden, für das offizielle Bühnenprogramm der KuloKo keine Acts zu buchen, die ausschließlich aus weißen abled hetero-cis-Männern bestehen.

Du identifizierst dich als _weiß_ und cis-männlich und möchtest mit deinem Act
trotzdem super gern für globale Klimagerechtigkeit und den Kohleausstieg
spielen? Mega nice! Tramp doch einfach zu uns runter und erfreu dich und uns
mit Straßenmusik oder -theater – bloß Gage und die große Bühne wird es nicht
werden bei uns.