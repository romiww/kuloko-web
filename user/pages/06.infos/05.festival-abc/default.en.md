---
title: 'Festival ABC'
menu: 'Festival ABC'
hero_background: /user/pages/images/ABC.jpg
hero_foreground: /user/pages/images/bar.svg
hero_title_color: light
---

The festival ABC should help that all visitors can feel comfortable at the Kultur-ohne-Kohle Festival. In order to achieve this together, we wish that the instructions listed here are kept in mind and that we treat each other with respect. We understand the ABC as an unfinished, open process. Please feel free to give your feedback.

#### Journey/ Arrival
See [Journey](/infos/how-to-get-there)

#### Dismantling Period
The KuloKo festival is organized collectively by all participants, which is why every supporting hand, every creative head and everyone who simply enjoys participating is important.
Help to dismantle the KuloKo together!
**Dismantling period: 16.-22.08.2021**

#### Festival Contribution And Donations
The festival is mainly supported by grants and donations. The idea is that everyone participates according to their financial situation. One thing is certain: participation should in no case fail because of money. Every contribution helps us. Thank you for your support! <br>
[Donate now.](/contribute/donate)

#### Fire
Fire and barbecue is not allowed due to high fire risk. Please pay attention to this not only on the camp site, but also in other places of the KuLoKo and point it out to others. We ask you to take mobile ashtrays with you, also with regard to the fire hazard. Do not throw your cigarette stubs into garbage cans or into the environment, because things can easily catch fire. Please keep yourself and others safe!

#### Photos, film and press
Whenever you pull out your video or photo camera, all people who will be recognizable on the pictures must be informed in advance and asked for permission. This also applies to the press. We kindly ask you to inform other people about the sensitive handling of pictures. If you feel that your photo rights are being violated by other participants, please come to the press tent or the awareness tent. 
Spontaneous filming and snapping is prohibited in the workshop tents, retreats, sanitary facilities, kitchen and sleeping areas. This applies equally to everyone - festival participants, film and camera teams, and the press. Since the areas are sometimes difficult to distinguish, it is best to always ask if it is okay for bystanders to take pictures. 

The Press AG takes care of press representatives at the Kuloko. 
Since the AG cannot have its eyes everywhere at the same time, we ask you to refer press representatives to the press tent, especially if you are approached directly. If you want to give an interview, please answer as a private person.

#### Lost And Found
At the vigils Lützerath and Keyenberg and at the info tent of the climate camp there are lost and found boxes. We will keep them for one month after the camp. If you are missing something, please contact us at info@kultur-ohne-kohle.de until the end of September. After that we will give the things to a free shop.

#### Health 
Take care of yourself and others, drink enough water and please pay attention to the Corona info you will get at the vigils and other info points.

#### Info Points
The vigils Lützerath and Keyenberg are your first contact points. Here you will find information about the Kuloko, the area, the program, the Lost and Found and also the donation box for the camp contribution. There is also a lot of literature, flyers and all kinds of things to discover. You can also sign up for shifts here. In addition to the vigils, there is also an information table at almost every event location with contact persons and information about the locations.
The Klimacamp Rheinland in Lützerath has its own info point.

#### Küfa (Community Kitchen)
There is a great vegan Küfa, which prepares food and is financed by donations. The money for this is collected by the kitchen itself. It is up to you how much you can and want to give. 
Your financial possibilities should not play a role in whether you will be well taken care of. So if you don't have the possibility to donate, that's fine. You do not have to feel bad. 
Please disinfect your washed hands before the food distribution, wear the mask and remind your neighbors as well.
For environmental and political reasons, the food will be vegan (with little exceptions which will be marked).
If you have any allergies or intolerances, please contact the kitchen team directly.

#### Upper Body And Nudity 
We would like KuloKo to be a place where we deal with privileges and mechanisms of domination and at the same time try out and reflect on possibilities of dealing with nudity. We would first like to hold on to the request that cis men clothe their upper bodies during the festival. We hope that the "toplessness debate" will provoke learning processes among all of us - especially among cis men - and that there will be a lively exchange about privileges and how to deal with them. 
Children are excluded here and should decide for themselves (or their caregivers) how they want to walk around.

#### Political Parties
Please keep in mind: The KuloKo Festival is non-partisan and not an election campaign event.  Diverse and creative banners that spruce up the camp with political messages are welcome. There is space for your flyers at the info points. 

#### Spaces For Relaxation 
There are several designated retreats at the festival, such as the Awareness Tent and a BIPoC Safer Space. Instructions for the use of these tents are posted at their entrances. Please respect that people want to relax here from the daily festival routine (see Awareness).

#### Sanitary Facilities
There will be composting toilets ("Kompoletten") at the festival. 
Use water sparingly and use only biodegradable soaps, shampoos and recycled toilet paper. When using the Kompolettes, be sure to follow the instructions. The cleaning of the toilets will be done together. It would be great if as many people as possible would sign up for the cleaning team. You can find the lists at the info points. 

#### Shuttle
To enable a barrier-free community, we will try to include a small shuttle concept. 
More detailed information can be found at the info points or in the Telegram channel.
At the info points at MaWa Lützi and MaWa Key there are lists where you can sign up if you have the possibility to drive people from the train station to KuloKo or if you can see if someone is driving. There is also a [Telegram Networking Channel](https://t.me/kuloko), which also serves as a ride-sharing platform, among other things. 
Please let us know in advance if you need to be shuttled. We will always find a way.
More info about travel and shuttle can be found at [Journey](/infos/how-to-get-there)


#### Shift Plans
The KuloKo Festival is a self-organized space.  For this to work well, the active participation of everyone is necessary.   At the info points, like the one at MaWa (vigil) Keyenberg, you will find shift plans with the open tasks. For most tasks, no prior knowledge is needed. More complex tasks can be seen as an opportunity to learn exciting new things. We would like to see especially repro work (e.g. cleaning, cooking...) distributed in a gender-balanced way.
Besides the possibility to actively participate in camp life by taking shifts, there are many other ideas of participation (see [Stations of Resistance](/program-stations-of-resistance)). Incidental tasks are for example sitting together and chopping vegetables for dinner or the moderation of a plenary or even freshening up the compoletes again. 
Information about the shift plans can be found in the [Telegram Infochannel](https://t.me/kulokofestival).

#### Security
Not all people want a quick end to lignite. For this reason, we must be prepared for disturbances of the camp. On the public areas, there may also be contact with securities from RWE and police officers. To avoid problems for the festival, there is a security team and a security concept. In order for the security concept to work, we absolutely need your cooperation, e.g. with night guards. You can get all information at the info points, e.g. at MaWaLü and MaWaKay.

#### Water
For the sake of sustainability, please think about saving water and consider how often you need to shower. Please also make sure that the soaps, toothpastes, shower gels etc. you use are biodegradable. 
Drinkable water is specially marked. 

#### Telegram 
Telegram is a messenger similar to Whatsapp and Signal. There are currently 2 different Kuloko Telegram channels:
1. [The Kuloko Infochannel](https://t.me/kulokofestival) <br>
        Here you can get daily info about shifts, schedules and much more. The channel only works one way, which means not everyone can post messages in here.
2 [The Kuloko Networking Group](https://t.me/kuloko) <br>
        This is a place to network with other festival participants. Whether you're looking for a ride, soccer teammates, or your bag, you can forward your requests and info directly to everyone here.

#### Camping
Tent possibilities are available at the camp of MaStaMo (Keyenberg) and the Klimacamp Rheinland (Lützerath). There will be marked areas where you can pitch your tents. Please camp only within these areas. Please pay attention to a space-saving set-up, so that later arrivals will still find enough space. Campers and other vehicles can be parked at the campground in Keyenberg.
