---
title: 'Festival ABC'
menu: 'Festival ABC'
hero_background: /user/pages/images/ABC.jpg
hero_foreground: /user/pages/images/bar.svg
hero_title_color: light
---

Das Festival ABC soll dabei helfen, dass sich alle Besucher\*innen auf dem Kultur-ohne-Kohle Festival wohlfühlen können. Um das gemeinsam zu schaffen, wünschen wir uns, dass die hier aufgeführten HInweise beachtet werden und wir uns gegenseitig mit Respekt begegnen. Wir verstehen das ABC als unvollendeten, offenen Prozess. Gebt uns hierzu gerne Euer Feedback.

#### Anreise/Anfahrt
Siehe [Anfahrt](/infos/how-to-get-there)

#### Abbauphase
Das KuloKo-Festival wird von allen Teilnehmenden kollektiv organisiert, weshalb jede unterstützende Hand, jeder kreative Kopf und alle denen es einfach Spaß macht, mitzugestalten, wichtig sind.
Helft mit, die KuloKo gemeinsam abzubauen!
**Abbauphase: 16.-22.08.2021**

#### Festivalbeitrag und Spenden
Das Festival trägt sich hauptsächlich durch Förderungen und Spenden. Die Idee ist, dass sich alle gemäß ihrer finanziellen Situation beteiligen. Fest steht: am Geld sollte die Teilnahme auf keinen Fall scheitern. Jeder Beitrag hilft uns. Vielen Dank für deine Unterstützung! <br>
[Jetzt Spenden](/contribute/donate)

#### Feuer
Feuer und Grillen ist aufgrund großer Brandgefahr nicht erlaubt. Achtet nicht nur auf dem Campgelände, sondern auch an anderen Orten der KuLoKo darauf und weist auch Andere darauf hin. Wir bitten Euch mobile Aschenbecher auch hinsichtlich der Brandgefahr mit zu nehmen. Schmeißt auf keinen Fall Eure Zigarettenstümmel in Mülltonnen oder in die freie Natur, da sich hier leicht Sachen entzünden können. Please keep yourself and others safe!

#### Fotos, Film und Presse
Wann immer Ihr Eure Video- oder Fotokamera zückt, müssen alle Menschen, die auf den Bildern erkennbar sein werden, vorher informiert und um Erlaubnis gefragt werden. Dies gilt ausdrücklich auch für die Presse. Wir bitten Euch, auch andere Menschen freundlich auf den sensiblen Umgang mit Bildern hinzuweisen. Solltet Ihr Euch durch andere Teilnehmer\*innen in Euren Fotorechten verletzt fühlen, kommt gerne zum Presse- oder Awarenesszelt. 
In den Workshopzelten, Rückzugsorten, Sanitäranlagen, der Küche und den Schlafbereichen ist spontanes Filmen und Knipsen untersagt. Dies gilt für alle gleichermaßen – für Festivalteilnehmer*innen, für Film- und Kamerateams sowie für die Presse. Da die Bereiche manchmal schwer abzugrenzen sind, fragt bei Unklarheiten am besten einfach immer, ob Aufnahmen für umstehende Personen in Ordnung sind. 

Die Presse AG betreut Pressevertreter\*innen auf der KuLoKo. 
Da die AG ihre Augen nicht überall gleichzeitig haben kann, bitten wir euch, Pressevertreter\*innen auf das Pressezelt zu verweisen, insbesondere wenn ihr direkt angesprochen werdet. Solltet ihr ein Interview geben wollen, dann antwortet bitte als Privatperson.

#### Fundgrube
An den Mahnwachen Lützerath und Keyenberg und dem Infozelt des Klimacamps gibt es Kisten mit Fundsachen. Wir bewahren die Sachen nach dem Camp für einen Monat auf. Solltet Ihr etwas vermissen, meldet Euch bitte bis Ende September unter info@kultur-ohne-kohle.de. Danach geben wir die Sachen in einen Umsonstladen.
Gesundheit 
Achte auf dich und Andere, trinke genug Wasser und bitte beachte die Corona-Infos, die Du an den Mahnwachen und anderen Infopunkten erhälst.
Infopunkte
Die Mahnwachen Lützerath und Keyenberg sind Eure ersten Anlaufstellen. Hier findet Ihr Informationen zur Kuloko, zum Gelände, dem Programm, der Fundgrube und auch der Kasse für den Campbeitrag. Außerdem gibt es viel Schmökerstoff, Flyer und allerlei zu entdecken. Außerdem könnt Ihr Euch hier für Schichten eintragen. Neben den Mahnwachen gibt es auch bei fast jedem Veranstaltungsort einen Infotisch mit Ansprechpersonen und Infos zu den Orten.
Das Klimacamp Rheinland in Lützerath hat noch mal einen eigenen Infopunkt.
#### Hunde
Hunde bitte auf dem Festivalgelände angeleint lassen. Danke dir dafür!
#### Küfa (Küche für alle)
Es gibt eine großartige vegane Küfa, die sich durch Spenden finanziert. Das Geld hierfür sammelt die Küche selbst. Es liegt bei Dir wie viel Du geben kannst und willst. 
Deine finanziellen Möglichkeiten sollen aber keine Rolle dabei spielen ob Du bei uns gut versorgt wirst. Wenn Du also keine Möglichkeit hast zu spenden ist das völlig in Ordnung. Du brauchst dich nicht schlecht zu fühlen. 
Bitte desinfiziert Eure gewaschenen Hände vor der Essensausgabe, tragt die Maske und erinnert auch Eure Nachbar\*innen daran.
Aus ökologischen und politischen Gründen wird für alle, bis auf kleine gekennzeichnete Ausnahmen, vegan gekocht.
Wenn Du irgendwelche Allergien oder Unverträglichkeiten hast, melde Dich bitte direkt beim Küchenteam.

#### Oberkörper und Nacktheit 
Wir wünschen uns, dass die KuloKo ein Ort ist, an dem wir uns mit Privilegien und Herrschaftsmechanismen auseinandersetzen und gleichzeitig Möglichkeiten des Umgangs mit Nacktheit ausprobieren und reflektieren. Wir möchten zunächst an der Bitte festhalten, dass Cis*-Männer während des Festivals ihre Oberkörper bekleiden. Wir wünschen uns, dass die „Oben-mit-Debatte“  Lernprozesse  bei  uns  allen  –  besonders  bei  Cis*-Männern anstößt und ein reger Austausch über Privilegien und wie damit umgegangen werden könnte stattfindet. 
Kinder sind hier ausgeklammert und sollen selbst (oder Ihre Bezugspersonen) entscheiden, wie sie herumlaufen möchten.

#### Parteien
Bitte  bedenkt:  Das KuloKo-Festival  ist  parteiunabhängig  und  keine  Wahlkampfveranstaltung.   Vielfältige   und   kreative   Banner,   die   das  Camp mit politischen Botschaften verschönern, sind willkommen. Für Eure Flyer ist Platz an den Infopunkten. 
#### RÜckzugsorte
Auf dem Festival gibt es mehrere ausgewiesene Rückzugsorte, wie z.B. das Awareness-Zelt und einen BIPoC Safer Space. Hinweise für die Nutzung dieser Zelte hängen an ihren Eingängen. Bitte respektiert, dass sich hier Menschen vom Festival-Alltag erholen möchten (siehe Awareness).
#### Sanitäre Anlagen
Es wird auf dem Festival Kompoletten geben. 
Geht sparsam mit dem Wasser um und benutzt ausschließlich biologisch abbaubare Seifen, Shampoos und recyceltes Toilettenpapier. Bei den Kompoletten gilt es die Anleitungen zu beachten. Die Reinigung der Toiletten erfolgt gemeinsam. Es wäre super, wenn sich möglichst viele Menschen für das Putzteam melden. Die Listen dafür findet ihr an den Infopunkten. 
#### Shuttle
Um ein barrierefreies Miteinander zu ermöglichen, werden wir versuchen ein kleines Shuttle-Konzept mit aufzunehmen. 
Genauere Infos dazu sind an den Infopunkten oder im Telegram-Channel zu finden.
An den Infopunkten an der MaWa Lützi und MaWa Key hängen Listen, in die Ihr Euch eintragen könnt, wenn Ihr die Möglichkeit habt, Menschen vom Bahnhof zur KuloKo zu fahren oder einsehen könnt, ob gerade jemensch fährt. Außerdem gibt es einen [Telegram Vernetzungs Channel](https://t.me/kuloko), der unter anderem auch als Mitfahrbörse dient. 
Bitte lasst uns im Vorhinein wissen, falls Ihr geshuttelt werden müsst. Eine Möglichkeit wird sich immer finden.
Mehr Infos zu Anreise und Shuttle gibt es unter [Anreise](/infos/how-to-get-there)


#### Schichtpläne
Das  KuloKo Festival  ist  ein  selbstverwalteter  Raum.  Damit  dies  gut funktioniert,  ist  die  aktive  Beteiligung  von  allen  nötig.   An den Infopunkte, wie die an der MaWa Keyenberg, findet ihr Schichtpläne mit den offenen Aufgaben. Für die meisten Aufgaben wird kein Vorwissen benötigt. Komplexere Tätigkeiten, können als Möglichkeit gesehen werden, spannende neue Sachen zu lernen. Wir wünschen  uns,  dass  wir  besonders  Repro-Arbeiten  (z.B. Putzen,  Kochen...)  geschlechtergerecht verteilen.
Neben  der  Möglichkeit,  sich  durch  Schichten  aktiv  ins  Campleben  einzubringen, gibt  es  viele  weitere  Ideen  der  Beteiligung (Siehe [Stationen des Widerstands](/program-stations-of-resistance)).Anfallende Aufgaben sind zum Beispiel zusammen sitzen und Gemüse für das Abendessen schnippeln oder die  Moderation eines Plenums oder auch die Kompoletten wieder frisch machen. 
Infos zu den Schichtplänen gibts im [Telegram Infochannel](https://t.me/kulokofestival)

#### Sicherheit
Nicht alle Menschen wünschen sich ein schnelles Ende der Braunkohle. Aus diesem Grund müssen wir auf Störungen des Camps vorbereitet sein. Auf den öffentlichen Flächen kann es außerdem zu Kontakt mit Securities von RWE und Beamt\*innen der Polizei kommen. Damit daraus keine Probleme für das Festival entstehen, gibt es ein Sicherheitsteam und ein Sicherheitskonzept. Damit das Sicherheitskonzept funktioniert, brauchen wir unbedingt Eure Mitarbeit, z.B. bei Nachtwachen. Alle Infos dazu erfahrt Ihr an den Infopunkten z.B. der MaWaLü und der MaWaKay.
Wasser
Bitte denkt im Sinne der Nachhaltigkeit daran Wasser zu sparen und überlegt Euch, wie oft ihr duschen müsst. Bitte achtet auch beim Duschen und Waschen darauf, dass Eure verwendeten Seifen, Zahnpasten, Duschgels etc. aus biologisch abbaubaren Inhalten bestehen. 
Trinkbares Wasser ist extra gekennzeichnet. 

#### Telegram 
Telegram ist ein Messenger, ähnlich wie Whatsapp und Signal. Es gibt zur Zei 2 verschiedene Kuloko Telegram Kanäle:
1. [Der Kuloko Infochannel](https://t.me/kulokofestival) <br>
        Hier gibt es tagesaktuelle Infos zu Schichten, Zeitplänen und vielem mehr. Der Kanal funktioniert nur in eine Richtung, das heißt nicht alle können Nachrichten hier rein schreiben.
2. [Die Kuloko Vernetzungsgruppe](https://t.me/kuloko) <br>
        Hier ist Platz, um sich mit anderen Festivalteilnehmer\*innen zu vernetzen. Ob ihr eine Mitfahrgelegenheit, Fußball Mitspieler\*innen oder euren Brustbeutel sucht, hier könnt ihr eure Anfragen und Infos direkt an alle weiterleiten.

#### Zelten
Zeltmöglichkeiten gibt es auf dem Camp von MaStaMo und dem Klimacamp Rheinland. Es wird abgesteckte Flächen geben, auf denen ihr Eure Zelte aufstellen könnt. Bitte zeltet nur innerhalb dieser Bereiche. Achtet bitte auf ein platzsparendes Aufstellen, damit später Anreisende noch genug Platz finden. Wohnmobile und andere Schalfgefährte, können auf dem Camperplatz in Keyenberg abgestellt werden.
