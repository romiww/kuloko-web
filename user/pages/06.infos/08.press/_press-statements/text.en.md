---
title: 'Press Key Theses'
body_classes: modular
image_align: left
---

## CULTURE WITHOUT COAL IS...
"Kultur ohne Kohle - eine rheinische Landpartie" ("Culture Without Coal - a Rhenish Country Outing") is a festival around the villages at the open cast mine Garzweiler II. For 10 days the festival invites people to art and culture to strengthen the resistance against lignite and to stand up for global climate justice and to stop the excavators. Culture Without Coal wants to bring together art, resistance, culture and above all people to build a sustainable, democratic and just future.
    
For context on the resistance at the Garzweiler II open pit mine, see [Resistance].
(https://kultur-ohne-kohle.de/resistance/the_villages).

**Email: **presse@kultur-ohne-kohle.de

**Phone: **+4915758128656 (Noura Hammouda)