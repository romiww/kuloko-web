---
title: 'Press Releases'
content:
    items:
        -
            '@taxonomy.category': press_release
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
show_sidebar: false
show_pagination: true
---

