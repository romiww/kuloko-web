---
title: 'Erste Pressemitteilung'
---

### „Kultur ohne Kohle“-Festival am Garzweiler-Tagebau: Mit Kunst und Kultur gegen Zerstörung

Der Widerstand gegen Garzweiler II wird groß und bunt

Klimaaktive planen vom 6.-15. August 2021 das „Kultur ohne Kohle-Festival“ im Rheinland rund um den Tagebau Garzweiler II. Die ehrenamtlichen Organisator*innen wollen die vom Abriss bedrohten Dörfer zum Treffpunkt für Menschen von nah und fern machen, um lebendig und kreativ den lokalen Widerstand der Dörfer zu unterstützen, die durch RWE-Bagger bedroht sind.

Die Pressesprecherin Noura Hammouda zu der Idee des Festivals: „ Wir feiern den Widerstand, der seit Jahren wächst und für eine klimagerechte, braunkohlefreie Zukunft und für den Erhalt der Dörfer kämpft. Die Corona-Krise hat gezeigt, wie ungerecht die Verteilung finanzieller Mittel ist. Deshalb wollen wir einen Ort schaffen, an dem wir uns unabhängig von Geld auf Augenhöhe begegnen.”

„Kultur ohne Kohle“ lädt Menschen ein, kostenlos an Kultur teilzuhaben. Das Festival bietet Musik, Ausstellungen, Kunst, Workshops, politische Diskussionen, familienfreundliche Unterhaltung mit einem Zirkus und Raum für Kreativität.

„Zwangsumsiedlungen, Kulturverlust, soziale Zermürbung und Klimazerstörung finden mitten in Deutschland statt. Mit der Veranstaltung wollen wir darauf aufmerksam machen und ein politisches Statement setzen: Braunkohle hat mit uns keine Zukunft!“, sagt Lisa Allisat aus dem Organisationskomitee des Festivals.

Noch 18 weitere Jahre möchte der Kohlekonzern RWE am Tagebau Garzweiler II Braunkohle abbaggern. Trotz zahlreicher Gutachten, die belegen, dass die Braunkohle für die Energieversorgung in Deutschland nicht mehr nötig ist, wird diese Entscheidung vom Land NRW gestützt. Wenn der Tagebau weiterhin vergrößert wird, kann Deutschland seinen Beitrag zu den Pariser Klimazielen nicht einhalten.