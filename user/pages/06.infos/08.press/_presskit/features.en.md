---
title: 'Presse Kacheln'
class: standard
features:
    -
        header: Contact
        text: 'Feel free to contact us.'
        url: 'mailto:presse@kultur-ohne-kohle.de'
        button: 'Send us a mail!'
        colors: turkoise-bg
    -
        header: 'Press releases'
        text: 'Here you will find our press releases shortly.'
        url: /press/press-releases
        button: PM-Portal
        target: blank
        colors: turkoise-bg
    -
        header: Photos
        text: 'Here is a photo portal for all kinds of photos of the KuloKo. Please indicate the photographer if necessary.'
        url: /photos
        button: 'Show it!'
        target: blank
        colors: turkoise-bg
---

## Press Kit