---
title: 'Presse Kacheln'
class: standard
features:
    -
        header: Kontakt
        text: 'Kontaktieren Sie uns gerne'
        url: 'mailto:presse@kultur-ohne-kohle.de'
        button: 'Mail schreiben!'
        colors: turkoise-bg
    -
        header: Pressemitteilungen
        text: 'Hier finden Sie in Kürze unsere Pressemitteilungen'
        url: /press/press-releases
        button: PM-Portal
        target: blank
        colors: turkoise-bg
    -
        header: Fotos
        text: 'Hier gibt es ein Fotoportal für alle möglichen Fotos von der Kuloko. Bitte gebt ggf. die Fotograf*in an.'
        url: /photos
        button: 'Zeig her!'
        target: blank
        colors: turkoise-bg
---

## Pressekit