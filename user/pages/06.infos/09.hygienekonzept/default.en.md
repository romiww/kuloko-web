---
title: 'Hygienic Concept'
published: true
---

## Hygiene Concept - Our Handling of Corona
We want a safe, mindful and responsible festival where everyone can feel comfortable. This also includes an appropriate handling of the covid situation. Therefore the following hygiene concept has been developed for the Kuloko.
**Important:** Depending on the incidence, the covid concept can still change, so please always check the website for current info.

### What should I be aware of when arriving?
Please get **tested at a testing center prior to arrival** unless you are fully vaccinated or recovered. Of course, if you have a positive test result or corona symptoms, please do not travel. **At KuloKo** there will also be several **test centers** where you can take a free rapid test.

### How do I behave at the Kuloko?
Depending on the incidence situation, different rules apply at KuloKo: 

**Incidence of less than 10 (incidence level 0):**
In this case, there will generally be no contact tracing, masking or negative testing requirement. However, you will still have the option to check in and out of venues via the Corona app, get tested regularly at our mobile testing centers, and ask people near you to keep their distance and wear masks. If a person asks you to keep your distance and wear a mask, please respect that and help make sure we can all feel comfortable and safe at Kuloko.

**Incidence of over 10 (incidence level 1):**
If the incidence is over 10 we will need to take your contact information at the venue entry points. We will burn this data 14 days after the end of KuloKo. In addition, there will be a negative test obligation: Without a 48h-fresh test (or complete vaccination protection or proof of recovery) you will unfortunately not be allowed to enter. In addition, there is a general mask obligation on the KuloKo. Since we are outside we do not have to keep any distances. 

**General:** Please pay attention to signs and announcements during the Kuloko!

### What do I do if I test positive?
If you receive a positive test result at the KuloKo testing center, you must have a PCR test performed immediately. In this case, please contact the KuloKo Corona-AG via the test center, who can accompany and support you in the further process. If you **test positive for corona** after KuloKo, please notify the KuloKo crew immediately at **info@kultur-ohne-kohle.de**.

Together, we can create a safe and corona-compatible KuloKo!

**Take care of yourself, others, and their wishes in return. Thank you!