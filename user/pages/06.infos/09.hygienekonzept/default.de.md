---
title: Hygienekonzept
published: true
---

## Hygienekonzept - Unser Umgang mit Corona
Wir wollen ein sicheres, achtsames und verantwortungsvolles Festival, auf dem sich alle Menschen wohlfühlen können. Dazu gehört auch ein angemessener Umgang mit der Coronalage. Dafür wurde folgendes Hygienekonzept für die Kuloko erarbeit.
**Wichtig:** Je nach Inszidenz kann sich das Coronakonzept noch ändern, also schau bitte immer auf die Website für aktuelle Infos.

### Was sollte ich bei der Anreise beachten?
Bitte lasst euch **vor der Anreise in einem Testzentrum testen**, sofern ihr nicht vollständig geimpft oder genesen seid. Mit einem positiven Testergebnis oder Coronasymptomen reist ihr natürlich bitte nicht an. **Auf der Kuloko** wird es zudem mehrere **Testzentren** geben, an denen du einen kostenlosen Schnelltest machen kannst.

### Wie verhalte ich mich auf der Kuloko?
Je nach Inzidenzlage gelten auf der Kuloko verschiedene Regeln: 

**Inzidenz von unter 10 (Inzidenzstufe 0):**
In diesem Fall wird es generell keine Kontaktnachverfolgung, Masken- oder Negativtestpflicht geben. Dennoch habt ihr die Möglichkeit, über die Corona-App euch bei den Veranstaltungsorten ein- und auszuchecken, euch regelmäßig bei unseren mobilen Testzentren testen zu lassen und Personen in eurer Nähe zu bitten, Abstände zu wahren und Masken zu tragen. Wenn dich eine Person um Abstand und Maske bittet, so respektiere dies bitte und sorge mit dafür, dass wir alle uns auf der Kuloko wohl und sicher fühlen können.

**Inzidenz von über 10 (Inzidenzstufe 1):**
Liegt die Inzidenz über 10 müssen wir an den Eingangspunkten der Veranstaltungsorte eure Kontaktdaten aufnehmen. Diese Daten verbrennen wir 14 Tage nach Ende der Kuloko. Außerdem gilt dann eine Negativtestpflicht: Ohne 48h-frischen Test (oder vollständigen Impfschutz bzw. Genesungsnachweis) kommst du leider nicht rein. Außerdem gilt dann eine generelle Maskenpflicht auf der Kuloko. Da wir draußen sind müssen wir keine Abstände einhalten. 

**Ganz allgemein:** Bitte achtet während der Kuloko auf Beschilderungen und Durchsagen!

### Was mache ich bei einem positiven Testergebnis?
Wenn du im Kuloko-Testzentrum ein positives Testergebnis erhältst, musst du umgehend einen PCR-Test durchführen lassen. Bitte nimm in diesem Fall über das Testzentrum Kontakt zur Corona-AG der Kuloko auf, die dich im weiteren Verlauf begleiten und unterstützen kann. Wenn du **nach der Kuloko positiv auf Corona getestet** wirst, teil dies bitte sofort der Kuloko-Crew unter **info@kultur-ohne-kohle.de** mit.

Gemeinsam können wir eine sichere und coronakompatible Kuloko schaffen!

**Achtet dafür auf euch, andere und deren Wünsche. Danke!**