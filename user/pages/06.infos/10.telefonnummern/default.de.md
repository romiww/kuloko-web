---
title: Telefonnummern
---

## Telefonnummern
Hier versuchen wir, die Wichtigsten Struktur-Telefonnummern online zu stellen.

#### Sanitäter\*innen: 004915213048682
Das Sanizelt ist auf dem Klimacamp Rheinland in Lützerath.
#### Awareness: 0049 15750780976
Das Awarenessteam der Kuloko ist hier während des Festivals jederzeit erreichbar. Mehr Infos zu Awareness [hier](/infos/awareness).
#### Shuttle: 004915212103150
#### Presse: 004915758128656
Hier erreicht ihr die Presse AG der Kuloko. Mehr infos unter [presse](/infos/press)