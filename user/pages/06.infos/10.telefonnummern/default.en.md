---
title: 'Phone Numbers'
---

## Telephone numbers
Here we try to put online the most important phone numbers.

#### AG Camp Life: 00491634441976
Here you can report during the day for e.g. shifts.
#### Awareness: 0049 15750780976
The KuloKo awareness team can be reached here at any time during the festival. More information about awareness [here](/infos/awareness).
#### AG Press: 004915758128656
Here you can reach the press AG of KuloKo. More info at [presse](/infos/press)