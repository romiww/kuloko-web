---
title: Awareness
---

#### BEAUTIFUL THAT YOU ARE AT KuloKo!
Hopefully you have a great time with everyone else here. Help us make this camp a safe and beautiful place for everyone. Should you observe or even experience transgressive or assaultive situations yourself, you will always get support from us! Talk to us - we are the awareness team with the pink marker.

#### How can you reach us?
+ The awareness tent is located at the Klimacamp Rheinland. It is open from about 10 am until evening.
+ You can reach us at any time on the awareness cell phone: +4915750780976
+ Via a mailbox at the awareness tent. During the camp you can use it to contact us if people can't or don't want to reach us personally or by phone.
+ Via an e-mail address, which is published on the homepage. You can still reach us after the camp.

#### Why are we here and what are we doing anyway?
**Awareness means** to be aware, to be informed, to be sensitized for certain problems.
We understand awareness structures not as symptom treatment, but as a structure that holds up a mirror, that encourages discussion and argument - until we know how we can and want to live together.
Awareness can support to draw attention to discrimination and discriminatory structures, can stimulate us to empower or to reflect.
Climate change and the accompanying social and ecological crises cannot be separated from a society based on exploitation, compulsion to compete, oppression and exclusion of people.  Even in self-organized political groups that work critically of domination, we are not free of certain mechanisms and can unconsciously reproduce discrimination.

#### Awareness concerns us all!
That's why we have an awareness concept again this year, so that we can get one step closer to our utopias and learn how to design spaces where everyone feels comfortable.
We want retreat spaces where we can put plasters on our wounds. To then stand up again together, to stand up for each other, to have fun, to form gangs, to make noise and to confront this shit! 
#### What are our working principles?
The working principles of the awareness crew of KuloKo and the climate camp in the Rhineland 2021 consist of several building blocks:
1. the first building block consists of the awareness workshops, skillsharings, discussion rounds, where people are invited to become part of the awareness crew and develop the concept further. They take place primarily at the beginning of the camp and/or in advance. If you don't find any workshops in the program, just contact us. You will find us in the awareness tent. 
2. The second building block is formed by the different retreat spaces and the possibilities to get into conversation and to empower each other. There is a silence tent, here we do not talk or if only very quietly and not permanently. Then there is a FLINTA* only tent for women*, lesbians*, inter*, non-binary*, trans*, a-gender* persons and a BIPoC only tent for Black persons, Indigenous persons and People of Color. 
3. the third component is a bit new this year. Based on transformative justice/community accountability (concepts developed by Black women in the USA and Canada), it is planned that there will be a (pro)feminist and a critical whiteness support crew at the camp. The goal of these crews is that there is a reflective space for discriminating individuals to reflect with peers in order to address long-term change with discriminating individuals. And then there is an open conversation space to just get a listening ear or outrage together.

##### Defining power and partiality 
Two tools are fundamental to our support work.
1. first, that persons themselves define how they experienced the situation.
2. second, our political stance, which includes an attempt at an intersectional and multidimensional perspective. For us, this means that we try to critically question our own positioning and that of all those involved and include them in the analysis of the situation.
 Since in cases of rape and sexualized violence, non-consensuality cannot be discerned by outsiders or through political analysis, an exclusive power of definition is non-negotiable.
 We work with partiality. Partiality means for us to support the (affected) person and to stand by him/her in the further process. By offering support work, we want to open spaces in which people are listened to without questioning the "correctness" of their experiences. Being able to define for oneself what has been experienced is an important moment of empowerment and reflection and should become the norm anyway!   
 We do not work with sanctioning power. A person thus defines what happened, but not what will happen.  Being able to identify and formulate needs and desires is an important step and also part of our process. Our hope is that all wishes and expectations can be realized. However, it may happen that wishes and possible demands cannot automatically be fulfilled and implemented.
 
 #### 4. What we demand from you
 "We are not hypersensitive, but critical of power! We want to dismantle hierarchies and we don't want:
 + racist, sexist, homo- or trans\*-hostile, ableist remarks and swear words.
 + Grabbers → hair, butts or breasts - keep your hands to yourself!
 + Naked torsos of cis men (For more info: www.klimacamp-im-rheinland.de/campinfo-2/vereinbarungen/oben-ohne)
 + national symbols
 + Uniforms and camouflage clothing (glorifying war in general).
 + unsolicited photos → respect the privacy and safety of others!
 + any appropriation and instrumentalization of cultural, religious and political symbolism! --> For us, this means the wearing of kimonos, bindis, warbonnets, "afro" wigs, so-called dreadlocks, the kufiya or certain tattoos by white people. " Source: Invision

#### What about smoking, alcohol and other drugs?
There will be marked smoking areas. We would like to see smoking only in these places as well. If that is not possible, just ask if it is okay with the people around you. Alcohol is consumed where it is served. Please be generally mindful of the people around you and what consumption they are comfortable with. We would like people to be sensitive to drug use of any form and be aware that public use can be retraumatizing for others and can lead to social exclusion. Be aware and have fun :)

### Attention: Sexism, racism, homo- and transphobia, anti-Semitism and other discriminatory behavior, as well as national pride, have no place here!