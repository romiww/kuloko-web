---
title: Awareness
---

## Awareness
#### SCHÖN, DASS DU auf der KUloko BIST!
Hoffentlich hast du viel Spaß mit allen Anderen hier. Unterstütze uns dabei, dieses Camp zu einem sicheren und schönen Ort für alle zu machen. Solltest du grenzüberschreitende oder übergriffige Situationen beobachten oder gar selbst erfahren, erhältst du von uns jederzeit Unterstützung! Sprich uns an – wir sind das Awareness-Team mit der pinken Markierung.

#### Wie könnt ihr uns erreichen?
+ Auf dem Klimacamp Rheinland befindet sich das Awarenesszelt. Es ist von ca. 10 Uhr bis abends offen.
+ Auf dem Awareness-Handy sind wir jederzeit erreichbar: +4915750780976
+ Über einen Briefkasten am Awareness-Zelt. Während des Camps könnt ihr darüber Kontakt zu uns aufzunehmen, falls Menschen uns nicht persönlich oder per Telefon erreichen können oder wollen.
+ Über eine E-Mail-Adresse, die auf der Homepage veröffentlicht ist. Darüber sind wir auch nach dem Camp noch für euch erreichbar.

#### Warum sind wir hier und was machen wir überhaupt?
**Awareness bedeutet** sich bewusst sein, sich informieren, für gewisse Problematiken sensibilisiert sein.
Wir verstehen Awarenessstrukturen nicht als Symptombehandlung, sondern als eine Struktur, die einen Spiegel vorhält, die anregt zu diskutieren und zu streiten- bis wir wissen, wie wir gemeinsam leben können und wollen.
Awareness kann unterstützen, auf Diskriminierung und diskriminierende Strukturen aufmerksam zu machen, kann anregen uns zu empowern oder zu reflektieren.
Der Klimawandel und die damit einhergehenden sozialen und ökologischen Krisen sind nicht zu trennen von einer Gesellschaft, die auf Ausbeutung, Zwang zur Konkurrenz, Unterdrückung und Ausschluss von Menschen beruht. Auch in selbstorganisierten politischen Gruppen, die herrschaftskritisch arbeiten, sind wir nicht frei von bestimmten Mechanismen und können unbewusst Diskriminierung reproduzieren.

#### Awareness geht uns alle was an!
Deshalb haben wir auch dieses Jahr ein Awarenesskonzept, damit wir unseren Utopien einen Schritt näher kommen und lernen, wie wir Räume gestalten können, in denen sich alle wohl fühlen.
Wir wünschen uns Rückzugsräume, wo wir Pflaster auf unsere Wunden kleben können. Um dann gemeinsam wieder aufzustehen, füreinander einzustehen, um Spaß zu haben, Banden zu bilden, Krach zu machen und dieser Kackscheiße entgegenzutreten! 
#### Was sind unsere Arbeitsgrundsätze?
Die Arbeitsgrundsätze der Awarenesscrew der Kuloko und des Klimacamps im Rheinland 2021 bestehen aus mehreren Bausteinen:
1. Der erste Baustein besteht aus den Awareness-Workshops, Skillsharings, Diskussionsrunden, in denen Menschen eingeladen sind, Teil der Awarenesscrew zu werden und das Konzept weiterzuentwickeln. Sie finden primär zu Beginn des Camps und/oder im Vorhinein statt. Falls ihr keine Workshops im Programm findet, sprecht uns einfach an. Ihr findet uns im Awarenesszelt. 
2. Der zweite Baustein bildet sich durch die unterschiedlichen Rückzugsräume und den Möglichkeiten, ins Gespräch zu kommen und sich gegenseitig zu empowern. Es gibt ein Ruhe Zelt, hier wird nicht gesprochen oder wenn nur sehr leise und nicht dauerhaft. Dann gibt es ein FLINTA* only Zelt nur für Frauen*, Lesben*, Inter*, Nicht-Binäre*, Trans*, A-Gender* Personen und ein BIPoC only Zelt nur für Schwarze Personen, Indigene Personen und People of Colour. 
3. Der dritte Baustein kommt dieses Jahr ein bisschen neu dazu. Angelehnt, an transformativ justice/communitiy accountibility (Konzepte die von Schwarzen Frauen in den USA und Kanada entwickelt worden sind) ist geplant, dass es auf dem Camp einen (pro)feministischen und einen critical whitness Unterstützer\*innenkreis gibt. Das Ziel dieser Crews ist, dass es für diskriminierende Personen einen Reflexionsraum gibt, in dem mit den Peers reflektiert wird, um eine langfristige Veränderung bei diskriminierenden Personen anzugehen. Und dann gibt es noch ein offenes Gesprächsangebot, um einfach mal ein offenes Ohr zu bekommen oder sich gemeinsam zu empören.

##### Definitionsmacht und Parteilichkeit 
Für unsere Unterstützungsarbeit sind zwei Tools grundlegend.
1. Erstens, dass Personen selbst definieren, wie sie die Situation erlebt haben.
2. Zweitens unsere politische Haltung, die den Versuch einer intersektionalen und multidimensionalen Perspektive beinhaltet. Das bedeutet für uns, dass wir unsere eigene Positionierung und die aller Beteiligten versuchen kritisch zu hinterfragen und in die Analyse der Situation mit einzubeziehen.
 Da bei Fällen von Vergewaltigung und sexualisierter Gewalt, die nicht-Konsensualität nicht von außenstehenden Personen oder durch politische Analyse erkennbar ist, ist eine ausschließliche Definitionsmacht unverhandelbar.
 Wir arbeiten mit Parteilichkeit. Parteilichkeit bedeutet für uns, (der betroffenen) Person den Rücken zu stärken und ihr bei dem weiteren Prozess zur Seite zu stehen. Indem wir Unterstützungsarbeit anbieten, möchten wir Räume eröffnen, in denen Menschen zugehört wird, ohne die "Richtigkeit" ihrer Erfahrungen infrage zu stellen. Selbst definieren zu können, was erlebt wurde, ist ein wichtiger Moment des Empowerments und der Reflexion und sollte sowieso zur Normalität werden!   
 Wir arbeiten nicht mit Sanktionsmacht. Eine Person definiert somit, was passiert ist, aber nicht, was passieren wird.  Bedürfnisse und Wünsche erkennen und formulieren zu können ist ein wichtiger Schritt und auch Teil unseres Prozesses. Unsere Hoffnung ist, dass alle Wünsche und Erwartungen umgesetzt werden können. Es kann jedoch passieren, dass Wünsche und eventuelle Forderungen nicht automatisch erfüllt und umgesetzt werden können.
 
 #### 4. Was wir von euch Fordern
 "Wir sind nicht überempfindlich, sondern machtkritisch! Wir wollen Hierarchien abbauen und haben kein Bock auf:
 + rassistische, sexistische, homo- bzw. trans\*feindliche, ableistische Äußerungen und Schimpfwörter
 + Grabscher → Haare, Hintern oder Brüste – lasst eure Hände bei euch!
 + Nackte Oberkörper von Cis-Männern (Für mehr Infos: www.klimacamp-im-rheinland.de/campinfo-2/vereinbarungen/oben-ohne)
 + nationale Symbole
 + Uniformen und Tarnkleidung (Kriegsverherrlichendes generell)
 + ungefragte Fotos → respektiert die Privatsphäre und Sicherheit anderer!
 + jede Aneignung und Instrumentalisierung von kulturellen, religiösen und politischen Symboliken! --> Das meint für uns das Tragen von Kimonos, Bindis, Warbonnets, „Afro“-Perücken, sogenannter Dreadlocks, der Kufiya oder bestimmter Tattoos durch weiße Menschen."Quelle: Invision

#### Was ist mit Rauchen Alkohol und anderen Drogen?
Es wird markierte Raucher\*innen Bereiche geben. Wir würden uns wünschen, dass auch nur an diesen Orten geraucht wird. Wenn das nicht möglich ist, fragt doch einfach nach, ob es für die Menschen um euch herum in Ordnung ist. Akohol wird da getrunken, wo er ausgeschenkt wird. Bitte achtet generell auf die Menschen in euerer Umgebung und darauf, mit welchem Konsum sie sich wohl fühlen. Wir würden uns wünschen, dass Menschen sensibel mit Drogenkonsum jeglicher Form umgehen und auf Schirm haben, dass öffentlicher Konsum retraumatiesierend für andere sein kann und zu sozialen Ausschlüssen führen kann. Be aware und habt Spaß :)
### Achtung: Sexismus, Rassismus, Homo- und Transphobie, Antisemitismus und anderes diskriminierendes Verhalten, sowie Nationalstolz, haben hier keinen Platz!