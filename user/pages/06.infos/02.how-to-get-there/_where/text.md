---
body_classes: modular
image_align: left
---

### Wo genau ist die Kuloko?
Das Kultur ohne Kohle Festival wird rund um die vom Tagebau Garzweiler II bedrohten Dörfer **Lützerath, Keyenberg, Unterwestrich, Oberwestrich, Kuckum und Berverath** stattfinden. Camping Plätze gibt es in Lützerath beim [Klimacamp Rheinland](http://www.klimacamp-im-rheinland.de/) und in Keyenberg bei [MaStaMo](https://www.bundjugend-nrw.de/projekte/mastamo/mastamo-camp/). In Keyenberg gibt es einen extra Camperplatz für Wohnwägen und co. Wo genau die verschiedenen Orte sind, was es wo gibt und all sowas findest du unter [Karte](/infos/map).