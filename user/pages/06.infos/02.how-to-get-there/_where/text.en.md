---
body_classes: modular
image_align: left
---

### Where exactly is the Kuloko?
The Kultur ohne Kohle Festival will take place around the villages **Lützerath, Keyenberg, Unterwestrich, Oberwestrich, Kuckum and Berverath**, which are threatened by the Garzweiler II open pit mine. Camping sites are available in Lützerath at [Klimacamp Rheinland](http://www.klimacamp-im-rheinland.de/) and in Keyenberg at [MaStaMo](https://www.bundjugend-nrw.de/projekte/mastamo/mastamo-camp/). In Keyenberg there is an extra camper place for caravans and co. Where exactly the different places are, what is available where and all that you can find under [map](/infos/map).
