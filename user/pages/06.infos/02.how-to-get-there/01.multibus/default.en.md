---
title: Multibus
---

### Wann shutelt euch der Multibus?
Mo-Fr: 20-24 Uhr
Sa: 6:30-24 Uhr
So: 9-24 Uhr
(Unter der woche fahren die Linienbusse EK1 und EK3 mindestens jede Stunde)

### 🕒 Wann fährt der Bus?
Jede Stunde. Immer am Bahnhof Erkelenz. Und zwar abgestimmt auf alle Regionalzüge, sowie die ICEs von/nach Berlin (ja, in Erkelenz halten auch ICEs). Natürlich fährt euch der Bus auch zu euren Zügen.
⚠️ Damit der Bus fährt müsst ihr mindestens 1 Stunde vorher anrufen: 02431 88-6688 alternativ geht auch eine Onlinebuchung (Link unten) Sagt bitte auch, aus welchem Zug ihr kommt, damit der Bus bei Verspätungen auf euch warten kann.

### 🚏Wo fährt der Bus?
Der Bus fährt von den Bahnhöfen Erkelenz und Herrath, aber NICHT von Hochneukirch und bringt euch bis zur Haltestelle Holzweiler, Niederstraße (2km vor der Mahnwache) und zurück. Manchmal fahren euch die Busfahrer*innen freundlicherweise auch nach Lützerath (Es kann vorkommen, dass ihr 1x umsteigen müsst, aber ALLE Busse warten immer aufeinender.)

### 💲 Kostet der Bus etwas?
In den meisten Fällen kostet euch der Bus nichts extra. Mit folgenden Tickets (die bis zum Bahnhof Erkelenz gelöst werden) ist die Fahrt kostenlos:
+  Schöner-Tag-Ticket (1 und 5 Pers.)
+  Alle AVV-Tickets (incl. Semesterticket)
+  Alle VRS-Tickets (die bis Erkelenz gelten)
+  Alle NRW-Tickets (schöne-Reise/Tag/Fahrt...)
+  Einfach-Weiter-Ticket
+  Alle Semestertickets die NRW-weit gültig sind
+  Schwerbehindertenausweise mit Wertmarke (Der Bus ist Barrierefrei)
+  NRWupgrade für Azubis
+  City-Mobil Tickets (nur bei Anreise mit ICE)
Ansonsten kostet die Fahrt 2,80€ pro Person (das ist der ganz normale Bustarif)

### 👨‍👩‍👧‍👦 Was ist mit Gruppen?
Der Multibus ist ein Kleinbus 🚐. Daher sollten sich Gruppen (auch 5er Gruppen) unbedingt möglichst früh anmelden.  Wenn ihr eure Anreise plant, könnt ihr euch schon eine Woche vorher anmelden

### 🦽🚲🛴 Was ist mit Fahrrädern, Kinderwägen, Rollstühlen, Gepäck?
Wird alles mitgenommen. 
⚠️ wenn ihr mehrere Fahrräder dabei habt, ruft einen Tag vorher an, damit der Bus seinen Fahrradanhänger mitnimmt.
ℹ️ Wenn euer Fahrradkarte im Zug gültig war gilt sie fast immer auch im Bus.

🌐 Alle Infos gibts auch auf https://www.west-verkehr.de/index.php/de/ihr-bus/multibus

### ❓ Weitere Fragen:

+ Was ist mit der Rückfahrt? - Geht genauso: Anrufen und shutteln lassen. Oft könnt ihr eure Zugtickets auch bei den Fahrer*innen kaufen und spart dann Zeit am Bahnhof. 
+ Fährt der Bus nur von/zum Bahnhof? -  Nein, im gesamten Landkreis werdet ihr geshuttelt. 
+ Gilt die Maskenpflicht im MultiBus? - Wie in jedem Bus müsst ihr auch eine Maske tragen.
+ Ist der Bus Barrierefrei? - Ja. Sagt bei der Anmeldung bitte, wenn ihr einen Rollstuhl ö.ä. dabei habt.
+ Warum muss ich mich überhaupt anmelden? - Der Bus fährt nur, wenn jemand mitfahren will. Daher müsst ihr euch anmelden.
