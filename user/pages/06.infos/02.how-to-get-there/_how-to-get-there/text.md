---
body_classes: modular
image_align: left
---

### Keine Kohle fürs Ticket?
Kultur ohne Kohle soll für alle zugänglich sein und an den Kosten für die Anreise soll das nicht scheitern. Wenn du also gerne kommen möchtest und dir die Anfahrt nicht leisten kannst, melde dich sehr gerne bei [finanzen-rheinland@riseup.net](mailto:finanzen-rheinland@riseup.net).

### Mitfahr-Börse
Es gibt eine Kuloko-Vernetzungs-Telegram-Gruppe, die unter anderem als Mitfahr-Börse dient. [Zur Telegram-Gruppe](https://t.me/kuloko?target=_blank)
<br> Die Ortsgruppe **[Fridays For Future Aachen](https://fridaysforfuture.de/ortsgruppen/aachen/)** hat ebenfalls einen Telegram-Channel für die Anreise aus Aachen eingerichtet. Diese findet ihr **[hier](https://t.me/joinchat/e0kqLLNLJdg0Njdi)** 

### Mit dem Fahrrad
Das ist natürlich die ökologischste Art, anzureisen. Wenn dir der Weg zu weit ist und du mit Zug, Bus oder dem Auto anreist, empfliehlt es sich trotzdem sehr, Fahrräder mitzunehmen, da das Gebiet der Kuloko sehr weitläufig ist (siehe [Karte](/infos/map)).
Es wird auch ein Fahrräder zum leihen geben, aber leider nur in begrenzter Anzahl. 
<br>
Wenn du fuktionstüchtige oder reparaturfähige Fahrräder hast, bring sie gerne nach Lützerath, damit wir mehr Soli-Räder zur Verfügung haben.

### Zug, Bus und Multibus
**Bahnhöfe** in der Nähe sind **Hochneukirch** und **Erkelenz**, sowie **Herrath**. Von Erkelenz aus fahren **Busse** (EK1 und EK3) in die Dörfer Keyenberg und Holzweiler, von denen aus der Rest der Festival-Punkte besser erreichbar ist. Fahrpläne: **[EK1](https://avv.de/files/avv/files/fahrplaene/linienfahrplaene/ek1_avv.pdf?target=_blank)** und **[EK3](https://avv.de/files/avv/files/fahrplaene/linienfahrplaene/ek3_avv.pdf?target=_blank)**  
An Wochenenden und Abends gibt es statt Linienbussen einen **Multibus** des Verkehrsverbands, der nur auf Anfrage fährt. Das oft sogar kostenlos, je nachdem was Du für ein Ticket hast. Der Bus fährt, wohin ihr wollt, wenn ihr nur eine Stunde vorher anruft. Große Gruppen, viel Gepäck, Rollstühle und Fahrräder sind meist auch kein Problem, dafür aber am besten ein paar Tage vorher bescheid geben.
**Multibus Telefonnummer: 02431 88-6688** <br>
**Genauere Infos zum Multibus: [hier](/infos/how-to-get-there/multibus)**

Am Bahnhof Hochneukirch wird es Soliräder zum Ausleihen geben, aber nur in begrenzter Anzahl. Code: Frag dich rum!. Das Schloss bitte an Ort und Stelle wieder anschließen und mit dem Fahrrad ohne Schloss Richtung Dörfer düsen. <br>
**Übrigens:** Wer ein Abo-Ticket bei einem Verkehrsverbund in NRW hat, kann vom 03.-17.08. fast in ganz NRW kostenlos Nahverkehr fahren. Mehr infos dazu gibt es [hier](https://www.vrr.de/de/magazin/in-den-sommerferien-nrw-weit-mobil-mit-bus-und-bahn/)

### Shuttle
Es wird auch die Möglichkeit eines **Shuttle-Angebotes** zwischen Bahnhöfen und Dörfern sowie zwischen den Dörfern selbst geben. Da dieses jedoch nur begrenzte Kapazitäten hat (und auch sowieso), ist es super, wenn ihr euer **Fahrrad dabei** habt. 
**Shuttle-Telfon: 004915212103150**

### Mit dem Auto 
Auch bei Anreise mit dem Auto empfiehlt es sich, Fahrräder mitzunehmen, um gemütlich und ökologisch von Ort zu Ort zu fahren. 