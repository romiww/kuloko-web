---
body_classes: modular
image_align: left
---

### No money for the ticket?
Kultur ohne Kohle should be accessible to all and the costs for the journey shouldn't stop anyone. So if you would like to come and can't afford the journey, please contact [finanzen-rheinland@riseup.net](mailto:finanzen-rheinland@riseup.net).

### Ride-sharing exchange
There is a Kuloko networking Telegram group that serves as a ride-sharing exchange, among other things. [To Telegram group](https://t.me/kuloko?target=_blank)

### By bike
This is of course the most ecological way to arrive. If the way is too far for you and you arrive by train, bus or car, it is still highly recommended to take bicycles with you, because the area of Kuloko is very extensive (see [map](/infos/map)).
There will also be some bikes to borrow (see below), but unfortunately only in limited numbers. 
<br>
If you have bikes in working order or in need of repair, feel free to bring them to Lützerath so we can have more Soli bikes available.

### By train, bus and multibus
**Train stations** nearby are **Hochneukirch** and **Erkelenz**, as well as **Herrath**. From Erkelenz, **buses** (EK1 and EK3) go to the villages of Keyenberg and Holzweiler, from which the rest of the festival points are more easily accessible. Timetables: **[EK1](https://avv.de/files/avv/files/fahrplaene/linienfahrplaene/ek1_avv.pdf?target=_blank)** and **[EK3](https://avv.de/files/avv/files/fahrplaene/linienfahrplaene/ek3_avv.pdf?target=_blank)**  
On weekends and evenings, instead of regular buses, there is a **multibus** of the transport association, which runs only on request. Often even for free, depending on what kind of ticket you have. The bus goes where you want if you call only one hour in advance. Large groups, a lot of luggage, wheelchairs and bicycles are usually no problem, but it's best to let them know a few days in advance.
**Multibus Phone number: +49 2431 88-6688** <br>
**Detailed information about the Multibus: [hier](/infos/how-to-get-there/multibus)**

At the Hochneukirch train station, there will be Soli bikes to borrow, but only in limited numbers. Code: Ask People :D. Please reconnect the lock on the spot and jet off towards the villages with the bike without the lock. <br>
**By the way:** People who have an Abo ticket with a transport association in NRW, can use the public transport in almost whole NRW from 03.-17.08. More info about this can be found [here].(https://www.vrr.de/de/magazin/in-den-sommerferien-nrw-weit-mobil-mit-bus-und-bahn/)


### Shuttle
There will also be the possibility of a **shuttle offer** between stations and villages and between the villages themselves. However, since this has limited capacity (and also anyway), it is great if you have your **bike with you**. 

### By car 
Even if you arrive by car, it is advisable to take bicycles with you to travel comfortably and ecologically from place to place.