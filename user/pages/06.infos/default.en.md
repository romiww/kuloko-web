---
title: Info
menu: Info
---

## Orga Info
Here you will find all the information you need to know for your time at Kuloko. If you have the impression that something is missing, please send us an email to [media@kultur-ohne-kohle.de](mailto:media@kultur-ohne-kohle.de).
### Program And Info Booklet 
Unfortunately there was not enough time to translate the info booklet into English, so it is only available in "Schermän" [here](/blog/infoheft). We hope you will find all relevant information on the website.
### Journey And Arrival
Everything you need to know about your journey to Kuloko [here](/infos/how-to-get-there).
### Map And Description Of The Places
A map of the Kuloko Area you can find [here](/infos/map).
### Timetable
Currently, the timetable is only available in the [Info Booklet](/blog/infoheft) (only in German). But there will be more to come. You will find the daily program, when it is ready, on the website and in the [Telegram Channel](https://t.me/kulokofestival).
### Packing List
Take spoons with you!! And a bike!! Or several!!! [see the packing list](/infos/packing-list).
### Press
Information for press representatives can be found [here](info/press).