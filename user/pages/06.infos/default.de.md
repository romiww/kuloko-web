---
title: Infos
---

## Organisatorische Infos
Hier gibt es nach und nach alle Infos, die du für Deine Zeit auf der Kuloko wissen musst. Wenn Du den Eindruck hast, dass etwas fehlt, schreib uns das gerne in einer Mail an [media@kultur-ohne-kohle.de](mailto:media@kultur-ohne-kohle.de).
### Info- und Prgrammheft 
Viele Infos, die es noch nicht auf die Website geschafft haben, findest du [hier](/blog/infoheft) im Programm- und Infoheft.
### Anreise
Alles zur Anfahrt zur Kuloko gibt's [hier](/infos/how-to-get-there).
### Karte und Ortebeschreibungen
Zur Karte der Kuloko geht's [hier](/infos/map).
### Zeitplan
Aktuell gibts den Timetable nur im [Infoheft](/blog/infoheft). Da wird aber noch einiges dazukommen. Das tagesaktuelle Programm findest du, wenn es soweit ist, auf der Website und im [Telegram Kanal](https://t.me/kulokofestival).
### Hygienekonzept
Unser Hygienekonzept für den Umgang mit Corona findest du [hier](/infos/hgienekonzept).
### Festival ABC
Das Festival ABC soll dabei helfen, dass sich alle Besucher\*innen auf dem Kultur-ohne-Kohle Festival wohlfühlen können. [Schau gerne mal rein](/infos/festival-abc).
### Awareness 
Alle Infos zu Awareness gibt es [hier](/infos/awareness)
### Telegram
Es gibt einen Kuloko Infokanal und eine Vernetzungsgruppe auf Telegram. Mehr infos unter [Telegram](/infos/telegram)
### Packliste
An alles gedacht, um eine schöne und bestärkende Zeit auf der Kuloko zu haben? Falls du dir unsicher bist, schau doch mal auf unsere Packliste [hier](/infos/packing-list).
### Presse
Informationen für Presseverteter\*innen gibt es bei [Presse](info/press)
### Telefonnummern
Alle wichtigen Telefonnummern gibt es [hier](/infos/telefonnummern)

