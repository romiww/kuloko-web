---
title: 'Karte Anfang'
body_classes: modular
---

## Interaktive Karte
Hier ist die Interaktive Online-Karte mit allen Orten der Kuloko. Mit Klick auf den Pin gibt es mehr infos zu den Einzelnen Orten. Wie du überhaupt zur Kuloko kommst findest du unter [Anreise](/infos/how-to-get-there).