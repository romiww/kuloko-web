---
title: 'Karte Anfang'
body_classes: modular
image_align: left
---

## Interaktive Karte
Hier ist die Interaktive Online-Karte mit allen Orten der Kuloko. Mit Klick auf den Pin gibt es mehr Infos zu den Einzelnen Orten. Wie du überhaupt zur Kuloko kommst findest du unter [Anreise](/infos/how-to-get-there).

### Wichtig!
Die Mahnwache in Keyenberg (MawaKey) hat ihren Standort gewechselt. Ihr findet sie nun am Camperplatz, wo sie weiterhin viel Material und generelle Infos zur Kuloko für euch bereithält.