---
title: 'drawn map'
body_classes: modular
---

## Drawn Map
Here is a simplified, drawn map from the [Info- und Programmheft](/blog/infoheft). Below you can find descriptions of the places as pictures. <br>
![Map](/images/map-sketch.jpg)
![Places](/images/places.jpg)
