---
title: 'Karte Zeichnung'
body_classes: modular
---

## Zeichnung
Hier ist die vereinfachte, gezeichnete Karte aus dem [Info- und Programmheft](/blog/infoheft). Darunter gibt es dann noch die Ortebeschreibungen in Bildform. <br>
![Map](/images/map-sketch.jpg)
![Places](/images/places.jpg)
