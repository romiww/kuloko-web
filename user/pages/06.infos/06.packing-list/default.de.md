---
title: Packliste
---

## Packliste 
Du fragst du schon die ganze Zeit, was du eigentlich zur Kuloko mitnehmen sollst? Löffel!! Und ein Fahrrad. Alles weitere findest du hier auf der Packliste.


### Packliste
+ **Fahrräder** (perfekt geeignet um das dezentrale Festivalgelände zu erkunden), gerne für dich und andere
+ Longboards, Roller, Inliner, etc. sind bestimmt auch cool, manche Wege sind allerdings nicht geteert
+ viele **Löffel!!!** Für dich und andere, denn die Küfa  (Küche für alle) leidet an Löffel-Knappheit
+ **Deko**, um die Orte der Kuloko schick zu machen. Z.B. Banner und Demoschilder, Lichterketten, Möbel, etc. Aber bitte nicht zu viel Trash mitbringen ;)
+ **Gehörschutz** - Festivals sind Laut!
+ Zelt
+ Schlafsack
+ Isomatte
+ Kleiner Tagesrucksack
+ Wetterfeste Kleidung (Sonnen- und Regenschutz, Tanzoutfit)
+ Hygienezeug
+ Mückenzeug
+ medizinische Masken (FFP2-Masken, OP-Masken → denkt dran diese regelmäßig zu wechseln)
+ Desinfektionsmittel in Mitnehmgröße
+ eigenes Geschirr/Besteck/Brotdose
+ Trinkflasche
+ eigene Verpflegung für den Hunger zwischendurch oder falls es mal nicht genug Küfa-Essen gibt
+ Medikamente und Vitamine 
+ Campingstuhl für das richtige Festivalfeeling 
+ Hängematte für das richtige Lützerathfeeling
+ Stirnlampe/Taschenlampe
+ Kugelschreiber
+ Handy (Für aktuelle Infos von der Website und dem [Telegram Channel](https://t.me/kulokofestival)
+ Kopfhörer 
+ Ladekabel (es gibt Solar-Ladestationen!)
+ Spendengeld für Küfa und Camp → oder schon vorher **[hier](https://kultur-ohne-kohle.de/de/contribute/donate)** spenden
+ Kamera 
+ Musikinstrumente und Liederhefte, Spiele, ein gutes Buch oder was ihr sonst noch braucht, wenn ihr euch mal aus der Menge zurückziehen wollt