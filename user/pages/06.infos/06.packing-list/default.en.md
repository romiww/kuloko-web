---
title: 'Packing list'
---

## Packing list
You've been asking yourself all along what you should actually take to the Kuloko? Spoons!!! And a bicycle. You can find everything else here on the packing list.

+ **Bikes** (perfect for exploring the decentralized festival area), gladly for you and others
+ longboards, scooters, inline skates, etc. are definitely cool too, but some paths are not tarred
+ lots of **spoons!!!** for you and others, because the Küfa (kitchen for everyone) suffers from spoon shortage
+ **Decoration stuff**, to make the places of Kuloko nice and cozy. E.g. banners and demo signs, fairy lights, furniture, etc. But please don't bringt too much trash ;)
+ **Hearing protection** - Festivals are loud!
+  tent
+ sleeping bag
+ sleeping mat
+ Small daypack
+ Weatherproof clothing (sun and rain protection, dance outfit)
+ Hygiene stuff
+ Anti mosquito stuff
+ medical masks (FFP2 masks, surgical masks → remember to change them regularly)
+ disinfectant in take-away size
+ own dishes/cutlery/bread box
+ drinking bottle
+ own food for the hunger in between or if there is not enough Küfa food sometimes
+ medicines and vitamins 
+ camping chair for the festival feeling 
+ hammock for the Lützerathfeeling
+ headlamp/torch
+ pens
+ Mobile phone (For current info from the website and the [Telegram Channel](https://t.me/kulokofestival)
+ charging cable (there are solar charging stations!)
+ Donation money for Küfa and Camp → or donate **[here](https://kultur-ohne-kohle.de/de/contribute/donate)** beforehand.
+ camera
+ musical instruments and songbooks, games, a good book or whatever else you need if you want to get away from the crowd for a while
