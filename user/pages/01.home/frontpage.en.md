---
title: home
menu: home
heading:
    datestring: '02&ensp;-&ensp;22 August'
    place: 'near Garzweiler lignite pit'
    free_entrance: 'free entrance!'
    construction: '02 - 05 build up'
    programme: '06 - 15 Programme'
    dismantling: '15 - 22 dismantle'
news: News
content:
    items: '@self.modular'
    order:
        custom:
            - _come-along
---

<span  class="h1"> A festival with</span>
<span  class="pink h1">a lot of culture and without coal</span>
<span class="h1">art & culture instead of coal & destruction</span>


„Kultur ohne Kohle – eine rheinische Landpartie“ („Culture Without Coal - A Rhenish Country Outing“) is a free of charge, decentralized festival within and around the villages close to the open-face mine Garzweiler II.

Even though it’s clear that energy production through brown coal mining is an outdated technology, that boosts the climate crisis, the open-face mine is still supposed to expand and therefore destroy the homes of thousands of people. Because of that, there has been a fight for a climate just and brown coal free future for several decades now. We want to contribute to that!

Thats why Culture without coal invites people to strengthen and celebrate the resistance in anti-discriminatory spaces against brown coal and the International climate unjustice. Culture without Coal wants to try out to live the utopian dream of a fair and worth to live in society in a small space,  to organize itself free of the logic of exchange and therefore ensuring the possibility of participation for everybody. 

We want to fill the endangered villages with life, art, culture and resistance. We want to bring, in participation with the local residents, new liveliness to their homeland and celebrate with the resistant people in solidarity.
