---
title: 'Come along!'
body_classes: modular
image_align: left
---

## Come along!

There is a lot going to happen in the villages - stay curious!