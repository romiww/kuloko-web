---
title: 'Part of the Kuloko'
body_classes: modular
class: large
features:
    -
        text: '<strong> KCR – The core of the climate movement in Germany</strong> <br> Since 2010, climate camps have become one of the most important places where people come together around the topic of climate, to network, live together in a self-organized way that is tried to be aware of hierarchy, to educate themselves and to create diverse protests and actions together.'
        url: 'http://www.klimacamp-im-rheinland.de/'
        target: blank
        colors: turkoise-bg
        logo: klimacamp-im-rheinland-logo-2.svg
    -
        text: '<strong> Alle Dörfer bleiben - hier und weltweit!  </strong> <br> In the Germany-wide alliance, those affected by all lignite mining regions, the climate movement and citizens in solidarity are fighting together against forced resettlement and climate destruction.'
        url: 'https://www.alle-doerfer-bleiben.de/'
        target: blank
        logo: adb.png
        colors: 'pink-bg yellow-button'
    -
        text: '<strong> Unser aller Wald – In was für einer Welt wollen wir leben?  </strong> <br> The tree house village on the outskirts of Keyenberg not only wants to stop coal, but also to take a stand against fossil capitalism, which continues to feed global social injustice and the climate crisis. Here, people seek answers to the question: How do we shape our coexistence in a more equitable world? '
        target: blank
        colors: yellow-bg
        logo: uaw.png
        url: 'https://unserallerwald.org/'
        header: 'Unser aller Wald'
    -
        text: '<strong> Kirche(n) im Dorf lassen – für eine christliche Klimabewegung und globale Gerechtigkeit  </strong> <br> In the local resistance, Christians stand in the way of the hostile open-cast mining, the destruction of villages and churches with creative actions and fight for a good life for all within the framework of the global climate movement. '
        url: 'https://www.kirchen-im-dorf-lassen.de/'
        target: blank
        colors: turkoise-bg
        header: 'Kirchen im Dorf lassen'
        logo: Kidl_Logo.svg
    -
        text: '<strong> Mahnwache Lützerath – gemeinsam sind wir stark!  </strong> <br> Since 22.07.2021, the picket is located between Lützerath and the open pit mine as a place for networking, meeting and direct resistance. Only a few hundred meters away from the excavators, people from the threatened villages, activists and interested people meet there to set a sign together against the dirtiest fossil fuel.  '
        url: 'https://www.mahnwache-luetzerath.org/'
        target: blank
        colors: pink-bg
        header: 'Mahnwache Lützerath'
    -
        text: '<strong> MaStaMo – Machen statt motzen – Gemeinschaftlich und selbstorganisiert Freiräume ermöglichen und gestalten </strong> <br> Our world needs change! MaStaMo is a camp for connectedness, inspiring and constructive exchange, peace, joy (and vegan pancakes), mindful interaction with ourselves and the world, a desire to learn, liveliness, enthusiasm and responsibility to test a sustainable society! '
        url: 'https://www.bundjugend-nrw.de/projekte/mastamo/mastamo-camp/'
        target: blank
        colors: yellow-bg
        header: MaStaMo
    -
        text: '<strong> Lebenslaute – Widerständige Musik an unmöglichen Orten </strong> <br> Through civil disobedience, the open music and action group makes predominantly classical music sound where it is not expected - on military training grounds and deportation airports, in front of nuclear factories and missile depots, in immigration offices and other places that threaten people.'
        url: 'https://www.lebenslaute.net/'
        target: blank
        colors: turkoise-bg
        header: Lebenslaute
        logo: Lebenslaute.png
    -
        text: '<strong> BIPoC-Klimakonferenz - No Justice in a Racist Climate </strong> <br> BIPoCs create a BIPoC only space in Berverath City where people can formulate their own viewpoints, narratives, and opinions undisturbed and without having to pre-negotiate them with white perspectives. The discourse around the climate crisis is dominated by white supremacy, even though BIPoCS are much more extremely affected by its consequences. This needs to be disrupted.'
        url: 'https://kultur-ohne-kohle.de/de/bipoc-conference'
        target: blank
        colors: 'pink-bg yellow-button'
        header: BIPoC-Klimakonferenz
        logo: bipoc_logo.png
---

## Part of the Kuloko
Many different groups are part of the Kuloko and design individual actions, places or program points: