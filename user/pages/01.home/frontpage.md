---
title: home
menu: Start
heading:
    datestring: '02. - 22. August'
    place: 'Am Tagebau Garzweiler II'
    free_entrance: 'Eintritt Frei!'
    construction: '02. - 05. Aufbau'
    programme: '06. - 15. Programm'
    dismantling: '15. - 22. Abbau'
news: Neuigkeiten
content:
    items: '@self.modular'
    order:
        custom:
            - _part-of-kuloko
            - _start-cachel
            - _come-along
---

<span  class="h1"> Ein Festival mit</span>
<span  class="pink h1">viel Kultur und ohne Kohle</span>
<span class="h1">Kunst & Kultur statt Kohle & Zerstörung</span>


„Kultur ohne Kohle – eine rheinische Landpartie“ ist ein kostenloses, dezentral organisiertes Festival rund um die Dörfer am Tagebau Garzweiler II. 

Trotz der Tatsache, dass Energie durch Braunkohle eine längst überholte Technologie ist, die die Klimakrise massiv antreibt, soll sich der Tagebau weiter durch die Landschaft im Rheinland fressen und damit das Zuhause von tausenden Menschen zerstören. Deswegen setzen sich hier seit Jahrzehnten Menschen für eine klimagerechte, braunkohlefreie Zukunft ein. Dazu wollen wir was beitragen!
<p>Daher lädt Kultur ohne Kohle Menschen dazu ein, in diskriminierungsarmen Räumen den Widerstand der Dörfer gegen die Braunkohle und weltweite Klimaungerechtigkeit zu stärken und zu feiern. Kultur ohne Kohle  möchte versuchen im Kleinen die Utopie einer lebenswerten, gerechten Gesellschaft zu leben, sich <span class="popover">tauschlogikfrei  (?)<span class="popover-container">Tauschlogikfreie Umgangsformen zielen darauf ab, die Bedürfnisse von Menschen zu erfüllen, ohne dass diese dafür eine "Gegenleistung" in Form von Geld oder anderen materiellen oder nicht-materiellen Gütern erbringen müssen.</span></span> zu organisieren und somit Teilhabe für alle zu gewährleisten.</p>
Wir wollen die von der Umsiedlung bedrohten Dörfer zu Orten voller Leben, Kunst, Kultur und Widerstand Werden lassen. Wir wollen vor Ort gemeinsam mit den Bewohner*innen ihre Heimat mit neuem Leben füllen und solidarisch mit den widerständigen Menschen feiern. 

