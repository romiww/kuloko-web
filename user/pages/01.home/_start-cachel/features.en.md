---
title: 'Start Kachel'
class: medium
features:
    -
        header: 'Kuloko ist für...'
        text: 'Dorfliebhabende & Kulturbegeisterte, Dauercamper & Tanzhungrige, Waldbesetzende & Lagerfeuermusizierende, Protestierende & Resignierende, Familien & Widerständige'
        colors: yellow-bg
    -
        header: 'Du hast bock auf...?'
        text: 'Hofkonzerte & Tanznächte, Diskussionen & Lebenswurst <span class="popover">(?)<span class="popover-container">Das ist Lebenswurst, ein im Rheinland sehr beliebter Aufstrich:  <img src="/user/pages/images/Lebenswurst.png" alt="Lebenswurst in freier Wildbahn" width="250" align="center">  </span></span>, Kunsthandwerk & Utopiewerkstätte, Lesungen & Theaterstücke, Filmvorführungen & Radtouren'
        colors: turkoise-bg
    -
        header: Super!
        text: 'Dann komm ins Rheinland zum gemeinsamen Feiern, Diskutieren, Weiterdenken, Träumen, Ideen finden, Ausprobieren und Pläne schmieden!'
        colors: pink-bg
published: false
---

