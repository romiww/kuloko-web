---
body_classes: modular
image_align: left
---

### Anreise mit dem Zug
**Bahnhöfe** in der Nähe sind **Hochneukirch** und **Erkelenz**. Von Erkelenz aus fahren **Busse** in die Dörfer Keyenberg und Holzweiler, von denen aus der Rest der Festival-Punkte besser erreichbar ist. **[Hier](https://avv.de/files/avv/files/fahrplaene/linienfahrplaene/ek3_avv.pdf)** findet ihr einen der Busfahrpläne, die euch von Erkelenz in die Dörfer bringen. Außerdem wird es auch die Möglichkeit eines **Shuttle-Angebotes** zwischen den Bahnhöfen und Dörfern sowie zwischen den Dörfern selbst geben. Da dieses jedoch nur begrenzte Kapazitäten hat, ist es super, wenn ihr euer **Fahrrad dabei** habt. Auch um auf dem dezentralen Festivalgelände mobil zu sein, ist ein Fahrrad eine super Idee. 

### Mit dem Auto 
Auch bei Anreise mit dem Auto empfiehlt es sich, Fahrräder mitzunehmen, um gemütlich und ökologisch von Ort zu Ort zu fahren.