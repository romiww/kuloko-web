---
body_classes: modular
image_align: left
---

### Wo genau ist die Kuloko?
Das Kultur ohne Kohle Festival wird rund um die vom Tagebau Garzweiler II bedrohten Dörfer **Lützerath, Keyenberg, Unterwestrich, Oberwestrich, Kuckum und Berverath** stattfinden. Eingangspunkte sind in Lützerath, Keyenberg und Holzweiler geplant. Kurze Informationen zu den verschiedenen Orten findest du in der Karte.