---
title: discrimination
---

####ACH JA, LAST BUT NOT LEAST – EINE SACHE IST UNS FURCHTBAR WICHTIG

Schreib uns besonders gern, wenn du dich als **FINTA(1)**, **BIPoC(2)**, von der
Gesellschaft be_hindert, nicht-akademisch, LGBTQIA+ und/oder Refugee
indentifizierst !

Die Gesellschaft, in der wir leben, gibt weißen(3) cis(4)-Männern viel zu oft eine Bühne. Vielen Menschen wird dadurch Sichtbarkeit verwehrt und sie werden
strukturell in und an dem, was sie gern tun möchten, gehindert.
Um dem entgegenzuwirken, haben wir entschieden, für das offizielle
Bühnenprogramm der KuloKo keine Acts zu buchen, die ausschließlich aus
weißen abled hetero-cis-Männern bestehen.

Du identifizierst dich als weiß und cis-männlich und möchtest mit deinem Act
trotzdem super gern für globale Klimagerechtigkeit und den Kohleausstieg
spielen ? Mega nice ! Tramp doch einfach zu uns runter und erfreu dich und uns
mit Straßenmusik oder -theater – bloß Gage und die große Bühne wird es nicht
werden bei uns.