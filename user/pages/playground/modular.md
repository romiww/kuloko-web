---
menu: Playground
title: Playground
hero_title_color: light
hero_background: /user/pages/images/opencall_music.jpeg
hero_foreground: /user/pages/images/fist-with-mic.png
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
	    - _map-test
---

looks (can be applied to each module):
	- dark_bg: changes font color to light color
	- dark_mark: changes font color inside <mark> tags to light color