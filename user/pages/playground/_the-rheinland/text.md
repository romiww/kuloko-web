---
body_classes: modular
image_align: left
display: none
---

Im Westen Deutschlands, zwischen Aachen, Köln und Mönchengladbach, liegt das Rheinland. Dort werden in den drei Tagebauen Hambach, Garzweiler II und Inden jährlich ca. 100 Millionen Tonnen Braunkohle gefördert, womit es das größte Braunkohle-Abbaugebiet Europas und gleichzeitig eine der größten CO2-Quellen ist. Die im Rheinischen Revier stehenden RWE-Kraftwerke Neurath, Niederaußem und Weisweiler gehören dadurch zu den fünf schädlichsten Kraftwerken Europas – gemeinsam mit dem Kraftwerk Frimmersdorf verzeichnen sie einen jährlichen CO2-Ausstoß von 57,63 Millionen Tonnen (Stand 2019) und sind damit für ca. 13 % der Treibhausgasemissionen Deutschlands verantwortlich.