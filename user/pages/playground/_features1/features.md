---
class: large
features:
    -
        text: '<strong> KCR – Das Herz der Klimabewegung </strong> <br> Seid 2010 sind Klimacamps mit die wichtigsten Orte an denen Menschen rund ums Thema Klima zusammen kommen, sich vernetzen, herrschaftskritisch und selbstorganisiert zusammenleben, sich bilden und gemeinsam vielfältige Proteste und Aktionen gestalten.'
        url: 'http://www.klimacamp-im-rheinland.de/'
        target: blank
        colors: turkoise-bg
        logo: klimacamp-im-rheinland-logo-2.svg
    -
        text: '<strong> Alle Dörfer bleiben – hier und weltweit!  </strong> <br> In dem deutschlandweiten Bündnis kämpfen Betroffene aller Braunkohle-Revire, die Klimabewegung, sowie solidarische Bürger*innen gemeinsam gegen die Zwangsumsiedlung und Klimazerstörung.  '
        url: 'https://www.alle-doerfer-bleiben.de/'
        target: blank
        logo: adb.png
        colors: 'pink-bg yellow-button'
    -
        text: '<strong> Unser aller Wald – In was für einer Welt wollen wir leben?  </strong> <br> Das Baumhausdorf am Rande von Keyenberg, will nicht nur die Kohle stoppen, sondern gegen den fossilen Kapitalismus eintreten, der globale soziale Ungerechtigkeit und die Klimakrise immer weiter anheizt. Hier suchen Menschen Antworten auf die Frage: Wie gestalten wir unser Zusammenleben in einer gerechten Welt? '
        target: blank
        colors: yellow-bg
        logo: uaw.png
        url: 'https://unserallerwald.org/'
        header: 'Unser aller Wald'
    -
        text: '<strong> Kirche(n) im Dorf lassen – für eine christliche Klimabewegung und globale Gerechtigkeit  </strong> <br> Im lokalen Widerstand stellen sich Christ*innen mit kreativen Aktionen dem lebensfeindlichen Tagebau, der Zerstörung der Dörfer und Kirchen in den Weg und kämpfen im Rahmen der globalen Klimabewegung für ein gutes Leben für alle. '
        url: 'https://www.kirchen-im-dorf-lassen.de/'
        target: blank
        colors: turkoise-bg
        header: 'Kirchen im Dorf lassen'
        logo: Kidl_Logo.svg
    -
        text: '<strong> Mahnwache Lützerath – gemeinsam sind wir stark!  </strong> <br> Seid dem 22.07.2021 steht die Mahnwache als Ort der Vernetzung und Begegnung und des direkten Widerstandes zwischen Lützerath und dem Tagebau. Nur wenige hundert Meter von den Baggern entfernt treffen sich dort Menschen aus den bedrohten Dörfern, Aktive und interessierte Menschen um gemeinsam ein Zechen zu setzen gegen den dreckigsten, fossilen Energieträger.  '
        url: 'https://www.mahnwache-luetzerath.org/'
        target: blank
        colors: pink-bg
        header: 'Mahnwache Lützerath'
    -
        text: '<strong> MaStaMo – Machen statt motzen – Gemeinschaftlich und selbstorganisiert Freiräume ermöglichen und gestalten </strong> <br> Unsere Welt braucht Veränderung! Das MaStaMo ist ein Camp für Verbundenheit, inspirierenden und konstruktiven Austausch, Frieden, Freude (und vegane Eierkuchen), achtsamen Umgang mit uns und der Welt, Lust am Lernen, Lebendigkeit, Begeisterung und Verantwortung, um eine zukunftsfähige Gesellschaft zu erproben! '
        url: 'https://www.bundjugend-nrw.de/projekte/mastamo/mastamo-camp/'
        target: blank
        colors: yellow-bg
        header: MaStaMo
    -
        text: '<strong> Lebenslaute – Widerständige Musik an unmöglichen Orten </strong> <br> Seid 2010 sind Klimacamps mit die wichtigsten Orte an denen Menschen rund ums Thema Klima zusammen kommen, sich vernetzen,     herrschaftskritisch und selbstorganisiert zusammenleben, sich bilden und gemeinsam vielfältige Proteste und Aktionen gestalten.'
        url: 'http://www.klimacamp-im-rheinland.de'
        target: blank
        colors: turkoise-bg
        header: Lebenslaute
    -
        header: 'this and that'
        logo: uaw.png
        text: 'lorem ipsum undsoweiter dir kuloko wird so toll und wenn ich jetzt hier noch ganz viel mehr text anfüge wie sieht das dann aus?'
        colors: 'pink-bg yellow'
        link: /playground
    -
        text: 'Bock auf FDP-Werbung?'
        colors: 'yellow-bg pink'
        text_styles: left
    -
        text: 'Es gilt zu beachten dass Elemente sich in ihrer Größe immer am größten Element ihrer jeweiligen Reihe orientieren.'
        text_styles: bungee
        colors: turkoise-bg
    -
        header: demo
        text: 'so sehen die dinger aus wenn keine styles im yaml-frontmatter angegeben werden'
---

