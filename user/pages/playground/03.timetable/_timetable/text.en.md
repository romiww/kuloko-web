---
title: Timetable
body_classes: modular
image_align: left
---

## Timetable
Wir sind dran, den Timetable in ein Website-Fähiges Format zu bringen. Bis dahin kannst du dir den Timetable als PDF anschauen:
<br>
<div class="button-container" style="margin-top:-1rem"> 
    <a href="/blog/infoheft/Kuloko_Programm.pdf" target="_blank">
	<div class="feature-button rounded-edges yellow-bg black">Zum Zeitplan!</div>
    </a>
</div>
Wenn du mehr herumblättern willst gibt's auch das komplette [Info- und Programmheft ](/blog/infoheft) zum runterladen:
<br>
<div class="button-container" style="margin-top:-1rem"> 
    <a href="/blog/infoheft/Kuloko_Infoheft.pdf" target="_blank">
	<div class="feature-button rounded-edges yellow-bg black">Zum ganzen Infoheft!</div>
    </a>
</div>

Auf welches Programm Du dich auf jeden Fall freuen kannst, siehst du bei [Lineup](/program/lineup).


## Zeitstrahl
Hier ist eine kleine Übersicht über den Zeitlichen Ablauf im Rheinland. Wir versuchen, alles was uns zugetragen wird dort einzutragen.
<!--
<style>
.stripe img {width: 450px; height:255px; object-fit:cover}
.stripe {
  width: 225px;
  height:255px;
  overflow-x : scroll;
  overflow-y : hidden;
}
</style>

<div class="stripe">
<img src="/user/pages/images/timearrow_nobg.png" alt="Inhalt scrollen">
</div>
-->