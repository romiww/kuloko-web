---
title: null
body_classes: modular
background:
    image: /user/pages/images/guitars-light.jpg
    repeat: true
author: Noura
heading_styles: gray
paragraph_styles: light
mark_bg: null
---

<div> <img src="/user/pages/images/noura.jpg"> </div> Klimakrise überwinden heißt dekolonialisieren. Dort, wo nicht - weiße Menschen zusammen kommen und unsere Stimmen bilden, unsere Geschichten erzählen, entsteht ein schöner Raum. Die weiß dominierte Gesellschaft will nicht, dass wir zusammen kommen, dass wir uns Räume nehmen außerhalb derer, in die sie uns zwängt. Sie hat Angst davor. Sie bebt und zittert. Dabei weiß sie noch nicht, dass wir an unser aller Befreiung schmieden.