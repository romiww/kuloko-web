---
title: 'Presse Kernthesen'
body_classes: modular
image_align: left
---

## KULTUR OHNE KOHLE IST...
„Kultur ohne Kohle – eine rheinische Landpartie“ ist ein Festival rund um die Dörfer am Tagebau Garzweiler II. 10 Tage lang lädt das Festival Menschen zu Kunst und Kultur ein, um den Widerstand gegen die Braunkohle zu stärken und für weltweite Klimagerechtigkeit einzustehen und um die Bagger zu stoppen. Kultur ohne Kohle willl Kunst, Widerstand, Kultur und vor allem Menschen zusammenbringen, um eine nachhaltige, demokratische und gerechte Zukunft aufzubauen.
    
Den Kontext zum Widerstand am Tagebau Garzweiler II finden Sie unter [Widerstand](https://kultur-ohne-kohle.de/resistance/the_villages).

**Email: **presse@kultur-ohne-kohle.de

**Telefon: **+4915758128656 (Noura Hammouda)