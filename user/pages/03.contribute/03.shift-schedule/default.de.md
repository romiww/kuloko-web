---
title: 'Support auf der Kuloko'
menu: Schichtplan
---

#### Auf der Kuloko gibt es viele Möglichkeiten, wie Menschen sich einbringen, mitmachen und mitgestalten können.
<br> Auf den Camps braucht es immer Menschen die **Repro-Arbeiten** übernehmen. Damit ist alles gemeint was wir zur Reproduktion benötigen, wie spülen, Kompoletten putzen, beim Kochen helfen, aufräumen etc. Ihr werdet vor Ort über gedruckte Schichtpläne sowie in morgendlichen Plena die Möglichkeit haben euch dafür zu melden. 
Für andere Aufgaben, hilft es uns sehr, wenn ihr euch, gerne mit Bezugs- oder Ortsgruppen, jetzt schon in **online-Schichtpläne** eintragt. Die Schichtpläne umfassen Ordner:innenschichten, Infotischschichten, Mahnwachenschichten, Nachtschichten und Sanitärtouren. Alle weiteren Infos zu den Schichten gibt es in der Schichttabelle.

### Infos zu Schichtplänen gibt's im [Telegam Channel](https://t.me/kulokofestival)

<br> Außerdem brauchen wir **Dich** für unsere 
### Abbauphase vom 16.-22. August!
Damit der Abbau genauso viel Spaß macht wie der Aufbau, freuen wir uns, wenn viele Menschen Lust haben ein paar Tage länger zu bleiben und mit anzupacken.