---
title: 'Crew Inhalt'
body_classes: modular
button:
    text: 'Deine Crew hat Bock? Mail an: info@kultur-ohne-kohle.de!'
    url: 'mailto:info@kultur-ohne-kohle.de'
    styles: 'yellow-bg gray'
image_align: left
---

### Crew gesucht!
Für folgende Bereiche suchen wir noch Crews, die sich um den Zeitraum der Kuloko halbwegs eigenständig um ihren Aufgabenbereich kümmern:

+ Auf- und Abbau
+ Campschutz (Sicherheit vor Ort, Nachtwachen etc.)
+ Mahnwachencrews (MaWa schick machen, als ersten Anlaufpunkte betreuen, Menschen mit einbeziehen)
+ Fotograf*innen/Filmer*innen, die das Geschehen begleiten
+ Eine Live-Stream-Crew
+ Theater-Crew 
+ Kino-Crew
+ Werkstatt-Crew

