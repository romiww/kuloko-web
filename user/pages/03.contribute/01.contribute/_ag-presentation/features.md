---
title: 'AG Vorstellung'
body_classes: modular
class: medium
features:
    -
        header: 'Booking AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-Festival-Programm organisieren<br>-Künstler*innen anfragen und betreuen<br>-mit Technik AG, Infra- und Gelände AG koordinieren'
        url: 'mailto: booking@kultur-ohne-kohle.de'
        button: booking@kultur-ohne-kohle.de
        colors: turkoise-bg
    -
        header: 'Bildungsarbeit AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-organisieren von Workshops, Vorträgen, Diskussionen, Open Spaces und Kinderprogramm<br>-Referent*innen anfragen und betreuen<br>-mit Technik AG, Infra- und Gelände AG koordinieren'
        url: 'mailto: inputs@kultur-ohne-kohle.de'
        button: inputs@kultur-ohne-kohle.de
        colors: turkoise-bg
    -
        header: 'Gelände- und Orte AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-Überblick über Orte und ihre Nutzung<br>-Kontakt zu Flächenbesitzer*innen<br>-Anmeldung der Veranstaltung<br>-mit anderen AGen koordinieren'
        url: 'mailto: gelaendeundorte@kultur-ohne-kohle.de'
        button: 'gelaendeundorte @kultur-ohne-kohle.de'
        colors: turkoise-bg
    -
        header: 'Technik AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-Besorgung von Strom, Licht und Veranstaltungstechnik<br>-mit Technik, Infra- und Gelände AG, Booking AG koordinieren<br>-sämtliche IT-Aufgaben'
        url: 'mailto: Technik@kultur-ohne-kohle.de'
        button: Technik@kultur-ohne-kohle.de
        colors: turkoise-bg
    -
        header: 'Infrastruktur AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-Sanitäranlagen, Toiletten, Duschen, Wasser, Abwasser, Müll etc.<br>-Hygienekonzept und Barrierearmut<br>-Küche und Getränke<br>-Transportlogistik<br>-Flächenplanung mit Gelände AG<br>-Auf- und Abbauplanung'
        url: 'mailto: infra@kultur-ohne-kohle.de'
        button: infra@kultur-ohne-kohle.de
        colors: turkoise-bg
    -
        header: 'Prozess AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-Kontakt zu allen AGs und für Kommunikation sorgen<br>-Arbeitsprozesse begleiten und unterstützen<br>-Vorbereitung der Plena<br>-neue Menschen bei Orientierung helfen'
        url: 'mailto: Team@kultur-ohne-kohle.de'
        button: Team@kultur-ohne-kohle.de
        colors: turkoise-bg
    -
        header: 'Campleben AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-Infozelt und Helfer*innenkoordination<br>-Shuttle Koordination<br>-für Wohlfühlfaktor der Besucher*innen sorgen<br>-Entscheidungsstruktur auf Camp'
        url: 'mailto: info@kultur-ohne-kohle.de'
        button: info@kultur-ohne-kohle.de
        colors: turkoise-bg
    -
        header: 'Awareness AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-Awareness-Strukturen aufbauen<br>-Rahmenbedingung schaffen für diversere Orgacrew<br>-Reflektionsarbeit'
        url: 'mailto: info@kultur-ohne-kohle.de'
        button: info@kultur-ohne-kohle.de
        colors: turkoise-bg
    -
        header: 'Öffentlichkeits- und Presse AG'
        text: '<strong>Unsere Aufgaben</strong>:<br>-Webseite und Social Media<br>-Mobilisierung und Verbreitung<br>-Presse Kontakt<br>-Zusammenarbeit mit allen AGs'
        url: 'mailto: media@kultur-ohne-kohle.de'
        button: media@kultur-ohne-kohle.de
        colors: turkoise-bg
---

