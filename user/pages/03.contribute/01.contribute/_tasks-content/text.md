---
title: 'Aufgaben Inhalt'
body_classes: modular
button:
    text: 'Du hast Lust? Mail an: info@kultur-ohne-kohle.de!'
    url: 'mailto:info@kultur-ohne-kohle.de'
    styles: 'yellow-bg gray'
image_align: left
---

### Support gesucht

#### auf dem Festivalgelände 
 + **Betreuung und Koordination** von einem der zehn KuloKo-Orte: 
    	**Die Theaterwiese Kuckum**
        		+ du brauchst keine Vorkenntnisse, nur Lust Neues zu lernen
+ **Support der Barcrew** an der Hauptbühne
    	+ Organisation und Besorgung der Getränke, Koordination der Schichten
+ **Crew Rückzugsort**
	+ Gestaltung und Betreuung 

#### Infrastruktur
+ **Wasser-Infrastruktur planen** 
    	+ Wo müssen welche Anschlüsse angebunden werden, wo stehen Waschbecken etc. ?
+  **Unterstützung der Infrastruktur-Koordination** während der KuloKo 
    	 + beispielweise wo muss wann was an Material hingeliefert werden
+ **Internet-Infrastruktur** organisieren
+ **Livestream-Infrastruktur** organisieren und betreuen
    
#### als Teil eines Teams
+ **Pressesprecher:innenteam unterstützen**
    	+ Infomaterial, Trainings- und Pressebotschaften werden im Team verteilt, auch hier sind **keine Vorkenntnisse nötig**
+ **Unterstützung der Sicherheits-AG** des Klimacamp Rheinland
    	Einweisung und Koordination von Schichten und Nachtwachen
        
#### Hinter den Kulissen 
#####Alles auf dem Schirm haben
+ **Koordination des Shuttle-Konzepts**
    	+ Telefonbereitschaft, wo ist wann welches Shuttle und wer muss wo abgeholt werden
+ **Werkstatt betreuen**
    	+  Materialausgabe, Wissen wo welches Werkzeug ist, wann es neues Material braucht etc. 
+ **Betreuung von Mahnwachen** 
    	+ Mahnwachen dienen als Anlaufstelle und Infopunkt
    
