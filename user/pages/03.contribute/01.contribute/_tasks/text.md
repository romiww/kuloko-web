---
title: Aufgaben
body_classes: modular
---

## Aufgaben
Hier versuchen wir, die aktuell noch offenen spezifischen Aufgaben bei der Planung und Durchführung der Kuloko aufzulisten. Es werden einerseits noch Menschen gebraucht, die Organisationsaufgaben übernehmen, andererseits suchen wir auch Crews, die komplette Aufgabenpakete annehmen und sich selbstständig, aber natürlich trotzdem in Absprache mit dem Rest des Teams, für ihren Bereich verantwortlich fühlen. Hier kannst du auch immer gucken ob neue Aufgaben dazu gekommen sind, wir werden die Liste nämlich immer updaten wenn es was Neues gibt.