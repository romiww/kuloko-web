---
title: 'Genau richtig'
body_classes: modular
image_align: left
text: info@kultur-ohne-kohle.de
url: 'mailto:info@kultur-ohne-kohle.de'
styles: 'yellow-bg gray'
---

## Help us!
Du hast nicht nur Lust, gemeinsam zu feiern,  <span class="pink">sondern auch gemeinsam zu planen? </span>
### Dann bist du hier genau richtig!
Es gibt unterschiedlichste Aufgaben, die auf dich warten und tolle Menschen die sich auf dich freuen!  Mach mit! Mach mit uns Kultur ohne Kohle und stell eine unvergessliche rheinische Landpartie mit auf die Beine! 
## Unsere Arbeitsgruppen
Eine Möglichkeit zu helfen ist es, sich noch an der Orga-Arbeit in einer der AGs zu beteiligen. Im Prinzip können alle AGs noch Unterstützung gebrauchen. Vielleicht hast du ja schon mal in einer ähnlichen AG woanders dich beteiligt oder Verantwortungen übernommen, oder du bist ganz neu und hast quasi noch keine Erfahrung: so oder so freuen wir uns wenn du Bock hast mitzumachen! Egal was deine Erfahrungen bisher sind, es wird immer was geben was du machen kannst! Hier sind alle AGs aufgelistet mit deren Kontakten, damit du ein grobes Bild kriegst was welche AG macht. Wenn du Bock auf eine AG oder noch irgendwelche Fragen oder Unsicherheiten hast, dann schreib doch einfach eine Mail! Wenn du noch nicht genau weisst welche AG was für dich ist, du aber trotzdem helfen willst, dann schreib einfach eine Mail an: 
