---
title: donation-text
body_classes: modular
---

## Kultur ohne Kohle?
### Ja bitte! Aber ganz ohne Kohle geht es dann doch nicht.

Damit sich alle Menschen während unserer rheinischen Landpartie höchstens um Braunkohle und nicht um die Kohle in ihren Portemonnaies Gedanken machen müssen, brauchen wir im Voraus Geld. Kultur ohne Kohle soll ein Ort werden an dem alle teilnehmen können und wir ein solidarisches Miteinander pflegen. Dazu gehört auch, dass wir mitdenken, dass Menschen unterschiedliche Zugänge zu materiellen und finanziellen Ressourcen haben.

#### Wenn dir also gerade bewusst wird, dass …
+ du dir die Kuloko auch hättest leisten können, wenn sie Geld kosten würde,
+ du in der Charity-Abteilung von RWE arbeitest und ein wenig umverteilen willst,
+ du eine Menge Geld sparst, weil dir die Pandemie deine alljährlichen Lieblingsfestivals versaut hat,
+ oder du einfach nur dein lästiges Kleingeld loswerden willst, 

#### dann gib gerne ein paar Euros ab, um anderen Menschen die Teilnahme zu ermöglichen.
