---
title: donation-text
body_classes: modular
---

## Kultur ohne Kohle? (Culture without Money?)
### Yes, please! But unfortunately it doesn't work completely without money.

So that all people during our Rhenish country outing have to worry at most about brown coal and not about the cash in their wallets, we need money in advance. Kultur ohne Kohle should be a place where everyone can participate and where we cultivate solidarity. This also means that we have to take into account that people have different access to material and financial resources.

#### So if you're just realizing that …
+ you could have afforded the Kuloko even if it cost money,
+ you work in the charity department of RWE and want to redistribute a little,
+ you saved a lot of money because the pandemic has ruined your favorite annual festivals,
+ or you just want to get rid of your annoying small change, 

#### then feel free to give a few euros to enable all of us to make the Kuloko happen.
