---
title: Kontodaten
background:
    image: /user/pages/images/guitars-light.jpg
    repeat: true
heading_styles: gray
paragraph_styles: light
---

Empfänger\*in: KIB e.V. - <br>
IBAN: DE24 4306 0967 1204 1870 01 <br>
Verwendungszweck: Spende Kuloko <br>
Verwendungszweck für die Küfa: Spende Kuloko Küfa