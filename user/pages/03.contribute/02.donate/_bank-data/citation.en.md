---
title: 'Account details'
background:
    image: /user/pages/images/guitars-light.jpg
    repeat: true
heading_styles: gray
paragraph_styles: light
mark_bg: null
---

Empfänger*in (Receiver): KIB e.V. - <br>
IBAN: DE24 4306 0967 1204 1870 01 <br>
Verwendungszweck (Purpose): Spende Kuloko <br>