---
title: Crowdfunding
body_classes: modular
image_align: left
button:
    text: 'Jetzt Spenden!'
    url: 'https://www.startnext.com/kultur-ohne-kohle-2021'
    styles: 'yellow-bg gray'
published: false
---

## Crowdfunding Gestartet!
Die Kuloko-Crowdfunding Kampagne auf Startnext ist jetzt Online. Schau sie dir doch mal an!

