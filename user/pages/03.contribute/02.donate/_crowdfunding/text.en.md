---
title: Crowdfunding
body_classes: modular
image_align: left
button:
    text: 'Donate now!'
    url: 'https://www.startnext.com/kultur-ohne-kohle-2021'
    styles: 'yellow-bg gray'
published: false
---

## Crowdfunding started!
The Kuloko-Crowdfunding campaign on Startnext is online. Have a look!

