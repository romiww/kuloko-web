---
title: Donate
hero_title_color: light
hero_background: /user/pages/images/Donate2.jpg
hero_foreground: /user/pages/images/bar.svg
content:
    items: '@self.modular'
    order:
        custom:
            - _crowdfunding
            - _donation-text
            - _if-you
            - _bank-data
published: true
menu: Donate
---

