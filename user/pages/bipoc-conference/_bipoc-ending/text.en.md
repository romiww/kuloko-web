---
title: 'BIPoC Conference'
body_classes: modular
button:
    text: 'Mail to: bipoc-konferenz@kultur-ohne-kohle.de!'
    url: 'mailto:info@kultur-ohne-kohle.de'
    styles: 'yellow-bg gray'
image_align: left
---

### What it's about?
Coal mining in the Garzweiler open pit mine in the Rhenish lignite mining area is part of Europe's largest source of CO2. Countries in the global north continue to build their fossil fuel wealth on colonial domination - at the expense of indigenous, Black and People of Color worldwide. While white supremacy enables the explotation of nature and countless people, voices of BIPoCs are still not heard in debates about the climate crisis. 
And in Germany, too, the climate justice movement in the recent years has been shaped and dominated by white people - even though the climate crisis is about us! Everywhere we look, climate change reinforces intersectional racisms and other power structures. If we want to overcome the climate crisis, we have to start at the root of white supremacy. That's why we see it as a political act to create a space only for us, at the hearh of this climate crisis, in the Rhineland lignite mining district. Together we are strong.


### Where?
The conference will take place in a courtyard in Berverath. You will receive the details when you register or on request (via email). Additionally, there will be direction signs at other places in the festival.
    
Sleeping and mobility

We will sleep in Lützerath at the clima camp. This camping area is not a BIPoC only space, but there will be a tent, that is just for us. You will need a tent and a bike (optional but recommended). If you don't have the possibility to bring a tent or you prefer to sleep under a solid roof, note it down in the registration form and we'll find a solution. There will also be a BIPoC only car shuttle from Lützerath to Beverath and back and soli bikes that everyone is able to share. 

More information about the location and how to get there can be found **[here.](/how-to-get-there)**

We will provide you with vegan food and drinks. 
Participation in the conference is for free. If you need money for your travel expenses, let us know in the registration.  


### Awareness:
In our BIPoC Community space we are mindful of and with each other. We wish to create a safer space for as many as possible. There will be a team on site that will take care of making as many as possible feel comfortable. They will always have an open ear for you. You will get more information about our awareness structures at the conference after your registration.

## Registration
In order to plan the conference better, we would love for you to registrate. Please fill out the form on **[this page](https://nrw.bundjugend.de/termin/bipoc-klima-konferenz/).**

You will receive the latest information via email after you registered.

Do you have any further questions? You want to participate? Offer a workshop or display your art? Send us an email via **[this Form](https://nrw.bundjugend.de/termin/bipoc-klima-konferenz/)**.

## Program
Program will be uploaded here shortly. 