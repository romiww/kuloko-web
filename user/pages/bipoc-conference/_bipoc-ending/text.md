---
title: 'BIPoC Konferenz'
body_classes: modular
button:
    text: 'Mail an: bipoc-konferenz@kultur-ohne-kohle.de!'
    url: 'mailto:bipoc-konferenz@kultur-ohne-kohle.de'
    styles: 'yellow-bg gray'
image_align: left
---

### Worum geht's?
Der Kohleabbau im Tagebau Garzweiler ist im rheinischen Braunkohlerevier Teil von der größten CO2 Quelle Europas. Europäische und andere Länder des globalen Nordens bauen ihren fossilen Wohlstand bis heute auf Kolonialherrschaft - auf Kosten von Indigenen, von Schwarzen und People of Color weltweit. Während die Logik der weißen Vorherrschaft die Ausbeutung der Natur und unzähliger Menschen möglich macht, werden Stimmen von BIPOCs in Debatten um die Klimakrise immer noch nicht gehört. Und auch in Deutschland sind die Klimabewegungen der vergangenen Jahre von weißen Menschen geprägt und dominiert - dabei geht es in der Klimakrise um uns! Überall wo wir hinschauen verstärkt der Klimawandel intersektionale Rassismen und andere Machtverhältnisse. Wenn wir die Klimakrise überwinden wollen, bedeutet das, an der Wurzel weißer Vorherrschaft anzufangen. Deshalb verstehen wir es als politischen Akt, einen Raum nur für uns zu schaffen, an einem Herd dieser Klimakrise, im rheinischen Braunkohlerevier. Zusammen sind wir stark.


### Wo?
Die Konferenz findet in einem Hof in Berverath statt. Die Adresse bekommst du wenn du dich anmeldest oder auf Anfrage. Die Wege werden auch von anderen Orten von Kultur ohne Kohle ausgeschildert sein. 
    
### Schlafen und Mobilität
Wir übernachten in Lützerath auf dem Klimacamp Rheinland. Das ist ca. 3,5 Kilometer von dem Konferenzort entfernt. Die Campfläche ist kein BIPoC only Space. Es gibt dort aber ein Gemeinschaftszelt, das nur für uns bestimmt ist. Du brauchst also ein Zelt und am besten ein Fahrrad. Wenn du kein Zelt mitbringen kannst oder lieber unter einem festen Dach schlafen möchtest, schreib uns das in die Anmeldung, dann finden wir eine Lösung. Es wird auch BIPoC only Auto Shuttles von Lützerath nach Berverath und zurück geben und Soli Fahrräder, die alle gemeinsam benutzen.

Mehr Infos zum Ort und zur Anreise findest du **[hier.](https://kultur-ohne-kohle.de/how-to-get-there)**

Wir sorgen für veganes Essen und Getränke.
Die Teilnahme an der Konferenz ist für dich kostenlos. Wenn du Geld für deine Fahrtkosten brauchst, kannst du uns das in der Anmeldung mitangeben.


### Awareness:
Beim BIPoC Community Space gehen wir achtsam miteinander um und wünschen uns, einen safer space für möglichst Viele zu schaffen. Vor Ort wird es ein Team geben, dass sich darum kümmert, dass viele sich wohl fühlen können und das immer ein offenes Ohr für dich hat. Genauere Infos zu den Awareness Strukturen der Konferenz bekommst du nach der Anmeldung.


## Anmeldung
Um die Konferenz besser planen zu können, bitten wir dich, uns eine Anmeldung zu schicken. Füll dazu das Formular auf **[dieser Seite](https://nrw.bundjugend.de/termin/bipoc-klima-konferenz/)** aus. 

Vor der Konferenz bekommst du noch eine Mail mit letzten Infos.

Du hast noch Fragen? Du willst mitmachen? Einen Workshop anbieten oder deine Kunst zeigen? Schreib uns an:
