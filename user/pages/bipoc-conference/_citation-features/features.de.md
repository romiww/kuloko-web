---
title: citation-features
body_classes: modular
class: large
features:
    -
        text: '"Klimakrise überwinden heißt dekolonialisieren. Dort, wo nicht - weiße Menschen zusammen kommen und unsere Stimmen bilden, unsere Geschichten erzählen, entsteht ein schöner Raum. Die weiß dominierte Gesellschaft will nicht, dass wir zusammen kommen, dass wir uns Räume nehmen außerhalb derer, in die sie uns zwängt. Sie hat Angst davor. Sie bebt und zittert. Dabei weiß sie noch nicht, dass wir an unser aller Befreiung schmieden." <br><br> <span style="text-align:right; font-style:italic"> <strong> -Noura </strong> </span> '
        colors: turkoise-bg
        logo: noura.jpg
    -
        text: '"Wir müssen uns unseren Raum zurückholen und einen Safe Space kreieren in dem wir uns wohlfühlen, empowern und austauschen können." <br><br> <span style="text-align:right; font-style:italic"> <strong> -Anuria </strong> </span> '
        logo: anuria.jpg
        colors: 'pink-bg yellow-button'
---

