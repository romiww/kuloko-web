---
title: bipoc-sharepics
body_classes: modular
button:
    text: 'Download sharepics!'
    url: /user/pages/bipoc-conference/_bipoc-ending/BIPoC_Conference_Sharepics.zip
    target: download
    styles: 'yellow-bg gray'
---

## Mobilisation material
Please help us to promote the BIPoC conference by downloading and sharing these **sharepics**: