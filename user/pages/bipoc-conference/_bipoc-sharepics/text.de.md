---
title: bipoc-sharepics
body_classes: modular
button:
    text: 'Download der Sharepics'
    url: /user/pages/bipoc-conference/_bipoc-ending/BIPoC_Conference_Sharepics.zip
    target: download
    styles: 'yellow-bg gray'
---

## Mobi Material
Hilf gerne mit, die BIPoC Konferenz zu bewerben, indem du z.B. **diese Sharepics** herunterlädst und teilst: