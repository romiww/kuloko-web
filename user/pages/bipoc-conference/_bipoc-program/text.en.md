---
title: bipoc-program
body_classes: modular
---

 ## Program
 ### 06.08. FRIDAY - SET-UP 
 + 08:00 Set-up
 + 10:00 Welcome plenary - Distribution of tasks
 + 14:00 Set-up of ISD Pavillion/Space
 + 18:00 Dinner
 
### 07.08. SATURDAY 
+ 08:00 Arrival from Lützerath 
+ 09:00 Breakfast
+ 10:00 Welcome plenary
+ 11:00 Getting to know each other
+ 12:00 Workshop: (Neo-) Colonialism And Resistance
+ 14:00 Joint participation in the ADB demonstration 14.30-16.00 Uhr
+ 15:00 ADB Demonstration:  Closing rally - BIPoC Panel with Zade Abdullah
+ 16:00 Relax after the demonstration, going back to Berverath
+ 17:00 Joint arrival 
+ 17:30 BIPoC Empowerment-Session 
+ 19:00 Dinner
+ 20:30 Mohammad Reza Dashtoomi - persian singer songwriter
+ 21:00 DJ Set by Mr. Biatch 

### 8.8. SUNDAY
+ 08:00 Arrival from Lützerath
+ 09:00 Breakfast
+ 10:00 Welcome plenary
+ 11:00 Presentation and reading of the brochure “Colonialism and Climate Crisis” -  Shayli Kartal and Laura Bechert and Dodo
+ African Lunch 
+ 13:00 Workshop: Afro-Futurism - Adrian Blount
+ 15:00 Workshop: Intersectional, Queer Feminist Justice Practices
+ 17:00 BIPoC History- Labor migration in Allmanya - Discussion 
+ 18:00 Initiatives and groups introduce themselves - connecting space 
+ 20:00 Spoken Word Night: Mamadou 
+ 20:45 Sami El Poet + open stage
+ 21:30 Muhammed - multilingual Spoken Word
+ 22:00 pamela yigoras - singer songwriter music - afterwards: Jam Session
+ 22:30 Spotify Party: everybody DJs who feels like it 

### 09.08. MONDAY
+ 08:00 Arrival from Lützerath
+ 09:00 Breakfast 
+ 10:00 Welcome plenary - Distribution of tasks
+ 11:00 Workshops:
    +   Mental Health and Burn Out Prevention - Larissa
    +   Writing Workshop - Kadir


