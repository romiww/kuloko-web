---
title: bipoc-program
body_classes: modular
---

 ## Programm
 ### 06.08. FREITAG - AUFBAUTAG 
 + 08:00 Aufbau
 + 10:00 Willkommensplenum - Aufgabenverteilung 
 + 14:00 Aufbau ISD Pavillion/Space
 + 18:00 Abendessen
 
### 07.08. Samstag 
+ 08:00 Ankommen aus Lützi 
+ 09:00 Ankommen - Frühstück 
+ 10:00 Willkommensplenum
+ 11:00 Kennenlernen
+ 12:00 Workshop: (Neo-) Kolonialismus und Widerstand 
+ 14:00 Gemeinsame Teilnahme an der ADB Demo 14.30-16 Uhr
+ 15:00 ADB Demo:  Abschlusskundgebung - BIPoC Panel mit Zade Abdullah
+ 16:00 Bei Demo Chillen und nach Beverath zurück kommen
+ 17:00 Gemeinsames Ankommen 
+ 17:30 BIPoC Empowerment-Session 
+ 19:00 Abendessen
+ 20:30 Mohammad Reza Dashtoomi - persischer singer songwriter
+ 21:00 DJ Set von Mr. Biatch 

### 8.8. SONNTAG
+ 08:00 Ankommen aus Lützi 
+ 09:00 Ankommen - Frühstück 
+ 10:00 Willkommensplenum
+ 11:00 Vorstellung und Lesung der Broschüre: “Kolonialismus und Klimakrise” -  Shayli Kartal, und Laura Bechert und Dodo
+ Afrikanisches Mittagessen 
+ 13:00 Workshop: Afro-Futurism - Adrian Blount
+ 15:00 Workshop: intersektionale, queer-feministische Gerechtigkeitspraktiken
+ 17:00 BIPoC History- Arbeitsmigration in Allmanya - Gesprächsrunde 
+ 18:00 Initiativen und Gruppen stellen sich vor - connecting space 
+ 20:00 Spoken Word Night: Mamadou 
+ 20:45 Sami El Poet + open stage
+ 21:30 Muhammed - vielsprachige Spoken Word
+ 22:00 pamela yigoras - singer songwriter Musik - danach Jam Session
+ 22:30 Spotify Party, alle legen auf, die es fühlen 

### 09.08. Montag
+ 08:00 Ankommen aus Lützi 
+ 09:00 Ankommen - Frühstück 
+ 10:00 Willkommensplenum - Aufgabenverteilung
+ 11:00 Workshops:
    +   Mentale Gesundheit und Burn Out- Prävention - Larissa
    +   Schreibwerkstatt - Kadir


