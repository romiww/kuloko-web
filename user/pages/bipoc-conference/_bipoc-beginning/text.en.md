---
title: bipoc-beginning
body_classes: modular
image_align: left
---

## 7th - 9th August BIPoC Community Space <br> & Climate Conference in the Rhineland



We as BIPoCs are creating a BIPoC only space in Beverath City. There we can share our own perspectives, narratives and opinions undisturbed and without having to justify them in front of white perspectives. This space is all about US.

Participate, if you want to:

+ exchange opinions, ideas and perspectives with other BIPoCs
+ drink tea
+ participate in workshops, panel discussions and table discussions
+ make art
+ network politically
+ heal
+ listen to music
+ smoke shisha
+ dance
+ figure out together how to get out of the white supremacist climate crisis...

We want to bring BIPoCs from different movements together to share our expertise and experiences. Come to fill this space with our culture!