---
title: bipoc-beginning
body_classes: modular
image_align: left
---

## 7.-9. August BIPOC Community Space & <br>  Klima Konferenz im Rheinland 



Wir als <span class="popover">BIPoCs (?)<span class="popover-container">BIPoC (Black, Indigenous and People of Color) ist eine Abkürzung/ein Begriff, der für Menschen benutzt wird, die rassistisch diskriminiert werden</span></span> schaffen einen BIPoC only Raum in Berverath City, in dem wir ungestört unsere eigenen Standpunkte, Narrativen und Meinungen formulieren können, ohne sie mit weißen Perspektiven vorher aushandeln zu müssen. In diesen Raum geht es nur um UNS.

Nimm teil, um:

+ dich mit anderen BIPoCs austauschen 
+ Tee zu trinken 
+ an Workshops und Rederunden teilzunehmen 
+ Kunst zu machen 
+ dich politisch zu vernetzen 
+ zu heilen
+ Musik zu lauschen 
+ Shisha zu rauchen 
+ zu tanzen
+ zusammen herauszufinden wie wir aus der white supremacist Klimakrise rauskommen...


Wir möchten BIPoCs aus verschiedenen Bewegungen und Strömungen zusammenbringen, damit wir unsere Expertisen und Erfahrungen weitergegeben können. Kommt reichlich, um diesen Raum mit unserer Kultur zu füllen.